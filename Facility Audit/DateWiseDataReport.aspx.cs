﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BL;
using System.Data;
using System.IO;
using System.Text;
//using Newtonsoft.Json;

using OfficeOpenXml;
using OfficeOpenXml.Drawing;
using OfficeOpenXml.Style;
using OfficeOpenXml.Drawing.Chart;
using System.Drawing;

using System.Configuration;
using FacilityAudit.Code;
using System.Globalization;
using System.Net.Mail;
using System.Text.RegularExpressions;


namespace Facility_Audit
{
    public partial class DateWiseDataReport : System.Web.UI.Page
    {
        #region "Datas"

        private DataTable ReportDet
        {
            get
            {
                if (ViewState["reportdetails"] == null)
                    ViewState["reportdetails"] = null;
                return (DataTable)ViewState["reportdetails"];
            }

            set
            {
                ViewState["reportdetails"] = value;
            }
        }

        #endregion

        BLTransaction objbltran = new BLTransaction();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string dFirstDayOfThisMonth = DateTime.Today.AddDays(-(DateTime.Today.Day - 1)).ToString("MM/dd/yyyy");
                string lastOfThisMonth = DateTime.Today.ToString("MM/dd/yyyy");

                this.txtfrmdate.Text = dFirstDayOfThisMonth;
                this.txttodate.Text = lastOfThisMonth;
                if (!txtfrmdate.Text.Contains("/"))
                {
                    char datesplit = txtfrmdate.Text.ToCharArray()[2];
                    txtfrmdate.Text = txtfrmdate.Text.Replace(datesplit, '/');
                    txttodate.Text = txttodate.Text.Replace(datesplit, '/');
                }
                BindDetails();
            }
        }



        private void BindDetails()
        {
            try
            {
                lblError.Text = string.Empty;
                divmsg.Visible = false;
                if (txtfrmdate.Text.Trim() != string.Empty && txttodate.Text.Trim() != string.Empty)
                {
                    DateTime dtFDate = new DateTime();
                    DateTime dtTDate = new DateTime();
                    dtFDate = DateTime.ParseExact(txtfrmdate.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                    dtTDate = DateTime.ParseExact(txttodate.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                    DataSet dsreport = new DataSet();
                    if (UserSession.Companyid == 2915)
                    {
                        //dsreport = objbltran.GetDateWiseReport(UserSession.locationid, dtFDate, dtTDate);
                        dsreport = objbltran.GetDateWiseReport_Group(UserSession.locationid, dtFDate, dtTDate, UserSession.GroupID);
                    }
                    else
                    {
                        //dsreport = objbltran.GetDateWiseReport(0, dtFDate, dtTDate);
                        dsreport = objbltran.GetDateWiseReport_Group(0, dtFDate, dtTDate, UserSession.GroupID);
                    }
                    if (dsreport.Tables.Count > 0 && dsreport.Tables[0].Rows.Count > 0)
                    {
                        rptrdatewiserpt.DataSource = dsreport.Tables[0];
                        rptrdatewiserpt.DataBind();

                        ReportDet = dsreport.Tables[0];
                    }
                    else
                    {
                        rptrdatewiserpt.DataSource = null;
                        rptrdatewiserpt.DataBind();
                        NotifyMessages("No Data Found", "info");
                    }
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, "error");
            }
        }

        public void NotifyMessages(string message, string errortype)
        {
            divmsg.Visible = true;
            lblError.Text = message;
            if (errortype == "info")
            {
                divmsg.Attributes.Add("class", "alert alert-success fade in");
            }
            else if (errortype == "error")
            {
                divmsg.Attributes.Add("class", "alert alert-danger fade in");
            }
        }



        private string GetSiteID(string sitename)
        {
            string[] sitedet = sitename.Split('-');
            return sitedet[sitedet.Length - 1];
        }

        public class Observations
        {
            public string JS_SCM { get; set; }
            public string JS_Others { get; set; }
            public string JS_HR { get; set; }
            public string JS_Complaince { get; set; }
            public string JS_Training { get; set; }
            public string JS_Operation { get; set; }
        }

        protected void rptrdatewiserpt_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Download")
            {
                try
                {
                    HiddenField hdnlocationid = (HiddenField)e.Item.FindControl("locationid");
                    HiddenField hdnsbuid = (HiddenField)e.Item.FindControl("sbuid");
                    HiddenField hdnsectionid = (HiddenField)e.Item.FindControl("sectionid");
                    HiddenField hdnactualdate = (HiddenField)e.Item.FindControl("actualdate");
                    HiddenField Score = (HiddenField)e.Item.FindControl("hdnscore");
                    Label lAuditorName = (Label)e.Item.FindControl("AuditorName");
                    if (hdnlocationid != null && hdnsbuid != null && hdnsectionid != null && hdnactualdate != null && Score != null && lAuditorName != null)
                    {
                        string Getscored = Score.Value;
                        int locationid = Convert.ToInt32(hdnlocationid.Value);
                        int sbuid = Convert.ToInt32(hdnsbuid.Value);
                        int sectorid = Convert.ToInt32(hdnsectionid.Value);
                        DateTime dtactualdate = Convert.ToDateTime(hdnactualdate.Value);
                        DataSet dsRawdatas = new DataSet();
                        dsRawdatas = objbltran.GetRawDatasDateWiseReport(dtactualdate, locationid, sbuid, sectorid, lAuditorName.Text);
                        MakeExcel(dsRawdatas, Getscored);
                    }
                }
                catch (Exception ex)
                {
                    NotifyMessages(ex.Message, "error");
                }

            }
            else if (e.CommandName == "SendMail")
            {
                HiddenField hdnlocationid = (HiddenField)e.Item.FindControl("locationid");
                HiddenField hdnsbuid = (HiddenField)e.Item.FindControl("sbuid");
                HiddenField hdnsectionid = (HiddenField)e.Item.FindControl("sectionid");
                HiddenField hdnactualdate = (HiddenField)e.Item.FindControl("actualdate");
                Label sbuname = (Label)e.Item.FindControl("sbuname");
                HiddenField hdncompanyid = (HiddenField)e.Item.FindControl("hdncompanyid");
                Label Score = (Label)e.Item.FindControl("Score");
                Label lAuditorName = (Label)e.Item.FindControl("AuditorName");
                if (hdnlocationid != null && hdnsbuid != null && hdnsectionid != null && hdnactualdate != null && Score != null && hdncompanyid != null && sbuname != null && lAuditorName != null)
                {
                    string Getscored = Score.Text;

                    int locationid = Convert.ToInt32(hdnlocationid.Value);
                    int sbuid = Convert.ToInt32(hdnsbuid.Value);
                    int sectorid = Convert.ToInt32(hdnsectionid.Value);
                    int companyid = Convert.ToInt32(hdncompanyid.Value);
                    DateTime dtactualdate = Convert.ToDateTime(hdnactualdate.Value);
                    SendMail(dtactualdate, companyid, locationid, sbuid, sectorid, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, 0, "0.0", sbuname.Text, lAuditorName.Text);
                }
            }
        }

        #region "Send Mail For ClientWise  MailIDs"
        private void SendMail(DateTime auditdate, int companyid, int locationid, int sbuid, int sectorid, string operations, string training, string scm, string compliance, string hr, string others, string followupdate, int closuredate, string totalscore, string sbuname, string auditorname)
        {
            try
            {
                DataSet dsRawdatas = objbltran.GetRawDatasDateWiseReport(auditdate, locationid, sbuid, sectorid, auditorname);

                if (dsRawdatas.Tables.Count > 0 && dsRawdatas.Tables[0].Rows.Count > 0)
                {
                    string path = System.AppDomain.CurrentDomain.BaseDirectory + "AuditFiles";
                    //string filename = path + "\\" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ".xlsx";


                    string filename = Guid.NewGuid().ToString();
                    if (dsRawdatas.Tables[3].Rows.Count > 0)
                    {
                        DataRow drfilename = dsRawdatas.Tables[3].Rows[0];
                        filename = drfilename["locsitename"].ToString().Replace(",", string.Empty).Replace("/", string.Empty).Replace(@"\", string.Empty).Replace("<", string.Empty).Replace(">", string.Empty).Replace("*", string.Empty).Replace("?", string.Empty).Replace("\"", string.Empty);

                        RegexOptions options = RegexOptions.None;
                        Regex regex = new Regex("[ ]{2,}", options);
                        filename = regex.Replace(filename, " ");
                    }
                    string excelfilepath = Server.MapPath("DownloadExcels/") + filename + ".xlsx";

                    string bodymail = File.ReadAllText(Server.MapPath("AppThemes/images/MailTemplate.txt"), Encoding.UTF8);
                    string subject = "Customer survey report of - #SbuName#";

                    if (dsRawdatas.Tables.Count > 4)
                    {
                        if (dsRawdatas.Tables[5].Rows.Count > 0)
                        {
                            DataRow drsubjectbody = dsRawdatas.Tables[5].Rows[0];
                            subject = drsubjectbody["mailsubject"].ToString();
                            bodymail = drsubjectbody["mailbody"].ToString();
                        }
                    }

                    //WriteToFile(filename);

                    //GenerateExcel(dsRawdatas, totalscore, filename);

                    if (dsRawdatas.Tables[4].Rows.Count > 0)
                    {
                        DataRow drscore = dsRawdatas.Tables[4].Rows[0];
                        totalscore = drscore["score"].ToString();
                    }

                    MakeExcel(dsRawdatas, totalscore, excelfilepath);

                    DataRow drHeader = dsRawdatas.Tables[3].Rows[0];
                    sbuname = drHeader["locsitename"].ToString();


                    bodymail = bodymail.Replace("#Operations#", operations);
                    bodymail = bodymail.Replace("#Training#", training);
                    bodymail = bodymail.Replace("#SCM#", scm);
                    bodymail = bodymail.Replace("#Compliance#", compliance);
                    bodymail = bodymail.Replace("#HR#", hr);
                    bodymail = bodymail.Replace("#Others#", others);
                    bodymail = bodymail.Replace("#Score#", totalscore + "%");
                    bodymail = bodymail.Replace("#Closure#", auditdate.AddDays(closuredate).ToShortDateString());
                    bodymail = bodymail.Replace("#SbuName#", sbuname);

                    subject = subject.Replace("#SbuName#", sbuname);

                    if (followupdate.Trim() != string.Empty)
                    {
                        bodymail = bodymail.Replace("#Follow#", followupdate);
                    }
                    else
                    {
                        bodymail = bodymail.Replace("#Follow#", "Not Applicable");
                    }

                    SendingMail(bodymail, subject, excelfilepath, companyid, locationid, training, hr, scm, compliance, operations);

                }
            }
            catch (Exception)
            {
                throw;
            }
        }


        private bool CheckNotApplicable(string crticalobservation)
        {
            try
            {
                bool bObservation = false;
                string[] stringArray = { "nil", "na", "n/a" };
                string value = crticalobservation.ToLower().Trim();
                int pos = Array.IndexOf(stringArray, value);
                if (pos > -1)
                {
                    bObservation = true;
                }
                return bObservation;
            }
            catch (Exception)
            {
                throw;
            }
        }


        private void SendingMail(string body, string subject, string attachment, int companyid, int locationid,
            string training, string hr, string scm, string compliance, string operations)
        {
            try
            {

                bool btraining = false, bhr = false, bscm = false, bcompliance = false, boperations = false;
                //btraining = CheckNotApplicable(training);
                //bhr = CheckNotApplicable(hr);
                //bscm = CheckNotApplicable(scm);
                //bcompliance = CheckNotApplicable(compliance);
                //boperations = CheckNotApplicable(operations);

                DataSet dsEmailDet = objbltran.getlocationmailid(companyid, locationid, btraining, bhr, bscm, bcompliance, boperations);
                if (dsEmailDet.Tables.Count > 0 && dsEmailDet.Tables[0].Rows.Count > 0)
                {
                    DataRow drEmail = dsEmailDet.Tables[0].Rows[0];
                    string toMail = drEmail["ToMail"].ToString();
                    string CcMail = drEmail["CCMail"].ToString();

                    NotifyMessages("To :" + toMail + "</br>CC Mail :" + CcMail + "", "info");
                    string username = string.Empty;
                    string password = string.Empty;
                    string smtpServer = string.Empty;
                    string frommail = string.Empty;
                    int portno = 0;


                    MailMessage mailMessage = new MailMessage();
                    if (ConfigurationManager.AppSettings["UserName"] != null)
                    {
                        username = ConfigurationManager.AppSettings["UserName"];
                    }

                    if (ConfigurationManager.AppSettings["Password"] != null)
                    {
                        password = ConfigurationManager.AppSettings["Password"];
                    }

                    if (ConfigurationManager.AppSettings["SmtpServer"] != null)
                    {
                        smtpServer = ConfigurationManager.AppSettings["SmtpServer"];
                    }

                    if (ConfigurationManager.AppSettings["Port"] != null)
                    {
                        portno = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]);
                    }

                    if (ConfigurationManager.AppSettings["FromMail"] != null)
                    {
                        frommail = ConfigurationManager.AppSettings["FromMail"];
                    }

                    //usermail = "venkadesan.n@i2isoftwares.com";
                    //ccmail="aravind@i2isoftwares.com";

                    //toMail = "stephenson.f@dtss.in";
                    //CcMail = "selvam.chokkappa@dtss.in,NHOperations@dtss.in";
                    if (toMail != string.Empty)
                    {
                        foreach (var address in toMail.Split(new[] { ",", ";" }, StringSplitOptions.RemoveEmptyEntries))
                        {
                            mailMessage.To.Add(address);
                        }
                        if (CcMail.Trim() != string.Empty)
                        {
                            string[] emailaddress = CcMail.Split(new[] { ",", ";" }, StringSplitOptions.RemoveEmptyEntries).Distinct().ToArray();
                            foreach (var address in emailaddress)
                            {
                                if (address != string.Empty)
                                {
                                    if (IsValidEmail(address))
                                    {
                                        mailMessage.CC.Add(address);
                                    }
                                }
                            }
                        }

                        mailMessage.From = new MailAddress(frommail, "Facility Audit");
                        mailMessage.Bcc.Add(frommail);
                        mailMessage.Subject = subject;
                        mailMessage.IsBodyHtml = true;
                        mailMessage.Body = body;
                        mailMessage.Attachments.Add(new Attachment(attachment));
                        SmtpClient smtpClient = new SmtpClient(smtpServer, portno);
                        smtpClient.UseDefaultCredentials = false;
                        smtpClient.Credentials = new System.Net.NetworkCredential(username, password);
                        smtpClient.EnableSsl = true;
                        smtpClient.Send(mailMessage);
                        NotifyMessages("Mail Sent To: " + toMail + "," + CcMail + "", "info");
                        //FileInfo prevFile = new FileInfo(attachment);
                        //if (prevFile.Exists)
                        //{
                        //    prevFile.Delete();
                        //}


                    }
                }
                else
                {
                    NotifyMessages("SBU or Other Mail ID's Not Found.", "error");
                }
            }
            catch
            {
                throw;
            }
        }

        bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

        private void MakeExcel(DataSet dsRawdatas, string Getscored, string excelfilepath)
        {
            try
            {

                //string filename = Guid.NewGuid().ToString();
                //if (dsRawdatas.Tables[3].Rows.Count > 0)
                //{
                //    DataRow drfilename = dsRawdatas.Tables[3].Rows[0];
                //    filename = drfilename["locsitename"].ToString().Replace(",", string.Empty).Replace("/", string.Empty).Replace(@"\", string.Empty).Replace("<", string.Empty).Replace(">", string.Empty);
                //}
                //string excelfilepath = Server.MapPath("DownloadExcels/") + filename + ".xlsx";

                FileInfo newFile = new FileInfo(excelfilepath);
                if (newFile.Exists)
                {
                    newFile.Delete();  // ensures we create a new workbook
                    newFile = new FileInfo(excelfilepath);
                }

                using (ExcelPackage package = new ExcelPackage(newFile))
                {
                    // add a new worksheet to the empty workbook
                    ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("DownloadeExcel");

                    //Add the headers

                    StringBuilder sDatas = new StringBuilder();

                    float fEnteirAvg = 0;
                    int iTotalScore = 0;
                    int iTotalWeightage = 0;
                    int iMaxScore = 0;

                    DataTable dtChartdet = new DataTable();
                    dtChartdet.Columns.Add("Categoryname", typeof(string));
                    dtChartdet.Columns.Add("Value", typeof(float));
                    dtChartdet.Columns.Add("auditId", typeof(int));

                    if (dsRawdatas.Tables.Count > 0 && dsRawdatas.Tables[0].Rows.Count > 0)
                    {

                        DataTable dtAudit = dsRawdatas.Tables[0];
                        DataTable dtCategory = dsRawdatas.Tables[1];
                        DataTable dtRawData = dsRawdatas.Tables[2];
                        DataRow drHeader = dsRawdatas.Tables[3].Rows[0];

                        // worksheet.Cells[1, 1].Value = "Image Here";
                        //worksheet.Cells[1, 1].Value = "DUSTERS TOTAL SOLUTIONS SERVICES PVT LTD";

                        worksheet.Cells[1, 1].Value = drHeader["GroupName"].ToString().ToUpper();

                        var headerFont = worksheet.Cells[1, 1].Style.Font;
                        headerFont.Bold = true;
                        headerFont.Size = 16;
                        worksheet.Cells[1, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Cells[1, 1].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        worksheet.Cells["A1:E4"].Merge = true;

                        string logofile = drHeader["UploadLogoName"].ToString();
                        if (logofile.Trim() != string.Empty)
                        {
                            string filepath = ConfigurationManager.AppSettings["companylogo"].ToString() + logofile;

                            FileInfo fileinfo = new FileInfo(filepath);
                            var picture = worksheet.Drawings.AddPicture("logo", fileinfo);
                            picture.SetSize(100, 50);
                            picture.SetPosition(1, 0, 1, 0);
                        }



                        worksheet.Cells[5, 1].Value = "CLIENT FEEDBACK FORM";
                        worksheet.Cells[5, 1].Style.Font.Bold = true;
                        worksheet.Cells["A" + 5 + ":E" + 5 + ""].Merge = true;
                        worksheet.Cells[5, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;



                        worksheet.Cells[6, 1].Value = "Name of the Client";
                        worksheet.Cells[6, 3].Value = drHeader["clientname"].ToString();
                        //worksheet.Cells[5, 6].Value = "";
                        worksheet.Cells["A6:B6"].Merge = true;
                        worksheet.Cells["C6:E6"].Merge = true;


                        worksheet.Cells[7, 1].Value = "Name of the Site";
                        worksheet.Cells[7, 3].Value = drHeader["locsitename"].ToString();
                        worksheet.Name = drHeader["locsitename"].ToString();
                        worksheet.Cells["A7:B7"].Merge = true;
                        worksheet.Cells["C7:E7"].Merge = true;


                        worksheet.Cells[8, 1].Value = "Site ID";
                        worksheet.Cells[8, 3].Value = GetSiteID(drHeader["locsitename"].ToString());
                        worksheet.Cells["A8:B8"].Merge = true;
                        worksheet.Cells["C8:E8"].Merge = true;

                        worksheet.Cells[9, 1].Value = "Name/EmailId";
                        worksheet.Cells[9, 3].Value = GetSiteID(drHeader["auditor"].ToString());
                        worksheet.Cells["A9:B9"].Merge = true;
                        worksheet.Cells["C9:E9"].Merge = true;

                        worksheet.Cells[10, 1].Value = "Date";
                        worksheet.Cells[10, 3].Value = GetSiteID(drHeader["dispauditdate"].ToString());
                        worksheet.Cells["A10:B10"].Merge = true;
                        worksheet.Cells["C10:E10"].Merge = true;


                        int imaxrowcount = 11;

                        for (int iauditcount = 0; iauditcount < dtAudit.Rows.Count; iauditcount++)
                        {
                            DataRow draudit = dtAudit.Rows[iauditcount];
                            int auditid = Convert.ToInt32(draudit["auditid"].ToString());
                            string auditname = draudit["auditname"].ToString();

                            worksheet.Cells[imaxrowcount, 1].Value = auditname;
                            worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;

                            worksheet.Cells["A" + imaxrowcount + ":E" + imaxrowcount + ""].Merge = true;
                            worksheet.Cells[imaxrowcount, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            imaxrowcount++;

                            worksheet.Cells[imaxrowcount, 1].Value = "Sl.No";
                            worksheet.Cells[imaxrowcount, 2].Value = "PARTICULARS";
                            worksheet.Cells[imaxrowcount, 3].Value = "Maximum Score";
                            worksheet.Cells[imaxrowcount, 4].Value = "Score Obtained";
                            worksheet.Cells[imaxrowcount, 5].Value = "Remarks";

                            var range = worksheet.Cells[imaxrowcount, 1, imaxrowcount, 5];
                            range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                            range.Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FCB133"));

                            imaxrowcount++;

                            int iscore = 0;
                            int iweightage = 0;
                            int iSingleAvg = 0;
                            int iTotalAvg = 0;
                            int iAuditMax = 0;
                            DataRow[] drCategory = dtCategory.Select("auditid=" + auditid);
                            for (int icategroryCount = 0; icategroryCount < drCategory.Length; icategroryCount++)
                            {
                                int categoryid = Convert.ToInt32(drCategory[icategroryCount]["categoryid"].ToString());
                                string categoryname = drCategory[icategroryCount]["categoryname"].ToString();

                                worksheet.Cells[imaxrowcount, 1].Value = icategroryCount + 1;
                                worksheet.Cells[imaxrowcount, 2].Value = categoryname;
                                worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
                                worksheet.Cells[imaxrowcount, 2].Style.Font.Bold = true;
                                worksheet.Cells[imaxrowcount, 1].Style.Font.Size = 14;

                                worksheet.Cells["B" + imaxrowcount + ":E" + imaxrowcount + ""].Merge = true;
                                imaxrowcount++;

                                DataRow[] drRawDatas = dtRawData.Select("categoryid=" + categoryid);

                                int iCategoryScore = 0;
                                int iCategoryTotal = 0;
                                int iCatMax = 0;

                                for (int iRawDatacount = 0; iRawDatacount < drRawDatas.Length; iRawDatacount++)
                                {
                                    string question = drRawDatas[iRawDatacount]["auditqname"].ToString();
                                    string remarks = drRawDatas[iRawDatacount]["remarks"].ToString();
                                    int score = Convert.ToInt32(drRawDatas[iRawDatacount]["score"].ToString());
                                    int weightage = Convert.ToInt32(drRawDatas[iRawDatacount]["weightage"].ToString());
                                    string scorename = drRawDatas[iRawDatacount]["scorename"].ToString();
                                    string imgFile = drRawDatas[iRawDatacount]["uploadfilename"].ToString();
                                    string transactionid = drRawDatas[iRawDatacount]["TransactionId"].ToString();
                                    string filelink = string.Empty;
                                    if (imgFile.Trim() != string.Empty)
                                    {
                                        string filepath = "http://ifazility.com/facilityaudit/ImageLists.aspx";
                                        if (ConfigurationManager.AppSettings["Imaglists"] != null)
                                        {
                                            filepath = ConfigurationManager.AppSettings["Imaglists"].ToString();
                                        }
                                        filelink = filepath + "?TransactionId=" + transactionid;
                                    }

                                    int tScore = 0;
                                    int MaxScore = 0;

                                    //if (score == -1)
                                    //{
                                    //    score = 0;
                                    //    tScore = -1;
                                    //    weightage = 0;
                                    //    MaxScore = 0;
                                    //}
                                    //else
                                    //{
                                    //    MaxScore = 1;
                                    //}

                                    if (score == -1)
                                    {
                                        score = 0;
                                        weightage = 0;
                                        MaxScore = 0;
                                        tScore = -1;
                                    }
                                    else
                                    {
                                        MaxScore = 5;
                                    }

                                    iscore += score;
                                    iweightage += weightage;

                                    iCategoryScore += score;
                                    iCategoryTotal += weightage;

                                    iTotalScore += score;
                                    iTotalWeightage += weightage;

                                    iSingleAvg = score * weightage;
                                    iTotalAvg += iSingleAvg;

                                    iMaxScore += MaxScore;
                                    iCatMax += MaxScore;
                                    iAuditMax += MaxScore;

                                    string _score = string.Empty;
                                    string _maxscore = string.Empty;
                                    if (tScore == 0)
                                    {
                                        _score = " " + score;
                                        _maxscore = " " + MaxScore;
                                    }
                                    else
                                    {
                                        _score = scorename;
                                        _maxscore = scorename;
                                    }


                                    worksheet.Cells[imaxrowcount, 1].Value = iRawDatacount + 1;
                                    worksheet.Cells[imaxrowcount, 2].Value = question;

                                    if (question.Length >= 75)
                                    {
                                        worksheet.Cells[imaxrowcount, 2].Style.WrapText = true;
                                    }

                                    //worksheet.Cells[imaxrowcount, 2].Style.WrapText = true;
                                    //worksheet.Cells[imaxrowcount, 3].Value = _maxscore;

                                    int MaxRefId;
                                    int ScorerefID;

                                    bool isNumeric = int.TryParse(_maxscore.Trim(), out MaxRefId);

                                    if (isNumeric)
                                    {
                                        worksheet.Cells[imaxrowcount, 3].Value = MaxRefId;
                                        worksheet.Cells[imaxrowcount, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    }
                                    else
                                    {
                                        worksheet.Cells[imaxrowcount, 3].Value = _maxscore;
                                        worksheet.Cells[imaxrowcount, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    }

                                    isNumeric = int.TryParse(_score.Trim(), out ScorerefID);

                                    if (isNumeric)
                                    {
                                        worksheet.Cells[imaxrowcount, 4].Value = ScorerefID;
                                        worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    }
                                    else
                                    {
                                        worksheet.Cells[imaxrowcount, 4].Value = _score;
                                        worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    }

                                    //worksheet.Cells[imaxrowcount, 4].Value = _score;
                                    worksheet.Cells[imaxrowcount, 5].Value = remarks;

                                    if (remarks.Length >= 75)
                                    {
                                        worksheet.Cells[imaxrowcount, 5].Style.WrapText = true;
                                    }

                                    worksheet.Cells[imaxrowcount, 6].Value = string.Empty;

                                    //if (imgFile.Trim() != string.Empty)
                                    //{
                                    //    var cell = worksheet.Cells[imaxrowcount, 7];
                                    //    cell.Hyperlink = new Uri(filelink);
                                    //    cell.Value = "ImageList";
                                    //}

                                    imaxrowcount++;
                                }

                                float _CateWisTot = 0;
                                if (iCategoryTotal != 0)
                                {
                                    _CateWisTot = ((float)(iCategoryScore / iCategoryTotal));
                                }
                                else
                                {
                                    _CateWisTot = 0;
                                }

                                dtChartdet.Rows.Add(categoryname, _CateWisTot, auditid);


                                string sCatNA = string.Empty;
                                string sCatMaxNA = string.Empty;
                                sCatNA = iCatMax.ToString();
                                sCatMaxNA = iCategoryScore.ToString();
                                if (iCatMax == 0)
                                {
                                    sCatNA = "N/A";
                                }
                                if (iCategoryScore == 0)
                                {
                                    sCatMaxNA = "N/A";
                                }

                                worksheet.Cells[imaxrowcount, 1].Value = "Sub Total " + categoryname;

                                int _catena;
                                int _catmaxna;
                                bool Numeric = int.TryParse(sCatNA.Trim(), out _catena);

                                if (Numeric)
                                {
                                    worksheet.Cells[imaxrowcount, 3].Value = _catena;
                                    worksheet.Cells[imaxrowcount, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                }
                                else
                                {
                                    worksheet.Cells[imaxrowcount, 3].Value = sCatNA;
                                    worksheet.Cells[imaxrowcount, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                }
                                worksheet.Cells[imaxrowcount, 3].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                worksheet.Cells[imaxrowcount, 3].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FCB133"));

                                Numeric = int.TryParse(sCatMaxNA.Trim(), out _catmaxna);

                                if (Numeric)
                                {
                                    worksheet.Cells[imaxrowcount, 4].Value = _catmaxna;
                                    worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                }
                                else
                                {
                                    worksheet.Cells[imaxrowcount, 4].Value = sCatMaxNA;
                                    worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                }
                                worksheet.Cells[imaxrowcount, 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                worksheet.Cells[imaxrowcount, 4].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FCB133"));



                                //worksheet.Cells[imaxrowcount, 3].Value = sCatNA;
                                //worksheet.Cells[imaxrowcount, 4].Value = sCatMaxNA;

                                worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
                                worksheet.Cells[imaxrowcount, 3].Style.Font.Bold = true;
                                worksheet.Cells[imaxrowcount, 4].Style.Font.Bold = true;
                                worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                                imaxrowcount++;
                            }
                            float iScoredAvg = 0;
                            iScoredAvg = ((float)(iscore * 100 / iweightage));
                            fEnteirAvg += iScoredAvg;

                            //worksheet.Cells[imaxrowcount, 1].Value = auditname;
                            //worksheet.Cells[imaxrowcount, 3].Value = iscore;
                            //worksheet.Cells[imaxrowcount, 4].Value = iAuditMax;

                            //worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
                            //worksheet.Cells[imaxrowcount, 3].Style.Font.Bold = true;
                            //worksheet.Cells[imaxrowcount, 4].Style.Font.Bold = true;
                            //worksheet.Cells[imaxrowcount, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            //worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                            //worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                            //imaxrowcount++;

                            //worksheet.Cells[imaxrowcount, 1].Value = "Percentage (%)";
                            //worksheet.Cells[imaxrowcount, 3].Value = string.Empty;
                            //worksheet.Cells[imaxrowcount, 4].Value = iScoredAvg + "%";

                            //worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
                            //worksheet.Cells[imaxrowcount, 4].Style.Font.Bold = true;
                            //worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            //worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                            //imaxrowcount++;
                        }
                        float tAvg = fEnteirAvg / (dsRawdatas.Tables[0].Rows.Count);

                        worksheet.Cells[imaxrowcount, 1].Value = "GRAND TOTAL SCORE";
                        worksheet.Cells[imaxrowcount, 3].Value = iMaxScore;
                        worksheet.Cells[imaxrowcount, 4].Value = iTotalScore;
                        worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
                        worksheet.Cells[imaxrowcount, 3].Style.Font.Bold = true;
                        worksheet.Cells[imaxrowcount, 4].Style.Font.Bold = true;
                        worksheet.Cells[imaxrowcount, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Cells[imaxrowcount, 3].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        worksheet.Cells[imaxrowcount, 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        worksheet.Cells[imaxrowcount, 3].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FCB133"));
                        worksheet.Cells[imaxrowcount, 4].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FCB133"));
                        worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                        imaxrowcount++;

                        decimal scoreGot = Convert.ToDecimal(Getscored);


                        worksheet.Cells[imaxrowcount, 1].Value = "Total Score";
                        worksheet.Cells[imaxrowcount, 3].Value = string.Empty;
                        worksheet.Cells[imaxrowcount, 4].Value = Getscored;
                        worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
                        worksheet.Cells[imaxrowcount, 4].Style.Font.Bold = true;
                        //string setColour = string.Empty;
                        //if (scoreGot >= 83)
                        //{
                        //    worksheet.Cells[imaxrowcount, 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        //    worksheet.Cells[imaxrowcount, 4].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#00FF00"));
                        //}
                        //else
                        //{
                        //    worksheet.Cells[imaxrowcount, 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        //    worksheet.Cells[imaxrowcount, 4].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFBE00"));
                        //}
                        worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        imaxrowcount++;


                        worksheet.Cells[imaxrowcount, 1].Value = "Percentage (%)";
                        worksheet.Cells[imaxrowcount, 3].Value = string.Empty;
                        worksheet.Cells[imaxrowcount, 4].Value = ((decimal)(scoreGot * 100) / 5) + "%";
                        worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
                        worksheet.Cells[imaxrowcount, 4].Style.Font.Bold = true;
                        worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        imaxrowcount++;



                        string observations = drHeader["observation"].ToString();
                        string clientremarks = drHeader["feedback"].ToString();


                        worksheet.Cells[imaxrowcount, 1].Value = "Additional Feedback by Client";
                        worksheet.Cells[imaxrowcount, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
                        worksheet.Cells["A" + imaxrowcount + ":E" + imaxrowcount + ""].Merge = true;
                        imaxrowcount++;


                        worksheet.Cells[imaxrowcount, 1].Value = clientremarks;
                        worksheet.Cells["A" + imaxrowcount + ":E" + imaxrowcount + ""].Merge = true;
                        imaxrowcount++;


                        worksheet.Cells[1, 1, imaxrowcount - 1, 5].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        worksheet.Cells[1, 1, imaxrowcount - 1, 5].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        worksheet.Cells[1, 1, imaxrowcount - 1, 5].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        worksheet.Cells[1, 1, imaxrowcount - 1, 5].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                        // worksheet.Cells.AutoFitColumns(0);  //Autofit columns for all cells
                        // worksheet.Cells.AutoFitColumns(1);
                        //// worksheet.Cells.AutoFitColumns(2);

                        //worksheet.Column(2).Width = 140;
                        //worksheet.Column(5).Width = 140;

                        worksheet.Column(1).Width = 5;
                        worksheet.Column(2).Width = 65;
                        worksheet.Column(3).Width = 15;
                        worksheet.Column(4).Width = 15;
                        worksheet.Column(5).Width = 65;

                        //worksheet.Cells.AutoFitColumns(2);
                        //worksheet.Cells.AutoFitColumns(5);

                        //worksheet.Cells.AutoFitColumns(1);
                        //worksheet.Cells.AutoFitColumns(2);
                        //worksheet.Cells.AutoFitColumns(3);
                        //worksheet.Cells.AutoFitColumns(4);
                        //worksheet.Cells.AutoFitColumns(5);
                        //worksheet.Cells.AutoFitColumns(6);
                        //worksheet.Cells.AutoFitColumns(7);

                        int c1;

                        imaxrowcount = 7;
                        ExcelWorksheet worksheetChart = package.Workbook.Worksheets.Add("CategoryChart");

                        string Alphabetic = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                        int startchar = 0;
                        int colspace = 0;
                        foreach (DataRow drAuditRow in dtAudit.Rows)
                        {
                            int auditid = Convert.ToInt32(drAuditRow["auditid"].ToString());
                            string auditname = drAuditRow["auditname"].ToString();
                            DataRow[] drAuditCategories = dtChartdet.Select("auditid=" + auditid);
                            if (drAuditCategories.Length > 0)
                            {
                                DataTable dtCategoryChart = drAuditCategories.CopyToDataTable();
                                int startvalue = imaxrowcount;

                                char Firstcol = Alphabetic[startchar];
                                char Secondcol = Alphabetic[startchar + 1];

                                foreach (DataRow drchartdet in dtCategoryChart.Rows)
                                {
                                    string categoryname = drchartdet["categoryname"].ToString();
                                    decimal score = Convert.ToDecimal(drchartdet["Value"].ToString());

                                    worksheetChart.Cells["" + Firstcol + imaxrowcount].Value = categoryname;
                                    worksheetChart.Cells["" + Secondcol + imaxrowcount].Value = score;
                                    imaxrowcount++;
                                }

                                var chart = worksheetChart.Drawings.AddChart("chart" + imaxrowcount, eChartType.ColumnClustered3D);
                                var series = chart.Series.Add("" + Secondcol + startvalue + ":" + Secondcol + imaxrowcount, "" + Firstcol + startvalue + ":" + Firstcol + imaxrowcount);
                                //series.HeaderAddress = new ExcelAddress("'Sheet1'!B" + (startvalue - 1));
                                chart.SetSize(510, 300);
                                chart.Title.Text = auditname;
                                chart.SetPosition(7, 0, colspace, 0);
                                colspace += 9;

                                startchar += 2;
                            }
                        }


                        package.Save();
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion



        private void MakeExcel(DataSet dsRawdatas, string Getscored)
        {
            try
            {

                string filename = Guid.NewGuid().ToString();
                if (dsRawdatas.Tables[3].Rows.Count > 0)
                {
                    DataRow drfilename = dsRawdatas.Tables[3].Rows[0];
                    filename = drfilename["locsitename"].ToString().Replace(",", string.Empty).Replace("/", string.Empty).Replace(@"\", string.Empty).Replace("<", string.Empty).Replace(">", string.Empty);
                }
                string excelfilepath = Server.MapPath("DownloadExcels/") + filename + ".xlsx";

                FileInfo newFile = new FileInfo(excelfilepath);
                if (newFile.Exists)
                {
                    newFile.Delete();  // ensures we create a new workbook
                    newFile = new FileInfo(excelfilepath);
                }

                using (ExcelPackage package = new ExcelPackage(newFile))
                {
                    // add a new worksheet to the empty workbook
                    ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("DownloadeExcel");

                    //Add the headers

                    StringBuilder sDatas = new StringBuilder();

                    float fEnteirAvg = 0;
                    int iTotalScore = 0;
                    int iTotalWeightage = 0;
                    int iMaxScore = 0;

                    DataTable dtChartdet = new DataTable();
                    dtChartdet.Columns.Add("Categoryname", typeof(string));
                    dtChartdet.Columns.Add("Value", typeof(float));
                    dtChartdet.Columns.Add("auditId", typeof(int));

                    if (dsRawdatas.Tables.Count > 0 && dsRawdatas.Tables[0].Rows.Count > 0)
                    {

                        DataTable dtAudit = dsRawdatas.Tables[0];
                        DataTable dtCategory = dsRawdatas.Tables[1];
                        DataTable dtRawData = dsRawdatas.Tables[2];
                        DataRow drHeader = dsRawdatas.Tables[3].Rows[0];

                        // worksheet.Cells[1, 1].Value = "Image Here";
                        //worksheet.Cells[1, 1].Value = "DUSTERS TOTAL SOLUTIONS SERVICES PVT LTD";

                        worksheet.Cells[1, 1].Value = drHeader["GroupName"].ToString().ToUpper();

                        var headerFont = worksheet.Cells[1, 1].Style.Font;
                        headerFont.Bold = true;
                        headerFont.Size = 16;
                        worksheet.Cells[1, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Cells[1, 1].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        worksheet.Cells["A1:E4"].Merge = true;

                        string logofile = drHeader["UploadLogoName"].ToString();
                        if (logofile.Trim() != string.Empty)
                        {
                            string filepath = ConfigurationManager.AppSettings["companylogo"].ToString() + logofile;

                            FileInfo fileinfo = new FileInfo(filepath);
                            var picture = worksheet.Drawings.AddPicture("logo", fileinfo);
                            picture.SetSize(100, 50);
                            picture.SetPosition(1, 0, 1, 0);
                        }



                        worksheet.Cells[5, 1].Value = "CLIENT FEEDBACK FORM";
                        worksheet.Cells[5, 1].Style.Font.Bold = true;
                        worksheet.Cells["A" + 5 + ":E" + 5 + ""].Merge = true;
                        worksheet.Cells[5, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;



                        worksheet.Cells[6, 1].Value = "Name of the Client";
                        worksheet.Cells[6, 3].Value = drHeader["clientname"].ToString();
                        //worksheet.Cells[5, 6].Value = "";
                        worksheet.Cells["A6:B6"].Merge = true;
                        worksheet.Cells["C6:E6"].Merge = true;


                        worksheet.Cells[7, 1].Value = "Name of the Site";
                        worksheet.Cells[7, 3].Value = drHeader["locsitename"].ToString();
                        worksheet.Name = drHeader["locsitename"].ToString();
                        worksheet.Cells["A7:B7"].Merge = true;
                        worksheet.Cells["C7:E7"].Merge = true;


                        worksheet.Cells[8, 1].Value = "Site ID";
                        worksheet.Cells[8, 3].Value = GetSiteID(drHeader["locsitename"].ToString());
                        worksheet.Cells["A8:B8"].Merge = true;
                        worksheet.Cells["C8:E8"].Merge = true;

                        worksheet.Cells[9, 1].Value = "Name/EmailId";
                        worksheet.Cells[9, 3].Value = GetSiteID(drHeader["auditor"].ToString());
                        worksheet.Cells["A9:B9"].Merge = true;
                        worksheet.Cells["C9:E9"].Merge = true;

                        worksheet.Cells[10, 1].Value = "Date";
                        worksheet.Cells[10, 3].Value = GetSiteID(drHeader["dispauditdate"].ToString());
                        worksheet.Cells["A10:B10"].Merge = true;
                        worksheet.Cells["C10:E10"].Merge = true;


                        int imaxrowcount = 11;

                        for (int iauditcount = 0; iauditcount < dtAudit.Rows.Count; iauditcount++)
                        {
                            DataRow draudit = dtAudit.Rows[iauditcount];
                            int auditid = Convert.ToInt32(draudit["auditid"].ToString());
                            string auditname = draudit["auditname"].ToString();

                            worksheet.Cells[imaxrowcount, 1].Value = auditname;
                            worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;

                            worksheet.Cells["A" + imaxrowcount + ":E" + imaxrowcount + ""].Merge = true;
                            worksheet.Cells[imaxrowcount, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            imaxrowcount++;

                            worksheet.Cells[imaxrowcount, 1].Value = "Sl.No";
                            worksheet.Cells[imaxrowcount, 2].Value = "PARTICULARS";
                            worksheet.Cells[imaxrowcount, 3].Value = "Maximum Score";
                            worksheet.Cells[imaxrowcount, 4].Value = "Score Obtained";
                            worksheet.Cells[imaxrowcount, 5].Value = "Remarks";

                            var range = worksheet.Cells[imaxrowcount, 1, imaxrowcount, 5];
                            range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                            range.Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FCB133"));

                            imaxrowcount++;

                            int iscore = 0;
                            int iweightage = 0;
                            int iSingleAvg = 0;
                            int iTotalAvg = 0;
                            int iAuditMax = 0;
                            DataRow[] drCategory = dtCategory.Select("auditid=" + auditid);
                            for (int icategroryCount = 0; icategroryCount < drCategory.Length; icategroryCount++)
                            {
                                int categoryid = Convert.ToInt32(drCategory[icategroryCount]["categoryid"].ToString());
                                string categoryname = drCategory[icategroryCount]["categoryname"].ToString();

                                worksheet.Cells[imaxrowcount, 1].Value = icategroryCount + 1;
                                worksheet.Cells[imaxrowcount, 2].Value = categoryname;
                                worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
                                worksheet.Cells[imaxrowcount, 2].Style.Font.Bold = true;
                                worksheet.Cells[imaxrowcount, 1].Style.Font.Size = 14;

                                worksheet.Cells["B" + imaxrowcount + ":E" + imaxrowcount + ""].Merge = true;
                                imaxrowcount++;

                                DataRow[] drRawDatas = dtRawData.Select("categoryid=" + categoryid);

                                int iCategoryScore = 0;
                                int iCategoryTotal = 0;
                                int iCatMax = 0;

                                for (int iRawDatacount = 0; iRawDatacount < drRawDatas.Length; iRawDatacount++)
                                {
                                    string question = drRawDatas[iRawDatacount]["auditqname"].ToString();
                                    string remarks = drRawDatas[iRawDatacount]["remarks"].ToString();
                                    int score = Convert.ToInt32(drRawDatas[iRawDatacount]["score"].ToString());
                                    int weightage = Convert.ToInt32(drRawDatas[iRawDatacount]["weightage"].ToString());
                                    string scorename = drRawDatas[iRawDatacount]["scorename"].ToString();
                                    string imgFile = drRawDatas[iRawDatacount]["uploadfilename"].ToString();
                                    string transactionid = drRawDatas[iRawDatacount]["TransactionId"].ToString();
                                    string filelink = string.Empty;
                                    if (imgFile.Trim() != string.Empty)
                                    {
                                        string filepath = "http://ifazility.com/facilityaudit/ImageLists.aspx";
                                        if (ConfigurationManager.AppSettings["Imaglists"] != null)
                                        {
                                            filepath = ConfigurationManager.AppSettings["Imaglists"].ToString();
                                        }
                                        filelink = filepath + "?TransactionId=" + transactionid;
                                    }

                                    int tScore = 0;
                                    int MaxScore = 0;

                                    //if (score == -1)
                                    //{
                                    //    score = 0;
                                    //    tScore = -1;
                                    //    weightage = 0;
                                    //    MaxScore = 0;
                                    //}
                                    //else
                                    //{
                                    //    MaxScore = 1;
                                    //}

                                    if (score == -1)
                                    {
                                        score = 0;
                                        weightage = 0;
                                        MaxScore = 0;
                                        tScore = -1;
                                    }
                                    else
                                    {
                                        MaxScore = 5;
                                    }

                                    iscore += score;
                                    iweightage += weightage;

                                    iCategoryScore += score;
                                    iCategoryTotal += weightage;

                                    iTotalScore += score;
                                    iTotalWeightage += weightage;

                                    iSingleAvg = score * weightage;
                                    iTotalAvg += iSingleAvg;

                                    iMaxScore += MaxScore;
                                    iCatMax += MaxScore;
                                    iAuditMax += MaxScore;

                                    string _score = string.Empty;
                                    string _maxscore = string.Empty;
                                    if (tScore == 0)
                                    {
                                        _score = " " + score;
                                        _maxscore = " " + MaxScore;
                                    }
                                    else
                                    {
                                        _score = scorename;
                                        _maxscore = scorename;
                                    }


                                    worksheet.Cells[imaxrowcount, 1].Value = iRawDatacount + 1;
                                    worksheet.Cells[imaxrowcount, 2].Value = question;

                                    if (question.Length >= 75)
                                    {
                                        worksheet.Cells[imaxrowcount, 2].Style.WrapText = true;
                                    }

                                    //worksheet.Cells[imaxrowcount, 2].Style.WrapText = true;
                                    //worksheet.Cells[imaxrowcount, 3].Value = _maxscore;

                                    int MaxRefId;
                                    int ScorerefID;

                                    bool isNumeric = int.TryParse(_maxscore.Trim(), out MaxRefId);

                                    if (isNumeric)
                                    {
                                        worksheet.Cells[imaxrowcount, 3].Value = MaxRefId;
                                        worksheet.Cells[imaxrowcount, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    }
                                    else
                                    {
                                        worksheet.Cells[imaxrowcount, 3].Value = _maxscore;
                                        worksheet.Cells[imaxrowcount, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    }

                                    isNumeric = int.TryParse(_score.Trim(), out ScorerefID);

                                    if (isNumeric)
                                    {
                                        worksheet.Cells[imaxrowcount, 4].Value = ScorerefID;
                                        worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    }
                                    else
                                    {
                                        worksheet.Cells[imaxrowcount, 4].Value = _score;
                                        worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    }

                                    //worksheet.Cells[imaxrowcount, 4].Value = _score;
                                    worksheet.Cells[imaxrowcount, 5].Value = remarks;

                                    if (remarks.Length >= 75)
                                    {
                                        worksheet.Cells[imaxrowcount, 5].Style.WrapText = true;
                                    }

                                    worksheet.Cells[imaxrowcount, 6].Value = string.Empty;

                                    //if (imgFile.Trim() != string.Empty)
                                    //{
                                    //    var cell = worksheet.Cells[imaxrowcount, 7];
                                    //    cell.Hyperlink = new Uri(filelink);
                                    //    cell.Value = "ImageList";
                                    //}

                                    imaxrowcount++;
                                }

                                float _CateWisTot = 0;
                                if (iCategoryTotal != 0)
                                {
                                    _CateWisTot = ((float)(iCategoryScore / iCategoryTotal));
                                }
                                else
                                {
                                    _CateWisTot = 0;
                                }

                                dtChartdet.Rows.Add(categoryname, _CateWisTot, auditid);


                                string sCatNA = string.Empty;
                                string sCatMaxNA = string.Empty;
                                sCatNA = iCatMax.ToString();
                                sCatMaxNA = iCategoryScore.ToString();
                                if (iCatMax == 0)
                                {
                                    sCatNA = "N/A";
                                }
                                if (iCategoryScore == 0)
                                {
                                    sCatMaxNA = "N/A";
                                }

                                worksheet.Cells[imaxrowcount, 1].Value = "Sub Total " + categoryname;

                                int _catena;
                                int _catmaxna;
                                bool Numeric = int.TryParse(sCatNA.Trim(), out _catena);

                                if (Numeric)
                                {
                                    worksheet.Cells[imaxrowcount, 3].Value = _catena;
                                    worksheet.Cells[imaxrowcount, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                }
                                else
                                {
                                    worksheet.Cells[imaxrowcount, 3].Value = sCatNA;
                                    worksheet.Cells[imaxrowcount, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                }
                                worksheet.Cells[imaxrowcount, 3].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                worksheet.Cells[imaxrowcount, 3].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FCB133"));

                                Numeric = int.TryParse(sCatMaxNA.Trim(), out _catmaxna);

                                if (Numeric)
                                {
                                    worksheet.Cells[imaxrowcount, 4].Value = _catmaxna;
                                    worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                }
                                else
                                {
                                    worksheet.Cells[imaxrowcount, 4].Value = sCatMaxNA;
                                    worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                }
                                worksheet.Cells[imaxrowcount, 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                worksheet.Cells[imaxrowcount, 4].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FCB133"));



                                //worksheet.Cells[imaxrowcount, 3].Value = sCatNA;
                                //worksheet.Cells[imaxrowcount, 4].Value = sCatMaxNA;

                                worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
                                worksheet.Cells[imaxrowcount, 3].Style.Font.Bold = true;
                                worksheet.Cells[imaxrowcount, 4].Style.Font.Bold = true;
                                worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                                imaxrowcount++;
                            }
                            float iScoredAvg = 0;
                            iScoredAvg = ((float)(iscore * 100 / iweightage));
                            fEnteirAvg += iScoredAvg;

                            //worksheet.Cells[imaxrowcount, 1].Value = auditname;
                            //worksheet.Cells[imaxrowcount, 3].Value = iscore;
                            //worksheet.Cells[imaxrowcount, 4].Value = iAuditMax;

                            //worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
                            //worksheet.Cells[imaxrowcount, 3].Style.Font.Bold = true;
                            //worksheet.Cells[imaxrowcount, 4].Style.Font.Bold = true;
                            //worksheet.Cells[imaxrowcount, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            //worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                            //worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                            //imaxrowcount++;

                            //worksheet.Cells[imaxrowcount, 1].Value = "Percentage (%)";
                            //worksheet.Cells[imaxrowcount, 3].Value = string.Empty;
                            //worksheet.Cells[imaxrowcount, 4].Value = iScoredAvg + "%";

                            //worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
                            //worksheet.Cells[imaxrowcount, 4].Style.Font.Bold = true;
                            //worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            //worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                            //imaxrowcount++;
                        }
                        float tAvg = fEnteirAvg / (dsRawdatas.Tables[0].Rows.Count);

                        worksheet.Cells[imaxrowcount, 1].Value = "GRAND TOTAL SCORE";
                        worksheet.Cells[imaxrowcount, 3].Value = iMaxScore;
                        worksheet.Cells[imaxrowcount, 4].Value = iTotalScore;
                        worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
                        worksheet.Cells[imaxrowcount, 3].Style.Font.Bold = true;
                        worksheet.Cells[imaxrowcount, 4].Style.Font.Bold = true;
                        worksheet.Cells[imaxrowcount, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Cells[imaxrowcount, 3].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        worksheet.Cells[imaxrowcount, 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        worksheet.Cells[imaxrowcount, 3].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FCB133"));
                        worksheet.Cells[imaxrowcount, 4].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FCB133"));
                        worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                        imaxrowcount++;

                        decimal scoreGot = Convert.ToDecimal(Getscored);


                        worksheet.Cells[imaxrowcount, 1].Value = "Total Score";
                        worksheet.Cells[imaxrowcount, 3].Value = string.Empty;
                        worksheet.Cells[imaxrowcount, 4].Value = Getscored;
                        worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
                        worksheet.Cells[imaxrowcount, 4].Style.Font.Bold = true;
                        //string setColour = string.Empty;
                        //if (scoreGot >= 83)
                        //{
                        //    worksheet.Cells[imaxrowcount, 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        //    worksheet.Cells[imaxrowcount, 4].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#00FF00"));
                        //}
                        //else
                        //{
                        //    worksheet.Cells[imaxrowcount, 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        //    worksheet.Cells[imaxrowcount, 4].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFBE00"));
                        //}
                        worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        imaxrowcount++;


                        worksheet.Cells[imaxrowcount, 1].Value = "Percentage (%)";
                        worksheet.Cells[imaxrowcount, 3].Value = string.Empty;
                        worksheet.Cells[imaxrowcount, 4].Value = ((decimal)(scoreGot * 100) / 5) + "%";
                        worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
                        worksheet.Cells[imaxrowcount, 4].Style.Font.Bold = true;
                        worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        imaxrowcount++;



                        string observations = drHeader["observation"].ToString();
                        string clientremarks = drHeader["feedback"].ToString();


                        worksheet.Cells[imaxrowcount, 1].Value = "Additional Feedback by Client";
                        worksheet.Cells[imaxrowcount, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
                        worksheet.Cells["A" + imaxrowcount + ":E" + imaxrowcount + ""].Merge = true;
                        imaxrowcount++;


                        worksheet.Cells[imaxrowcount, 1].Value = clientremarks;
                        worksheet.Cells["A" + imaxrowcount + ":E" + imaxrowcount + ""].Merge = true;
                        imaxrowcount++;


                        worksheet.Cells[1, 1, imaxrowcount - 1, 5].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        worksheet.Cells[1, 1, imaxrowcount - 1, 5].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        worksheet.Cells[1, 1, imaxrowcount - 1, 5].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        worksheet.Cells[1, 1, imaxrowcount - 1, 5].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                        // worksheet.Cells.AutoFitColumns(0);  //Autofit columns for all cells
                        // worksheet.Cells.AutoFitColumns(1);
                        //// worksheet.Cells.AutoFitColumns(2);

                        //worksheet.Column(2).Width = 140;
                        //worksheet.Column(5).Width = 140;

                        worksheet.Column(1).Width = 5;
                        worksheet.Column(2).Width = 65;
                        worksheet.Column(3).Width = 15;
                        worksheet.Column(4).Width = 15;
                        worksheet.Column(5).Width = 65;

                        //worksheet.Cells.AutoFitColumns(2);
                        //worksheet.Cells.AutoFitColumns(5);

                        //worksheet.Cells.AutoFitColumns(1);
                        //worksheet.Cells.AutoFitColumns(2);
                        //worksheet.Cells.AutoFitColumns(3);
                        //worksheet.Cells.AutoFitColumns(4);
                        //worksheet.Cells.AutoFitColumns(5);
                        //worksheet.Cells.AutoFitColumns(6);
                        //worksheet.Cells.AutoFitColumns(7);

                        int c1;

                        imaxrowcount = 7;
                        ExcelWorksheet worksheetChart = package.Workbook.Worksheets.Add("CategoryChart");

                        string Alphabetic = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                        int startchar = 0;
                        int colspace = 0;
                        foreach (DataRow drAuditRow in dtAudit.Rows)
                        {
                            int auditid = Convert.ToInt32(drAuditRow["auditid"].ToString());
                            string auditname = drAuditRow["auditname"].ToString();
                            DataRow[] drAuditCategories = dtChartdet.Select("auditid=" + auditid);
                            if (drAuditCategories.Length > 0)
                            {
                                DataTable dtCategoryChart = drAuditCategories.CopyToDataTable();
                                int startvalue = imaxrowcount;

                                char Firstcol = Alphabetic[startchar];
                                char Secondcol = Alphabetic[startchar + 1];

                                foreach (DataRow drchartdet in dtCategoryChart.Rows)
                                {
                                    string categoryname = drchartdet["categoryname"].ToString();
                                    decimal score = Convert.ToDecimal(drchartdet["Value"].ToString());

                                    worksheetChart.Cells["" + Firstcol + imaxrowcount].Value = categoryname;
                                    worksheetChart.Cells["" + Secondcol + imaxrowcount].Value = score;
                                    imaxrowcount++;
                                }

                                var chart = worksheetChart.Drawings.AddChart("chart" + imaxrowcount, eChartType.ColumnClustered3D);
                                var series = chart.Series.Add("" + Secondcol + startvalue + ":" + Secondcol + imaxrowcount, "" + Firstcol + startvalue + ":" + Firstcol + imaxrowcount);
                                //series.HeaderAddress = new ExcelAddress("'Sheet1'!B" + (startvalue - 1));
                                chart.SetSize(510, 300);
                                chart.Title.Text = auditname;
                                chart.SetPosition(7, 0, colspace, 0);
                                colspace += 9;

                                startchar += 2;
                            }
                        }


                        package.Save();

                        Response.ContentType = ContentType;
                        Response.AppendHeader("Content-Disposition", "attachment; filename=" + Path.GetFileName(excelfilepath));
                        Response.WriteFile(excelfilepath);
                        Response.End();

                        //this.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        //this.Response.AddHeader(
                        //          "content-disposition",
                        //          string.Format("attachment;  filename={0}", "ExcellData.xlsx"));
                        //this.Response.BinaryWrite(package.GetAsByteArray());
                    }

                }

                //FileInfo prevFile = new FileInfo(excelfilepath);
                //if (prevFile.Exists)
                //{
                //    prevFile.Delete();
                //}
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, "error");
            }
        }

        protected void lnkdownload_Click(object sender, EventArgs e)
        {
            if (rptrdatewiserpt.Items.Count > 0)
            {
                if (ReportDet == null)
                {
                    NotifyMessages("No Record Found", "error");
                    return;
                }

                DataSet dsreport = new DataSet();
                dsreport.Tables.Add(ReportDet);
                //dsreport = objbltran.GetDateWiseReport(0, month, year);
                if (dsreport.Tables.Count > 0 && dsreport.Tables[0].Rows.Count > 0)
                {
                    if (dsreport.Tables[0].Columns.Contains("count"))
                    {
                        dsreport.Tables[0].Columns.Remove("count");
                    }
                    if (dsreport.Tables[0].Columns.Contains("count"))
                    {
                        dsreport.Tables[0].Columns.Remove("count");
                    }
                    if (dsreport.Tables[0].Columns.Contains("locationid"))
                    {
                        dsreport.Tables[0].Columns.Remove("locationid");
                    }
                    if (dsreport.Tables[0].Columns.Contains("sbuid"))
                    {
                        dsreport.Tables[0].Columns.Remove("sbuid");
                    }
                    if (dsreport.Tables[0].Columns.Contains("sectorid"))
                    {
                        dsreport.Tables[0].Columns.Remove("sectorid");
                    }
                    if (dsreport.Tables[0].Columns.Contains("actualdate"))
                    {
                        dsreport.Tables[0].Columns.Remove("actualdate");
                    }


                    dsreport.Tables[0].Select()
.ToList<DataRow>()
.ForEach(r =>
{
    r["Score"] = CalculateScore((string)r["Score"]);
});

                    string excelfilepath = Server.MapPath("DownloadExcels/") + Guid.NewGuid().ToString() + ".xlsx";
                    Common.DataTableToExcel(dsreport.Tables[0], "Date Wise Report", "Date Wise Report", excelfilepath);
                    Response.ContentType = ContentType;
                    Response.AppendHeader("Content-Disposition", "attachment; filename=" + Path.GetFileName(excelfilepath));
                    Response.WriteFile(excelfilepath);
                    Response.Flush();
                    System.IO.File.Delete(excelfilepath);
                    Response.End();
                }
                else
                {
                    NotifyMessages("No Data Found", "info");
                }
            }
        }

        private string CalculateScore(string score)
        {
            decimal scoreGot = Convert.ToDecimal(score.Trim());
            return ((decimal)(scoreGot * 100) / 5) + "%";
        }

        protected void rptrdatewiserpt_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    Label Score = (Label)e.Item.FindControl("Score");
                    if (Score != null)
                    {
                        Score.Text = CalculateScore(Score.Text.Trim());
                    }
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, "error");
            }
        }

        protected void lnksearch_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtfrmdate.Text.Trim() != string.Empty && txttodate.Text.Trim() != string.Empty)
                {
                    BindDetails();
                }
                else
                {
                    NotifyMessages("Plz Select Date Range", "error");
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, "error");
            }
        }



        //public void GeneratePlot()
        //{
        //    //DataPoint objpt = new DataPoint();
        //    //objpt.XValue = (double)63;       
        //    //objpt.Label="";

        //    DataTable dtchart = new DataTable();
        //    dtchart.Columns.Add("Lable", typeof(string));
        //    dtchart.Columns.Add("value", typeof(double));

        //    for (int icount = 66; icount < 80; icount++)
        //    {
        //        dtchart.Rows.Add("Lable" + icount, (double)icount);
        //    }



        //    using (var ch = new Chart1())
        //    {
        //        Chart1.ChartAreas.Add(new ChartArea());

        //        var s = new Series();
        //        //foreach (var pnt in series) s.Points.Add(pnt);

        //        foreach (DataRow drchar in dtchart.Rows)
        //        {
        //            DataPoint pnt = new DataPoint();
        //            pnt.XValue = Convert.ToDouble(drchar["value"].ToString());
        //            pnt.Label = drchar["Lable"].ToString();
        //            s.Points.Add(pnt);
        //        }

        //        Chart1.Series.Add(
        //        //ch.SaveImage(outputStream, ChartImageFormat.Jpeg);
        //        ch.SaveImage("E:\\venkadesan\\sample.png", System.Drawing.Imaging.ImageFormat.Png);
        //    }
        //}
    }
}