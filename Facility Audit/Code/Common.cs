﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using FacilityAudit.Code;
using System.Web.UI.HtmlControls;
using System.Data;
using System.IO;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace FacilityAudit.Code
{
    public static class Common
    {
        public enum ErrorType
        {
            Error,
            Exclamation,
            Information,
            None,
            Question,
            Stop,
            Warning
        }
        public enum ButtonStatus
        {
            New = 0,
            Edit = 1,
            Delete = 2,
            Cancel = 3,
            View = 4,
            Save = 5,
            Views = 6,
            Listout = 7
        }
        public static string ConverDateDDMMToMMDD(string val)
        {
            try
            {
                string UrDate = val;
                System.Globalization.DateTimeFormatInfo dateInfo = new System.Globalization.DateTimeFormatInfo();
                dateInfo.ShortDatePattern = "dd/MM/yyyy";
                DateTime validDate = Convert.ToDateTime(UrDate, dateInfo);
                if (validDate.ToString() == "01/01/0001" || validDate.ToString() == "01/01/1900")
                {
                    return string.Empty;
                }
                else
                    return validDate.ToString();
            }
            catch { }
            return "";
        }


        public static string ConverDateMMDDToDDMM(string val)
        {
            //string oldstr1 = "1/1/2011";
            //string strDate = DateTime.ParseExact(oldstr1, "MM/dd/yyyy", null).ToString("dd/MM/yyyy");

            try
            {
                DateTime temp = Convert.ToDateTime(val);
                string oldstr = temp.ToString("MM/dd/yyyy");
                string date = DateTime.ParseExact(oldstr, "MM/dd/yyyy", null).ToString("dd/MM/yyyy");

                if (date == "01/01/0001" || date == "01/01/1900")
                {
                    return string.Empty;
                }
                else
                    return date;

            }
            catch { }
            return "";
        }

        public static string ToTitleCase(string mText)
        {

            string rText = "";
            try
            {
                System.Globalization.CultureInfo cultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture;
                System.Globalization.TextInfo TextInfo = cultureInfo.TextInfo;
                rText = TextInfo.ToTitleCase(mText);
            }
            catch
            {
                rText = mText;
            }
            return rText;
        }

        private const string _companyIDCurrent = "CompanyIDCurrent";
        public static int CompanyIDCurrent
        {
            get
            {
                if (HttpContext.Current.Session.Contents[_companyIDCurrent] == null)
                    return 0;
                else
                    return Convert.ToInt32(HttpContext.Current.Session.Contents[_companyIDCurrent]);
            }
            set
            {
                HttpContext.Current.Session.Contents[_companyIDCurrent] = value;
            }
        }

        private const string _GroupIDCurrent = "GroupIDCurrent";
        public static int GroupIDCurrent
        {
            get
            {
                if (HttpContext.Current.Session.Contents[_GroupIDCurrent] == null)
                    return 0;
                else
                    return Convert.ToInt32(HttpContext.Current.Session.Contents[_GroupIDCurrent]);
            }
            set
            {
                HttpContext.Current.Session.Contents[_GroupIDCurrent] = value;
            }
        }

        public static bool DDVal(DropDownList dd, out int val)
        {
            val = 0;
            if (dd.SelectedIndex >= 0)
            {
                if (!Int32.TryParse(dd.SelectedItem.Value, out val))
                    return false;
                else
                    return true;
            }
            return false;
        }

        public static string formatPrice(object val)
        {
            return String.Format("{0:n2}", val);
        }

        public static string GetCurrentPageName()
        {
            string Path = System.Web.HttpContext.Current.Request.Url.AbsolutePath;
            System.IO.FileInfo Info = new System.IO.FileInfo(Path);
            string pageName = Info.Name;
            return pageName;
        }

        public static void SetHeader(HtmlGenericControl HMain, HtmlGenericControl Hsub)
        {
            DataSet ds = new DataSet();
            ds = UserSession.MenuDetails;
            DataTable dt = ds.Tables[0];

            var PageHeading = (from DataRow dRow in ds.Tables[0].Rows
                               where dRow.Field<string>("SubFormpagename") == GetCurrentPageName()
                               select new { MFrom = dRow["Mainformname"], SFrom = dRow["SubFormname"] });

            HMain.InnerText = PageHeading.FirstOrDefault().MFrom.ToString();
            Hsub.InnerText = PageHeading.FirstOrDefault().SFrom.ToString();
        }


        public static void DecodeDDVal(object sender, EventArgs e)
        {
            foreach (ListItem item in ((DropDownList)sender).Items)
            {
                item.Text = System.Web.HttpUtility.HtmlDecode(item.Text);
            }
        }

        public static string Encode(string val)
        {
            val = System.Web.HttpUtility.HtmlEncode(val);
            return val;
        }

        public static string Decode(string val)
        {
            val = System.Web.HttpUtility.HtmlDecode(val);
            return val;
        }

        private const string _IsFramework = "Yes";
        public static string IsFramework
        {
            get
            {
                if (HttpContext.Current.Session.Contents[_IsFramework] == null)
                    return "Yes";
                else
                    return HttpContext.Current.Session.Contents[_IsFramework].ToString();
            }
            set
            {
                HttpContext.Current.Session.Contents[_IsFramework] = value;
            }
        }

        public static bool SplitDates(string twoDateTime, out DateTime val)
        {
            val = new DateTime();
            if (twoDateTime.Contains("-"))
            {
                if (DateTime.TryParse(twoDateTime.Split('-')[0].Trim(), out val))
                    return true;
                else
                    return false;
            }
            else if (DateTime.TryParse(twoDateTime.Trim(), out val))
            {
                return true;
            }
            return false;
        }

        #region "Summary Report"

        private static object CheckInteger(string values)
        {
            try
            {
                int NumericValue;
                decimal decimalvalue;
                bool isNumeric = int.TryParse(values.Trim(), out NumericValue);
                if (isNumeric)
                {
                    return (object)NumericValue;
                }
                else
                {

                    bool isDecimal = decimal.TryParse(values.Replace(",", string.Empty), out decimalvalue);
                    if (isDecimal)
                    {
                        return (object)decimalvalue;
                    }
                }
                return (object)values;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        static public void DataTableToExcel(this DataTable Tbl, string title, string listname, string ExcelFilePath = null, bool isheaderneeded = true)
        {
            try
            {
                FileInfo newFile = new FileInfo(ExcelFilePath);
                if (newFile.Exists)
                {
                    newFile.Delete();  // ensures we create a new workbook
                    newFile = new FileInfo(ExcelFilePath);
                }

                int irow = 1;
                using (ExcelPackage package = new ExcelPackage(newFile))
                {

                    int _irowcount = 2;
                    // add a new worksheet to the empty workbook
                    ExcelWorksheet worksheet = package.Workbook.Worksheets.Add(listname);
                    //Add the headers
                    if (isheaderneeded)
                    {
                        worksheet.Cells[irow, 1].Value = title;
                        var headerFont = worksheet.Cells[1, 1].Style.Font;
                        headerFont.Bold = true;
                        worksheet.Cells[irow, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        irow++;
                        _irowcount = 3;
                    }
                    for (int icolcount = 0; icolcount < Tbl.Columns.Count; icolcount++)
                    {
                        worksheet.Cells[irow, icolcount + 1].Value = Tbl.Columns[icolcount].ColumnName;

                        worksheet.Cells[irow, icolcount + 1].Style.Font.Bold = true;
                    }

                    for (int irowcount = 0; irowcount < Tbl.Rows.Count; irowcount++)
                    {

                        for (int icolcount = 0; icolcount < Tbl.Columns.Count; icolcount++)
                        {
                            worksheet.Cells[_irowcount, (icolcount + 1)].Value = CheckInteger(Tbl.Rows[irowcount][icolcount].ToString());
                        }

                        worksheet.Row(_irowcount).Height = 20;
                        _irowcount++;
                    }

                    if (isheaderneeded)
                    {
                        string Alphabetic = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                        char Secondcol = Alphabetic[Tbl.Columns.Count - 1];
                        worksheet.Cells["A1:" + Secondcol + "1"].Merge = true;
                    }
                    for (int icolcount = 0; icolcount < Tbl.Columns.Count; icolcount++)
                    {
                        worksheet.Cells.AutoFitColumns(icolcount);//Autofit columns for all cells
                    }

                    package.Save();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #endregion
    }
}