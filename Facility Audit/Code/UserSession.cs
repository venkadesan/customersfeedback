﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Configuration;

namespace FacilityAudit.Code
{
    public class UserSession
    {
        public static bool ClearSession
        {
            get
            {
                HttpContext.Current.Session.Clear();
                HttpContext.Current.Session.Abandon();
                return true;
            }
        }
        private const string _Groupcompanyid = "Groupcompanyid";
        public static int Groupcompanyid
        {
            get
            {
                if (HttpContext.Current.Session.Contents[_Groupcompanyid] == null)
                    return 0;
                else
                    return Convert.ToInt32(HttpContext.Current.Session.Contents[_Groupcompanyid]);
            }
            set
            {
                HttpContext.Current.Session.Contents[_Groupcompanyid] = value;
            }
        }
        private const string _Companyid = "Companyid";
        public static int Companyid
        {
            get
            {
                if (HttpContext.Current.Session.Contents[_Companyid] == null)
                    return 0;
                else
                    return Convert.ToInt32(HttpContext.Current.Session.Contents[_Companyid]);
            }
            set
            {
                HttpContext.Current.Session.Contents[_Companyid] = value;
            }
        }

        private const string _EmpID = "EmpID";
        public static int EmpID
        {
            get
            {
                if (HttpContext.Current.Session.Contents[_EmpID] == null)
                    return 0;
                else
                    return Convert.ToInt32(HttpContext.Current.Session.Contents[_EmpID]);
            }
            set
            {
                HttpContext.Current.Session.Contents[_EmpID] = value;
            }
        }

        private const string _invoiceIDCurrent = "InvoiceIDCurrent";
        public static int InvoiceIDCurrent
        {
            get
            {
                if (HttpContext.Current.Session.Contents[_invoiceIDCurrent] == null)
                    return 0;
                else
                    return Convert.ToInt32(HttpContext.Current.Session.Contents[_invoiceIDCurrent]);
            }
            set
            {
                HttpContext.Current.Session.Contents[_invoiceIDCurrent] = value;
            }
        }

        private const string _pageHeadingName = "PageHeadingName";
        public static string PageHeadingName
        {
            get
            {
                if (HttpContext.Current.Session.Contents[_pageHeadingName] == null)
                    return "";
                else
                    return HttpContext.Current.Session.Contents[_pageHeadingName].ToString();
            }
            set
            {
                HttpContext.Current.Session.Contents[_pageHeadingName] = value;
            }
        }


        private const string _countryid = "countryid";
        public static int countryid
        {
            get
            {
                if (HttpContext.Current.Session.Contents[_countryid] == null)
                    return 0;
                else
                    return Convert.ToInt32(HttpContext.Current.Session.Contents[_countryid]);
            }
            set
            {
                HttpContext.Current.Session.Contents[_countryid] = value;
            }
        }
        private const string _cityid = "cityid";
        public static int cityid
        {
            get
            {
                if (HttpContext.Current.Session.Contents[_cityid] == null)
                    return 0;
                else
                    return Convert.ToInt32(HttpContext.Current.Session.Contents[_cityid]);
            }
            set
            {
                HttpContext.Current.Session.Contents[_cityid] = value;
            }
        }
        private const string _locationid = "locationid";
        public static int locationid
        {
            get
            {
                if (HttpContext.Current.Session.Contents[_locationid] == null)
                    return 0;
                else
                    return Convert.ToInt32(HttpContext.Current.Session.Contents[_locationid]);
            }
            set
            {
                HttpContext.Current.Session.Contents[_locationid] = value;
            }
        }

        private const string _CountryID = "CountryID";
        public static int CountryID
        {
            get
            {
                if (HttpContext.Current.Session.Contents[_CountryID] == null)
                    return 0;
                else
                    return Convert.ToInt32(HttpContext.Current.Session.Contents[_CountryID]);
            }
            set
            {
                HttpContext.Current.Session.Contents[_CountryID] = value;
            }
        }



        private const string _CountryIDCurrent = "CountryIDCurrent";
        public static int CountryIDCurrent
        {
            get
            {
                if (HttpContext.Current.Session.Contents[_CountryIDCurrent] == null)
                    return 0;
                else
                    return Convert.ToInt32(HttpContext.Current.Session.Contents[_CountryIDCurrent]);
            }
            set
            {
                HttpContext.Current.Session.Contents[_CountryIDCurrent] = value;
            }
        }

        private const string _CompanyIDUser = "CompanyIDUser";
        public static int CompanyIDUser
        {
            get
            {
                if (HttpContext.Current.Session.Contents[_CompanyIDUser] == null)
                    return 0;
                else
                    return Convert.ToInt32(HttpContext.Current.Session.Contents[_CompanyIDUser]);
            }
            set
            {
                HttpContext.Current.Session.Contents[_CompanyIDUser] = value;
            }
        }


        private const string _LocationIDUser = "LocationIDUser";
        public static int LocationIDUser
        {
            get
            {
                if (HttpContext.Current.Session.Contents[_LocationIDUser] == null)
                    return 0;
                else
                    return Convert.ToInt32(HttpContext.Current.Session.Contents[_LocationIDUser]);
            }
            set
            {
                HttpContext.Current.Session.Contents[_LocationIDUser] = value;
            }
        }


        private const string _buildingid = "buildingid";
        public static int buildingid
        {
            get
            {
                if (HttpContext.Current.Session.Contents[_buildingid] == null)
                    return 0;
                else
                    return Convert.ToInt32(HttpContext.Current.Session.Contents[_buildingid]);
            }
            set
            {
                HttpContext.Current.Session.Contents[_buildingid] = value;
            }
        }
        private const string _Jobfamily = "Jobfamily";
        public static int Jobfamily
        {
            get
            {
                if (HttpContext.Current.Session.Contents[_Jobfamily] == null)
                    return 0;
                else
                    return Convert.ToInt32(HttpContext.Current.Session.Contents[_Jobfamily]);
            }
            set
            {
                HttpContext.Current.Session.Contents[_Jobfamily] = value;
            }
        }
        private const string _Jobrole = "Jobrole";
        public static int Jobrole
        {
            get
            {
                if (HttpContext.Current.Session.Contents[_Jobrole] == null)
                    return 0;
                else
                    return Convert.ToInt32(HttpContext.Current.Session.Contents[_Jobrole]);
            }
            set
            {
                HttpContext.Current.Session.Contents[_Jobrole] = value;
            }
        }

        private const string _requestid = "requestid";
        public static int requestid
        {
            get
            {
                if (HttpContext.Current.Session.Contents[_requestid] == null)
                    return 0;
                else
                    return Convert.ToInt32(HttpContext.Current.Session.Contents[_requestid]);
            }
            set
            {
                HttpContext.Current.Session.Contents[_requestid] = value;
            }
        }

        private const string _sbuid = "sbu";
        public static int sbu
        {
            get
            {
                if (HttpContext.Current.Session.Contents[_sbuid] == null)
                    return 0;
                else
                    return Convert.ToInt32(HttpContext.Current.Session.Contents[_sbuid]);
            }
            set
            {
                HttpContext.Current.Session.Contents[_sbuid] = value;
            }
        }


        private const string _sectorid = "sectorid";
        public static int sectorid
        {
            get
            {
                if (HttpContext.Current.Session.Contents[_sectorid] == null)
                    return 0;
                else
                    return Convert.ToInt32(HttpContext.Current.Session.Contents[_sectorid]);
            }
            set
            {
                HttpContext.Current.Session.Contents[_sectorid] = value;
            }
        }


        private const string _statusid = "statusid";
        public static int statusid
        {
            get
            {
                if (HttpContext.Current.Session.Contents[_statusid] == null)
                    return 0;
                else
                    return Convert.ToInt32(HttpContext.Current.Session.Contents[_statusid]);
            }
            set
            {
                HttpContext.Current.Session.Contents[_statusid] = value;
            }
        }
        private const string _isadmin = "isadmin";
        public static Boolean isadmin
        {
            get
            {
                if (HttpContext.Current.Session.Contents[_isadmin] == null)
                    return false;
                else
                    return Convert.ToBoolean(HttpContext.Current.Session.Contents[_isadmin]);
            }
            set
            {
                HttpContext.Current.Session.Contents[_isadmin] = value;
            }
        }
        
      
        //*****************************

        private const string _fromFramework = "FromFramework";
        public static bool FromFramework
        {
            get
            {
                if (HttpContext.Current.Session.Contents[_fromFramework] == null)
                    return false;
                else
                    return Convert.ToBoolean(HttpContext.Current.Session.Contents[_fromFramework]);
            }
            set
            {
                HttpContext.Current.Session.Contents[_fromFramework] = value;
            }
        }

        public static class Settings
        {
            private const string _gaugeHeight = "GaugeHeight";
            public static int GaugeHeight
            {
                get
                {
                    if (HttpContext.Current.Session.Contents[_gaugeHeight] == null)
                        return 250;
                    else
                        return Convert.ToInt32(HttpContext.Current.Session.Contents[_gaugeHeight]);
                }
                set
                {
                    HttpContext.Current.Session.Contents[_gaugeHeight] = value;
                }
            }

            private const string _gaugeWidth = "GaugeWidth";
            public static int GaugeWidth
            {
                get
                {
                    if (HttpContext.Current.Session.Contents[_gaugeWidth] == null)
                        return 500;
                    else
                        return Convert.ToInt32(HttpContext.Current.Session.Contents[_gaugeWidth]);
                }
                set
                {
                    HttpContext.Current.Session.Contents[_gaugeWidth] = value;
                }


            }

            public static class Images
            {
                private const string _showCompanyImage = "ShowCompanyImage";
                public static bool ShowCompanyImage
                {
                    get
                    {
                        if (HttpContext.Current.Session.Contents[_showCompanyImage] == null)
                            return false;
                        else
                            return Convert.ToBoolean(HttpContext.Current.Session.Contents[_showCompanyImage]);
                    }
                    set
                    {
                        HttpContext.Current.Session.Contents[_showCompanyImage] = value;
                    }
                }

                private const string _companyImage = "CompanyImage";
                //public static string CompanyImage
                //{
                //    get
                //    {
                //        string url = ConfigurationManager.AppSettings["ThemesDomainName"].ToString().TrimEnd('/').Trim() + "/" + ConfigurationManager.AppSettings["StaticFolderPathName"].ToString().TrimEnd('/').Trim() + "/" + ConfigurationManager.AppSettings["CompanyImageFolderName"].ToString().TrimEnd('/').Trim() + "/";
                //        if (ShowCompanyImage)
                //            return url + CompanyIDCurrent.ToString().Trim() + ".png";
                //        else
                //            return url + "default.png";
                //    }
                //}

                private const string _groupImage = "GroupImage";
                public static string GroupImage
                {
                    get
                    {
                        string url = ConfigurationManager.AppSettings["ThemesDomainName"].ToString().TrimEnd('/').Trim() + "/" + ConfigurationManager.AppSettings["StaticFolderPathName"].ToString().TrimEnd('/').Trim() + "/" + ConfigurationManager.AppSettings["GroupImageFolderName"].ToString().TrimEnd('/').Trim() + "/";
                        return url + GroupID.ToString().Trim() + ".png";
                    }
                }
            }
        }

        #region "Others"
        private const string _pageSize = "PageSize";
        public static int PageSize
        {
            get
            {
                if (HttpContext.Current.Session.Contents[_pageSize] == null)
                    return 10;
                else
                    return Convert.ToInt32(HttpContext.Current.Session.Contents[_pageSize]);
            }
            set
            {
                HttpContext.Current.Session.Contents[_pageSize] = value;
            }
        }
        #endregion
        #region "Company Details"
        private const string _groupID = "GroupID";
        public static int GroupID
        {
            get
            {
                if (HttpContext.Current.Session.Contents[_groupID] == null)
                    return 0;
                else
                    return Convert.ToInt32(HttpContext.Current.Session.Contents[_groupID]);
            }
            set
            {
                HttpContext.Current.Session.Contents[_groupID] = value;
            }
        }

      
      
       
        //private const string _companyIDCurrent = "CompanyIDCurrent";
        //public static int CompanyIDCurrent
        //{
        //    get
        //    {
        //        if (HttpContext.Current.Session.Contents[_companyIDCurrent] == null)
        //            return 0;
        //        else
        //            return Convert.ToInt32(HttpContext.Current.Session.Contents[_companyIDCurrent]);
        //    }
        //    set
        //    {
        //        HttpContext.Current.Session.Contents[_companyIDCurrent] = value;
        //    }
        //}
     
        private const string _companyName = "CompanyName";
        public static string CompanyName
        {
            get
            {
                if (HttpContext.Current.Session.Contents[_companyName] == null)
                    return string.Empty;
                else
                    return HttpContext.Current.Session.Contents[_companyName].ToString();
            }
            set
            {
                HttpContext.Current.Session.Contents[_companyName] = value;
            }
        }
        private const string _groupName = "GroupName";
        public static string GroupName
        {
            get
            {
                if (HttpContext.Current.Session.Contents[_groupName] == null)
                    return string.Empty;
                else
                    return HttpContext.Current.Session.Contents[_groupName].ToString();
            }
            set
            {
                HttpContext.Current.Session.Contents[_groupName] = value;
            }
        }
        private const string _locationName = "LocationName";
        public static string LocationName
        {
            get
            {
                if (HttpContext.Current.Session.Contents[_locationName] == null)
                    return string.Empty;
                else
                    return HttpContext.Current.Session.Contents[_locationName].ToString();
            }
            set
            {
                HttpContext.Current.Session.Contents[_locationName] = value;
            }
        }
     
        #endregion
        #region "User Details"
        private const string _roleID = "RoleID";
        public static int RoleID
        {
            get
            {
                if (HttpContext.Current.Session.Contents[_roleID] == null)
                    return 0;
                else
                    return Convert.ToInt32(HttpContext.Current.Session.Contents[_roleID]);
            }
            set
            {
                HttpContext.Current.Session.Contents[_roleID] = value;
            }
        }
        private const string _userID = "UserID";
        public static int UserID
        {
            get
            {
                if (HttpContext.Current.Session.Contents[_userID] == null)
                    return 0;
                else
                    return Convert.ToInt32(HttpContext.Current.Session.Contents[_userID]);
            }
            set
            {
                HttpContext.Current.Session.Contents[_userID] = value;
            }
        }


        private const string _userImageName = "UserImageName";
        public static string UserImageName
        {
            get
            {
                if (HttpContext.Current.Session.Contents[_userImageName] == null)
                {
                    return string.Empty;
                }
                else
                {
                    return HttpContext.Current.Session.Contents[_userImageName].ToString();
                }
            }
            set
            {
                HttpContext.Current.Session.Contents[_userImageName] = value;
            }
        }


        private const string _userFirstName = "UserFirstName";
        public static string UserFirstName
        {
            get
            {
                if (HttpContext.Current.Session.Contents[_userFirstName] == null)
                {
                    return string.Empty;
                }
                else
                {
                    return HttpContext.Current.Session.Contents[_userFirstName].ToString();
                }
            }
            set
            {
                HttpContext.Current.Session.Contents[_userFirstName] = value;
            }
        }

        private const string _userLastName = "UserLastName";
        public static string UserLastName
        {
            get
            {
                if (HttpContext.Current.Session.Contents[_userLastName] == null)
                {
                    return string.Empty;
                }
                else
                {
                    return HttpContext.Current.Session.Contents[_userLastName].ToString();
                }
            }
            set
            {
                HttpContext.Current.Session.Contents[_userLastName] = value;
            }
        }


        private const string _themeFolderPath = "ThemeFolderPath";
        public static string ThemeFolderPath
        {
            get
            {
                if (HttpContext.Current.Session.Contents[_themeFolderPath] == null)
                {
                    return string.Empty;
                }
                else
                {
                    return HttpContext.Current.Session.Contents[_themeFolderPath].ToString();
                }
            }
            set
            {
                HttpContext.Current.Session.Contents[_themeFolderPath] = value;
            }
        }
        private const string _announcement = "Announcement";
        public static string Announcement
        {
            get
            {
                if (HttpContext.Current.Session.Contents[_announcement] == null)
                    return string.Empty;
                else
                    return HttpContext.Current.Session.Contents[_announcement].ToString();
            }
            set
            {
                HttpContext.Current.Session.Contents[_announcement] = value;
            }
        }
        private const string _companyLogoName = "CompanyLogoName";
        public static string CompanyLogoName
        {
            get
            {
                if (HttpContext.Current.Session.Contents[_companyLogoName] == null)
                    return "default.png";
                else
                    return HttpContext.Current.Session.Contents[_companyLogoName].ToString();
            }
            set
            {
                if (value.Trim() == string.Empty)
                    HttpContext.Current.Session.Contents[_companyLogoName] = "default.png";
                else
                    HttpContext.Current.Session.Contents[_companyLogoName] = value;
            }
        }
        #endregion
        private const string _languageID = "LanguageID";
        public static int LanguageID
        {
            get
            {
                if (HttpContext.Current.Session.Contents[_languageID] == null)
                    return 0;
                else
                    return Convert.ToInt32(HttpContext.Current.Session.Contents[_languageID]);
            }
            set
            {
                HttpContext.Current.Session.Contents[_languageID] = value;
            }
        }
        private const string _rowCount = "RowCount";
        public static int RowCount
        {
            get
            {
                if (HttpContext.Current.Session.Contents[_rowCount] == null)
                    return 0;
                else
                    return Convert.ToInt32(HttpContext.Current.Session.Contents[_rowCount]);
            }
            set
            {
                HttpContext.Current.Session.Contents[_rowCount] = value;
            }
        }
        private const string _menuDetails = "MenuDetails";
        public static DataSet MenuDetails
        {
            get
            {
                if (HttpContext.Current.Session.Contents[_menuDetails] == null)
                    return null;
                else
                    return (DataSet)(HttpContext.Current.Session.Contents[_menuDetails]);
            }
            set
            {
                HttpContext.Current.Session.Contents[_menuDetails] = value;
            }
        }
    }
}