﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Mainpage.Master" AutoEventWireup="true"
    CodeBehind="CateLocMapping.aspx.cs" Inherits="Facility_Audit.CateLocMapping" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="col-md-11">
        <div id="crumbs">
            <ul>
                <li class="active" style="width: 250px;"><a href="#">Category - Location Mapping</a></li>
            </ul>
        </div>
    </div>
    <div class="main">
        <div class=" col-md-5 col-xs-offset-4 alert alert-big alert-danger alert-dismissable fade in margin-top-10"
            runat="server" id="divmsg" visible="false">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                ×</button>
            <asp:Label runat="server" ID="lblError"></asp:Label>
        </div>
        <div class="row">
            <div class="col-md-12">
                <section class="tile">
                 <section class="tile cornered margin-top-10">
        	    	<div class="tile-body nobottompadding">
                      <div class="row form-horizontal">
                        <div class="col-lg-12">
                            <div class="form-group">
                                   <label class="col-sm-2 control-label" for="">Category</label>
                               <div class="col-sm-9">
                                   <asp:DropDownList ID="ddlcategory" runat="server"  AutoPostBack="True"  
                                     CssClass="form-control chosen-select" 
                                       onselectedindexchanged="ddlcategory_SelectedIndexChanged">
                                   </asp:DropDownList>
                               </div>
                               <div class="col-sm-1">
                                 <asp:LinkButton ID="lnkdownload" Style="text-decoration: none;" class="fa fa-2x fa-download"
                                ToolTip="Export To Excel" runat="server" OnClick="lnkdownload_Click"></asp:LinkButton>
                               </div>
                            </div>
                        </div> 
                      
                        <div class="col-lg-12">
                            <div class="form-group">
                                   <label class="col-sm-2 control-label" for="">Location</label>
                               <div class="col-sm-10">
                                   <asp:ListBox ID="lstlocation" runat="server" CssClass="form-control chosen-select" SelectionMode="Multiple" >
                                   </asp:ListBox>
                               </div>
                            </div>
                        </div> 
                      </div>
                    </div>
                 </section>

                 <div class="tile-footer color cyan text-muted">
                           <div class="row">
                              <div class="col-md-5">
                                 <div class="col-md-offset-5">
                                    <asp:Button ID="btnsave" class="btn btn-primary" runat="server" Text="Save" 
                                         onclick="btnsave_Click"  />
                                   
                                 </div>
                              </div>
                           </div>
                       </div>
                 </section>
            </div>
        </div>
</asp:Content>
