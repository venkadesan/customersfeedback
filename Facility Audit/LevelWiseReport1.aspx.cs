﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BL;
using System.Data;
using System.Text;
using FacilityAudit.Code;

using OfficeOpenXml;
using OfficeOpenXml.Drawing;
using OfficeOpenXml.Style;
using System.Drawing;
using OfficeOpenXml.Drawing.Chart;
using System.Configuration;
using System.IO;

namespace Facility_Audit
{
    public partial class LevelWiseReport1 : System.Web.UI.Page
    {
        BLTransaction objbltran = new BLTransaction();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindYears();
                //BindDetails();
                //BindRegionWise();
                BindOverall();
            }
        }

        private void BindOverall()
        {
            try
            {
                ClearDatas();
                //DataTable dtRegion = new DataTable(); ;
                //dtRegion.Columns.Add("RegionId", typeof(int));
                //dtRegion.Columns.Add("Region", typeof(string));
                //dtRegion.Columns.Add("Score", typeof(decimal));

                //dtRegion.Rows.Add(1, "North", 68.4);
                //dtRegion.Rows.Add(2, "East", 40.7);
                //dtRegion.Rows.Add(3, "West", 79.4);
                //dtRegion.Rows.Add(4, "South", 98.4);
                loverall.Text = string.Empty;
                lregionwise.Text = string.Empty;
                //lsbuwise.Text = string.Empty;
                llocationwise.Text = string.Empty;
                lrawdata.Text = string.Empty;
                regiondiv.Visible = false;
                DataSet dsreport = new DataSet();
                int month = 0, year = 0;
                Common.DDVal(ddlmonth, out month);
                Common.DDVal(ddlyear, out year);
                dsreport = objbltran.GetRegionWiseScore(month, year, UserSession.GroupID);

                if (dsreport.Tables[1].Rows.Count > 0)
                {
                    rptroverall.DataSource = dsreport.Tables[1];
                    rptroverall.DataBind();

                    loverall.Text = "Overall";
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, "error");
            }
        }

        private void BindRegionWise()
        {
            try
            {
                ClearDatas();
                //DataTable dtRegion = new DataTable(); ;
                //dtRegion.Columns.Add("RegionId", typeof(int));
                //dtRegion.Columns.Add("Region", typeof(string));
                //dtRegion.Columns.Add("Score", typeof(decimal));

                //dtRegion.Rows.Add(1, "North", 68.4);
                //dtRegion.Rows.Add(2, "East", 40.7);
                //dtRegion.Rows.Add(3, "West", 79.4);
                //dtRegion.Rows.Add(4, "South", 98.4);
                loverall.Text = string.Empty;
                lregionwise.Text = string.Empty;
                //lsbuwise.Text = string.Empty;
                llocationwise.Text = string.Empty;
                lrawdata.Text = string.Empty;
                DataSet dsreport = new DataSet();
                int month = 0, year = 0;
                Common.DDVal(ddlmonth, out month);
                Common.DDVal(ddlyear, out year);
                regiondiv.Visible = false;
                dsreport = objbltran.GetRegionWiseScore(month, year, UserSession.GroupID);
                if (dsreport.Tables.Count > 0 && dsreport.Tables[0].Rows.Count > 0)
                {
                    rptrregion.DataSource = dsreport.Tables[0];
                    rptrregion.DataBind();

                    regiondiv.Visible = true;

                    lregionwise.Text = "Region - SBU Wise";
                }
                if (dsreport.Tables[1].Rows.Count > 0)
                {
                    rptroverall.DataSource = dsreport.Tables[1];
                    rptroverall.DataBind();

                    loverall.Text = "Overall";
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, "error");
            }
        }

        private void BindYears()
        {
            try
            {
                int year = DateTime.Now.Year;
                for (int icount = (year - 1); icount < (year + 2); icount++)
                {
                    ddlyear.Items.Add(new ListItem(icount.ToString(), icount.ToString()));
                }

                ddlyear.SelectedValue = DateTime.Now.Year.ToString();
                ddlmonth.SelectedValue = DateTime.Now.Month.ToString();
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, "error");
            }
        }

        private void BindDetails(int regionid, Repeater rptrsbuwise)
        {
            try
            {

                //lsbuwise.Text = string.Empty;
                llocationwise.Text = string.Empty;
                lrawdata.Text = string.Empty;
                divmsg.Visible = false;
                llocationwise.Text = string.Empty;
                lrawdata.Text = string.Empty;
                lblError.Text = string.Empty;

                rptrsbuwise.DataSource = null;
                rptrsbuwise.DataBind();

                rptrlocationwise.DataSource = null;
                rptrlocationwise.DataBind();

                rptrdatewiserpt.DataSource = null;
                rptrdatewiserpt.DataBind();

                DataSet dsreport = new DataSet();
                int month = 0, year = 0;
                Common.DDVal(ddlmonth, out month);
                Common.DDVal(ddlyear, out year);
                dsreport = objbltran.GetSbuWiseScore(month, year, regionid, UserSession.GroupID);
                if (dsreport.Tables.Count > 0 && dsreport.Tables[0].Rows.Count > 0)
                {

                    rptrsbuwise.DataSource = dsreport.Tables[0];
                    rptrsbuwise.DataBind();


                    //lsbuwise.Text = "SBU - Wise";
                }
                else
                {
                    lrawdata.Text = string.Empty;
                    loverall.Text = string.Empty;
                    //lsbuwise.Text = string.Empty;
                    rptrsbuwise.DataSource = null;
                    rptrsbuwise.DataBind();

                    NotifyMessages("No Data Found", "info");
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, "error");
            }
        }

        public class Observations
        {
            public string JS_SCM { get; set; }
            public string JS_Others { get; set; }
            public string JS_HR { get; set; }
            public string JS_Complaince { get; set; }
            public string JS_Training { get; set; }
            public string JS_Operation { get; set; }
        }

        private void ClearDatas()
        {
            rptrregion.DataSource = null;
            rptrregion.DataBind();


            //rptrsbuwise.DataSource = null;
            //rptrsbuwise.DataBind();

            rptrlocationwise.DataSource = null;
            rptrlocationwise.DataBind();

            rptrdatewiserpt.DataSource = null;
            rptrdatewiserpt.DataBind();
        }

        public void NotifyMessages(string message, string errortype)
        {
            divmsg.Visible = true;
            lblError.Text = message;
            if (errortype == "info")
            {
                divmsg.Attributes.Add("class", "alert alert-success fade in");
            }
            else if (errortype == "error")
            {
                divmsg.Attributes.Add("class", "alert alert-danger fade in");
            }
        }

        protected void btnoverall_Click(object sender, EventArgs e)
        {
            try
            {
                BindRegionWise();
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, "error");
            }
        }

        protected void btnsbuwise_Click(object sender, EventArgs e)
        {
            try
            {

                llocationwise.Text = string.Empty;
                lrawdata.Text = string.Empty;

                divmsg.Visible = false;
                lblError.Text = string.Empty;
                lrawdata.Text = string.Empty;

                DataSet dsreport = new DataSet();
                int month = 0, year = 0;
                Common.DDVal(ddlmonth, out month);
                Common.DDVal(ddlyear, out year);
                int sbuid = Convert.ToInt32(hdnsbuid.Value);
                dsreport = objbltran.GetLocationwiseScore(sbuid, month, year, UserSession.GroupID);
                if (dsreport.Tables.Count > 0 && dsreport.Tables[0].Rows.Count > 0)
                {
                    rptrlocationwise.DataSource = dsreport.Tables[0];
                    rptrlocationwise.DataBind();

                    rptrdatewiserpt.DataSource = null;
                    rptrdatewiserpt.DataBind();

                    llocationwise.Text = "Location - Wise";
                }
                else
                {
                    llocationwise.Text = string.Empty;
                    rptrlocationwise.DataSource = null;
                    rptrlocationwise.DataBind();

                    rptrdatewiserpt.DataSource = null;
                    rptrdatewiserpt.DataBind();
                    NotifyMessages("No Data Found", "info");
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, "error");
            }
        }

        protected void btnregionwise_Click(object sender, EventArgs e)
        {
            try
            {
                int regionid = Convert.ToInt32(hdnregionid.Value);
                //BindDetails(regionid);
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, "error");
            }
        }
        protected void btnlocwise_Click(object sender, EventArgs e)
        {
            try
            {

                lrawdata.Text = string.Empty;

                divmsg.Visible = false;
                lblError.Text = string.Empty;
                DataSet dsreport = new DataSet();
                int locationid = Convert.ToInt32(hdnlocationid.Value);
                int month = 0, year = 0;
                Common.DDVal(ddlmonth, out month);
                Common.DDVal(ddlyear, out year);
                dsreport = objbltran.GetDateWiseReportByLocation(locationid, month, year);
                if (dsreport.Tables.Count > 0 && dsreport.Tables[0].Rows.Count > 0)
                {
                    rptrdatewiserpt.DataSource = dsreport.Tables[0];
                    rptrdatewiserpt.DataBind();

                    lrawdata.Text = "Date Wise";
                }
                else
                {
                    rptrdatewiserpt.DataSource = null;
                    rptrdatewiserpt.DataBind();

                    lrawdata.Text = string.Empty;
                    NotifyMessages("No Data Found", "info");
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, "error");
            }
        }



        private string GetSiteID(string sitename)
        {
            string[] sitedet = sitename.Split('-');
            return sitedet[sitedet.Length - 1];
        }

        protected void rptrdatewiserpt_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Download")
            {
                try
                {
                    HiddenField hdnlocationid = (HiddenField)e.Item.FindControl("locationid");
                    HiddenField hdnsbuid = (HiddenField)e.Item.FindControl("sbuid");
                    HiddenField hdnsectionid = (HiddenField)e.Item.FindControl("sectionid");
                    HiddenField hdnactualdate = (HiddenField)e.Item.FindControl("actualdate");
                    HiddenField Score = (HiddenField)e.Item.FindControl("hdnscore");
                    Label lAuditorName = (Label)e.Item.FindControl("AuditorName");
                    if (hdnlocationid != null && hdnsbuid != null && hdnsectionid != null && hdnactualdate != null && Score != null && lAuditorName != null)
                    {
                        string Getscored = Score.Value;
                        int locationid = Convert.ToInt32(hdnlocationid.Value);
                        int sbuid = Convert.ToInt32(hdnsbuid.Value);
                        int sectorid = Convert.ToInt32(hdnsectionid.Value);
                        DateTime dtactualdate = Convert.ToDateTime(hdnactualdate.Value);
                        DataSet dsRawdatas = new DataSet();
                        dsRawdatas = objbltran.GetRawDatasDateWiseReport(dtactualdate, locationid, sbuid, sectorid, lAuditorName.Text);
                        MakeExcel(dsRawdatas, Getscored);
                    }
                }
                catch (Exception ex)
                {
                    NotifyMessages(ex.Message, "error");
                }

            }
        }


        protected void rptrlocationwise_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if ((e.Item.ItemType == ListItemType.Item) ||
                  (e.Item.ItemType == ListItemType.AlternatingItem))
                {
                    Label score = (Label)e.Item.FindControl("score");
                    HiddenField hdnregionid = (HiddenField)e.Item.FindControl("hdnregionid");
                    Repeater rptrsbuwise = (Repeater)e.Item.FindControl("rptrsbuwise");
                    System.Web.UI.WebControls.Image imgicon = (System.Web.UI.WebControls.Image)e.Item.FindControl("imgicon");
                    if (score != null && imgicon != null)
                    {
                        double dscore = Convert.ToDouble(score.Text);
                        if (dscore >= 4.15)
                        {
                            imgicon.ImageUrl = "~/AppThemes/images/smile-icon5.png";
                        }
                        else if (dscore <= 4.14 && dscore >= 3.75)
                        {
                            imgicon.ImageUrl = "~/AppThemes/images/smile-icon4.png";
                        }
                        else
                        {
                            imgicon.ImageUrl = "~/AppThemes/images/smile-icon1.png";
                        }

                    }

                    if (score != null)
                    {
                        score.Text = CalculateScore(score.Text.Trim());
                    }

                    Label location = (Label)e.Item.FindControl("location");
                    if (location != null)
                    {
                        string locname = location.Text;
                        if (locname.Length > 25)
                        {
                            string[] Splitname = locname.Split('-');
                            string displocname = Splitname[0].Substring(0, Math.Min(Splitname[0].Length, 25));
                            if (Splitname.Length > 1)
                            {
                                displocname += " - " + Splitname[Splitname.Length - 1];
                            }
                            location.Text = displocname;
                        }
                    }

                    if (hdnregionid != null && rptrsbuwise != null)
                    {
                        int regionid = Convert.ToInt32(hdnregionid.Value);
                        BindDetails(regionid, rptrsbuwise);
                    }
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, "error");
            }
        }

        private string CalculateScore(string score)
        {
            decimal scoreGot = Convert.ToDecimal(score.Trim());
            return ((decimal)(scoreGot * 100) / 5) + "%";
        }


        private void MakeExcel(DataSet dsRawdatas, string Getscored)
        {
            try
            {

                string filename = Guid.NewGuid().ToString();
                if (dsRawdatas.Tables[3].Rows.Count > 0)
                {
                    DataRow drfilename = dsRawdatas.Tables[3].Rows[0];
                    filename = drfilename["locsitename"].ToString().Replace(",", string.Empty).Replace("/", string.Empty).Replace(@"\", string.Empty).Replace("<", string.Empty).Replace(">", string.Empty);
                }
                string excelfilepath = Server.MapPath("DownloadExcels/") + filename + ".xlsx";

                FileInfo newFile = new FileInfo(excelfilepath);
                if (newFile.Exists)
                {
                    newFile.Delete();  // ensures we create a new workbook
                    newFile = new FileInfo(excelfilepath);
                }

                using (ExcelPackage package = new ExcelPackage(newFile))
                {
                    // add a new worksheet to the empty workbook
                    ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("DownloadeExcel");

                    //Add the headers

                    StringBuilder sDatas = new StringBuilder();

                    float fEnteirAvg = 0;
                    int iTotalScore = 0;
                    int iTotalWeightage = 0;
                    int iMaxScore = 0;

                    DataTable dtChartdet = new DataTable();
                    dtChartdet.Columns.Add("Categoryname", typeof(string));
                    dtChartdet.Columns.Add("Value", typeof(float));
                    dtChartdet.Columns.Add("auditId", typeof(int));

                    if (dsRawdatas.Tables.Count > 0 && dsRawdatas.Tables[0].Rows.Count > 0)
                    {

                        DataTable dtAudit = dsRawdatas.Tables[0];
                        DataTable dtCategory = dsRawdatas.Tables[1];
                        DataTable dtRawData = dsRawdatas.Tables[2];
                        DataRow drHeader = dsRawdatas.Tables[3].Rows[0];

                        // worksheet.Cells[1, 1].Value = "Image Here";
                        //worksheet.Cells[1, 1].Value = "DUSTERS TOTAL SOLUTIONS SERVICES PVT LTD";

                        worksheet.Cells[1, 1].Value = drHeader["GroupName"].ToString().ToUpper();

                        var headerFont = worksheet.Cells[1, 1].Style.Font;
                        headerFont.Bold = true;
                        headerFont.Size = 16;
                        worksheet.Cells[1, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Cells[1, 1].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        worksheet.Cells["A1:E4"].Merge = true;

                        string logofile = drHeader["UploadLogoName"].ToString();
                        if (logofile.Trim() != string.Empty)
                        {
                            string filepath = ConfigurationManager.AppSettings["companylogo"].ToString() + logofile;

                            FileInfo fileinfo = new FileInfo(filepath);
                            var picture = worksheet.Drawings.AddPicture("logo", fileinfo);
                            picture.SetSize(100, 50);
                            picture.SetPosition(1, 0, 1, 0);
                        }



                        worksheet.Cells[5, 1].Value = "CLIENT FEEDBACK FORM";
                        worksheet.Cells[5, 1].Style.Font.Bold = true;
                        worksheet.Cells["A" + 5 + ":E" + 5 + ""].Merge = true;
                        worksheet.Cells[5, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;



                        worksheet.Cells[6, 1].Value = "Name of the Client";
                        worksheet.Cells[6, 3].Value = drHeader["clientname"].ToString();
                        //worksheet.Cells[5, 6].Value = "";
                        worksheet.Cells["A6:B6"].Merge = true;
                        worksheet.Cells["C6:E6"].Merge = true;


                        worksheet.Cells[7, 1].Value = "Name of the Site";
                        worksheet.Cells[7, 3].Value = drHeader["locsitename"].ToString();
                        worksheet.Name = drHeader["locsitename"].ToString();
                        worksheet.Cells["A7:B7"].Merge = true;
                        worksheet.Cells["C7:E7"].Merge = true;


                        worksheet.Cells[8, 1].Value = "Site ID";
                        worksheet.Cells[8, 3].Value = GetSiteID(drHeader["locsitename"].ToString());
                        worksheet.Cells["A8:B8"].Merge = true;
                        worksheet.Cells["C8:E8"].Merge = true;

                        worksheet.Cells[9, 1].Value = "Name/EmailId";
                        worksheet.Cells[9, 3].Value = GetSiteID(drHeader["auditor"].ToString());
                        worksheet.Cells["A9:B9"].Merge = true;
                        worksheet.Cells["C9:E9"].Merge = true;

                        worksheet.Cells[10, 1].Value = "Date";
                        worksheet.Cells[10, 3].Value = GetSiteID(drHeader["dispauditdate"].ToString());
                        worksheet.Cells["A10:B10"].Merge = true;
                        worksheet.Cells["C10:E10"].Merge = true;


                        int imaxrowcount = 11;

                        for (int iauditcount = 0; iauditcount < dtAudit.Rows.Count; iauditcount++)
                        {
                            DataRow draudit = dtAudit.Rows[iauditcount];
                            int auditid = Convert.ToInt32(draudit["auditid"].ToString());
                            string auditname = draudit["auditname"].ToString();

                            worksheet.Cells[imaxrowcount, 1].Value = auditname;
                            worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;

                            worksheet.Cells["A" + imaxrowcount + ":E" + imaxrowcount + ""].Merge = true;
                            worksheet.Cells[imaxrowcount, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            imaxrowcount++;

                            worksheet.Cells[imaxrowcount, 1].Value = "Sl.No";
                            worksheet.Cells[imaxrowcount, 2].Value = "PARTICULARS";
                            worksheet.Cells[imaxrowcount, 3].Value = "Maximum Score";
                            worksheet.Cells[imaxrowcount, 4].Value = "Score Obtained";
                            worksheet.Cells[imaxrowcount, 5].Value = "Remarks";

                            var range = worksheet.Cells[imaxrowcount, 1, imaxrowcount, 5];
                            range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                            range.Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FCB133"));

                            imaxrowcount++;

                            int iscore = 0;
                            int iweightage = 0;
                            int iSingleAvg = 0;
                            int iTotalAvg = 0;
                            int iAuditMax = 0;
                            DataRow[] drCategory = dtCategory.Select("auditid=" + auditid);
                            for (int icategroryCount = 0; icategroryCount < drCategory.Length; icategroryCount++)
                            {
                                int categoryid = Convert.ToInt32(drCategory[icategroryCount]["categoryid"].ToString());
                                string categoryname = drCategory[icategroryCount]["categoryname"].ToString();

                                worksheet.Cells[imaxrowcount, 1].Value = icategroryCount + 1;
                                worksheet.Cells[imaxrowcount, 2].Value = categoryname;
                                worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
                                worksheet.Cells[imaxrowcount, 2].Style.Font.Bold = true;
                                worksheet.Cells[imaxrowcount, 1].Style.Font.Size = 14;

                                worksheet.Cells["B" + imaxrowcount + ":E" + imaxrowcount + ""].Merge = true;
                                imaxrowcount++;

                                DataRow[] drRawDatas = dtRawData.Select("categoryid=" + categoryid);

                                int iCategoryScore = 0;
                                int iCategoryTotal = 0;
                                int iCatMax = 0;

                                for (int iRawDatacount = 0; iRawDatacount < drRawDatas.Length; iRawDatacount++)
                                {
                                    string question = drRawDatas[iRawDatacount]["auditqname"].ToString();
                                    string remarks = drRawDatas[iRawDatacount]["remarks"].ToString();
                                    int score = Convert.ToInt32(drRawDatas[iRawDatacount]["score"].ToString());
                                    int weightage = Convert.ToInt32(drRawDatas[iRawDatacount]["weightage"].ToString());
                                    string scorename = drRawDatas[iRawDatacount]["scorename"].ToString();
                                    string imgFile = drRawDatas[iRawDatacount]["uploadfilename"].ToString();
                                    string transactionid = drRawDatas[iRawDatacount]["TransactionId"].ToString();
                                    string filelink = string.Empty;
                                    if (imgFile.Trim() != string.Empty)
                                    {
                                        string filepath = "http://ifazility.com/facilityaudit/ImageLists.aspx";
                                        if (ConfigurationManager.AppSettings["Imaglists"] != null)
                                        {
                                            filepath = ConfigurationManager.AppSettings["Imaglists"].ToString();
                                        }
                                        filelink = filepath + "?TransactionId=" + transactionid;
                                    }

                                    int tScore = 0;
                                    int MaxScore = 0;

                                    //if (score == -1)
                                    //{
                                    //    score = 0;
                                    //    tScore = -1;
                                    //    weightage = 0;
                                    //    MaxScore = 0;
                                    //}
                                    //else
                                    //{
                                    //    MaxScore = 1;
                                    //}

                                    if (score == -1)
                                    {
                                        score = 0;
                                        weightage = 0;
                                        MaxScore = 0;
                                        tScore = -1;
                                    }
                                    else
                                    {
                                        MaxScore = 5;
                                    }

                                    iscore += score;
                                    iweightage += weightage;

                                    iCategoryScore += score;
                                    iCategoryTotal += weightage;

                                    iTotalScore += score;
                                    iTotalWeightage += weightage;

                                    iSingleAvg = score * weightage;
                                    iTotalAvg += iSingleAvg;

                                    iMaxScore += MaxScore;
                                    iCatMax += MaxScore;
                                    iAuditMax += MaxScore;

                                    string _score = string.Empty;
                                    string _maxscore = string.Empty;
                                    if (tScore == 0)
                                    {
                                        _score = " " + score;
                                        _maxscore = " " + MaxScore;
                                    }
                                    else
                                    {
                                        _score = scorename;
                                        _maxscore = scorename;
                                    }


                                    worksheet.Cells[imaxrowcount, 1].Value = iRawDatacount + 1;
                                    worksheet.Cells[imaxrowcount, 2].Value = question;

                                    if (question.Length >= 75)
                                    {
                                        worksheet.Cells[imaxrowcount, 2].Style.WrapText = true;
                                    }

                                    //worksheet.Cells[imaxrowcount, 2].Style.WrapText = true;
                                    //worksheet.Cells[imaxrowcount, 3].Value = _maxscore;

                                    int MaxRefId;
                                    int ScorerefID;

                                    bool isNumeric = int.TryParse(_maxscore.Trim(), out MaxRefId);

                                    if (isNumeric)
                                    {
                                        worksheet.Cells[imaxrowcount, 3].Value = MaxRefId;
                                        worksheet.Cells[imaxrowcount, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    }
                                    else
                                    {
                                        worksheet.Cells[imaxrowcount, 3].Value = _maxscore;
                                        worksheet.Cells[imaxrowcount, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    }

                                    isNumeric = int.TryParse(_score.Trim(), out ScorerefID);

                                    if (isNumeric)
                                    {
                                        worksheet.Cells[imaxrowcount, 4].Value = ScorerefID;
                                        worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    }
                                    else
                                    {
                                        worksheet.Cells[imaxrowcount, 4].Value = _score;
                                        worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    }

                                    //worksheet.Cells[imaxrowcount, 4].Value = _score;
                                    worksheet.Cells[imaxrowcount, 5].Value = remarks;

                                    if (remarks.Length >= 75)
                                    {
                                        worksheet.Cells[imaxrowcount, 5].Style.WrapText = true;
                                    }

                                    worksheet.Cells[imaxrowcount, 6].Value = string.Empty;

                                    //if (imgFile.Trim() != string.Empty)
                                    //{
                                    //    var cell = worksheet.Cells[imaxrowcount, 7];
                                    //    cell.Hyperlink = new Uri(filelink);
                                    //    cell.Value = "ImageList";
                                    //}

                                    imaxrowcount++;
                                }

                                float _CateWisTot = 0;
                                if (iCategoryTotal != 0)
                                {
                                    _CateWisTot = ((float)(iCategoryScore / iCategoryTotal));
                                }
                                else
                                {
                                    _CateWisTot = 0;
                                }

                                dtChartdet.Rows.Add(categoryname, _CateWisTot, auditid);


                                string sCatNA = string.Empty;
                                string sCatMaxNA = string.Empty;
                                sCatNA = iCatMax.ToString();
                                sCatMaxNA = iCategoryScore.ToString();
                                if (iCatMax == 0)
                                {
                                    sCatNA = "N/A";
                                }
                                if (iCategoryScore == 0)
                                {
                                    sCatMaxNA = "N/A";
                                }

                                worksheet.Cells[imaxrowcount, 1].Value = "Sub Total " + categoryname;

                                int _catena;
                                int _catmaxna;
                                bool Numeric = int.TryParse(sCatNA.Trim(), out _catena);

                                if (Numeric)
                                {
                                    worksheet.Cells[imaxrowcount, 3].Value = _catena;
                                    worksheet.Cells[imaxrowcount, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                }
                                else
                                {
                                    worksheet.Cells[imaxrowcount, 3].Value = sCatNA;
                                    worksheet.Cells[imaxrowcount, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                }
                                worksheet.Cells[imaxrowcount, 3].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                worksheet.Cells[imaxrowcount, 3].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FCB133"));

                                Numeric = int.TryParse(sCatMaxNA.Trim(), out _catmaxna);

                                if (Numeric)
                                {
                                    worksheet.Cells[imaxrowcount, 4].Value = _catmaxna;
                                    worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                }
                                else
                                {
                                    worksheet.Cells[imaxrowcount, 4].Value = sCatMaxNA;
                                    worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                }
                                worksheet.Cells[imaxrowcount, 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                worksheet.Cells[imaxrowcount, 4].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FCB133"));



                                //worksheet.Cells[imaxrowcount, 3].Value = sCatNA;
                                //worksheet.Cells[imaxrowcount, 4].Value = sCatMaxNA;

                                worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
                                worksheet.Cells[imaxrowcount, 3].Style.Font.Bold = true;
                                worksheet.Cells[imaxrowcount, 4].Style.Font.Bold = true;
                                worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                                imaxrowcount++;
                            }
                            float iScoredAvg = 0;
                            iScoredAvg = ((float)(iscore * 100 / iweightage));
                            fEnteirAvg += iScoredAvg;

                            //worksheet.Cells[imaxrowcount, 1].Value = auditname;
                            //worksheet.Cells[imaxrowcount, 3].Value = iscore;
                            //worksheet.Cells[imaxrowcount, 4].Value = iAuditMax;

                            //worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
                            //worksheet.Cells[imaxrowcount, 3].Style.Font.Bold = true;
                            //worksheet.Cells[imaxrowcount, 4].Style.Font.Bold = true;
                            //worksheet.Cells[imaxrowcount, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            //worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                            //worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                            //imaxrowcount++;

                            //worksheet.Cells[imaxrowcount, 1].Value = "Percentage (%)";
                            //worksheet.Cells[imaxrowcount, 3].Value = string.Empty;
                            //worksheet.Cells[imaxrowcount, 4].Value = iScoredAvg + "%";

                            //worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
                            //worksheet.Cells[imaxrowcount, 4].Style.Font.Bold = true;
                            //worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            //worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                            //imaxrowcount++;
                        }
                        float tAvg = fEnteirAvg / (dsRawdatas.Tables[0].Rows.Count);

                        worksheet.Cells[imaxrowcount, 1].Value = "GRAND TOTAL SCORE";
                        worksheet.Cells[imaxrowcount, 3].Value = iMaxScore;
                        worksheet.Cells[imaxrowcount, 4].Value = iTotalScore;
                        worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
                        worksheet.Cells[imaxrowcount, 3].Style.Font.Bold = true;
                        worksheet.Cells[imaxrowcount, 4].Style.Font.Bold = true;
                        worksheet.Cells[imaxrowcount, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Cells[imaxrowcount, 3].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        worksheet.Cells[imaxrowcount, 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        worksheet.Cells[imaxrowcount, 3].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FCB133"));
                        worksheet.Cells[imaxrowcount, 4].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FCB133"));
                        worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                        imaxrowcount++;

                        decimal scoreGot = Convert.ToDecimal(Getscored);


                        worksheet.Cells[imaxrowcount, 1].Value = "Total Score";
                        worksheet.Cells[imaxrowcount, 3].Value = string.Empty;
                        worksheet.Cells[imaxrowcount, 4].Value = Getscored;
                        worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
                        worksheet.Cells[imaxrowcount, 4].Style.Font.Bold = true;
                        //string setColour = string.Empty;
                        //if (scoreGot >= 83)
                        //{
                        //    worksheet.Cells[imaxrowcount, 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        //    worksheet.Cells[imaxrowcount, 4].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#00FF00"));
                        //}
                        //else
                        //{
                        //    worksheet.Cells[imaxrowcount, 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        //    worksheet.Cells[imaxrowcount, 4].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFBE00"));
                        //}
                        worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        imaxrowcount++;


                        worksheet.Cells[imaxrowcount, 1].Value = "Percentage (%)";
                        worksheet.Cells[imaxrowcount, 3].Value = string.Empty;
                        worksheet.Cells[imaxrowcount, 4].Value = ((decimal)(scoreGot * 100) / 5) + "%";
                        worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
                        worksheet.Cells[imaxrowcount, 4].Style.Font.Bold = true;
                        worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        imaxrowcount++;



                        string observations = drHeader["observation"].ToString();
                        string clientremarks = drHeader["feedback"].ToString();


                        worksheet.Cells[imaxrowcount, 1].Value = "Additional Feedback by Client";
                        worksheet.Cells[imaxrowcount, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
                        worksheet.Cells["A" + imaxrowcount + ":E" + imaxrowcount + ""].Merge = true;
                        imaxrowcount++;


                        worksheet.Cells[imaxrowcount, 1].Value = clientremarks;
                        worksheet.Cells["A" + imaxrowcount + ":E" + imaxrowcount + ""].Merge = true;
                        imaxrowcount++;


                        worksheet.Cells[1, 1, imaxrowcount - 1, 5].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        worksheet.Cells[1, 1, imaxrowcount - 1, 5].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        worksheet.Cells[1, 1, imaxrowcount - 1, 5].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        worksheet.Cells[1, 1, imaxrowcount - 1, 5].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                        // worksheet.Cells.AutoFitColumns(0);  //Autofit columns for all cells
                        // worksheet.Cells.AutoFitColumns(1);
                        //// worksheet.Cells.AutoFitColumns(2);

                        //worksheet.Column(2).Width = 140;
                        //worksheet.Column(5).Width = 140;

                        worksheet.Column(1).Width = 5;
                        worksheet.Column(2).Width = 65;
                        worksheet.Column(3).Width = 15;
                        worksheet.Column(4).Width = 15;
                        worksheet.Column(5).Width = 65;

                        //worksheet.Cells.AutoFitColumns(2);
                        //worksheet.Cells.AutoFitColumns(5);

                        //worksheet.Cells.AutoFitColumns(1);
                        //worksheet.Cells.AutoFitColumns(2);
                        //worksheet.Cells.AutoFitColumns(3);
                        //worksheet.Cells.AutoFitColumns(4);
                        //worksheet.Cells.AutoFitColumns(5);
                        //worksheet.Cells.AutoFitColumns(6);
                        //worksheet.Cells.AutoFitColumns(7);

                        int c1;

                        imaxrowcount = 7;
                        ExcelWorksheet worksheetChart = package.Workbook.Worksheets.Add("CategoryChart");

                        string Alphabetic = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                        int startchar = 0;
                        int colspace = 0;
                        foreach (DataRow drAuditRow in dtAudit.Rows)
                        {
                            int auditid = Convert.ToInt32(drAuditRow["auditid"].ToString());
                            string auditname = drAuditRow["auditname"].ToString();
                            DataRow[] drAuditCategories = dtChartdet.Select("auditid=" + auditid);
                            if (drAuditCategories.Length > 0)
                            {
                                DataTable dtCategoryChart = drAuditCategories.CopyToDataTable();
                                int startvalue = imaxrowcount;

                                char Firstcol = Alphabetic[startchar];
                                char Secondcol = Alphabetic[startchar + 1];

                                foreach (DataRow drchartdet in dtCategoryChart.Rows)
                                {
                                    string categoryname = drchartdet["categoryname"].ToString();
                                    decimal score = Convert.ToDecimal(drchartdet["Value"].ToString());

                                    worksheetChart.Cells["" + Firstcol + imaxrowcount].Value = categoryname;
                                    worksheetChart.Cells["" + Secondcol + imaxrowcount].Value = score;
                                    imaxrowcount++;
                                }

                                var chart = worksheetChart.Drawings.AddChart("chart" + imaxrowcount, eChartType.ColumnClustered3D);
                                var series = chart.Series.Add("" + Secondcol + startvalue + ":" + Secondcol + imaxrowcount, "" + Firstcol + startvalue + ":" + Firstcol + imaxrowcount);
                                //series.HeaderAddress = new ExcelAddress("'Sheet1'!B" + (startvalue - 1));
                                chart.SetSize(510, 300);
                                chart.Title.Text = auditname;
                                chart.SetPosition(7, 0, colspace, 0);
                                colspace += 9;

                                startchar += 2;
                            }
                        }


                        package.Save();

                        Response.ContentType = ContentType;
                        Response.AppendHeader("Content-Disposition", "attachment; filename=" + Path.GetFileName(excelfilepath));
                        Response.WriteFile(excelfilepath);
                        Response.End();

                        //this.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        //this.Response.AddHeader(
                        //          "content-disposition",
                        //          string.Format("attachment;  filename={0}", "ExcellData.xlsx"));
                        //this.Response.BinaryWrite(package.GetAsByteArray());
                    }

                }

                //FileInfo prevFile = new FileInfo(excelfilepath);
                //if (prevFile.Exists)
                //{
                //    prevFile.Delete();
                //}
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, "error");
            }
        }



        protected void btnsearch_Click(object sender, EventArgs e)
        {
            try
            {
                BindOverall();
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, "error");
            }
        }
    }
}