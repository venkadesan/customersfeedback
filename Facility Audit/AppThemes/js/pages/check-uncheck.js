  $(document).ready(function () {
        $('input:checkbox').click(function () {
        var $this = $(this);
        if ($this.attr('id') != 'checkall') {
            $(".select-all").attr('checked', false);
        }
        else {
            //Select All
            var $checked = $this.is(':checked');
            $('input:checkbox').each(function () {
                $(this).attr('checked', $checked);
            })
            $(".select-all").attr('checked', $checked);
        }
    })
});