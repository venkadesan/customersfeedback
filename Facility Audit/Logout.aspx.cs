﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FacilityAudit.Code;
using System.Configuration;


namespace Facility_Audit
{
    public partial class Logout : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!UserSession.FromFramework)
            {
                Session.Abandon();
                Session.Clear();
                bool isclear = UserSession.ClearSession;
                Response.Redirect("Login.aspx");
            }
            else
            {
                bool isclear = UserSession.ClearSession;
                Response.Redirect("http://" + Request.Url.Authority.ToLower() + "/", true);
            }
        }
    }
}