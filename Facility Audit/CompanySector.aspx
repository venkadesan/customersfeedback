﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Mainpage.Master" AutoEventWireup="true" CodeBehind="CompanySector.aspx.cs" Inherits="Facility_Audit.CompanySector" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="submenu">
            <h1>Company Sector</h1>
                  
          </div>
<div class="main">
             <div class=" col-md-5 col-xs-offset-4 alert alert-big alert-danger alert-dismissable fade in margin-top-10"
        runat="server" id="divmsg" visible="false"> 
         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <asp:Label runat="server" ID="lblError"></asp:Label></div>
              
<div class="row">
<div class="col-md-12">
<section class="tile cornered">

 

                <!-- row -->
               <div class="tile-body nobottompadding">
                    <div class="row form-horizontal">
                   <div class="col-md-10">
                    
                    <form id="basicvalidations" parsley-validate="" role="form" class="form-horizontal">
                     
                     
                      
                      <div class="form-group">
                        <label class="col-sm-3 control-label" for="">Company Sector <span class="red-text">*</span> : </label>
                        <div class="col-sm-4">

                           <asp:DropDownList ID="ddlsector" runat="server"  AutoPostBack="True"  
                                parsley-validation-minlength="1" parsley-type="email" parsley-minlength="4" 
                                parsley-required="true" parsley-trigger="change" 
                                class="form-control parsley-validated"  
                                kl_virtual_keyboard_secure_input="on" 
                                onselectedindexchanged="ddlsector_SelectedIndexChanged" ></asp:DropDownList>
                              
                            
                        
                          
                        </div>
                      </div>
                   
                     
       
                    </form>

                  </div>
                  <!-- /tile body -->

                
              </div>
              <!--col-6-->
         
              
              </div>
             
              <!-- end row -->
              
              <!--col-md-12-->
         
                <%-- </form>--%>
                 
                </section>
                </div>
           </div>

           <!-- end col-md-12 -->
         
           
 <!--row-->
<div class="row">
<div class="col-md-12">
<div class="tile-body nopadding margin-top-10">
                    
                    <div class="table-responsive" id="divrep" runat="server">
                      <asp:Repeater ID="rptrsector"  DataSourceID="" runat="server"                          
                            onitemdatabound="rptrsector_ItemDataBound">
                           
                             
                    <HeaderTemplate>
                     <table width="100%" border="0" cellspacing="0" class="table table-bordered" cellpadding="0">
                      <tbody>
                                        <tr>
                                        <th width="5%">Sl.No</th>
                          <th>Company</th>
                                                   
                          <th>
                          <asp:CheckBox ID="chkDeleteAll" runat="server"   ClientIDMode="Static" />
                          <asp:Label Text="Location" runat="server" ID="Label5"></asp:Label>
                          &nbsp;&nbsp;
                                                        </th>
                                           
                                        </tr>                    
                    </HeaderTemplate>
                    <ItemTemplate>
                     <tr id="rid">
                                   
                                    <td>
                                        <asp:Label ID="Label2" runat="server" Text='<%#(((RepeaterItem)Container).ItemIndex+1).ToString() %>' />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblcmpname" runat="server" Text='<%# Eval("CompanyName") %>' />
                                    </td>
                                    <td>
                                                <asp:HiddenField ID="hidcompanyid" runat="server" Value='<%# Eval("companyid") %>' />
                                                <asp:Repeater runat="server" ID="rptrLocation" DataSourceID="">
                                                    <ItemTemplate>
                                                        <asp:CheckBox runat="server" ID="chkLocation" CssClass="chkLocations" />
                                                        <asp:Label runat="server" ID="lblLocationName" Text='<%# Eval("LocationName") %>'></asp:Label><br />
                                                        <asp:HiddenField runat="server" ID="hLocationID" Value='<%# Eval("LocationID")%>' />
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </td>
                                   
                                   
                                </tr>
                    
                    </ItemTemplate>
                      <FooterTemplate>
                                </</table>
                            </FooterTemplate>
                        </asp:Repeater>
                      
                    </div>

                  </div>

           		 <div class="tile-footer color cyan text-muted">
                    <div class="row">
                    <div class="col-md-5">
                    <div class="col-md-offset-5">
                        <asp:Button ID="btnsave" class="btn btn-primary" runat="server" Text="Save" 
                                onclick="btnsave_Click" />
                              <asp:Button ID="btncncl" class="btn btn-default" runat="server" Text="Cancel" 
                                onclick="btncncl_Click" />
                         
                         
                         
                        </div>
                      </div>
                      </div>
                      </div>
</div>

 
</div>
<!--end row-->  
      
</div>







</asp:Content>

