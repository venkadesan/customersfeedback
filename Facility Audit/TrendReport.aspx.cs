﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FacilityAudit.Code;
using KB = Kowni.BusinessLogic;
using KCB = Kowni.Common.BusinessLogic;
using System.Data;
using System.Configuration;
using System.Text;
using FacilityAudit.BLL;
using BL;
using System.Web.UI.HtmlControls;
using System.IO;
using Ionic.Zip;
using System.Threading;
using System.Globalization;
using System.Collections;


namespace Facility_Audit
{
    public partial class TrendReport : System.Web.UI.Page
    {
        #region Properties

        private bool IsAdd
        {
            get
            {
                if (!ID.HasValue)
                    return false;
                else if (ID.Value != 0)
                    return false;
                else
                    return true;
            }
        }


        private int? ID
        {
            get
            {
                if (ViewState["id"] == null)
                    ViewState["id"] = 0;
                return (int?)ViewState["id"];
            }

            set { ViewState["id"] = value; }
        }

        private bool DisableStatus
        {
            get
            {
                if (ViewState["disablestatus"] == null)
                    ViewState["disablestatus"] = 0;
                return (bool)ViewState["disablestatus"];
            }

            set { ViewState["disablestatus"] = value; }
        }

        #endregion

        BLGroupcompany objgrp = new BLGroupcompany();
        BLTransaction objtransaction = new BLTransaction();
        BLGroupcompany objgrpcmp = new BLGroupcompany();
        BLAudit objaudit = new BLAudit();
        KB.BLCompanyMainForms objMainForms = new KB.BLCompanyMainForms();
        KB.BLCompanySubForms objSubForms = new KB.BLCompanySubForms();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

                DateTime dt = Convert.ToDateTime(DateTime.Now.ToString("MM/dd/yyyy"));
                loadsbu();
                loadincharge();
                loadauditorname();
                loadsectorname();
                BindCompany();
                BindLocation();

            }
        }

        private void loadsbu()
        {
            try
            {
                DataSet ds = new DataSet();
                //ds = objgrpcmp.getsbu();
                ds = objgrpcmp.getsbu_Group(UserSession.GroupID);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlsbu.DataSource = ds;
                    ddlsbu.DataTextField = "locationsettingsname";
                    ddlsbu.DataValueField = "locationsettingsid";
                    ddlsbu.DataBind();
                    this.ddlsbu.Items.Insert(0, new ListItem("All", "0"));
                }
            }
            catch
            {

            }
        }

        private void loadincharge()
        {
            try
            {
                int sbu = 0;
                DataSet ds = new DataSet();
                if (ddlsbu.SelectedValue == string.Empty)
                {
                    sbu = 0;
                }
                else
                {
                    sbu = Convert.ToInt32(ddlsbu.SelectedValue);
                }

                //ds = objgrpcmp.getinchargelistbysbu(sbu);
                ds = objgrpcmp.getinchargelistbysbu_Group(sbu, UserSession.GroupID);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlincharge.DataSource = ds;
                    ddlincharge.DataTextField = "aom";
                    ddlincharge.DataValueField = "aom";
                    ddlincharge.DataBind();
                    this.ddlincharge.Items.Insert(0, new ListItem("All", "0"));
                }
            }
            catch
            {

            }
        }

        private void loadauditorname()
        {
            try
            {
                int sbu = 0;
                DataSet ds = new DataSet();
                if (ddlsbu.SelectedValue == string.Empty)
                {
                    sbu = 0;
                }
                else
                {
                    sbu = Convert.ToInt32(ddlsbu.SelectedValue);
                }
                //ds = objgrpcmp.getauditornamebysbu(sbu);
                ds = objgrpcmp.getauditornamebysbu_Group(sbu, UserSession.GroupID);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlauditor.DataSource = ds;
                    ddlauditor.DataTextField = "auditor";
                    ddlauditor.DataValueField = "auditor";
                    ddlauditor.DataBind();
                    this.ddlauditor.Items.Insert(0, new ListItem("All", "0"));
                }
            }
            catch
            {

            }
        }

        private void loadsectorname()
        {
            try
            {
                DataSet ds = new DataSet();
                //ds = objgrpcmp.getsectorbytransaction();
                ds = objgrpcmp.getsectorbytransaction_Group(UserSession.GroupID);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlsector.DataSource = ds;
                    ddlsector.DataTextField = "locationsectorname";
                    ddlsector.DataValueField = "locationsectorid";
                    ddlsector.DataBind();
                    this.ddlsector.Items.Insert(0, new ListItem("All", "0"));

                }

            }
            catch
            {

            }
        }


        private void BindCompany()
        {
            try
            {
                DataSet ds = new DataSet();
                //ds = objgrpcmp.GetTransactionCompany();
                ds = objgrpcmp.GetTransactionCompany_Group(UserSession.GroupID);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlcustomer.DataSource = ds;
                    ddlcustomer.DataTextField = "CompanyName";
                    ddlcustomer.DataValueField = "companyid";
                    ddlcustomer.DataBind();
                    this.ddlcustomer.Items.Insert(0, new ListItem("All", "0"));
                }
            }
            catch (Exception ex)
            {
            }
        }


        private void BindLocation()
        {
            try
            {
                DataSet ds = new DataSet();
                //ds = objgrpcmp.GetTransactionLocation();
                ds = objgrpcmp.GetTransactionLocation_Group(UserSession.GroupID);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddllocation.DataSource = ds;
                    ddllocation.DataTextField = "LocationName";
                    ddllocation.DataValueField = "LocationID";
                    ddllocation.DataBind();
                    this.ddllocation.Items.Insert(0, new ListItem("All", "0"));
                }
            }
            catch (Exception ex)
            {
            }
        }


        protected void btnlink_Click(object sender, EventArgs e)
        {
            loadrprtdetails();
        }

        private void loadrprtdetails()
        {
            try
            {
                string incharge = string.Empty;
                string auditor = string.Empty;

                if (Convert.ToString(ddlincharge.SelectedItem) == "All")
                {
                    incharge = "";

                }
                else
                {
                    incharge = Convert.ToString(ddlincharge.SelectedItem);
                }

                if (Convert.ToString(ddlauditor.SelectedItem) == "All")
                {
                    auditor = "";

                }
                else
                {
                    auditor = Convert.ToString(ddlauditor.SelectedItem);
                }

                if (txtfrmdate.Text != string.Empty && txttodate.Text.Trim() != string.Empty)
                {
                    DateTime fromdate = Convert.ToDateTime(txtfrmdate.Text.Trim());
                    DateTime todate = Convert.ToDateTime(txttodate.Text.Trim());
                    if (todate < fromdate)
                    {
                        NotifyMessages("Data range in Invalid", Common.ErrorType.Error);
                        return;
                    }
                    else
                    {
                        int customerid = 0, locationid = 0;
                        Common.DDVal(ddlcustomer, out customerid);
                        Common.DDVal(ddllocation, out locationid);

                        DataSet ds = new DataSet();
                        //ds = objaudit.gettrendreport1(Convert.ToInt32(ddlregion.SelectedValue), Convert.ToInt32(ddlsbu.SelectedValue), incharge, auditor,
                        //    Convert.ToInt32(ddlsector.SelectedValue), fromdate, todate, customerid, locationid);
                        ds = objaudit.gettrendreport1_Group(Convert.ToInt32(ddlregion.SelectedValue), Convert.ToInt32(ddlsbu.SelectedValue), incharge, auditor,
                                                    Convert.ToInt32(ddlsector.SelectedValue), fromdate, todate, customerid, locationid, UserSession.GroupID);
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            DataTable dt = new DataTable();
                            dt.Columns.Add("Companyname");
                            dt.Columns.Add("locationname");
                            dt.Columns.Add("SBU");
                            dt.Columns.Add("Vertical");
                            dt.Columns.Add("SBU Head");
                            dt.Columns.Add("incharge");
                            dt.Columns.Add("daterange");

                            var names = (from DataRow dRow in ds.Tables[0].Rows
                                         orderby (Int32)dRow["monthid"], (Int32)dRow["yearname"]
                                         select new
                                         {
                                             monthname = dRow["monthname"],
                                             yearname = dRow["yearname"]


                                         }).Distinct();

                            foreach (var val in names)
                            {

                                dt.Columns.Add(Convert.ToString(val.monthname + "-" + val.yearname));
                                //divide++;

                            }
                            dt.Columns.Add("AverageScore", typeof(float));

                            var accountname = (from DataRow dRow in ds.Tables[0].Rows
                                               orderby (string)dRow["companyname"], (string)dRow["locationname"]
                                               select new
                                               {
                                                   companyname = dRow["companyname"],
                                                   locationname = dRow["locationname"],
                                                   monthname = dRow["monthname"],
                                                   yearname = dRow["yearname"],
                                                   sbu = dRow["sbu"],
                                                   vertical = dRow["vertical"],
                                                   SBUHead = dRow["auditor"],
                                                   incharge = dRow["incharge"]



                                               }).Distinct();

                            foreach (var val in accountname)
                            {
                                dt.Rows.Add(Convert.ToString(val.companyname), Convert.ToString(val.locationname), Convert.ToString(val.sbu),
                                     Convert.ToString(val.vertical), Convert.ToString(val.SBUHead), Convert.ToString(val.incharge), Convert.ToString(val.monthname + "-" + val.yearname));

                            }

                            int mRow = 0;
                            foreach (DataRow row in dt.Rows)
                            {
                                string companyname = row["companyname"].ToString();
                                string locationname = row["locationname"].ToString();
                                string daterange = row["daterange"].ToString();
                                decimal totalAmount = 0;
                                int divide = 0;
                                int scol = 0;
                                foreach (DataColumn clm in dt.Columns)
                                {

                                    if (scol > 6)
                                    {
                                        var rows = (from DataRow dRow in ds.Tables[0].Rows
                                                    where (string)dRow["monthname"] + "-" + (Int32)dRow["yearname"] == clm.ColumnName.ToString() &&
                                                    (string)dRow["companyname"] == companyname.ToString() && (string)dRow["locationname"] == locationname.ToString()
                                                    select new
                                                    {
                                                        Amount = Convert.ToDecimal(dRow["total"])
                                                    });

                                        foreach (var val in rows)
                                        {
                                            dt.Rows[mRow][clm.ColumnName] = val.Amount.ToString();
                                            totalAmount += (decimal)val.Amount;
                                            divide += divide;
                                            //divide++;
                                            if (divide == 0)
                                            {
                                                divide = 1;
                                            }
                                            dt.Rows[mRow]["AverageScore"] = Math.Round(Convert.ToDecimal(totalAmount / Convert.ToDecimal(divide)), 2);
                                        }
                                        //if (divide == 0)
                                        //{
                                        //    divide = 1;
                                        //}


                                    }
                                    scol++;


                                }
                                mRow++;
                            }



                            divrep.Visible = true;
                            RemoveDuplicateRows(dt, "locationname");
                            dt.Columns.Remove("daterange");

                            object sumObject;
                            sumObject = dt.Compute("Sum(AverageScore)", "1> 0");

                            // float total = (float) dt.AsEnumerable().Sum(r=>r.Field<float>("AverageScore"));

                            //  var sum = dt.AsEnumerable().Sum(x => x.Field<float>("AverageScore"));
                            decimal avg = Convert.ToDecimal(sumObject) / (Convert.ToDecimal(dt.Rows.Count));
                            lbltotalscore.Text = "Average Score Percentage is " + Convert.ToString(Math.Round(avg, 2));
                            gridreport.DataSource = dt;
                            gridreport.DataBind();



                        }

                    }


                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gridreport.PageIndex = e.NewPageIndex;
            loadrprtdetails(); //bindgridview will get the data source and bind it again
        }

        protected void gridreport_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            // apply custom formatting to the header cells
            //if (e.Row.RowType == DataControlRowType.Header)
            //{
            //    string hex = "#FFAB38";
            //    string hex2 = "#FDFCFA";


            //    foreach (TableCell cell in e.Row.Cells)
            //    {
            //        cell.Style["border-bottom"] = "2px solid #666666";
            //        cell.BackColor = System.Drawing.ColorTranslator.FromHtml(hex);
            //        cell.ForeColor = System.Drawing.ColorTranslator.FromHtml(hex2);

            //    }
            //}
        }
        public DataTable RemoveDuplicateRows(DataTable dTable, string colName)
        {
            Hashtable hTable = new Hashtable();
            ArrayList duplicateList = new ArrayList();
            foreach (DataRow dtRow in dTable.Rows)
            {
                if (hTable.Contains(dtRow["locationname"]))
                    duplicateList.Add(dtRow);
                else
                    hTable.Add(dtRow["locationname"], string.Empty);
            }
            foreach (DataRow dtRow in duplicateList)
                dTable.Rows.Remove(dtRow);
            return dTable;
        }





        protected void btncncl_Click(object sender, EventArgs e)
        {

        }

        const int MAX_PATH = 43;



        public void NotifyMessages(string message, Common.ErrorType et)
        {
            divmsg.Visible = true;
            lblError.Text = message;
            if (et == Common.ErrorType.Error)
            {

            }
            else if (et == Common.ErrorType.Information)
            {

            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            //Response.Clear();
            //Response.Buffer = true;

            //Response.AddHeader("content-disposition", "attachment;filename=FacilityAudit-Report.xls");

            //Response.Charset = "";

            //Response.ContentType = "application/vnd.ms-excel";

            //StringWriter sw = new StringWriter();

            //HtmlTextWriter hw = new HtmlTextWriter(sw);

            //rptrAnswer.RenderControl(hw);

            //Response.Write("<table>");
            //Response.Write(sw.ToString());
            //Response.Write("</table>");

            //Response.Flush();
            //Response.End();
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
               server control at run time. */
        }

        protected void ddlregion_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadrprtdetails();
        }

        protected void ddlsbu_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadrprtdetails();
        }

        protected void ddlincharge_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadrprtdetails();
        }

        protected void ddlauditor_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadrprtdetails();
        }

        protected void ddlsector_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadrprtdetails();

        }

        protected void Button2_Click1(object sender, EventArgs e)
        {
            Response.Clear();
            Response.Buffer = true;
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Charset = "";
            string FileName = "Facility Audit Report.xls";
            StringWriter strwritter = new StringWriter();
            HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName);
            gridreport.GridLines = GridLines.Both;
            gridreport.HeaderStyle.Font.Bold = true;
            gridreport.RenderControl(htmltextwrtter);
            Response.Write(strwritter.ToString());
            Response.End();
        }


    }
}