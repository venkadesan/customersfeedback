﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FacilityAudit.Code;
using KB = Kowni.BusinessLogic;
using KCB = Kowni.Common.BusinessLogic;
using System.Data;
using System.Configuration;
using System.Text;
using FacilityAudit.BLL;
using BL;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Threading;
using System.Globalization;

namespace Facility_Audit
{
    public partial class Transaction : System.Web.UI.Page
    {

        #region Properties
        private bool IsAdd
        {
            get
            {
                if (!ID.HasValue)
                    return false;
                else if (ID.Value != 0)
                    return false;
                else
                    return true;
            }
        }


        private int? ID
        {
            get
            {
                if (ViewState["id"] == null)
                    ViewState["id"] = 0;
                return (int?)ViewState["id"];
            }

            set
            {
                ViewState["id"] = value;
            }
        }

        private bool DisableStatus
        {
            get
            {
                if (ViewState["disablestatus"] == null)
                    ViewState["disablestatus"] = 0;
                return (bool)ViewState["disablestatus"];
            }

            set
            {
                ViewState["disablestatus"] = value;
            }
        }

        #endregion

        BLGroupcompany objgrp = new BLGroupcompany();
        BLGroupcompany objgrpcmp = new BLGroupcompany();
        BLAudit objaudit = new BLAudit();
        BLCategory objcat = new BLCategory();
        BLQuestion objquestion = new BLQuestion();
        BLAuditscore objscore = new BLAuditscore();
        KB.BLCompanyMainForms objMainForms = new KB.BLCompanyMainForms();
        KB.BLCompanySubForms objSubForms = new KB.BLCompanySubForms();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                // var cultureinfo = CultureInfo.CreateSpecificCulture("en-US"); 

                //string format = "MMM d yyyy";
                DateTime dt = Convert.ToDateTime(DateTime.Now.ToString("MM/dd/yyyy"));
                txtauditdate.Text = string.Format("{0:D}", dt);
                loadsbu();
         
                loadcompany();
                loadlocation();
                loadaudit();
                loadchkanswer();
              
                ButtonStatus(Common.ButtonStatus.Cancel);
                
                
                
            }
        }
        private void loadauditlastdate()
        {
            try
            {
                string lastdate = string.Empty;
                DataSet ds = new DataSet();
                ds = objquestion.getlastauditdate(UserSession.Companyid, UserSession.locationid, Convert.ToInt32(ddlaudit.SelectedValue));
                if (ds.Tables[0].Rows.Count > 0)
                {
                    lastdate = ds.Tables[0].Rows[0]["lastdate"].ToString();
                    if (lastdate == "01/01/1900")
                    {
                        txtlastauditdate.Text = "Nil";
                    }
                    else
                    {
                        txtlastauditdate.Text = lastdate;
                    }
                }
                else
                {
                    txtlastauditdate.Text = "Nil";

                }



            }
            catch { }
        }

        int categoryid = 0;
        private void loadsbu()
        {
            try
            {
                DataSet ds = new DataSet();
                ds = objgrpcmp.getsbu();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlsbu.DataSource = ds;
                    ddlsbu.DataTextField = "locationsettingsname";
                    ddlsbu.DataValueField = "locationsettingsid";
                    ddlsbu.DataBind();
                }


            }
            catch { }
        }
        private void loadcompany()
        {
            try
            {
                DataSet ds = new DataSet();
                ds = objgrpcmp.getcompanybysbu(Convert.ToInt32(ddlsbu.SelectedValue));
                
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ds.Tables[0].DefaultView.ToTable(true, "CompanyName");
                    ddlcompany.DataSource = ds;
                    ddlcompany.DataTextField = "CompanyName";
                    ddlcompany.DataValueField = "CompanyID";
                    ddlcompany.DataBind();

                }
                else
                {
                    divmsg.Visible = false;
                }
            }
            catch { }
        }

        private void loadlocation()
        {
            try
            {
                DataSet ds = new DataSet();
               // ds = objgrpcmp.getlocationbycompany(Convert.ToInt32(ddlsbu.SelectedValue));
                ds = objgrpcmp.getlocationbysbu(Convert.ToInt32(ddlsbu.SelectedValue),Convert.ToInt32(ddlcompany.SelectedValue));
                
                if (ds.Tables[0].Rows.Count > 0)
                {
                    
                        ddllocation.DataSource = ds;
                        ddllocation.DataTextField = "LocationName";
                        ddllocation.DataValueField = "LocationID";
                        ddllocation.DataBind();
                        UserSession.sectorid = Convert.ToInt32(ds.Tables[0].Rows[0]["sectorid"].ToString());
                        loadsectorname();   
                    
                }
                else
                {
                    UserSession.sectorid = 0;
                    divmsg.Visible = false;
                }
            }
            catch { }
        }
        private void loadsectorname()
        {
            try
            {
                DataSet ds = new DataSet();
                ds = objgrp.getsectorbyid(Convert.ToInt32(UserSession.sectorid));
                if (ds.Tables[0].Rows.Count > 0)
                {
                    lblsector.Text = ds.Tables[0].Rows[0]["locationsectorname"].ToString();
                }
                else
                {
                    lblsector.Text = "Nil";
                }
            }
            catch { }
        }

        private void loadaudit()
        {
            try
            {
                DataSet ds = new DataSet();
                ds = objaudit.getmaudit(0, 0,Convert.ToInt32(UserSession.sectorid));
                if (ds.Tables[0].Rows.Count > 0)
                {

                    ddlaudit.DataSource = ds;
                    ddlaudit.DataTextField = "auditname";
                    ddlaudit.DataValueField = "auditid";
                    ddlaudit.DataBind();

                }
                else
                {
                    divmsg.Visible = false;
                }
            }
            catch { }
        }

        private void loadchkanswer()
        {
            try
            {
                DataSet ds = new DataSet();
                ds = objscore.getmauditscore(Convert.ToInt32(ddlaudit.SelectedValue),0,0);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlCheckAnswer.DataSource = ds;
                    ddlCheckAnswer.DataTextField = "scorename";
                    ddlCheckAnswer.DataValueField = "scoreid";
                    ddlCheckAnswer.DataBind();
                }

            }
            catch { }
        }

        private void ButtonStatus(Common.ButtonStatus status)
        {
            txtclient.Text = txtauditee.Text = txtauditor.Text = txtclientperson.Text = txtomname.Text = txtsite.Text = txtssano.Text = txtsubname.Text = string.Empty;


            if (status == Common.ButtonStatus.New)
            {
              
              
              
                this.lblError.Text = "";
                this.divmsg.Visible = false;
            }
            else if (status == Common.ButtonStatus.Edit)
            {
               
             
                //this.btnsave.Enabled = true;
                this.lblError.Text = "";
                this.divmsg.Visible = false;
            }
            else if (status == Common.ButtonStatus.Cancel)
            {
              
               
               // this.btnsave.Enabled = false;
                this.ID = 0;
                divrep.Visible = true;
                loadrprtdetails();
                loadauditlastdate();
                this.lblError.Text = "";
                this.divmsg.Visible = false;
            }
            else if (status == Common.ButtonStatus.Save)
            {
               // this.pnlnew.Visible = false;
             
               // this.btnsave.Enabled = false;
                this.ID = 0;
                divrep.Visible = true;
               loadrprtdetails();
            }
        }
        public void NotifyMessages(string message, Common.ErrorType et)
        {
            divmsg.Visible = true;
            lblError.Text = message;
            if (et == Common.ErrorType.Error)
            {

            }
            else if (et == Common.ErrorType.Information)
            {

            }
        }
        protected void bNew_Click(object sender, EventArgs e)
        {
            ButtonStatus(Common.ButtonStatus.New);
        }

        protected void ddlaudit_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadrprtdetails();
            loadauditlastdate();
        }

        private void loadrprtdetails()
        {
            try
            {



                DataSet dcat = new DataSet();
                dcat = objcat.getquestionbyauditid(Convert.ToInt32(ddlaudit.SelectedValue), Convert.ToDateTime(txtauditdate.Text.Trim()),Convert.ToInt32(UserSession.sectorid));
                
                if (dcat.Tables[0].Rows.Count > 0)
                {

                    divrep.Visible = true;
                    rptrAnswer.DataSource = dcat;
                    rptrAnswer.DataBind();
                }
                else
                {
                    divrep.Visible = false;

                    //NotifyMessages("Inspection done", Common.ErrorType.Error);
                }
            }
            catch (Exception)
            {
                divrep.Visible = false;

            }
        }
        protected void rptrAnswer_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                try
                {
                    Repeater rptrAnswer = (Repeater)e.Item.FindControl("rptrAnswer"); 
                    HiddenField qustionID = (HiddenField)e.Item.FindControl("hQuestionID");
                    HiddenField catid = (HiddenField)e.Item.FindControl("hcategoryID");
                    RadioButtonList chkbox = (RadioButtonList)e.Item.FindControl("chkqnswer");
                    HtmlTableRow tr = (HtmlTableRow)e.Item.FindControl("tcat");
                    if (categoryid == Convert.ToInt32(catid.Value))
                    {
                        tr.Visible = false;
                    }
                    else
                    {
                        tr.Visible = true;
                    }

                    
                    foreach (ListItem itm in this.ddlCheckAnswer.Items)
                    {
                        chkbox.Items.Add(new ListItem(itm.Text, itm.Value));
                        
                    
                        
                    }
                    categoryid =Convert.ToInt32(catid.Value);
                }
                catch { }
            }

        }


       
    
    

        private DataTable SetSerialno(DataTable dtTable)
        {
            try
            {
                int icount = 1;
                foreach (DataRow drrow in dtTable.Rows)
                {
                    drrow["sno"] = icount.ToString();
                    icount += 1;
                }
                dtTable.AcceptChanges();
                return dtTable;
            }
            catch (Exception)
            {
                throw;
            }
        }

        protected void btnlink_Click(object sender, EventArgs e)
        {
            DateTime feedbackDate;
            if (!DateTime.TryParse(Common.ConverDateDDMMToMMDD(this.txtauditdate.Text.Trim()), out feedbackDate))
            {
                NotifyMessages("Invalid Date", Common.ErrorType.Error);
                return;
            }

            string filename = string.Empty;
            string Guidfilename = string.Empty;
            string questionno = string.Empty;
            string XmLIS = string.Empty;
            if (this.txtlastauditdate.Text == "Nil")
            {
                txtlastauditdate.Text = "01/01/1900";
            }
            int qno = 0;
            StringBuilder sb = new StringBuilder();
            sb.Append("<root>");
            foreach (RepeaterItem rt in rptrAnswer.Items)
            {

                HiddenField categoryid = (HiddenField)rt.FindControl("hcategoryID");
                HiddenField questionid = (HiddenField)rt.FindControl("hQuestionID");
                RadioButtonList answerid = (RadioButtonList)rt.FindControl("chkqnswer");
                Label question = (Label)rt.FindControl("Label7");
                FileUpload image = (FileUpload)rt.FindControl("fuimageupload");
                TextBox remarks = (TextBox)rt.FindControl("txtfeedback");
                questionno = questionno + ((Label)rt.FindControl("lblNo")).Text + ",";
                qno = Convert.ToInt32(((Label)rt.FindControl("lblNo")).Text);
                if (txtlastauditdate.Text.Trim() == "Nil")
                {
                    txtlastauditdate.Text = "01/01/1900";
                }

                if (answerid.SelectedValue != string.Empty)
                {
                    if (image.HasFile)
                    {
                        string path = Server.MapPath("~/auditimage/");
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }
                        filename = Path.GetFileName(image.FileName);
                        string fileExt = Path.GetExtension(image.FileName).ToLower();
                        string guid = Guid.NewGuid().ToString();
                        image.SaveAs(path + guid + fileExt);
                        Guidfilename = guid + fileExt;
                    }
                    else
                    {
                        Guidfilename = string.Empty;
                    }

                    sb.Append("<files ");
                    sb.Append(" companyid=\"" + Convert.ToInt32(ddlcompany.SelectedValue) + "\"");
                    sb.Append(" locationid=\"" + Convert.ToInt32(ddllocation.SelectedValue) + "\"");
                    sb.Append(" sector=\"" + UserSession.sectorid+ "\"");
                    sb.Append(" sbuid=\"" + Convert.ToInt32(ddlsbu.SelectedValue) + "\"");
                    sb.Append(" auditid=\"" + Convert.ToInt32(ddlaudit.SelectedValue) + "\"");
                    sb.Append(" auditdate=\"" + string.Format("{0:MM/dd/yyyy}", Convert.ToDateTime(txtauditdate.Text)) + "\"");
                    sb.Append(" categoryid=\"" + Convert.ToInt32(categoryid.Value)  + "\"");
                    sb.Append(" questionid=\"" + Convert.ToInt32(questionid.Value) + "\"");
                    sb.Append(" answerid=\"" +  Convert.ToInt32(answerid.SelectedValue) + "\"");
                    sb.Append(" image=\"" + Guidfilename + "\"");
                    sb.Append(" remarks=\"" + remarks.Text.Trim() + "\"");
                    sb.Append(" clientname=\"" + txtclient.Text.Trim() + "\"");
                    sb.Append(" sitename=\"" + txtsite.Text.Trim() + "\"");
                    sb.Append(" ssano=\"" + txtssano.Text.Trim() + "\"");
                    sb.Append(" clientperson=\"" + txtclientperson.Text.Trim() + "\"");
                    sb.Append(" sbu=\"" + txtsubname.Text.Trim() + "\"");
                    sb.Append(" aom=\"" + txtomname.Text.Trim() + "\"");
                    sb.Append(" auditor=\"" + txtauditor.Text.Trim() + "\"");
                    sb.Append(" auditee=\"" + txtauditee.Text.Trim() + "\"");
                    sb.Append(" lastauditdate=\"" +  string.Format("{0:MM/dd/yyyy}", Convert.ToDateTime( txtlastauditdate.Text.Trim())) + "\"");
                    sb.Append(" cuid=\"" + UserSession.UserID + "\"");
                    sb.Append(" cdate=\"" + DateTime.Now.AddHours(Convert.ToInt32(ConfigurationManager.AppSettings["addhour"].ToString())).AddMinutes(Convert.ToInt32(ConfigurationManager.AppSettings["addminute"].ToString()))  + "\"");
                    
                    sb.Append(" />");

                }

                else
                {
                    questionno = Convert.ToString(qno);
                    NotifyMessages("please select the answers for following list of questions " + questionno, Common.ErrorType.Error);
                    return;
                }

               
            }
            sb.Append("</root>");
            XmLIS = sb.ToString();
            DataSet ds = new DataSet();
            if (objquestion.savetransaction(XmLIS) > 0)
            {
                NotifyMessages("Saved Sucessfully", Common.ErrorType.Information);
                ButtonStatus(Common.ButtonStatus.Cancel);

            }
            else
            {
                NotifyMessages("Not saved ", Common.ErrorType.Error);
                return;

            }



        }

        protected void btnback_Click(object sender, EventArgs e)
        {
            ButtonStatus(Common.ButtonStatus.Cancel);
        }

        protected void ddllocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DataSet ds = new DataSet();
                ds = objgrpcmp.getlocationsbubylocationid(Convert.ToInt32(ddllocation.SelectedValue));

                UserSession.sectorid = Convert.ToInt32(ds.Tables[0].Rows[0]["sectorid"].ToString());
                loadsectorname();
            }
            catch { }
            loadaudit();
            loadchkanswer();
            loadrprtdetails();
            loadauditlastdate();
            
                
        }


        protected void ddlsector_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadaudit();
            loadchkanswer();
            ButtonStatus(Common.ButtonStatus.Cancel);
            
        }

        protected void ddlsbu_SelectedIndexChanged(object sender, EventArgs e)
        {
          
          
            loadcompany();
            loadlocation();
            loadaudit();
            loadchkanswer();
            loadrprtdetails();
            loadauditlastdate();
            
           
        }

        protected void ddlcompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadlocation();
            loadaudit();
            loadchkanswer();
            loadrprtdetails();
            loadauditlastdate();
            
        }
           

    }
}