﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Mainpage.Master" AutoEventWireup="true"
    CodeBehind="AuditReport.aspx.cs" Inherits="Facility_Audit.AuditReport" EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .test tr input
        {
            border: 1px solid red;
            margin-right: 10px;
            margin-left: 10px;
            padding-right: 10px;
        }
    </style>
    <script type="text/javascript" src="AppThemes/js/minoral.min.js"></script>
    <script language="javascript" type="text/javascript">

        function expandcollapse(obj, row) {

            var img = document.getElementById('img' + obj);

            if ($('tr.' + obj).css('display') === 'none') {
                $("." + obj).css("display", "");
                img.src = "AppThemes/images/minus.png";
            }
            else {
                $("." + obj).css("display", "none");
                img.src = "AppThemes/images/plus.png";
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <cc1:toolkitscriptmanager id="ToolkitScriptManager" runat="server">
    </cc1:toolkitscriptmanager>
    <div class="main" style="margin-left: 20px;">
        <div class=" col-md-5 col-xs-offset-4 alert alert-big alert-danger alert-dismissable fade in margin-top-10"
            runat="server" id="divmsg" visible="false">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <asp:Label runat="server" ID="lblError"></asp:Label>
        </div>
        
        <div class="row">
            <div class="col-md-12">
                <section class="tile">
                   <section class="tile cornered margin-top-10">
        			 <div class="tile-body nobottompadding">
                        <div class="row form-horizontal">
                          <div class="col-lg-12">
                          <div class="form-group">
                                    <label class="col-sm-2 control-label" for=""> Region:</label>
                                <div class="col-sm-4">
                                    <asp:DropDownList ID="ddlregion" runat="server" 
                                        parsley-validation-minlength="1" parsley-type="email" parsley-minlength="4" 
                                        parsley-required="true" parsley-trigger="change" class="form-control parsley-validated"  
                                        kl_virtual_keyboard_secure_input="on" 
                                        onselectedindexchanged="ddlregion_SelectedIndexChanged" >
                                         <asp:ListItem Text="All" Value="0"></asp:ListItem>
                            <asp:ListItem Text="North" Value="1"></asp:ListItem>
                            <asp:ListItem Text="South" Value="2"></asp:ListItem>
                            <asp:ListItem Text="East" Value="3"></asp:ListItem>
                            <asp:ListItem Text="West" Value="4"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                          
                                    <label class="col-sm-1 control-label" for=""> SBU:</label>
                                <div class="col-sm-4">
                                    <asp:DropDownList ID="ddlsbu" runat="server"  AutoPostBack="True"  
                                        parsley-validation-minlength="1" parsley-type="email" parsley-minlength="4" 
                                        parsley-required="true" parsley-trigger="change" class="form-control parsley-validated"  
                                        kl_virtual_keyboard_secure_input="on" onselectedindexchanged="ddlsbu_SelectedIndexChanged" >
                                    </asp:DropDownList>
                                </div>
                             </div>
                      
                             <div class="form-group">
                                    <label class="col-sm-2 control-label" for=""> Customer:</label>
                                <div class="col-sm-10">
                                    <asp:DropDownList ID="ddlcompany" runat="server"  AutoPostBack="True"  
                                        parsley-validation-minlength="1" parsley-type="email" parsley-minlength="4" 
                                        parsley-required="true" parsley-trigger="change" class="form-control parsley-validated"  
                                        kl_virtual_keyboard_secure_input="on" onselectedindexchanged="ddlcompany_SelectedIndexChanged" >
                                    </asp:DropDownList>
                                </div>
                             </div>

                             <div class="form-group">
                                    <label class="col-sm-2 control-label" for=""> Location:</label>
                                <div class="col-sm-10">
                                    <asp:DropDownList ID="ddllocation" runat="server"  AutoPostBack="True"  
                                        parsley-validation-minlength="1" parsley-type="email" parsley-minlength="4" 
                                        parsley-required="true" parsley-trigger="change" class="form-control parsley-validated"  
                                        kl_virtual_keyboard_secure_input="on" onselectedindexchanged="ddllocation_SelectedIndexChanged" >
                                    </asp:DropDownList>
                                </div>
                             </div>
        
                             <div class="form-group">
                                   <label class="col-sm-2 control-label" for=""> Sector:</label>
                               <div class="col-sm-10">
                                   <asp:label ID="lblsector"  runat="server"></asp:label>
                               </div>
                             </div>
                    
                            
                            
                             <div class="form-group">
                                    <label class="col-sm-2 control-label" for="">From Date : </label>
                               <div class="col-sm-4">
                                    <asp:TextBox ID="txtfrmdate"  runat="server" parsley-validation-minlength="1" parsley-type="email" parsley-minlength="4" parsley-required="true" parsley-trigger="change" class="form-control parsley-validated" kl_virtual_keyboard_secure_input="on" ></asp:TextBox>
                                    <cc1:CalendarExtender ID="calexcompletion" runat="server" TargetControlID="txtfrmdate"
                                         CssClass="ajax__calendar_title div" PopupButtonID="txtcompletiondate" Format="MM/dd/yyyy"></cc1:CalendarExtender>
                               </div>
                                 <label class="col-sm-1 control-label" for="">To Date : </label>
                               <div class="col-sm-4">
                                    <asp:TextBox ID="txttodate"  runat="server" parsley-validation-minlength="1" parsley-type="email" parsley-minlength="4" parsley-required="true" parsley-trigger="change" class="form-control parsley-validated" kl_virtual_keyboard_secure_input="on" ></asp:TextBox>
                                    <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txttodate"
                                         CssClass="ajax__calendar_title div" PopupButtonID="txtcompletiondate2" Format="MM/dd/yyyy"></cc1:CalendarExtender>
                               </div>
                             </div>
                          </div>
                        </div>
                     </div>
                   </section>
                </section>
            </div>
            
            <div class="tile-footer color cyan text-muted">
                <div class="row">
                    <div class="col-md-10">
                        <div class="col-md-offset-5">
                            <asp:Button ID="Button1" runat="server" class="btn btn-primary" Text="Submit" OnClick="btnlink_Click" />
                            <asp:Button ID="btncncl" class="btn btn-default" runat="server" Text="Cancel" OnClick="btncncl_Click" />
                        </div>
                    </div>
                </div>
            </div>
             <asp:Label ID="lbltotalscore" style="font-weight:bold;font-size:13px;"  runat="server"></asp:Label>
            <asp:Button ID="Button2" runat="server" class="btn btn-primary" Style="float: right;
                margin-right: 40px;" Text="Export To Excel" OnClick="Button2_Click" />
            <div class="row">
                <div class="col-md-12" id="divrep" runat="server">
                    <div class="tile-body nopadding margin-top-10">
                        <div class="table-responsive">
                            <asp:Repeater ID="rptrAnswer" runat="server" DataSourceID="" OnItemDataBound="rptrTransaction_ItemDataBound">
                                 <HeaderTemplate>
                                        <table width="100%" border="0" cellspacing="0" class="table table-bordered" cellpadding="0">
                                          <tbody>
                                             <tr>
                                                <th width="5%">Sl.No</th>

                                               <%-- <th>Audit Name</th>--%>
                                               <th>SBU </th>
                                               <th>Customer</th>                                               
                                               <th> Location</th>
                                               <th> Sector</th>
                                               <th> DATE</th>
                                               <th> Score</th>
                                             </tr>                    
                                     </HeaderTemplate>

                                     <ItemTemplate>
                                             <tr id="rid">
                                                   
                                               <td>
                                                   <asp:Label ID="Label2" runat="server" Text='<%#(((RepeaterItem)Container).ItemIndex+1).ToString() %>' />
                                               </td>
                                               
                                            
                                               
                                               <td>
                                                   <asp:Label ID="lblcmpshrtname" runat="server" Text='<%# Eval("sbu") %>' />
                                               </td>
                                               <td>
                                                   <asp:Label ID="Label1" runat="server" Text='<%# Eval("companyname") %>' />
                                               </td>
                                               <td>
                                                   <asp:Label ID="Label3" runat="server" Text='<%# Eval("locationname") %>' />
                                               </td>
                                               <td>
                                                   <asp:Label ID="Label4" runat="server" Text='<%# Eval("sector") %>' />
                                               </td>
                                               <td>
                                                   <asp:Label ID="Label5" runat="server" Text='<%# Eval("auditdate") %>' />
                                               </td>
                                               <td>
                                               <asp:HiddenField ID="hidtotal" runat="server" value='<%# Eval("total") %>' />
                                                   <asp:Label ID="lbltotal" runat="server" Text='<%# Eval("total") %>' />
                                               </td>
                                             </tr>
                                     </ItemTemplate>

                                     <FooterTemplate>
                                       </table>
                                     </FooterTemplate>
                                 </asp:Repeater>
                        </div>
                   
                    </div>
                </div>
                <!-- tile body -->
                <!-- /tile body -->
            </div>
        </div>
    </div>
</asp:Content>
