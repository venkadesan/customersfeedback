﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Mainpage.Master" AutoEventWireup="true"
    CodeBehind="Audit.aspx.cs" Inherits="Facility_Audit.Audit" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <cc1:ToolkitScriptManager ID="ToolkitScriptManager" runat="server">
    </cc1:ToolkitScriptManager>
    <div class="col-md-11">
        <div id="crumbs">
            <ul>
                <li class="active"><a href="#">Audit</a></li>
                <li id="linewbtn" runat="server"><a>
                    <asp:LinkButton ID="bNew" OnClick="bNew_Click" runat="server">New</asp:LinkButton></a></li>
            </ul>
        </div>
    </div>
    <div class="main">
        <div class=" col-md-5 col-xs-offset-4 alert alert-big alert-danger alert-dismissable fade in margin-top-10"
            runat="server" id="divmsg" visible="false">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                ×</button>
            <asp:Label runat="server" ID="lblError"></asp:Label>
        </div>
        <div class="row">
            <div class="col-md-12">
                <section class="tile cornered">
                 <section class="tile cornered margin-top-10">
        			<div class="tile-body nobottompadding">
                       <div class="row form-horizontal">
                           <div class="col-lg-12">
                             <div class="form-group">
                                  <label class="col-sm-2 control-label" for="">Sector :</label>
                               <div class="col-sm-10">
                                  <asp:DropDownList ID="ddlsector" runat="server"  AutoPostBack="True"  
                                    parsley-validation-minlength="1" parsley-type="email" parsley-minlength="4" 
                                    parsley-required="true" parsley-trigger="change" class="form-control parsley-validated chosen-select"  
                                    kl_virtual_keyboard_secure_input="on" onselectedindexchanged="ddlsector_SelectedIndexChanged" >
                                  </asp:DropDownList>
                               </div>
                             </div>
                           </div> 
                       </div>
                    </div>
                 </section>
           <asp:Panel ID="pnlnew" runat="server">
               <div class="tile-body nobottompadding">
                  <div class="row form-horizontal">
                     <div class="col-md-10">
                        <form id="basicvalidations" parsley-validate="" role="form" class="form-horizontal">
                             <div class="form-group">
                                    <label class="col-sm-3 control-label" for="">Feedback name <span class="red-text">*</span> : </label>
                                <div class="col-sm-7">
                                    <asp:TextBox ID="txtaudname"  runat="server" parsley-validation-minlength="1" parsley-type="email" parsley-minlength="4" parsley-required="true" parsley-trigger="change" class="form-control parsley-validated" kl_virtual_keyboard_secure_input="on"></asp:TextBox>
                                </div>
                             </div>
                      
                             <div class="form-group">
                                    <label class="col-sm-3 control-label" for="">Start date : </label>
                                <div class="col-sm-7">
                                    <asp:TextBox ID="txtstrtdate"  runat="server" parsley-validation-minlength="1" parsley-type="email" parsley-minlength="4" parsley-required="true" parsley-trigger="change" class="form-control parsley-validated" kl_virtual_keyboard_secure_input="on" > </asp:TextBox>
                                    <cc1:CalendarExtender ID="calexcompletion" runat="server" TargetControlID="txtstrtdate"
                                         CssClass="ajax__calendar_title div" PopupButtonID="txtcompletiondate" Format="MM/dd/yyyy">
                                    </cc1:CalendarExtender>
                                </div>
                             </div>
                      
                             <div class="form-group">
                                    <label class="col-sm-3 control-label" for="">End date : </label>
                                <div class="col-sm-7">
                                    <asp:TextBox ID="txtenddate"  runat="server" parsley-validation-minlength="1" parsley-type="email" parsley-minlength="4" parsley-required="true" parsley-trigger="change" class="form-control parsley-validated" kl_virtual_keyboard_secure_input="on" > </asp:TextBox>
                                    <cc1:CalendarExtender ID="calexcompletion2" runat="server" TargetControlID="txtenddate"
                                         CssClass="ajax__calendar_title div" PopupButtonID="txtcompletiondate" Format="MM/dd/yyyy">
                                    </cc1:CalendarExtender>
                                </div>
                             </div>

                             <div class="form-group">
                                   <label class="col-sm-3 control-label" for="">Description : </label>
                               <div class="col-sm-9">
                                   <asp:TextBox id="txtdes" Width="100%" TextMode="multiline" Columns="50" Rows="5" runat="server" ></asp:TextBox>
                               </div>
                             </div>
                        </form>
                     </div>
                  </div>
               </div>

           	   <div class="tile-footer color cyan text-muted">
                   <div class="row">
                      <div class="col-md-5">
                         <div class="col-md-offset-5">
                            <asp:Button ID="btnsave" class="btn btn-primary" runat="server" Text="Save" onclick="btnsave_Click" />
                            <asp:Button ID="btncncl" class="btn btn-default" runat="server" Text="Cancel" onclick="btncncl_Click" />
                         </div>
                      </div>
                   </div>
               </div>
            </section>
            </div>
        </div>
        </asp:Panel>
        <!-- end col-md-12 -->
        <!--row-->
        <div class="row">
            <div class="col-md-12">
                <div class="tile-body nopadding margin-top-10">
                    <div class="table-responsive" id="divrep" runat="server">
                        <asp:Repeater ID="rptaudit" DataSourceID="" runat="server" OnItemCommand="rptaudit_ItemCommand"
                            OnItemDataBound="rptaudit_ItemDataBound">
                            <HeaderTemplate>
                                <table width="100%" border="0" cellspacing="0" class="table table-bordered" cellpadding="0">
                                    <tbody>
                                        <tr class="color green">
                                            <th width="5%">
                                                Sl.No
                                            </th>
                                            <th>
                                                Feedback Name
                                            </th>
                                            <th>
                                                Option
                                            </th>
                                        </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr id="rid">
                                    <asp:HiddenField ID="hidjobfamilyid" runat="server" Value='<%# Eval("auditid") %>' />
                                    <td>
                                        <asp:Label ID="Label2" runat="server" Text='<%#(((RepeaterItem)Container).ItemIndex+1).ToString() %>' />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblcmpname" runat="server" Text='<%# Eval("auditname") %>' />
                                    </td>
                                    <td class="actions text-center">
                                        <asp:LinkButton ID="lnkEdit" runat="server" CommandName="Edit" class="btn btn-primary"
                                            Text="Edit" CommandArgument='<%# Eval("auditid") %>'></asp:LinkButton>
                                        <asp:LinkButton ID="lnkDelete" runat="server" class="btn btn-danger" meta:resourcekey="BtnUserDeleteResource1"
                                            OnClientClick="if ( !confirm('Are you sure you want to delete this user?')) return false;"
                                            CommandName="Delete" Text="Delete" CommandArgument='<%# Eval("auditid") %>'></asp:LinkButton>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>
        </div>
        <!--end row-->
    </div>
</asp:Content>
