﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Mainpage.Master" AutoEventWireup="true"
    CodeBehind="TrendReport.aspx.cs" EnableEventValidation="false" Inherits="Facility_Audit.TrendReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .test tr input
        {
            border: 1px solid red;
            margin-right: 10px;
            margin-left: 10px;
            padding-right: 10px;
        }
    </style>
    <script type="text/javascript" src="AppThemes/js/minoral.min.js"></script>
    <script language="javascript" type="text/javascript">

        function expandcollapse(obj, row) {

            var img = document.getElementById('img' + obj);

            if ($('tr.' + obj).css('display') === 'none') {
                $("." + obj).css("display", "");
                img.src = "AppThemes/images/minus.png";
            }
            else {
                $("." + obj).css("display", "none");
                img.src = "AppThemes/images/plus.png";
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <cc1:ToolkitScriptManager ID="ToolkitScriptManager" runat="server">
    </cc1:ToolkitScriptManager>
    <div class="col-md-11">
        <div id="crumbs">
            <ul>
                <li class="active"><a href="#">Trend Report</a></li>
            </ul>
        </div>
    </div>
    <div class="main" style="margin-left: 20px;">
        <div class=" col-md-5 col-xs-offset-4 alert alert-big alert-danger alert-dismissable fade in margin-top-10"
            runat="server" id="divmsg" visible="false">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                ×</button>
            <asp:Label runat="server" ID="lblError"></asp:Label>
        </div>
        <div class="row">
            <div class="col-md-12">
                <section class="tile">
                   <section class="tile cornered margin-top-10">
        			 <div class="tile-body nobottompadding">
                        <div class="row form-horizontal">
                          <div class="col-lg-12">
                          <div class="form-group">
                                    <label class="col-sm-2 control-label" for=""> Region:</label>
                                <div class="col-sm-4">
                                    <asp:DropDownList ID="ddlregion" runat="server" 
                                        parsley-validation-minlength="1" parsley-type="email" parsley-minlength="4" 
                                        parsley-required="true" parsley-trigger="change" class="form-control parsley-validated"  
                                        kl_virtual_keyboard_secure_input="on" 
                                        onselectedindexchanged="ddlregion_SelectedIndexChanged" >
                                         <asp:ListItem Text="All" Value="0"></asp:ListItem>
                            <asp:ListItem Text="North" Value="1"></asp:ListItem>
                            <asp:ListItem Text="South" Value="2"></asp:ListItem>
                            <asp:ListItem Text="East" Value="3"></asp:ListItem>
                            <asp:ListItem Text="West" Value="4"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                          
                                    <label class="col-sm-1 control-label" for=""> SBU:</label>
                                <div class="col-sm-4">
                                    <asp:DropDownList ID="ddlsbu" runat="server"  AutoPostBack="True"  
                                        parsley-validation-minlength="1" parsley-type="email" parsley-minlength="4" 
                                        parsley-required="true" parsley-trigger="change" class="form-control parsley-validated  chosen-select"  
                                        kl_virtual_keyboard_secure_input="on" onselectedindexchanged="ddlsbu_SelectedIndexChanged" >
                                    </asp:DropDownList>
                                </div>
                             </div>

                                  <div class="form-group">
                                    <label class="col-sm-2 control-label" for=""> Customer:</label>
                                <div class="col-sm-4">
                                    <asp:DropDownList ID="ddlcustomer" runat="server" 
                                         class="form-control parsley-validated chosen-select"  >
                                        
                                    </asp:DropDownList>
                                </div>
                          
                                    <label class="col-sm-1 control-label" for=""> Location:</label>
                                <div class="col-sm-4">
                                    <asp:DropDownList ID="ddllocation" runat="server"  class="form-control parsley-validated chosen-select" 
                                         >
                                    </asp:DropDownList>
                                </div>
                             </div>
                      
                             <div class="form-group">
                                    <label class="col-sm-2 control-label" for=""> Incharge:</label>
                                <div class="col-sm-4">
                                    <asp:DropDownList ID="ddlincharge" runat="server" 
                                        parsley-validation-minlength="1" parsley-type="email" parsley-minlength="4" 
                                        parsley-required="true" parsley-trigger="change" class="form-control parsley-validated chosen-select"  
                                        kl_virtual_keyboard_secure_input="on" 
                                        onselectedindexchanged="ddlincharge_SelectedIndexChanged" >
                                        </asp:DropDownList>
                                </div>
                          
                                    <label class="col-sm-1 control-label" for=""> SBU Head:</label>
                                <div class="col-sm-4">
                                    <asp:DropDownList ID="ddlauditor" runat="server"  AutoPostBack="True"  
                                        parsley-validation-minlength="1" parsley-type="email" parsley-minlength="4" 
                                        parsley-required="true" parsley-trigger="change" class="form-control parsley-validated chosen-select"  
                                        kl_virtual_keyboard_secure_input="on" 
                                        onselectedindexchanged="ddlauditor_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </div>
                             </div>
        
                             <div class="form-group">
                                   <label class="col-sm-2 control-label" for=""> Vertical:</label>
                               <div class="col-sm-10">
                                   <asp:DropDownList ID="ddlsector" runat="server"  AutoPostBack="True"  
                                        parsley-validation-minlength="1" parsley-type="email" parsley-minlength="4" 
                                        parsley-required="true" parsley-trigger="change" class="form-control parsley-validated chosen-select"  
                                        kl_virtual_keyboard_secure_input="on" 
                                       onselectedindexchanged="ddlsector_SelectedIndexChanged">
                                    </asp:DropDownList>
                               </div>
                             </div>
                    
                            
                            
                             <div class="form-group">
                                    <label class="col-sm-2 control-label" for="">From Date : </label>
                               <div class="col-sm-4">
                                    <asp:TextBox ID="txtfrmdate"  runat="server" parsley-validation-minlength="1" parsley-type="email" parsley-minlength="4" parsley-required="true" parsley-trigger="change" class="form-control parsley-validated" kl_virtual_keyboard_secure_input="on" ></asp:TextBox>
                                    <cc1:CalendarExtender ID="calexcompletion" runat="server" TargetControlID="txtfrmdate"
                                        PopupButtonID="txtcompletiondate" Format="MM/dd/yyyy"></cc1:CalendarExtender>
                               </div>
                                 <label class="col-sm-1 control-label" for="">To Date : </label>
                               <div class="col-sm-4">
                                    <asp:TextBox ID="txttodate"  runat="server" parsley-validation-minlength="1" parsley-type="email" parsley-minlength="4" parsley-required="true" parsley-trigger="change" class="form-control parsley-validated" kl_virtual_keyboard_secure_input="on" ></asp:TextBox>
                                    <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txttodate"
                                        PopupButtonID="txtcompletiondate2" Format="MM/dd/yyyy"></cc1:CalendarExtender>
                               </div>
                             </div>
                          </div>
                        </div>
                     </div>
                   </section>
                </section>
            </div>
            <div class="tile-footer color cyan text-muted">
                <div class="row">
                    <div class="col-md-10">
                        <div class="col-md-offset-5">
                            <asp:Button ID="Button1" runat="server" class="btn btn-primary" Text="Submit" OnClick="btnlink_Click" />
                            <asp:Button ID="btncncl" class="btn btn-default" runat="server" Text="Cancel" OnClick="btncncl_Click" />
                            <asp:Button ID="Button2" runat="server" class="btn btn-primary" Text="Export to Excel"
                                OnClick="Button2_Click1" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <asp:Label ID="lbltotalscore" Style="font-weight: bold; font-size: 13px;" runat="server"></asp:Label>
                <asp:Panel ID="Panel1" runat="server" Height="100%" Width="100%" ScrollBars="Horizontal">
                    <div class="col-md-12" id="divrep" runat="server">
                        <div class="tile-body nopadding margin-top-10">
                            <div class="table-responsive">
                                <asp:GridView runat="server" class="table table-bordered" AllowPaging="true" AllowSorting="true"
                                    ID="gridreport" AutoGenerateColumns="true" PageSize="100" OnPageIndexChanging="GridView1_PageIndexChanging"
                                    OnRowDataBound="gridreport_RowDataBound">
                                    <HeaderStyle CssClass="color green" />
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <!-- tile body -->
                <!-- /tile body -->
            </div>
        </div>
    </div>
</asp:Content>
