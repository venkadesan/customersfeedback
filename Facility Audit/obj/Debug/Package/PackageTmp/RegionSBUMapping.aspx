﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Mainpage.Master" AutoEventWireup="true"
    CodeBehind="RegionSBUMapping.aspx.cs" Inherits="Facility_Audit.RegionSBUMapping" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="col-md-11">
        <div id="crumbs">
            <ul>
                <li class="active" style="width: 250px;"><a href="#">Region-SBU Mapping</a></li>
            </ul>
        </div>
    </div>
    <div class="main">
        <div class=" col-md-5 col-xs-offset-4 alert alert-big alert-danger alert-dismissable fade in margin-top-10"
            runat="server" id="divmsg" visible="false">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                ×</button>
            <asp:Label runat="server" ID="lblError"></asp:Label>
        </div>
        <div class="row">
            <div class="col-md-12">
                <section class="tile">
                 <section class="tile cornered margin-top-10">
        	    	<div class="tile-body nobottompadding">
                      <div class="row form-horizontal">
                        <div class="col-lg-12">
                            <div class="form-group">
                                   <label class="col-sm-2 control-label" for="">Region</label>
                               <div class="col-sm-10">
                                  

                                   <asp:DropDownList ID="ddlregion" runat="server" 
                                       class="form-control chosen-select" AutoPostBack="True" 
                                       onselectedindexchanged="ddlregion_SelectedIndexChanged" >
                                         
                            <asp:ListItem Text="North" Value="1"></asp:ListItem>
                            <asp:ListItem Text="South" Value="2"></asp:ListItem>
                            <asp:ListItem Text="East" Value="3"></asp:ListItem>
                            <asp:ListItem Text="West" Value="4"></asp:ListItem>
                                    </asp:DropDownList>
                               </div>
                            </div>
                        </div> 
                      
                        <div class="col-lg-12">
                            <div class="form-group">
                                   <label class="col-sm-2 control-label" for="">SBU</label>
                               <div class="col-sm-10">
                                   <asp:ListBox ID="lstsbu" runat="server" CssClass="form-control chosen-select" SelectionMode="Multiple" >
                                   </asp:ListBox>
                               </div>
                            </div>
                        </div> 
                      </div>
                    </div>
                 </section>

                 <div class="tile-footer color cyan text-muted">
                           <div class="row">
                              <div class="col-md-5">
                                 <div class="col-md-offset-5">
                                    <asp:Button ID="btnsave" class="btn btn-primary" runat="server" Text="Save" 
                                         onclick="btnsave_Click"  />
                                   
                                 </div>
                              </div>
                           </div>
                       </div>
                 </section>
            </div>
        </div>
</asp:Content>
