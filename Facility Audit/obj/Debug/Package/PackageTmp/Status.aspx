﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Mainpage.Master" AutoEventWireup="true" CodeBehind="Status.aspx.cs" Inherits="Facility_Audit.Status" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<div class="submenu">
        <h1>
           Status
           </h1>
        <asp:Button ID="bNew" runat="server" Text="New" class="btn btn-primary pull-right margin-top-10"
            OnClick="bNew_Click" />
    </div>
    <div class="main">
        <div class=" col-md-5 col-xs-offset-4 alert alert-big alert-danger alert-dismissable fade in margin-top-10"
            runat="server" id="divmsg" visible="false">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                ×</button>
            <asp:Label runat="server" ID="lblError"></asp:Label></div>
        <div class="row">
            <div class="col-md-12">
                <section class="tile">

   
                    
                     
 
                <!-- row -->
             <div class="tile-body nobottompadding">
                    <div class="row form-horizontal">
                   <div class="col-md-6">
                    
                    <form id="basicvalidations" parsley-validate="" role="form" class="form-horizontal">
                    

                      <div class="form-group">
                        <label class="col-sm-4 control-label" for="">Status Name <span class="red-text">*</span> : </label>
                        <div class="col-sm-8">

                         <asp:TextBox ID="txtstatus"  runat="server" parsley-validation-minlength="1" parsley-type="email" parsley-minlength="4" parsley-required="true" parsley-trigger="change" class="form-control parsley-validated" kl_virtual_keyboard_secure_input="on" > </asp:TextBox>
                        
                          
                        </div>
                      </div>

                       <div class="form-group">
                        <label class="col-sm-4 control-label" for="">Shortname : </label>
                        <div class="col-sm-8">

                         <asp:TextBox ID="txtshrtname"  runat="server" parsley-validation-minlength="1" parsley-type="email" parsley-minlength="4" parsley-required="true" parsley-trigger="change" class="form-control parsley-validated" kl_virtual_keyboard_secure_input="on" > </asp:TextBox>
                        
                          
                        </div>
                      </div>
                     

                     
                     
       
                    </form>

                  </div>
                  <!-- /tile body -->

                
              </div>
              <!--col-6-->
         
              
              </div>
            
              <!-- end row -->
              
              <!--col-md-12-->
       
           	 <div class="tile-footer color cyan text-muted">
                    <div class="row">
                    <div class="col-md-5">
                    <div class="col-md-offset-5">
                        <asp:Button ID="btnsave" class="btn btn-primary" runat="server" Text="Save" 
                                onclick="btnsave_Click" />
                               
                              <asp:Button ID="btncncl" class="btn btn-default" runat="server" Text="Cancel" 
                                onclick="btncncl_Click" />
                                
                               
                         
                         
                        </div>
                      </div>
                <%-- </form>--%>
                 
                
           </div>
           </div>
          
           <!-- end col-md-12 -->
         
           
 <!--row-->
<div class="row">
<div class="col-md-12">
<div class="tile-body nopadding margin-top-10">
                    
                    <div class="table-responsive" id="divrep" runat="server" >
                      <asp:Repeater ID="rptrstatus"  DataSourceID="" runat="server" 
                            onitemcommand="rptrstatus_ItemCommand" 
                            onitemdatabound="rptrstatus_ItemDataBound" >
                          
                            
                             
                    <HeaderTemplate>
                     <table width="100%" border="0" cellspacing="0" class="table table-bordered" cellpadding="0">
                      <tbody>
                                        <tr>
                                        <th width="5%">Sl.No</th>
                                        <th>Group Name</th>
                          <th>Status Name</th>
                     
                          
                          <th> Option</th>
                                           
                                        </tr>                    
                    </HeaderTemplate>
                    <ItemTemplate>
                     <tr id="rid">
                                    <asp:HiddenField ID="hidcompanyid" runat="server" Value='<%# Eval("statusid") %>' />
                                    <td>
                                        <asp:Label ID="Label2" runat="server" Text='<%#(((RepeaterItem)Container).ItemIndex+1).ToString() %>' />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblcmpname" runat="server" Text='<%# Eval("GroupName") %>' />
                                    </td>
                                     <td>
                                        <asp:Label ID="lblcmpshrtname" runat="server" Text='<%# Eval("statusname") %>' />
                                    </td>
                                     
                                     
                                   
                                    <td class="actions text-center">
                                        
                                            <asp:LinkButton ID="lnkEdit" runat="server" CommandName="Edit" class="btn btn-primary" Text="Edit" CommandArgument='<%# Eval("statusid") %>'></asp:LinkButton>
                                     
                                       
                                            <asp:LinkButton ID="lnkDelete" runat="server" class="btn btn-danger" meta:resourcekey="BtnUserDeleteResource1"
OnClientClick="if ( !confirm('Are you sure you want to delete this user?')) return false;"   CommandName="Delete" Text="Delete" CommandArgument='<%# Eval("statusid") %>'></asp:LinkButton>
                                               
                                     
                                    </td>
                                </tr>
                    
                    </ItemTemplate>
                      <FooterTemplate>
                                </</table>
                            </FooterTemplate>
                        </asp:Repeater>
                      
                    </div>

                  </div>

</div>
</div>
<!--end row-->      
 </section>
            </div>
        </div>
    </div>





</asp:Content>
