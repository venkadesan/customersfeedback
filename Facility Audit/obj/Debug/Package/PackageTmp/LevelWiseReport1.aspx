﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Mainpage.Master" AutoEventWireup="true"
    CodeBehind="LevelWiseReport1.aspx.cs" Inherits="Facility_Audit.LevelWiseReport1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="flipcss/css/style.css" rel="stylesheet">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <style>
        .borderless td, .borderless th
        {
            border: none !important;
        }
        
        .padding-top-50
        {
            padding-top: 20px !important;
        }
    </style>
    <script type="text/javascript">
        $(function () {

            // Initialize card flip
            $('.card.hover').hover(function () {
                $(this).addClass('flip');
            }, function () {
                $(this).removeClass('flip');
            });

            // Make card front & back side same

            $('.card').each(function () {
                var front = $('.card .front');
                var back = $('.card .back');
                var frontH = front.height();
                var backH = back.height();
                if (backH > frontH) {
                    front.height(backH - 8);
                }
            });
        });

         
    </script>
    <script>

        function OnOverAllClick() {
            document.getElementById('<%= btnoverall.ClientID%>').click();
        }

        function OnSbuClick(sbuid) {

            document.getElementById('<%= hdnsbuid.ClientID%>').value = sbuid;
            document.getElementById('<%= btnsbuwise.ClientID%>').click();
        }

        function OnlocationClick(locationid) {
            document.getElementById('<%= hdnlocationid.ClientID%>').value = locationid;
            document.getElementById('<%= btnlocwise.ClientID%>').click();
        }


        function OnRegionClick(regionid) {
            document.getElementById('<%= hdnregionid.ClientID%>').value = regionid;
            document.getElementById('<%= btnregionwise.ClientID%>').click();
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="col-md-11">
        <div id="crumbs">
            <ul>
                <li class="active" style="width: 250px;"><a href="#">Management Report</a></li>
            </ul>
        </div>
    </div>
    <div class="main" style="margin-left: 20px;">
        <div class=" col-md-5 col-xs-offset-4 alert alert-big alert-danger alert-dismissable fade in margin-top-10"
            runat="server" id="divmsg" visible="false">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                ×</button>
            <asp:Label runat="server" ID="lblError"></asp:Label>
        </div>
        <div id="wrap">
            <div class="row">
                <asp:HiddenField ID="hdnsbuid" Value="0" runat="server" />
                <asp:HiddenField ID="hdnlocationid" Value="0" runat="server" />
                <asp:HiddenField ID="hdnregionid" Value="0" runat="server" />
                <asp:Button ID="btnoverall" runat="server" Text="Button" OnClick="btnoverall_Click"
                    Style="display: none;" />
                <asp:Button ID="btnsbuwise" runat="server" Text="Button" OnClick="btnsbuwise_Click"
                    Style="display: none;" />
                <asp:Button ID="btnlocwise" runat="server" Text="Button" OnClick="btnlocwise_Click"
                    Style="display: none;" />
                <asp:Button ID="btnregionwise" runat="server" Text="Button" OnClick="btnregionwise_Click"
                    Style="display: none;" />
                <div class="row">
                    <div class="form-box-content">
                        <div class="row border-bottom margin-vertical-15">
                            <div class="col-md-8 col-xs-offset-4">
                                <div id="basicvalidations" class="form-horizontal">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="">
                                        </label>
                                        <div class="col-sm-1">
                                        </div>
                                        <label class="col-sm-1 control-label" for="">
                                            Month</label>
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlmonth" CssClass="form-control chosen-select" runat="server">
                                                <asp:ListItem Text="Jan" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="Feb" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="Mar" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="Apr" Value="4"></asp:ListItem>
                                                <asp:ListItem Text="May" Value="5"></asp:ListItem>
                                                <asp:ListItem Text="Jun" Value="6"></asp:ListItem>
                                                <asp:ListItem Text="Jul" Value="7"></asp:ListItem>
                                                <asp:ListItem Text="Aug" Value="8"></asp:ListItem>
                                                <asp:ListItem Text="Sep" Value="9"></asp:ListItem>
                                                <asp:ListItem Text="Oct" Value="10"></asp:ListItem>
                                                <asp:ListItem Text="Nov" Value="11"></asp:ListItem>
                                                <asp:ListItem Text="Dec" Value="12"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <label class="col-sm-1 control-label margin-left15" for="">
                                            Year</label>
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlyear" CssClass="form-control chosen-select" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                        <asp:Button ID="btnsearch" CssClass="btn btn-primary" runat="server" Text="Go" OnClick="btnsearch_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <h1 style="text-align: center;">
                    <asp:Label ID="loverall" runat="server" Text=""></asp:Label>
                </h1>
                <div class="row">
                    <div class="form-box-content">
                        <div class="row cards">
                            <asp:Repeater ID="rptroverall" DataSourceID="" runat="server" OnItemDataBound="rptrlocationwise_ItemDataBound">
                                <ItemTemplate>
                                    <div class="card-container col-lg-4">
                                    </div>
                                    <div class="card-container col-lg-4 col-md-12 col-sm-12">
                                        <div class="card card-orange hover" onclick="OnOverAllClick()">
                                            <div class="front">
                                                <h1 class="padding-top-50">
                                                    <asp:Label ID="Head" runat="server" Text='<%# Eval("Head") %>' /></h1>
                                                <asp:Image ID="imgicon" runat="server" />
                                            </div>
                                            <div class="back nopadding">
                                                <table class="table  no-bottom-margin">
                                                    <tr>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <p>
                                                                <asp:Label ID="score" runat="server" Text='<%# Eval("Score") %>'></asp:Label>
                                                            </p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
                <h1>
                    <asp:Literal ID="lregionwise" Text="" runat="server"></asp:Literal>
                </h1>
                <div class="row" runat="server" id="regiondiv" visible="false">
                    <div class="form-box-content">
                        <div class="row cards">
                            <table class="table" style="width: 98%">
                                <thead>
                                    <tr>
                                        <th style="text-align: center;">
                                            <h4>
                                                Region
                                            </h4>
                                        </th>
                                        <th style="text-align: center;">
                                            <h4>
                                                SBU
                                            </h4>
                                        </th>
                                    </tr>
                                </thead>
                                <asp:Repeater ID="rptrregion" DataSourceID="" runat="server" OnItemDataBound="rptrlocationwise_ItemDataBound">
                                    <HeaderTemplate>
                                        <tbody>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td style="width: 30%">
                                                <div class="card-container col-lg-9 col-md-12 col-sm-12">
                                                    <div class="card card-green hover">
                                                        <%-- onclick="OnRegionClick('<%# Eval("regionid") %>')"--%>
                                                        <div class="front">
                                                            <h1 class="padding-top-50">
                                                                <asp:Label ID="Head" runat="server" Text='<%# Eval("Region") %>' />
                                                                <asp:HiddenField ID="hdnregionid" Value='<%# Eval("regionid") %>' runat="server" />
                                                            </h1>
                                                            <asp:Image ID="imgicon" runat="server" />
                                                        </div>
                                                        <div class="back nopadding">
                                                            <table class="table  no-bottom-margin">
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <p>
                                                                            <asp:Label ID="score" runat="server" Text='<%# Eval("Score") %>'></asp:Label>
                                                                        </p>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td style="width: 70%">
                                                <asp:Repeater ID="rptrsbuwise" DataSourceID="" runat="server" OnItemDataBound="rptrlocationwise_ItemDataBound">
                                                    <ItemTemplate>
                                                        <div class="card-container col-lg-2 col-md-6 col-sm-12">
                                                            <div class="card card-cyan hover" onclick="OnSbuClick('<%# Eval("sbuid") %>')" style="cursor: pointer;">
                                                                <div class="front">
                                                                    <h1 class="padding-top-50">
                                                                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("sbu") %>' />
                                                                        <asp:HiddenField ID="sbuid" Value='<%# Eval("sbuid") %>' runat="server" />
                                                                    </h1>
                                                                    <asp:Image ID="imgicon" runat="server" />
                                                                </div>
                                                                <div class="back nopadding">
                                                                    <table class="table  no-bottom-margin">
                                                                        <tr>
                                                                            <td>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <p>
                                                                                    <asp:Label ID="score" runat="server" Text='<%# Eval("Score") %>'></asp:Label>
                                                                                </p>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </tbody>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </table>
                        </div>
                    </div>
                </div>
                <h1>
                    <asp:Literal ID="llocationwise" Text="" runat="server"></asp:Literal>
                </h1>
                <div class="row">
                    <div class="form-box-content">
                        <div class="row cards">
                            <asp:Repeater ID="rptrlocationwise" DataSourceID="" runat="server" OnItemDataBound="rptrlocationwise_ItemDataBound">
                                <ItemTemplate>
                                    <div class="card-container col-lg-3 col-md-4 col-sm-12">
                                        <div class="card card-amethyst hover" onclick="OnlocationClick('<%# Eval("LocationID") %>')"
                                            style="cursor: pointer;">
                                            <div class="front">
                                                <h1 class="padding-top-50">
                                                    <asp:Label ID="location" runat="server" Text='<%# Eval("LocationName") %>' />
                                                    <asp:HiddenField ID="LocationID" Value='<%# Eval("LocationID") %>' runat="server" />
                                                </h1>
                                                <asp:Image ID="imgicon" runat="server" />
                                            </div>
                                            <div class="back nopadding">
                                                <table class="table  no-bottom-margin">
                                                    <tr>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <p>
                                                                <asp:Label ID="score" runat="server" Text='<%# Eval("Score") %>'></asp:Label>
                                                            </p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
                <h1>
                    <asp:Literal ID="lrawdata" Text="" runat="server"></asp:Literal>
                </h1>
                <div class="row">
                    <asp:Label ID="lbltotalscore" Style="font-weight: bold; font-size: 13px;" runat="server"></asp:Label>
                    <asp:Panel ID="Panel1" runat="server" Height="100%" Width="100%">
                        <div class="col-md-12" id="div2" runat="server">
                            <div class="tile-body nopadding margin-top-10">
                                <div class="table-responsive">
                                    <asp:Repeater ID="rptrdatewiserpt" DataSourceID="" runat="server" OnItemDataBound="rptrlocationwise_ItemDataBound"
                                        OnItemCommand="rptrdatewiserpt_ItemCommand">
                                        <HeaderTemplate>
                                            <table width="100%" border="0" cellspacing="0" class="table table-bordered" cellpadding="0">
                                                <tbody>
                                                    <tr>
                                                        <th width="5%">
                                                            Sl.No
                                                        </th>
                                                        <th>
                                                            Date
                                                        </th>
                                                        <th>
                                                            Location
                                                        </th>
                                                        <th>
                                                            SBU
                                                        </th>
                                                        <th>
                                                            Vertical
                                                        </th>
                                                        <th>
                                                            Auditor
                                                        </th>
                                                        <th>
                                                            Score
                                                        </th>
                                                        <th>
                                                        </th>
                                                    </tr>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="Label2" runat="server" Text='<%#(((RepeaterItem)Container).ItemIndex+1).ToString() %>' />
                                                </td>
                                                <td>
                                                    <asp:Label ID="date" runat="server" Text='<%# Eval("Auditdate") %>' />
                                                </td>
                                                <td>
                                                    <asp:Label ID="LocationName" runat="server" Text='<%# Eval("LocationName") %>' />
                                                </td>
                                                <td>
                                                    <asp:Label ID="sbuname" runat="server" Text='<%# Eval("sbuname") %>' />
                                                </td>
                                                <td>
                                                    <asp:Label ID="vertical" runat="server" Text='<%# Eval("vertical") %>' />
                                                </td>
                                                <td>
                                                    <asp:Label ID="AuditorName" runat="server" Text='<%# Eval("AuditorName") %>' />
                                                </td>
                                                <td>
                                                    <asp:Label ID="Score" runat="server" Text='<%# Eval("Score") %>' />
                                                    <asp:HiddenField ID="hdnscore" Value='<%# Eval("Score") %>' runat="server" />
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="lnldownload" runat="server" CommandName="Download" class="fa fa-2x fa-download"
                                                        Style="text-decoration: none;"></asp:LinkButton>
                                                </td>
                                                <asp:HiddenField ID="locationid" Value='<%# Eval("locationid") %>' runat="server" />
                                                <asp:HiddenField ID="sbuid" Value='<%# Eval("sbuid") %>' runat="server" />
                                                <asp:HiddenField ID="sectionid" Value='<%# Eval("sectorid") %>' runat="server" />
                                                <asp:HiddenField ID="actualdate" Value='<%# Eval("actualdate") %>' runat="server" />
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </table>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <!-- tile body -->
                    <!-- /tile body -->
                </div>
            </div>
        </div>
    </div>
</asp:Content>
