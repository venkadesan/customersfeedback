function priceCalc(e) {
    var ids = $(this).attr("id");
    var id = ids.substring(ids.lastIndexOf("_") + 1);

    $("[id*=lTotalPrice_" + id + "]").calc(
	    "qty * price * others",
	    {
	        qty: $("input[type=text][id*=tQty_" + id + "]"),
	        price: $("input[type=text][id*=tPrice_" + id + "]"),
	        others: $("input[type=text][id*=tMonths_" + id + "]")
	    },
	    function (s) {
	        return s.toFixed(2);
	    },
	    function ($this) {

	        var sum = $("[id*=lTotalPrice]").sum();
	        $("[id*=lGrandTotalPrice]").text(
			    sum.toFixed(2)
		    );
	    }
    );
}