﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Mainpage.Master" AutoEventWireup="true"
    CodeBehind="AuditQuestion.aspx.cs" Inherits="Facility_Audit.AuditQuestion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="col-md-11">
        <div id="crumbs">
            <ul>
                <li class="active"><a href="#">Feedback Questions</a></li>
                <li id="linewbtn" runat="server"><a>
                    <asp:LinkButton ID="bNew" OnClick="bNew_Click" runat="server">New</asp:LinkButton></a></li>
            </ul>
        </div>
    </div>
    <div class="main">
        <div class=" col-md-5 col-xs-offset-4 alert alert-big alert-danger alert-dismissable fade in margin-top-10"
            runat="server" id="divmsg" visible="false">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                ×</button>
            <asp:Label runat="server" ID="lblError"></asp:Label>
        </div>
        <div class="row">
            <div class="col-md-12">
                <section class="tile">
                <section class="tile cornered margin-top-10">
        			<div class="tile-body nobottompadding">
                        <div class="row form-horizontal">
                           <div class="col-lg-12">
                              <div class="form-group">
                                      <label class="col-sm-2 control-label" for="">Sector : </label>
                                <div class="col-sm-10">
                                      <asp:label ID="lblsector" Visible="false"  runat="server" parsley-validation-minlength="1" parsley-type="email" parsley-minlength="4" parsley-required="true" parsley-trigger="change" class="form-control parsley-validated" kl_virtual_keyboard_secure_input="on" ></asp:label>
                                   <asp:DropDownList ID="ddlsector" runat="server"  AutoPostBack="True"  
                                        parsley-validation-minlength="1" parsley-type="email" parsley-minlength="4" 
                                        parsley-required="true" parsley-trigger="change" class="form-control parsley-validated chosen-select"  
                                        kl_virtual_keyboard_secure_input="on" onselectedindexchanged="ddlsector_SelectedIndexChanged" >
                                   </asp:DropDownList>
                                </div>
                              </div>
                          
                              <div class="form-group">
                                    <label class="col-sm-2 control-label" for="">Feedback :</label>
                                <div class="col-sm-10">
                                    <asp:DropDownList ID="ddlaudit" runat="server"  AutoPostBack="True"  
                                       parsley-validation-minlength="1" parsley-type="email" parsley-minlength="4" 
                                       parsley-required="true" parsley-trigger="change" class="form-control parsley-validated chosen-select"  
                                       kl_virtual_keyboard_secure_input="on" onselectedindexchanged="ddlaudit_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </div>
                              </div>

                              <div class="form-group">
                                    <label class="col-sm-2 control-label" for="">Feedback Category :</label>
                                <div class="col-sm-10">
                                    <asp:DropDownList ID="ddlauditcat" runat="server"  AutoPostBack="True"  
                                       parsley-validation-minlength="1" parsley-type="email" parsley-minlength="4" 
                                       parsley-required="true" parsley-trigger="change" class="form-control parsley-validated chosen-select"  
                                       kl_virtual_keyboard_secure_input="on" onselectedindexchanged="ddlauditcat_SelectedIndexChanged" >
                                    </asp:DropDownList>
                                </div>
                              </div>
                           </div>
                        </div>
                    </div>
                </section>
                   
                    <asp:Panel ID="pnlnew" runat="server">
                       <div class="tile-body nobottompadding">
                          <div class="row form-horizontal">
                             <div class="col-md-12">
                                <div class="form-group">
                                       <label class="col-sm-2 control-label" for="">Question name<span class="red-text">*</span> : </label>
                                   <div class="col-sm-10">
                                       <asp:TextBox ID="txtquestion"  runat="server" parsley-validation-minlength="1" parsley-type="email" parsley-minlength="4" parsley-required="true" parsley-trigger="change" class="form-control parsley-validated" kl_virtual_keyboard_secure_input="on" > </asp:TextBox>
                                   </div>
                                </div>
                     
                                <div class="form-group">
                                       <label class="col-sm-2 control-label" for="">Description : </label>
                                   <div class="col-sm-4">
                                       <asp:TextBox id="txtdes" Width="100%" TextMode="multiline" Columns="50" Rows="5" runat="server"></asp:TextBox>
                                   </div>
                                </div>

                                <div class="form-group">
                                       <label class="col-sm-2 control-label" for="">Rating :</label>
                                   <div class="col-sm-4">
                                       <asp:DropDownList ID="ddlrating" runat="server"  AutoPostBack="True"  
                                          parsley-validation-minlength="1" parsley-type="email" parsley-minlength="4" 
                                          parsley-required="true" parsley-trigger="change" class="form-control parsley-validated"  
                                          kl_virtual_keyboard_secure_input="on" onselectedindexchanged="ddlrating_SelectedIndexChanged" >
                                       </asp:DropDownList>
                                   </div>
                                </div>

                                <div class="form-group">
                                       <label class="col-sm-2 control-label" for="">Weightage :</label>
                                   <div class="col-sm-4">
                                       <asp:DropDownList ID="ddlweightage" runat="server"  AutoPostBack="True"  
                                          parsley-validation-minlength="1" parsley-type="email" parsley-minlength="4" 
                                          parsley-required="true" parsley-trigger="change" class="form-control parsley-validated"  
                                          kl_virtual_keyboard_secure_input="on" onselectedindexchanged="ddlweightage_SelectedIndexChanged" >
                                       </asp:DropDownList>
                                   </div>
                                </div>
                             </div>
                          </div>
                       </div> 
         
           		       <div class="tile-footer color cyan text-muted">
                           <div class="row">
                              <div class="col-md-5">
                                 <div class="col-md-offset-5">
                                    <asp:Button ID="btnsave" class="btn btn-primary" runat="server" Text="Save" onclick="btnsave_Click" />
                                    <asp:Button ID="btncncl" class="btn btn-default" runat="server" Text="Cancel" onclick="btncncl_Click" />
                                 </div>
                              </div>
                           </div>
                       </div>
                    </asp:Panel>
            </section>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="tile-body nopadding margin-top-10">
                        <div class="table-responsive" id="divrep" runat="server">
                            <asp:Repeater ID="rptrquestion" DataSourceID="" runat="server" OnItemCommand="rptrquestion_ItemCommand"
                                OnItemDataBound="rptrquestion_ItemDataBound">
                                <HeaderTemplate>
                                    <table width="100%" border="0" cellspacing="0" class="table table-bordered" cellpadding="0">
                                        <tbody>
                                            <tr class="green color">
                                                <th width="5%">
                                                    Sl.No
                                                </th>
                                                <th>
                                                    Category
                                                </th>
                                                <th>
                                                    Question Name
                                                </th>
                                                <th>
                                                    Rating
                                                </th>
                                                <th>
                                                    Weightage
                                                </th>
                                                <th>
                                                    Option
                                                </th>
                                            </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr id="rid">
                                        <asp:HiddenField ID="hidcompanyid" runat="server" Value='<%# Eval("auditqid") %>' />
                                        <td>
                                            <asp:Label ID="Label2" runat="server" Text='<%#(((RepeaterItem)Container).ItemIndex+1).ToString() %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="Label4" runat="server" Text='<%# Eval("categoryname") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("auditqname") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblcmpshrtname" runat="server" Text='<%# Eval("rating") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="Label5" runat="server" Text='<%# Eval("weightage") %>' />
                                        </td>
                                        <td class="actions text-center">
                                            <asp:LinkButton ID="lnkEdit" runat="server" CommandName="Edit" class="btn btn-primary"
                                                Text="Edit" CommandArgument='<%# Eval("auditqid") %>'></asp:LinkButton>
                                            <asp:LinkButton ID="lnkDelete" runat="server" class="btn btn-danger" meta:resourcekey="BtnUserDeleteResource1"
                                                OnClientClick="if ( !confirm('Are you sure you want to delete this user?')) return false;"
                                                CommandName="Delete" Text="Delete" CommandArgument='<%# Eval("auditqid") %>'></asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
