﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FacilityAudit.BLL;
using System.Data;
using FacilityAudit.Code;
using BL;

namespace Facility_Audit
{
    public partial class AuditQuestion : System.Web.UI.Page
    {
        #region Properties
        private bool IsAdd
        {
            get
            {
                if (!ID.HasValue)
                    return false;
                else if (ID.Value != 0)
                    return false;
                else
                    return true;
            }
        }


        private int? ID
        {
            get
            {
                if (ViewState["id"] == null)
                    ViewState["id"] = 0;
                return (int?)ViewState["id"];
            }

            set
            {
                ViewState["id"] = value;
            }
        }

        private bool DisableStatus
        {
            get
            {
                if (ViewState["disablestatus"] == null)
                    ViewState["disablestatus"] = 0;
                return (bool)ViewState["disablestatus"];
            }

            set
            {
                ViewState["disablestatus"] = value;
            }
        }

        #endregion

        BLAudit objaudit = new BLAudit();
        BLCategory objcat = new BLCategory();
        BLQuestion objquestion = new BLQuestion();
        BLAuditRating objrating = new BLAuditRating();
        BLAuditscore objscore = new BLAuditscore();
        BLGroupcompany objgrpcmp = new BLGroupcompany();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                loadsector();
                loadaudit();
                loadcategory();
                loadrating();
                loadweightage();
                loadrprtdetails();
                ButtonStatus(Common.ButtonStatus.Cancel);
            }
        }

        private void loadsector()
        {
            try
            {
                DataSet ds = new DataSet();
                //ds = objgrp.getsector();
                ds = objgrpcmp.GetSectorwithGroup(UserSession.GroupID);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlsector.DataSource = ds;
                    ddlsector.DataTextField = "locationsectorname";
                    ddlsector.DataValueField = "locationsectorid";
                    ddlsector.DataBind();
                    ListItem lisector = ddlsector.SelectedItem;
                    if (lisector != null)
                    {
                        lblsector.Text = ddlsector.SelectedItem.Text;
                    }

                }
            }
            catch
            {

            }
        }

        private void loadaudit()
        {
            try
            {
                DataSet ds = new DataSet();
                int sectorid = 0;
                Common.DDVal(ddlsector, out sectorid);
                ds = objaudit.getmaudit(0, 0, sectorid);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlaudit.DataSource = ds;
                    ddlaudit.DataTextField = "auditname";
                    ddlaudit.DataValueField = "auditid";
                    ddlaudit.DataBind();
                    divrep.Visible = true;
                }
                else
                {
                    divrep.Visible = false;
                }
            }
            catch
            {

            }
        }

        private void loadcategory()
        {
            try
            {
                DataSet ds = new DataSet();
                int auditid = 0;
                Common.DDVal(ddlaudit, out auditid);
                ds = objcat.getmauditcategory(auditid, 0, 0);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlauditcat.DataSource = ds;
                    ddlauditcat.DataTextField = "categoryname";
                    ddlauditcat.DataValueField = "categoryid";
                    ddlauditcat.DataBind();
                }
            }
            catch
            {

            }
        }

        private void loadrating()
        {
            try
            {
                DataSet ds = new DataSet();
                int auditid = 0;
                Common.DDVal(ddlaudit, out auditid);
                ds = objrating.getmauditrating(auditid, 0, 0);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlrating.DataSource = ds;
                    ddlrating.DataTextField = "rating";
                    ddlrating.DataValueField = "ratingid";
                    ddlrating.DataBind();
                }
            }
            catch
            {

            }
        }

        private void loadweightage()
        {
            try
            {
                DataSet ds = new DataSet();
                int auditid = 0;
                Common.DDVal(ddlaudit, out auditid);
                ds = objrating.getmauditweightage(auditid, 0, 0);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlweightage.DataSource = ds;
                    ddlweightage.DataTextField = "weightage";
                    ddlweightage.DataValueField = "weightageid";
                    ddlweightage.DataBind();
                }
            }
            catch
            {

            }
        }

        protected void bNew_Click(object sender, EventArgs e)
        {
            ButtonStatus(Common.ButtonStatus.New);
        }

        private void loadrprtdetails()
        {
            try
            {
                DataSet ds = new DataSet();

                int categoryid = 0;
                Common.DDVal(ddlauditcat, out categoryid);

                ds = objquestion.getmauditquestion(categoryid, 0, 0);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    divrep.Visible = true;
                    rptrquestion.DataSource = ds;
                    rptrquestion.DataBind();
                }
                else
                {
                    divrep.Visible = false;
                }
            }
            catch
            {

            }
        }

        protected void rptrquestion_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Edit")
                {
                    ButtonStatus(Common.ButtonStatus.Edit);
                    this.ID = Convert.ToInt32(e.CommandArgument);
                    BindToCtrls();
                }
                else if (e.CommandName == "Delete")
                {
                    this.ID = Convert.ToInt32(e.CommandArgument);
                    objquestion.deletemauditquestion(this.ID.Value);

                    NotifyMessages("Deleted successfully", Common.ErrorType.Information);
                    ButtonStatus(Common.ButtonStatus.Cancel);
                }
                loadrprtdetails();
            }
            catch
            {

            }
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.txtquestion.Text == string.Empty)
                {
                    NotifyMessages("Enter Question", Common.ErrorType.Error);
                    return;
                }

                if (this.IsAdd)
                {
                    if (objquestion.saveauditquestion(txtquestion.Text.Trim(), txtdes.Text.Trim(),
                        Convert.ToInt32(ddlauditcat.SelectedValue),
                        Convert.ToInt32(ddlrating.SelectedValue), Convert.ToInt32(ddlweightage.SelectedValue),
                        UserSession.UserID, DateTime.Now) > 0)
                    {
                        NotifyMessages("Saved successfully", Common.ErrorType.Information);
                    }
                    else
                    {
                        NotifyMessages("Question Name already exist", Common.ErrorType.Information);
                        return;
                    }
                }
                else if (this.ID != null && this.ID.Value > 0)
                {
                    if (objquestion.updatemauditquestion(Convert.ToInt32(this.ID.Value), txtquestion.Text.Trim(),
                        txtdes.Text.Trim(), Convert.ToInt32(ddlauditcat.SelectedValue),
                        Convert.ToInt32(ddlrating.SelectedValue), Convert.ToInt32(ddlweightage.SelectedValue),
                        UserSession.UserID, DateTime.Now) > 0)
                    {
                        NotifyMessages("Updated successfully", Common.ErrorType.Information);
                    }
                    else
                    {
                        NotifyMessages("Question Name already exist", Common.ErrorType.Information);
                        return;
                    }
                }

                ButtonStatus(Common.ButtonStatus.Save);
            }
            catch
            {

            }
        }

        protected void btncncl_Click(object sender, EventArgs e)
        {
            ButtonStatus(Common.ButtonStatus.Cancel);
        }

        public void BindToCtrls()
        {
            try
            {
                //loadaudit();
                //loadcategory();
                //loadrating();
                //loadweightage();

                DataSet ds = new DataSet();
                ds = objquestion.getmauditquestion(Convert.ToInt32(ddlauditcat.SelectedValue), this.ID.Value, 1);

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    ddlaudit.SelectedValue = ds.Tables[0].Rows[0]["auditid"].ToString();
                    ddlauditcat.SelectedValue = ds.Tables[0].Rows[0]["categoryid"].ToString();
                    loadrating();                   
                    ddlrating.SelectedValue = ds.Tables[0].Rows[0]["ratingid"].ToString();
                    loadweightage();
                    ddlweightage.SelectedValue = ds.Tables[0].Rows[0]["weightageid"].ToString();
                    this.txtquestion.Text = ds.Tables[0].Rows[0]["auditqname"].ToString();
                    this.txtdes.Text = ds.Tables[0].Rows[0]["qdescription"].ToString();
                }
            }
            catch
            {

            }
        }

        protected void rptrquestion_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

        }

        private void ButtonStatus(Common.ButtonStatus status)
        {
            this.txtdes.Text = txtquestion.Text = string.Empty;
            //ddlaudit.ClearSelection();
            //ddlauditcat.ClearSelection();
            //ddlrating.ClearSelection();
            //ddlweightage.ClearSelection();
            //ddlgroup.ClearSelection();

            if (status == Common.ButtonStatus.New)
            {
                this.pnlnew.Visible = true;
                this.bNew.Enabled = false;
                this.bNew.Visible = false;
                this.btnsave.Enabled = true;
                this.txtdes.Focus();
                this.lblError.Text = "";
                divmsg.Visible = false;
                this.linewbtn.Visible = false;
                loadaudit();
                loadrating();
                loadweightage();
            }
            else if (status == Common.ButtonStatus.Edit)
            {
                this.txtquestion.Focus();
                this.pnlnew.Visible = true;
                this.bNew.Enabled = false;
                this.bNew.Visible = false;
                this.btnsave.Enabled = true;
                this.lblError.Text = "";
                divmsg.Visible = false;
                this.linewbtn.Visible = false;
            }
            else if (status == Common.ButtonStatus.Cancel)
            {
                this.pnlnew.Visible = false;
                this.bNew.Visible = true;
                this.bNew.Enabled = true;
                this.btnsave.Enabled = false;
                this.ID = 0;
                divrep.Visible = true;
                loadrprtdetails();
                this.lblError.Text = "";
                divmsg.Visible = false;
                this.linewbtn.Visible = true;
            }
            else if (status == Common.ButtonStatus.Save)
            {
                this.pnlnew.Visible = false;
                this.bNew.Visible = true;
                this.bNew.Enabled = true;
                this.btnsave.Enabled = false;
                this.ID = 0;
                divrep.Visible = true;
                this.linewbtn.Visible = true;
                loadrprtdetails();
            }
        }

        public void NotifyMessages(string message, Common.ErrorType et)
        {
            divmsg.Visible = true;
            lblError.Text = message;
            if (et == Common.ErrorType.Error)
            {

            }
            else if (et == Common.ErrorType.Information)
            {

            }
        }

        protected void ddlaudit_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadcategory();
            loadrating();
            loadweightage();
            ButtonStatus(Common.ButtonStatus.Cancel);
        }

        protected void ddlauditcat_SelectedIndexChanged(object sender, EventArgs e)
        {
            ButtonStatus(Common.ButtonStatus.Cancel);
            loadrating();
            loadweightage();
        }

        protected void ddlrating_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void ddlweightage_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void ddlsector_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadaudit();
            loadcategory();
            ButtonStatus(Common.ButtonStatus.Cancel);
        }
    }
}