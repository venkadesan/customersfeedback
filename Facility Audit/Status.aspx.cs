﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BL;
using System.Data;
using FacilityAudit.Code;
using FacilityAudit.BLL;

namespace Facility_Audit
{
    public partial class Status : System.Web.UI.Page
    {
        #region Properties
        private bool IsAdd
        {
            get
            {
                if (!ID.HasValue)
                    return false;
                else if (ID.Value != 0)
                    return false;
                else
                    return true;
            }
        }


        private int? ID
        {
            get
            {
                if (ViewState["id"] == null)
                    ViewState["id"] = 0;
                return (int?)ViewState["id"];
            }

            set
            {
                ViewState["id"] = value;
            }
        }

        private bool DisableStatus
        {
            get
            {
                if (ViewState["disablestatus"] == null)
                    ViewState["disablestatus"] = 0;
                return (bool)ViewState["disablestatus"];
            }

            set
            {
                ViewState["disablestatus"] = value;
            }
        }

        #endregion

        BLGroupcompany objgrp = new BLGroupcompany();
        BLStatus objstatus = new BLStatus();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                
                ButtonStatus(Common.ButtonStatus.Cancel);

            }

        }

        private void ButtonStatus(Common.ButtonStatus status)
        {
            this.txtstatus.Text = this.txtshrtname.Text = string.Empty;
            //ddlgroup.ClearSelection();


            if (status == Common.ButtonStatus.New)
            {
                
                this.bNew.Enabled = false;
                this.bNew.Visible = false;
                this.btnsave.Enabled = true;
                this.txtstatus.Focus();
                this.lblError.Text = "";
                this.divmsg.Visible = false;
            }
            else if (status == Common.ButtonStatus.Edit)
            {
                this.txtstatus.Focus();
                
                this.bNew.Enabled = false;
                this.bNew.Visible = false;
                this.btnsave.Enabled = true;
                this.lblError.Text = "";
                this.divmsg.Visible = false;
            }
            else if (status == Common.ButtonStatus.Cancel)
            {
                
                this.bNew.Visible = true;
                this.bNew.Enabled = true;
                this.btnsave.Enabled = false;
                this.ID = 0;
                divrep.Visible = true;
                loadrprtdetails();
                this.lblError.Text = "";
                this.divmsg.Visible = false;
            }
            else if (status == Common.ButtonStatus.Save)
            {
                
                this.bNew.Visible = true;
                this.bNew.Enabled = true;
                this.btnsave.Enabled = false;
                this.ID = 0;
                divrep.Visible = true;
                loadrprtdetails();
            }
        }
        public void NotifyMessages(string message, Common.ErrorType et)
        {
            divmsg.Visible = true;
            lblError.Text = message;
            if (et == Common.ErrorType.Error)
            {

            }
            else if (et == Common.ErrorType.Information)
            {

            }
        }
        protected void bNew_Click(object sender, EventArgs e)
        {
            ButtonStatus(Common.ButtonStatus.New);
        }


        protected void btnsave_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.txtstatus.Text == string.Empty)
                {
                    NotifyMessages("Enter Status name", Common.ErrorType.Error);
                    return;
                }
                if (this.IsAdd)
                {
                    if (objstatus.addstatus( this.txtstatus.Text.Trim(), this.txtshrtname.Text.Trim(),UserSession.GroupID,UserSession.UserID, DateTime.Now) > 0)
                    {
                        NotifyMessages("Saved successfully", Common.ErrorType.Information);
                    }
                    else
                    {
                        NotifyMessages("Status already exist", Common.ErrorType.Information);
                        return;
                    }
                }
                else if (this.ID != null && this.ID.Value > 0)
                {
                    if (objstatus.updatemstatus(Convert.ToInt32(this.ID.Value),UserSession.GroupID, this.txtstatus.Text.Trim(), this.txtshrtname.Text.Trim(), UserSession.UserID, DateTime.Now) > 0)
                    {
                        NotifyMessages("Updated successfully", Common.ErrorType.Information);
                    }
                    else
                    {
                        NotifyMessages("Status already exist", Common.ErrorType.Information);
                        return;
                    }
                }

                ButtonStatus(Common.ButtonStatus.Save);
            }
            catch { }
        }
        
        private void loadrprtdetails()
        {
            try
            {

                DataSet ds = new DataSet();
                ds = objstatus.getmstatus(Convert.ToInt32(UserSession.GroupID), 0, 0);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    rptrstatus.DataSource = ds;
                    rptrstatus.DataBind();
                }
                else
                {
                    divrep.Visible = false;
                }
            }
            catch { }
        }

        protected void rptrstatus_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Edit")
                {
                    ButtonStatus(Common.ButtonStatus.Edit);
                    this.ID = Convert.ToInt32(e.CommandArgument);
                    BindToCtrls();
                }
                else if (e.CommandName == "Delete")
                {
                    this.ID = Convert.ToInt32(e.CommandArgument);
                    objstatus.deletemstatus(this.ID.Value);
                    ButtonStatus(Common.ButtonStatus.Cancel);
                    NotifyMessages("Deleted successfully", Common.ErrorType.Information);
                    loadrprtdetails();

                }
            }
            catch { }


        }
        public void BindToCtrls()
        {

            try
            {
                DataSet ds = new DataSet();
                ds = objstatus.getmstatus(Convert.ToInt32(UserSession.GroupID), this.ID.Value,0);

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {

                    this.txtstatus.Text = ds.Tables[0].Rows[0]["statusname"].ToString();
                    this.txtshrtname.Text = ds.Tables[0].Rows[0]["shortname"].ToString();
                    

                }
            }
            catch { }
        }

        protected void rptrstatus_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

        }

        protected void btncncl_Click(object sender, EventArgs e)
        {
            ButtonStatus(Common.ButtonStatus.Cancel);
        }

        protected void ddlaudit_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadrprtdetails();
            // ButtonStatus(Common.ButtonStatus.Cancel);
        }


        ////Repeater rptrAnswer = (Repeater)e.Item.FindControl("rptrAnswer"); 
        //HtmlTableRow tr = (HtmlTableRow)e.Item.FindControl("tcat");
        //HtmlTableRow totscore = (HtmlTableRow)e.Item.FindControl("totscore");
        //HtmlTableRow totpercentage = (HtmlTableRow)e.Item.FindControl("totpercentage");
        //HiddenField auditid = (HiddenField)e.Item.FindControl("hidauditid");
        //HiddenField categoryid = (HiddenField)e.Item.FindControl("hidcatid");
        //HiddenField questionid = (HiddenField)e.Item.FindControl("hidqid");
        //Label totrating = (Label)e.Item.FindControl("lbltotrating");
        //Label totweightage = (Label)e.Item.FindControl("lbltotweightage");
        //Label score = (Label)e.Item.FindControl("lbltotscore");
        //Label percentage = (Label)e.Item.FindControl("lbltotpercentage");

        //Control HeaderTemplate = rptrAnswer.Controls[0].Controls[0];
        //Label lblHeader = HeaderTemplate.FindControl("lblHeader") as Label;
        //Label client = HeaderTemplate.FindControl("lblclient") as Label;
        //Label site = HeaderTemplate.FindControl("lblsite") as Label;
        //Label ssano = (Label)e.Item.FindControl("lblssano") as Label;
        //Label clientperson = (Label)e.Item.FindControl("lblclientperson") as Label;
        //Label sbu = (Label)e.Item.FindControl("lblsbu") as Label;
        //Label aom = (Label)e.Item.FindControl("lblaom") as Label;
        //Label auditor = (Label)e.Item.FindControl("lblauditor") as Label;
        //Label auditee = (Label)e.Item.FindControl("lblauditee") as Label;
        //Label lastauditdate = (Label)e.Item.FindControl("lbllastauditdate") as Label;
        //Label auditdate = (Label)e.Item.FindControl("lblauditdate") as Label;
        //Label lastauditscore = (Label)e.Item.FindControl("lbllastscore") as Label;
        //Label lblscore = (Label)e.Item.FindControl("lblscore") as Label;

        //     DataSet ds = new DataSet();
        //            ds = objtransaction.gettransactionbycategoryid(Convert.ToInt32(UserSession.Companyid), Convert.ToInt32(ddllocation.SelectedValue), Convert.ToInt32(auditid.Value), Convert.ToDateTime(txtauditdate.Text.Trim()), Convert.ToInt32(categoryid.Value));
        //            if (ds.Tables[0].Rows.Count > 0)
        //            {

                       
                       
        //                if(category == Convert.ToInt32(categoryid.Value))
        //                {
        //                    tr.Visible=false;
        //                }
        //                else
        //                {
        //                    tr.Visible=true;
        //                }
        //                for (int i = 0; i < dp.Tables[0].Rows.Count - 1; i++)
        //                {


        //                    if (questioncount == Convert.ToInt32(dp.Tables[0].Rows[rowcount-1]["questionid"].ToString()))
        //                    {

        //                        totpercentage.Visible = true;
        //                        totscore.Visible = true;
        //                        totrating.Text =Convert.ToString(totalrating);
        //                        totweightage.Text = Convert.ToString(totalweightage);
        //                        score.Text = Convert.ToString(totalscore);
        //                      //  percentage.Text = Convert.ToString(((totalscore / totalrating) * 100));
        //                        percent = ((Convert.ToDouble(totalscore) / Convert.ToDouble(totalrating)) * 100);
        //                        percent = Math.Round(percent, 2);
        //                        percentage.Text = Convert.ToString(percent +" %");

        //                    }
        //                    else
        //                    {
        //                        totpercentage.Visible = false;
        //                        totscore.Visible = false;
        //                    }
        //                    category = Convert.ToInt32(categoryid.Value);
        //                    questioncount = Convert.ToInt32(questionid.Value);


        //                }
        //                client.Text = ds.Tables[0].Rows[0]["clientname"].ToString();
        //                site.Text = ds.Tables[0].Rows[0]["sitename"].ToString();
        //                ssano.Text = ds.Tables[0].Rows[0]["ssano"].ToString();
        //                clientperson.Text = ds.Tables[0].Rows[0]["clientperson"].ToString();
        //                sbu.Text = ds.Tables[0].Rows[0]["sbu"].ToString();
        //                aom.Text = ds.Tables[0].Rows[0]["aom"].ToString();
        //                auditor.Text = ds.Tables[0].Rows[0]["auditor"].ToString();
        //                auditee.Text = ds.Tables[0].Rows[0]["auditee"].ToString();
        //                lastauditdate.Text = ds.Tables[0].Rows[0]["lastauditdate"].ToString();
        //                auditdate.Text = ds.Tables[0].Rows[0]["lastauditdate"].ToString();
        //                lastauditscore.Text = Convert.ToString(totalscore);
        //                lblscore.Text = Convert.ToString(totalscore);

        //            }

    }
}