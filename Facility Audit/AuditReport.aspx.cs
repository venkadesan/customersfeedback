﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FacilityAudit.Code;
using KB = Kowni.BusinessLogic;
using KCB = Kowni.Common.BusinessLogic;
using System.Data;
using System.Configuration;
using System.Text;
using FacilityAudit.BLL;
using BL;
using System.Web.UI.HtmlControls;
using System.IO;
using Ionic.Zip;
using System.Threading;
using System.Globalization;
using System.Collections;

namespace Facility_Audit
{
    public partial class AuditReport : System.Web.UI.Page
    {
        #region Properties

        private bool IsAdd
        {
            get
            {
                if (!ID.HasValue)
                    return false;
                else if (ID.Value != 0)
                    return false;
                else
                    return true;
            }
        }


        private int? ID
        {
            get
            {
                if (ViewState["id"] == null)
                    ViewState["id"] = 0;
                return (int?) ViewState["id"];
            }

            set { ViewState["id"] = value; }
        }

        private bool DisableStatus
        {
            get
            {
                if (ViewState["disablestatus"] == null)
                    ViewState["disablestatus"] = 0;
                return (bool) ViewState["disablestatus"];
            }

            set { ViewState["disablestatus"] = value; }
        }

        #endregion

        BLGroupcompany objgrp = new BLGroupcompany();
        BLTransaction objtransaction = new BLTransaction();
        BLGroupcompany objgrpcmp = new BLGroupcompany();
        BLAudit objaudit = new BLAudit();
        KB.BLCompanyMainForms objMainForms = new KB.BLCompanyMainForms();
        KB.BLCompanySubForms objSubForms = new KB.BLCompanySubForms();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

                DateTime dt = Convert.ToDateTime(DateTime.Now.ToString("MM/dd/yyyy"));
                // txtauditdate.Text = string.Format("{0:D}", dt);
                //txtauditdate.Text = DateTime.Now.AddHours(
                //    Convert.ToInt32(ConfigurationManager.AppSettings["addhour"].ToString()))
                //    .AddMinutes(Convert.ToInt32(ConfigurationManager.AppSettings["addminute"].ToString()))
                //    .ToString("MM/dd/yyyy");
                loadsbu();
                loadcompany();
                loadlocation();
               
            }
        }

        private void loadsbu()
        {
            try
            {
                DataSet ds = new DataSet();
                ds = objgrpcmp.getsbu();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlsbu.DataSource = ds;
                    ddlsbu.DataTextField = "locationsettingsname";
                    ddlsbu.DataValueField = "locationsettingsid";
                    ddlsbu.DataBind();
                    this.ddlsbu.Items.Insert(0, new ListItem("-- All --", "0"));
                }
            }
            catch
            {

            }
        }

        private void loadsectorname()
        {
            try
            {
                DataSet ds = new DataSet();
                ds = objgrp.getsectorbyid(Convert.ToInt32(UserSession.sectorid));
                if (ds.Tables[0].Rows.Count > 0)
                {
                    lblsector.Text = ds.Tables[0].Rows[0]["locationsectorname"].ToString();
                }
                else
                {
                    lblsector.Text = "Nil";
                }
            }
            catch
            {

            }
        }

        private void loadcompany()
        {
            try
            {
                DataSet ds = new DataSet();
                ds = objgrpcmp.getcompanybysbu(Convert.ToInt32(ddlsbu.SelectedValue));

                if (ds.Tables[0].Rows.Count > 0)
                {
                    ds.Tables[0].DefaultView.ToTable(true, "CompanyName");
                    ddlcompany.DataSource = ds;
                    ddlcompany.DataTextField = "CompanyName";
                    ddlcompany.DataValueField = "CompanyID";
                    ddlcompany.DataBind();
                    this.ddlcompany.Items.Insert(0, new ListItem("-- All --", "0"));
                }
                else
                {
                    ddlcompany.DataSource = ds;
                    ddlcompany.DataBind();
                    divmsg.Visible = false;
                }
            }
            catch
            {

            }
        }

        private void loadlocation()
        {
            try
            {
                DataSet ds = new DataSet();
                // ds = objgrpcmp.getlocationbycompany(Convert.ToInt32(ddlsbu.SelectedValue));
                ds = objgrpcmp.getlocationbysbu(Convert.ToInt32(ddlsbu.SelectedValue),
                    Convert.ToInt32(ddlcompany.SelectedValue));

                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddllocation.DataSource = ds;
                    ddllocation.DataTextField = "LocationName";
                    ddllocation.DataValueField = "LocationID";
                    ddllocation.DataBind();
                    this.ddllocation.Items.Insert(0, new ListItem("-- All --", "0"));
                    UserSession.sectorid = Convert.ToInt32(ds.Tables[0].Rows[0]["sectorid"].ToString());
                    loadsectorname();
                }
                else
                {
                    UserSession.sectorid = 0;
                    divmsg.Visible = false;
                }
            }
            catch
            {

            }
        }

    

        protected void btnlink_Click(object sender, EventArgs e)
        {
            loadrprtdetails();
        }

        private void loadrprtdetails()
        {
            try
            {
                if (txtfrmdate.Text != string.Empty && txttodate.Text.Trim() != string.Empty)
                {
                    DataSet dcat = new DataSet();
                    dcat = objtransaction.getallauditscore(0, Convert.ToInt32(ddlsbu.SelectedValue), Convert.ToInt32(ddlcompany.SelectedValue),
                        Convert.ToInt32(ddllocation.SelectedValue), Convert.ToDateTime(txtfrmdate.Text.Trim()), Convert.ToDateTime(txttodate.Text.Trim()));


                    if (dcat.Tables[0].Rows.Count > 0)
                    {
                        var sum = dcat.Tables[0].AsEnumerable().Sum(dr => dr.Field<Decimal>("total"));
                        decimal avg = sum / (Convert.ToDecimal(dcat.Tables[0].Rows.Count));
                            lbltotalscore.Text = "Average Score Percentage is "+ Convert.ToString(avg);
                        //RemoveDuplicateRows(dcat.Tables[0], "questionid");
                        rptrAnswer.DataSource = dcat;
                        rptrAnswer.DataBind();
                        // loaddata();
                    }
                    else
                    {
                        rptrAnswer.DataSource = dcat;
                        rptrAnswer.DataBind();

                        //NotifyMessages("Inspection done", Common.ErrorType.Error);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable RemoveDuplicateRows(DataTable dTable, string colName)
        {
            Hashtable hTable = new Hashtable();
            ArrayList duplicateList = new ArrayList();
            foreach (DataRow dtRow in dTable.Rows)
            {
                if (hTable.Contains(dtRow["questionid"]))
                    duplicateList.Add(dtRow);
                else
                    hTable.Add(dtRow["questionid"], string.Empty);
            }
            foreach (DataRow dtRow in duplicateList)
                dTable.Rows.Remove(dtRow);
            return dTable;
        }

        private void loaddata()
        {
            //try
            //{
            //    DataSet dcat = new DataSet();
            //    dcat = objtransaction.gettransaction(Convert.ToInt32(ddlcompany.SelectedValue),
            //        Convert.ToInt32(ddllocation.SelectedValue), Convert.ToInt32(ddlaudit.SelectedValue),
            //        Convert.ToDateTime(txtauditdate.Text.Trim()));
            //    if (dcat.Tables[0].Rows.Count > 0)
            //    {
            //        RemoveDuplicateRows(dcat.Tables[0], "questionid");
            //        txtclient.Text = Convert.ToString(dcat.Tables[0].Rows[0]["clientname"].ToString());
            //        txtsite.Text = Convert.ToString(dcat.Tables[0].Rows[0]["sitename"].ToString());
            //        txtssano.Text = Convert.ToString(dcat.Tables[0].Rows[0]["ssano"].ToString());
            //        txtclientperson.Text = Convert.ToString(dcat.Tables[0].Rows[0]["clientperson"].ToString());
            //        txtsubname.Text = Convert.ToString(dcat.Tables[0].Rows[0]["sbuname"].ToString());
            //        txtomname.Text = Convert.ToString(dcat.Tables[0].Rows[0]["aom"].ToString());
            //        txtauditor.Text = Convert.ToString(dcat.Tables[0].Rows[0]["auditor"].ToString());
            //        txtauditee.Text = Convert.ToString(dcat.Tables[0].Rows[0]["auditee"].ToString());
            //        if (Convert.ToDateTime(dcat.Tables[0].Rows[0]["lastauditdate"].ToString()) ==
            //            Convert.ToDateTime("01/01/1900"))
            //        {
            //            txtlastauditdate.Text = "Nil";
            //        }
            //        else
            //        {
            //            txtlastauditdate.Text = Convert.ToString(dcat.Tables[0].Rows[0]["lastauditdate"].ToString());
            //        }

            //        txtcurrentname.Text = Convert.ToString(UserSession.UserFirstName);
            //        txtlastauditscore.Text = Convert.ToString(dcat.Tables[0].Rows[0]["score"].ToString());
            //    }
            //    else
            //    {

            //    }
            //}
            //catch
            //{
            //}
        }

        int category = 0;
        int lastrow = 0;
        int questioncount = 0;
        decimal tot = 0;
        protected void rptrTransaction_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            int rowcount = 0;
            int totalrating = 0;
            int cattotrating = 0;
            int totalweightage = 0;
            int cattotweightage = 0;
            int totalscore = 0;
            int cattotalscore = 0;
            double percent = 0.0;
            double catpercent = 0.0;
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                try
                {
                  

                    ////Repeater rptrAnswer = (Repeater)e.Item.FindControl("rptrAnswer"); 
                    //HtmlTableRow tr = (HtmlTableRow) e.Item.FindControl("tcat");
                    ////HtmlTableRow totscore = (HtmlTableRow)e.Item.FindControl("totscore");
                    ////HtmlTableRow totpercentage = (HtmlTableRow)e.Item.FindControl("totpercentage");
                    //HiddenField auditid = (HiddenField) e.Item.FindControl("hauditid");
                    //HiddenField categoryid = (HiddenField) e.Item.FindControl("hcategoryid");
                    //HiddenField questionid = (HiddenField) e.Item.FindControl("hQuestionID");
                    ////Label totrating = (Label)e.Item.FindControl("lblrating");
                    ////Label totweightage = (Label)e.Item.FindControl("lblweightage");
                    ////Label score = (Label)e.Item.FindControl("lblscore");
                    ////Label percentage = (Label)e.Item.FindControl("lbltotpercentage");

                    //Control HeaderTemplate = rptrAnswer.Controls[0].Controls[0];
                    //Label lblHeader = HeaderTemplate.FindControl("Label6") as Label;
                    //Label percentage = HeaderTemplate.FindControl("lbltotpercentage") as Label;
                    //Label lblaudit = HeaderTemplate.FindControl("Label6") as Label;
                    //Label lblcatpercentage = (Label) e.Item.FindControl("lblcatpercantage") as Label;
                    ////Label client = HeaderTemplate.FindControl("lblclient") as Label;
                    ////Label site = HeaderTemplate.FindControl("lblsite") as Label;
                    ////Label ssano = (Label)e.Item.FindControl("lblssano") as Label;
                    ////Label clientperson = (Label)e.Item.FindControl("lblclientperson") as Label;
                    ////Label sbu = (Label)e.Item.FindControl("lblsbu") as Label;
                    ////Label aom = (Label)e.Item.FindControl("lblaom") as Label;
                    ////Label auditor = (Label)e.Item.FindControl("lblauditor") as Label;
                    ////Label auditee = (Label)e.Item.FindControl("lblauditee") as Label;
                    ////Label lastauditdate = (Label)e.Item.FindControl("lbllastauditdate") as Label;
                    ////Label auditdate = (Label)e.Item.FindControl("lblauditdate") as Label;
                    ////Label lastauditscore = (Label)e.Item.FindControl("lbllastscore") as Label;
                    ////Label lblscore = (Label)e.Item.FindControl("lblscore") as Label;

                    //DataSet da = new DataSet();
                    //da = objtransaction.gettotalrating(Convert.ToInt32(auditid.Value),
                    //    Convert.ToInt16(ddlsbu.SelectedValue), Convert.ToInt16(ddlcompany.SelectedValue),
                    //    Convert.ToInt16(ddllocation.SelectedValue), Convert.ToDateTime(this.txtauditdate.Text));
                    //if (da.Tables[0].Rows.Count > 0)
                    //{
                    //    totalrating = Convert.ToInt32(da.Tables[0].Rows[0]["totalrating"].ToString());
                    //}
                    //DataSet dcattotrating = new DataSet();
                    //dcattotrating = objtransaction.getratingsumbycategory(Convert.ToInt32(auditid.Value),
                    //    Convert.ToInt32(categoryid.Value), Convert.ToInt16(ddlsbu.SelectedValue),
                    //    Convert.ToInt16(ddlcompany.SelectedValue), Convert.ToInt16(ddllocation.SelectedValue),
                    //    Convert.ToDateTime(this.txtauditdate.Text));
                    //if (dcattotrating.Tables[0].Rows.Count > 0)
                    //{
                    //    cattotrating = Convert.ToInt32(dcattotrating.Tables[0].Rows[0]["totalrating"].ToString());
                    //}

                    //DataSet dr = new DataSet();
                    //dr = objtransaction.gettotalweightage(Convert.ToInt32(auditid.Value),
                    //    Convert.ToInt16(ddlsbu.SelectedValue), Convert.ToInt16(ddlcompany.SelectedValue),
                    //    Convert.ToInt16(ddllocation.SelectedValue), Convert.ToDateTime(this.txtauditdate.Text));
                    //if (dr.Tables[0].Rows.Count > 0)
                    //{
                    //    totalweightage = Convert.ToInt32(dr.Tables[0].Rows[0]["totalweightage"].ToString());
                    //}

                    //DataSet dcatweightage = new DataSet();
                    //dcatweightage = objtransaction.getweightagesumbycategoryid(Convert.ToInt32(auditid.Value),
                    //    Convert.ToInt32(categoryid.Value), Convert.ToInt16(ddlsbu.SelectedValue),
                    //    Convert.ToInt16(ddlcompany.SelectedValue), Convert.ToInt16(ddllocation.SelectedValue),
                    //    Convert.ToDateTime(this.txtauditdate.Text));
                    //if (dcatweightage.Tables[0].Rows.Count > 0)
                    //{
                    //    cattotweightage = Convert.ToInt32(dcatweightage.Tables[0].Rows[0]["totalweightage"].ToString());
                    //}
                    //DataSet dss = new DataSet();
                    //dss = objtransaction.gettotalscore(Convert.ToInt32(auditid.Value),
                    //    Convert.ToInt16(ddlsbu.SelectedValue), Convert.ToInt16(ddlcompany.SelectedValue),
                    //    Convert.ToInt16(ddllocation.SelectedValue), Convert.ToDateTime(this.txtauditdate.Text));
                    //if (dss.Tables[0].Rows.Count > 0)
                    //{
                    //    totalscore = Convert.ToInt32(dss.Tables[0].Rows[0]["totalscore"].ToString());
                    //}

                    //DataSet dcatscore = new DataSet();
                    //dcatscore = objtransaction.getscoresumbycategory(Convert.ToInt32(auditid.Value),
                    //    Convert.ToInt32(categoryid.Value), Convert.ToInt16(ddlsbu.SelectedValue),
                    //    Convert.ToInt16(ddlcompany.SelectedValue), Convert.ToInt16(ddllocation.SelectedValue),
                    //    Convert.ToDateTime(this.txtauditdate.Text));
                    //if (dcatscore.Tables[0].Rows.Count > 0)
                    //{
                    //    cattotalscore = Convert.ToInt32(dcatscore.Tables[0].Rows[0]["totalscore"].ToString());
                    //}

                    //DataSet dp = new DataSet();
                    //dp = objtransaction.gettransaction(Convert.ToInt32(UserSession.Companyid),
                    //    Convert.ToInt32(ddllocation.SelectedValue), Convert.ToInt32(auditid.Value),
                    //    Convert.ToDateTime(txtauditdate.Text.Trim()));
                    //if (dp.Tables[0].Rows.Count > 0)
                    //{
                    //    rowcount = dp.Tables[0].Rows.Count;
                    //}

                    //if (category == Convert.ToInt32(categoryid.Value))
                    //{
                    //    tr.Visible = false;
                    //    // tr2.Visible = false;
                    //}
                    //else
                    //{
                    //    tr.Visible = true;
                    //    lblaudit.Text = Convert.ToString(ddlaudit.SelectedItem);
                    //    //tdremarks.Visible = true;
                    //    //tr2.Visible = true;
                    //    //totrating.Text = Convert.ToString(totalrating);
                    //    //totweightage.Text = Convert.ToString(totalweightage);
                    //    //score.Text = Convert.ToString(totalscore);
                    //    percent = ((Convert.ToDouble(totalscore)/Convert.ToDouble(totalrating))*100);
                    //    catpercent = ((Convert.ToDouble(cattotalscore)/Convert.ToDouble(cattotrating))*100);

                    //    if (percent != double.NaN)
                    //    {
                    //        percent = Math.Round(percent, 2);

                    //        percentage.Text = Convert.ToString("Overall Percentage is " + percent + " %");
                    //    }
                    //    else
                    //    {
                    //        percentage.Text = Convert.ToString("Overall Percentage is 0 %");
                    //    }
                    //    if (catpercent != double.NaN)
                    //    {
                    //        catpercent = Math.Round(catpercent, 2);

                    //        lblcatpercentage.Text = Convert.ToString("Percentage = " + catpercent + " %");
                    //    }
                    //    else
                    //    {
                    //        lblcatpercentage.Text = Convert.ToString("Percentage = 0 %");
                    //    }
                    //    category = Convert.ToInt32(categoryid.Value);
                    //}

                    //DataSet ds = new DataSet();
                    //ds = objtransaction.gettransactionbycategoryid(Convert.ToInt32(UserSession.Companyid), Convert.ToInt32(ddllocation.SelectedValue), Convert.ToInt32(auditid.Value), Convert.ToDateTime(txtauditdate.Text.Trim()), Convert.ToInt32(categoryid.Value));
                    //if (ds.Tables[0].Rows.Count > 0)
                    //{
                    //    if(category == Convert.ToInt32(categoryid.Value))
                    //    {
                    //        tr.Visible=false;
                    //    }
                    //    else
                    //    {
                    //        tr.Visible=true;
                    //    }
                    //    for (int i = 0; i < dp.Tables[0].Rows.Count - 1; i++)
                    //    {
                    //        if (questioncount == Convert.ToInt32(dp.Tables[0].Rows[rowcount-1]["questionid"].ToString()))
                    //        {
                    //            //totpercentage.Visible = true;
                    //            //totscore.Visible = true;
                    //            //totrating.Text =Convert.ToString(totalrating);
                    //            //totweightage.Text = Convert.ToString(totalweightage);
                    //            //score.Text = Convert.ToString(totalscore);
                    //          //  percentage.Text = Convert.ToString(((totalscore / totalrating) * 100));
                    //            percent = ((Convert.ToDouble(totalscore) / Convert.ToDouble(totalrating)) * 100);
                    //            percent = Math.Round(percent, 2);
                    //            percentage.Text = Convert.ToString(percent +" %");
                    //        }
                    //        else
                    //        {
                    //            //totpercentage.Visible = false;
                    //            //totscore.Visible = false;
                    //        }
                    //        category = Convert.ToInt32(categoryid.Value);
                    //        questioncount = Convert.ToInt32(questionid.Value);
                    //    }
                    //    //client.Text = ds.Tables[0].Rows[0]["clientname"].ToString();
                    //    //site.Text = ds.Tables[0].Rows[0]["sitename"].ToString();
                    //    //ssano.Text = ds.Tables[0].Rows[0]["ssano"].ToString();
                    //    //clientperson.Text = ds.Tables[0].Rows[0]["clientperson"].ToString();
                    //    //sbu.Text = ds.Tables[0].Rows[0]["sbu"].ToString();
                    //    //aom.Text = ds.Tables[0].Rows[0]["aom"].ToString();
                    //    //auditor.Text = ds.Tables[0].Rows[0]["auditor"].ToString();
                    //    //auditee.Text = ds.Tables[0].Rows[0]["auditee"].ToString();
                    //    //lastauditdate.Text = ds.Tables[0].Rows[0]["lastauditdate"].ToString();
                    //    //auditdate.Text = ds.Tables[0].Rows[0]["lastauditdate"].ToString();
                    //    //lastauditscore.Text = Convert.ToString(totalscore);
                    //    //lblscore.Text = Convert.ToString(totalscore);
                    //}
                }
                catch
                {

                }
            }
        }

        protected void ddllocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DataSet ds = new DataSet();
                ds = objgrpcmp.getlocationsbubylocationid(Convert.ToInt32(ddllocation.SelectedValue));

                UserSession.sectorid = Convert.ToInt32(ds.Tables[0].Rows[0]["sectorid"].ToString());
                loadsectorname();
            }
            catch
            {

            }

           // loadaudit();
            loadrprtdetails();
        }

        protected void ddlaudit_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void ddlsbu_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadcompany();
            loadlocation();
            //loadaudit();
        }

        protected void ddlcompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadlocation();
            //loadaudit();
            loadrprtdetails();
        }

        protected void btncncl_Click(object sender, EventArgs e)
        {

        }

        const int MAX_PATH = 43;

        protected void imguplodedfile_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ImageButton imguplodedfile = (ImageButton) sender;
                string upfilename = string.Empty;
                RepeaterItem repeaterItem = (RepeaterItem) imguplodedfile.NamingContainer;

                foreach (RepeaterItem rt in rptrAnswer.Items)
                {
                    HiddenField hdnuploadfileGUID = (HiddenField) rt.FindControl("hguid");
                    if (hdnuploadfileGUID.Value != string.Empty)
                    {
                        upfilename = upfilename + hdnuploadfileGUID.Value + "#";
                    }
                }

                if (upfilename != string.Empty)
                {
                    string files = upfilename.TrimEnd('#', ' ');

                    string[] file = files.Split('#');

                    using (ZipFile zip = new ZipFile())
                    {
                        zip.AlternateEncodingUsage = ZipOption.AsNecessary;
                        zip.AddDirectoryByName("Files");

                        foreach (string filename in file)
                        {
                            //string path = "ifazility.com/date/auditimages/";
                            string filePath = Server.MapPath("~/auditimage/") + filename;
                            // string filePath = ConfigurationManager.AppSettings["Auditimages"] + filename;
                            //if (File.Exists(filePath))
                            //{
                            if (filePath.Length >= MAX_PATH)
                            {
                                zip.AddFile(filePath, "Files");
                            }
                        }

                        Response.Clear();
                        Response.BufferOutput = false;
                        string zipName = String.Format("Zip_{0}.zip", DateTime.Now.ToString("yyyy-MMM-dd-HHmmss"));
                        Response.ContentType = "application/zip";
                        Response.AddHeader("content-disposition", "attachment; filename=" + zipName);
                        zip.Save(Response.OutputStream);
                        Response.End();
                    }
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message.ToString(), Common.ErrorType.Information);
            }
        }

        public void NotifyMessages(string message, Common.ErrorType et)
        {
            divmsg.Visible = true;
            lblError.Text = message;
            if (et == Common.ErrorType.Error)
            {

            }
            else if (et == Common.ErrorType.Information)
            {

            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Response.Clear();
            Response.Buffer = true;

            Response.AddHeader("content-disposition", "attachment;filename=FacilityAudit-Report.xls");

            Response.Charset = "";

            Response.ContentType = "application/vnd.ms-excel";

            StringWriter sw = new StringWriter();

            HtmlTextWriter hw = new HtmlTextWriter(sw);

            rptrAnswer.RenderControl(hw);

            Response.Write("<table>");
            Response.Write(sw.ToString());
            Response.Write("</table>");

            Response.Flush();
            Response.End();
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
               server control at run time. */
        }

        protected void ddlregion_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}