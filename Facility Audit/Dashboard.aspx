﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Mainpage.Master" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="Facility_Audit.Dashboard" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
  <style type="text/css">
        .modalBackground
        {
            background-color: Gray;
            filter: alpha(opacity=80);
            opacity: 0.8;
            z-index: 10000;
        }
    </style>
    <%-- <link rel="icon" href="images/favicon.ico" type="image/x-icon">--%>
    <!-- Bootstrap -->
   
    <link rel="stylesheet" href="AppThemes/js/plugins/tabdrop/css/tabdrop.css" />
    <script src="AppThemes/js/Chart/jquery.gvChart.js" type="text/javascript"></script>
    <script src="AppThemes/js/Chart/jquery.gvChart.min.js" type="text/javascript"></script>
    <script src="AppThemes/js/Extension.min.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">

        function expandcollapse(obj, row) {


            var div = document.getElementById(obj);
            var img = document.getElementById('img' + obj);

            if (div.style.display == "none") {
                div.style.display = "block";
                img.src = "AppThemes/images/minus.png";
            }
            else {
                div.style.display = "none";
                img.src = "AppThemes/images/plus.png";
            }
        }
    </script>
     <script language="javascript" type="text/javascript">

         function expandcollapse1(obj, row) {

             var img = document.getElementById('img' + obj);

             if ($('tr.' + obj).css('display') === 'none') {
                 $("." + obj).css("display", "");
                 img.src = "AppThemes/images/minus.png";
             }
             else {
                 $("." + obj).css("display", "none");
                 img.src = "AppThemes/images/plus.png";
             }


         }
    </script>
    <script>


        function drawChart() {
            googleLoaded.done(function () {
                $('#tblCtCompanyDet').gvChart({
                    chartType: 'ColumnChart',
                    gvSettings: {
                        vAxis: { title: 'Count' },
                        hAxis: { title: 'LocationName' }
                    }
                });



            });
        }

        function drawstauschat() {
            googleLoaded.done(function () {
                $('#tblcStatus').gvChart({
                    chartType: 'ColumnChart',
                    gvSettings: {
                        vAxis: { title: 'Count' },
                        hAxis: { title: 'DateWise' }
                    }
                });



            });
        }

        function drawassetchat() {

            googleLoaded.done(function () {
                $('#tblcAssetDet').gvChart({
                    chartType: 'PieChart',
                    gvSettings: {
                        vAxis: { title: 'Count' },
                        hAxis: { title: 'AssetName' }
                    }
                });

            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <cc1:ToolkitScriptManager ID="ToolkitScriptManager" runat="server">
    </cc1:ToolkitScriptManager>
 <%--<head id="HeadContent" runat="server" ></head>--%>
    <div class="brownish-scheme">
        <!-- Wrap all page content here -->
        <div id="wrap">
            <!-- Make page fluid -->
            <div class="row">
                <!-- submenu -->
                <div class="submenu" style="display: none">
                    <h1>
                        <asp:Literal ID="lmainheader" runat="server"></asp:Literal>
                    </h1>
                    <h1>
                        <asp:Literal ID="lsubheader" runat="server"></asp:Literal>
                    </h1>
                </div>
                <div class="col-md-5 col-xs-offset-4 alert alert-big alert-danger alert-dismissable fade in margin-top-10"
                    runat="server" id="divmsg" visible="false">
                    <asp:Label runat="server" ID="lblError"></asp:Label>
                </div>
                <!-- /submenu -->
                <!-- content main container -->
                <div class="main">
                    <div class="row border-bottom margin-vertical-15">
                        <div class="col-md-12">
                            <div id="basicvalidations" parsley-validate="" role="form" class="form-horizontal">
                                <div class="form-group">
                                <label class="col-sm-1 control-label" for="">
                                        Region</label>
                                    <div class="col-sm-4">
                                                 <asp:DropDownList ID="ddlregion" runat="server"  AutoPostBack="True"  
                                parsley-validation-minlength="1" parsley-type="email" parsley-minlength="4" 
                                parsley-required="true" parsley-trigger="change" 
                                class="form-control parsley-validated"  
                                kl_virtual_keyboard_secure_input="on"  
                                onselectedindexchanged="ddlsbu_SelectedIndexChanged" >
                                 <asp:ListItem Text="All" Value="0"></asp:ListItem>
                            <asp:ListItem Text="North" Value="1"></asp:ListItem>
                            <asp:ListItem Text="South" Value="2"></asp:ListItem>
                            <asp:ListItem Text="East" Value="3"></asp:ListItem>
                            <asp:ListItem Text="West" Value="4"></asp:ListItem>
                                </asp:DropDownList>
                               
                                    </div>
                                <label class="col-sm-1 control-label" for="">
                                        SBU</label>
                                    <div class="col-sm-4">
                                                 <asp:DropDownList ID="ddlsbu" runat="server"  AutoPostBack="True"  
                                parsley-validation-minlength="1" parsley-type="email" parsley-minlength="4" 
                                parsley-required="true" parsley-trigger="change" 
                                class="form-control parsley-validated"  
                                kl_virtual_keyboard_secure_input="on"  
                                onselectedindexchanged="ddlsbu_SelectedIndexChanged" ></asp:DropDownList>
                               
                                    </div>
                                     
                                   
                                </div>
                                  <div class="form-group">
                                 <label class="col-sm-1 control-label" for="">
                                        Company</label>
                                    <div class="col-sm-4">
                                                <asp:DropDownList ID="ddlcompany" runat="server"  AutoPostBack="True"  
                                parsley-validation-minlength="1" parsley-type="email" parsley-minlength="4" 
                                parsley-required="true" parsley-trigger="change" 
                                class="form-control parsley-validated"  
                                kl_virtual_keyboard_secure_input="on" 
                                onselectedindexchanged="ddlcompany_SelectedIndexChanged" ></asp:DropDownList>
                                
                                </div>
                                
                               
                                    
                                <label class="col-sm-1 control-label" for="">
                                        Location</label>
                                    <div class="col-sm-4">
                                               <asp:DropDownList ID="ddllocation" runat="server"  AutoPostBack="True"  
                                parsley-validation-minlength="1" parsley-type="email" parsley-minlength="4" 
                                parsley-required="true" parsley-trigger="change" 
                                class="form-control parsley-validated"  
                                kl_virtual_keyboard_secure_input="on" 
                                onselectedindexchanged="ddllocation_SelectedIndexChanged" ></asp:DropDownList>
                               
                               
                               
                                    </div>
                                    </div>

                                      <div class="form-group">
                                  <label class="col-sm-1 control-label" for="">
                                       From Date</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="tfromdate" runat="server" class="form-control parsley-validated"></asp:TextBox>
                                         <cc1:calendarextender ID="calexcompletion" runat="server" TargetControlID="tfromdate"
                                                        CssClass="ajax__calendar_title div" 
                                            PopupButtonID="tfromdate" Format="MM/dd/yyyy"></cc1:calendarextender>
                                    </div>

                                     <label class="col-sm-1 control-label" for="">
                                       To Date</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txttodate" runat="server" class="form-control parsley-validated"></asp:TextBox>
                                         <cc1:calendarextender ID="Calendarextender1" runat="server" TargetControlID="txttodate"
                                                        CssClass="ajax__calendar_title div" 
                                            PopupButtonID="txttodate" Format="MM/dd/yyyy"></cc1:calendarextender>
                                    </div>

                                      <asp:Button ID="btngo" runat="server" CssClass="btn btn-primary" Text="Go" 
                                        onclick="btngo_Click" />
                                   
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <asp:Panel ID="pnlrender" runat="server">
                        <!-- cards -->
                       
                       
                        <div class="row">
                            <div class="col-lg-12 col-md-12" style="margin-left:210px;">
                                <div class="col-md-6">
                                    <asp:Label ID="lblallaudit" runat="server"></asp:Label>

                                     <asp:Label ID="lblname" style="margin-left:190px;font-size:13px;font-weight:bold;"  runat="server"></asp:Label>
                                    
                                </div>
                               
                            </div>
                        </div>

                       
                        
                        <h2>
                            <asp:Label ID="lblauditwise" style="font-size:18px;font-weight:bold;" Text="Feedback Wise"   runat="server"></asp:Label></h2>
                        <div class="row">
                            <!-- col 8 -->
                            <div class="col-lg-12 col-md-12">
                                <div class="col-md-12">
                                    <asp:DataList BorderStyle="None" ID="dlguaguedashboard" CssClass="table  table-responsive"
                                        RepeatColumns="3" RepeatDirection="Horizontal" runat="server" OnItemDataBound="dlguaguedashboard_ItemDataBound">
                                        <ItemTemplate>
                                            <asp:Label ID="lblgauge" runat="server"></asp:Label>
                                            <asp:Label ID="lblcityName" Style="margin-left: 40%;" Font-Bold="True" runat="server"
                                                Text='<%# Eval("auditname")%>'></asp:Label>
                                            <asp:HiddenField runat="server" ID="hidauditid" Value='<%# Eval("auditid")%>' />
                                            <asp:HiddenField runat="server" ID="hidtotal" Value='<%# Eval("total")%>' />
                                            
                                        </ItemTemplate>
                                    </asp:DataList>
                                </div>
                            </div>
                        </div>
                       

                        <h2>
                            <asp:Label ID="lblcatwise" style="font-size:18px;font-weight:bold;" Text="Category Wise"  runat="server"></asp:Label></h2>
                        <div class="row">
                            <!-- col 8 -->
                            <div class="col-lg-12 col-md-12">
                                <div class="col-md-12">
                                    <asp:DataList BorderStyle="None" ID="datcatwise" CssClass="table  table-responsive"
                                        RepeatColumns="3" RepeatDirection="Horizontal" runat="server" OnItemDataBound="datcatwise_ItemDataBound">
                                        <ItemTemplate>
                                            <asp:Label ID="lblcatgauge" runat="server"></asp:Label>
                                            <asp:Label ID="lblcatname" Style="margin-left: 16%;" Font-Bold="True" runat="server"
                                                Text='<%# Eval("categoryname")%>'></asp:Label>
                                            <asp:HiddenField runat="server" ID="hidauditid" Value='<%# Eval("auditid")%>' />
                                            <asp:HiddenField runat="server" ID="hidcatid" Value='<%# Eval("categoryid")%>' />
                                            <asp:HiddenField runat="server" ID="hidtotal" Value='<%# Eval("total")%>' />
                                            
                                        </ItemTemplate>
                                    </asp:DataList>
                                </div>
                            </div>
                        </div>
                      
                    </asp:Panel>
            
                    </div>
                </div>
            </div>
            <!-- Include all compiled plugins (below), or include individual files as needed -->
            <%-- <script src="App_Themes/js/bootstrap.min.js"></script>--%>
            <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css&amp;skin=sons-of-obsidian"></script>
            <script src="App_Themes/js/minoral.min.js"></script>
            <script type="text/javascript">

                $(function () {

                    //chosen select input
                    $(".chosen-select").chosen({ disable_search_threshold: 10 });

                })
      
            </script>
            <script type="text/javascript">
                $(function () {

                    // Initialize card flip
                    $('.card.hover').hover(function () {
                        $(this).addClass('flip');
                    }, function () {
                        $(this).removeClass('flip');
                    });

                    // Make card front & back side same


                    //                // weather icons
                    //                var icons = new Skycons({ "color": "white" });
                    //                icons.set("partly-cloudy-day", Skycons.PARTLY_CLOUDY_DAY);
                    //                icons.set("clear-day", Skycons.CLEAR_DAY);
                    //                icons.set("cloudy", Skycons.CLOUDY);
                    //                icons.set("wind", Skycons.WIND);
                    //                icons.set("rain", Skycons.RAIN);
                    //                icons.set("clear-night", Skycons.CLEAR_NIGHT);
                    //                icons.play();



                    //social feed hover function
                    $(".social-feed li").on({
                        mouseenter: function () {
                            $('.social-feed .active').removeClass('active');
                        }, mouseleave: function () {
                            $(this).addClass('active');
                        }
                    });

                })
      
            </script>
        </div>

        </div>

</asp:Content>
