﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BL;
using System.Data;
using System.IO;
using System.Text;
using System.Data.SqlClient;
using System.Globalization;
using FacilityAudit.Code;
namespace Facility_Audit
{
    public partial class MailTrackingReport : System.Web.UI.Page
    {
        #region "Datas"

        private DataTable ReportDet
        {
            get
            {
                if (ViewState["reportdetails"] == null)
                    ViewState["reportdetails"] = null;
                return (DataTable)ViewState["reportdetails"];
            }

            set
            {
                ViewState["reportdetails"] = value;
            }
        }

        #endregion
        BLTransaction objbltran = new BLTransaction();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string dFirstDayOfThisMonth = DateTime.Today.AddDays(-(DateTime.Today.Day - 1)).ToString("MM/dd/yyyy");
                string lastOfThisMonth = DateTime.Today.ToString("MM/dd/yyyy");

                this.txtfrmdate.Text = dFirstDayOfThisMonth;
                this.txttodate.Text = lastOfThisMonth;
                if (!txtfrmdate.Text.Contains("/"))
                {
                    char datesplit = txtfrmdate.Text.ToCharArray()[2];
                    txtfrmdate.Text = txtfrmdate.Text.Replace(datesplit, '/');
                    txttodate.Text = txttodate.Text.Replace(datesplit, '/');
                }
                BindDetails();
            }
        }

        protected void lnksearch_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtfrmdate.Text.Trim() != string.Empty && txttodate.Text.Trim() != string.Empty)
                {
                    BindDetails();
                }
                else
                {
                    NotifyMessages("Plz Select Date Range", "error");
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, "error");
            }
        }

        protected void rptrtrackingrpt_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    Label lblccmail = (Label)e.Item.FindControl("lblccmail");

                    if (lblccmail != null)
                    {
                        string maildet = lblccmail.Text;
                        maildet = maildet.Replace(",", ",<br/>");
                        lblccmail.Text = maildet;
                    }


                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, "error");
            }
        }



        private void BindDetails()
        {
            try
            {
                lblError.Text = string.Empty;
                divmsg.Visible = false;
                if (txtfrmdate.Text.Trim() != string.Empty && txttodate.Text.Trim() != string.Empty)
                {
                    DateTime dtFDate = new DateTime();
                    DateTime dtTDate = new DateTime();
                    dtFDate = DateTime.ParseExact(txtfrmdate.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                    dtTDate = DateTime.ParseExact(txttodate.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                    DataSet dsreport = new DataSet();
                    //dsreport = objbltran.GetMailTrackingDetails(dtFDate, dtTDate);
                    dsreport = objbltran.GetMailTrackingDetails_Group(dtFDate, dtTDate, UserSession.GroupID);
                    if (dsreport.Tables.Count > 0 && dsreport.Tables[0].Rows.Count > 0)
                    {
                        rptrtrackingrpt.DataSource = dsreport.Tables[0];
                        rptrtrackingrpt.DataBind();

                        ReportDet = dsreport.Tables[0];
                    }
                    else
                    {
                        rptrtrackingrpt.DataSource = null;
                        rptrtrackingrpt.DataBind();
                        NotifyMessages("No Data Found", "info");
                    }
                }
            }
            catch (SqlException)
            {
                NotifyMessages("Server not responding. Please Try Again!!!!", "error");
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, "error");
            }
        }

        public void NotifyMessages(string message, string errortype)
        {
            divmsg.Visible = true;
            lblError.Text = message;
            if (errortype == "info")
            {
                divmsg.Attributes.Add("class", "alert alert-success fade in");
            }
            else if (errortype == "error")
            {
                divmsg.Attributes.Add("class", "alert alert-danger fade in");
            }
        }


        protected void lnkdownload_Click(object sender, EventArgs e)
        {
            if (rptrtrackingrpt.Items.Count > 0)
            {
                if (ReportDet == null)
                {
                    NotifyMessages("No Record Found", "error");
                    return;
                }

                DataSet dsreport = new DataSet();
                dsreport.Tables.Add(ReportDet);
                //dsreport = objbltran.GetDateWiseReport(0, month, year);
                if (dsreport.Tables.Count > 0 && dsreport.Tables[0].Rows.Count > 0)
                {
                    Response.Clear();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=Mail Tracking Details.xls");
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.ms-excel";
                    StringWriter sw = new StringWriter();
                    HtmlTextWriter hw = new HtmlTextWriter(sw);
                    rptrtrackingrpt.RenderControl(hw);
                    Response.Output.Write(sw.ToString());
                    Response.Flush();
                    Response.End();
                }
                else
                {
                    NotifyMessages("No Data Found", "info");
                }
            }
        }
    }
}