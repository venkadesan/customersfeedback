﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Mainpage.Master" AutoEventWireup="true"
    CodeBehind="LocationMailDetails.aspx.cs" Inherits="Facility_Audit.LocationMailDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="col-md-11">
        <div id="crumbs">
            <ul>
                <li class="active" style="width: 250px;"><a href="#">Location Mail Details</a></li>
                <li id="linewbtn" runat="server"><a>
                    <asp:LinkButton ID="bNew" OnClick="bNew_Click" runat="server">New</asp:LinkButton></a></li>
            </ul>
        </div>
    </div>
    <div class="main">
        <div class=" col-md-5 col-xs-offset-4 alert alert-big alert-danger alert-dismissable fade in margin-top-10"
            runat="server" id="divmsg" visible="false">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                ×</button>
            <asp:Label runat="server" ID="lblError"></asp:Label>
        </div>
        <div class="row">
            <div class="main">
                <div class="row border-bottom margin-vertical-15">
                    <div class="col-md-12">
                        <div id="Div1" parsley-validate="" role="form" class="form-horizontal">
                            <div class="form-group">
                                <label class="col-sm-1 control-label" for="">
                                    SBU</label>
                                <div class="col-sm-4">
                                    <asp:DropDownList ID="ddlsbu" runat="server" AutoPostBack="True" class="form-control chosen-select"
                                        OnSelectedIndexChanged="ddlsbu_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </div>
                                <label class="col-sm-1 control-label" for="">
                                    Company</label>
                                <div class="col-sm-4">
                                    <asp:DropDownList ID="ddlcompany" runat="server" AutoPostBack="True" class="form-control chosen-select"
                                        OnSelectedIndexChanged="ddlcompany_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-1 control-label" for="">
                                    Location</label>
                                <div class="col-sm-4">
                                    <asp:DropDownList ID="ddllocation" runat="server" AutoPostBack="True" class="form-control chosen-select"
                                        OnSelectedIndexChanged="ddllocation_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <h3>
                    Location Contacts - Multiple
                </h3>
                <div class="row border-bottom margin-vertical-15">
                    <div class="col-md-2">
                    </div>
                    <div class="col-md-1">
                        <asp:LinkButton ID="lnkdownload" runat="server" onclick="lnkdownload_Click"><i class="fa fa-download fa-2x"></i></asp:LinkButton>
                    </div>
                    <div class="col-md-3">
                        <asp:FileUpload ID="fufiles" runat="server"></asp:FileUpload>
                    </div>
                    <div class="col-md-1">
                        <asp:LinkButton ID="lnkupload" runat="server" onclick="lnkupload_Click"><i class="fa fa-upload fa-2x"></i></asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <section class="tile">
                 
                     
                    <asp:Panel ID="pnlnew" runat="server">
                       <div class="tile-body nobottompadding">
                          <div class="row form-horizontal">
                             <div class="col-md-8">
                                 <div id="basicvalidations"  class="form-horizontal">
                                     <div class="form-group">
                                             <label class="col-sm-4 control-label" for="">Region Name : </label>
                                        <div class="col-sm-8">
                                             <asp:TextBox ID="txtregname"  runat="server" parsley-validation-minlength="1" parsley-type="email" parsley-minlength="4" parsley-required="true" parsley-trigger="change" class="form-control parsley-validated" kl_virtual_keyboard_secure_input="on"></asp:TextBox>
                                        </div>
                                        <label class="col-sm-4 control-label" for="">Region Mailid  : </label>
                                        <div class="col-sm-8">
                                             <asp:TextBox ID="txtregmailid"  runat="server" parsley-validation-minlength="1" parsley-type="email" parsley-minlength="4" parsley-required="true" parsley-trigger="change" class="form-control parsley-validated" kl_virtual_keyboard_secure_input="on" > </asp:TextBox>
                                        </div>
                                     </div>

                                       <div class="form-group">
                                             <label class="col-sm-4 control-label" for="">State Name  : </label>
                                        <div class="col-sm-8">
                                             <asp:TextBox ID="txtstatename"  runat="server" parsley-validation-minlength="1" parsley-type="email" parsley-minlength="4" parsley-required="true" parsley-trigger="change" class="form-control parsley-validated" kl_virtual_keyboard_secure_input="on"></asp:TextBox>
                                        </div>
                                        <label class="col-sm-4 control-label" for="">State Mailid  : </label>
                                        <div class="col-sm-8">
                                             <asp:TextBox ID="txtstatemailid"  runat="server" parsley-validation-minlength="1" parsley-type="email" parsley-minlength="4" parsley-required="true" parsley-trigger="change" class="form-control parsley-validated" kl_virtual_keyboard_secure_input="on" > </asp:TextBox>
                                        </div>
                                     </div>


                                       <div class="form-group">
                                             <label class="col-sm-4 control-label" for="">SBU Name <span class="red-text">*</span> : </label>
                                        <div class="col-sm-8">
                                             <asp:TextBox ID="txtsbuname"  runat="server" parsley-validation-minlength="1" parsley-type="email" parsley-minlength="4" parsley-required="true" parsley-trigger="change" class="form-control parsley-validated" kl_virtual_keyboard_secure_input="on"></asp:TextBox>
                                        </div>
                                        <label class="col-sm-4 control-label" for="">SBU Mailid <span class="red-text">*</span> : </label>
                                        <div class="col-sm-8">
                                             <asp:TextBox ID="txtsbumailid"  runat="server" parsley-validation-minlength="1" parsley-type="email" parsley-minlength="4" parsley-required="true" parsley-trigger="change" class="form-control parsley-validated" kl_virtual_keyboard_secure_input="on" > </asp:TextBox>
                                        </div>
                                     </div>


                                       <div class="form-group">
                                             <label class="col-sm-4 control-label" for="">OM Name : </label>
                                        <div class="col-sm-8">
                                             <asp:TextBox ID="txtomname"  runat="server" parsley-validation-minlength="1" parsley-type="email" parsley-minlength="4" parsley-required="true" parsley-trigger="change" class="form-control parsley-validated" kl_virtual_keyboard_secure_input="on"></asp:TextBox>
                                        </div>
                                        <label class="col-sm-4 control-label" for="">OM Mailid : </label>
                                        <div class="col-sm-8">
                                             <asp:TextBox ID="txtommailid"  runat="server" parsley-validation-minlength="1" parsley-type="email" parsley-minlength="4" parsley-required="true" parsley-trigger="change" class="form-control parsley-validated" kl_virtual_keyboard_secure_input="on" > </asp:TextBox>
                                        </div>
                                     </div>

                                      <div class="form-group">
                                             <label class="col-sm-4 control-label" for="">AOM Name  : </label>
                                        <div class="col-sm-8">
                                             <asp:TextBox ID="txtaomname"  runat="server" parsley-validation-minlength="1" parsley-type="email" parsley-minlength="4" parsley-required="true" parsley-trigger="change" class="form-control parsley-validated" kl_virtual_keyboard_secure_input="on"></asp:TextBox>
                                        </div>
                                        <label class="col-sm-4 control-label" for="">AOM Mailid  : </label>
                                        <div class="col-sm-8">
                                             <asp:TextBox ID="txtaommailid"  runat="server" parsley-validation-minlength="1" parsley-type="email" parsley-minlength="4" parsley-required="true" parsley-trigger="change" class="form-control parsley-validated" kl_virtual_keyboard_secure_input="on" > </asp:TextBox>
                                        </div>
                                     </div>

                                      <div class="form-group">
                                             <label class="col-sm-4 control-label" for="">GM OPS Name   : </label>
                                        <div class="col-sm-8">
                                             <asp:TextBox ID="txtgmopsname"  runat="server"  class="form-control parsley-validated" ></asp:TextBox>
                                        </div>
                                        <label class="col-sm-4 control-label" for="">GM OPS  Mailid  : </label>
                                        <div class="col-sm-8">
                                             <asp:TextBox ID="txtgmopsemail"  runat="server"  class="form-control parsley-validated"  > </asp:TextBox>
                                        </div>
                                     </div>

                                        <div class="form-group">
                                             <label class="col-sm-4 control-label" for="">National Head Name   : </label>
                                        <div class="col-sm-8">
                                             <asp:TextBox ID="txtnationheadname"  runat="server"  class="form-control parsley-validated" ></asp:TextBox>
                                        </div>
                                        <label class="col-sm-4 control-label" for="">National Head  MailID  : </label>
                                        <div class="col-sm-8">
                                             <asp:TextBox ID="txtnationheademail"  runat="server"  class="form-control parsley-validated"  > </asp:TextBox>
                                        </div>
                                     </div>

                                       <div class="form-group">
                                             <label class="col-sm-4 control-label" for="">Client Name  : </label>
                                        <div class="col-sm-8">
                                             <asp:TextBox ID="txtcliname"  runat="server" parsley-validation-minlength="1" parsley-type="email" parsley-minlength="4" parsley-required="true" parsley-trigger="change" class="form-control parsley-validated" kl_virtual_keyboard_secure_input="on"></asp:TextBox>
                                        </div>
                                        <label class="col-sm-4 control-label" for="">Client Mailid  : </label>
                                        <div class="col-sm-8">
                                             <asp:TextBox ID="txtclimailid"  runat="server" parsley-validation-minlength="1" parsley-type="email" parsley-minlength="4" parsley-required="true" parsley-trigger="change" class="form-control parsley-validated" kl_virtual_keyboard_secure_input="on" > </asp:TextBox>
                                        </div>
                                     </div>

                                      <div class="form-group">
                                             <label class="col-sm-4 control-label" for="">Opeartion Head Name  : </label>
                                        <div class="col-sm-8">
                                             <asp:TextBox ID="txtOperationHeadName"  runat="server" parsley-validation-minlength="1" parsley-type="email" parsley-minlength="4" parsley-required="true" parsley-trigger="change" class="form-control parsley-validated" kl_virtual_keyboard_secure_input="on"></asp:TextBox>
                                        </div>
                                        <label class="col-sm-4 control-label" for="">Opeartion Head Mail  : </label>
                                        <div class="col-sm-8">
                                             <asp:TextBox ID="txtOperationHeadMail"  runat="server" parsley-validation-minlength="1" parsley-type="email" parsley-minlength="4" parsley-required="true" parsley-trigger="change" class="form-control parsley-validated" kl_virtual_keyboard_secure_input="on" > </asp:TextBox>
                                        </div>
                                     </div>
                                     
                                      <div class="form-group">
                                             <label class="col-sm-4 control-label" for="">Training Head Name  : </label>
                                        <div class="col-sm-8">
                                             <asp:TextBox ID="txtTrainingname"  runat="server" parsley-validation-minlength="1" parsley-type="email" parsley-minlength="4" parsley-required="true" parsley-trigger="change" class="form-control parsley-validated" kl_virtual_keyboard_secure_input="on"></asp:TextBox>
                                        </div>
                                        <label class="col-sm-4 control-label" for="">Training Head Mail  : </label>
                                        <div class="col-sm-8">
                                             <asp:TextBox ID="txtTrainingmail"  runat="server" parsley-validation-minlength="1" parsley-type="email" parsley-minlength="4" parsley-required="true" parsley-trigger="change" class="form-control parsley-validated" kl_virtual_keyboard_secure_input="on" > </asp:TextBox>
                                        </div>
                                     </div>


                                        <div class="form-group">
                                             <label class="col-sm-4 control-label" for="">SCM Head Name  : </label>
                                        <div class="col-sm-8">
                                             <asp:TextBox ID="txtSCMname"  runat="server" parsley-validation-minlength="1" parsley-type="email" parsley-minlength="4" parsley-required="true" parsley-trigger="change" class="form-control parsley-validated" kl_virtual_keyboard_secure_input="on"></asp:TextBox>
                                        </div>
                                        <label class="col-sm-4 control-label" for="">SCM Head Mail  : </label>
                                        <div class="col-sm-8">
                                             <asp:TextBox ID="txtSCMmail"  runat="server" parsley-validation-minlength="1" parsley-type="email" parsley-minlength="4" parsley-required="true" parsley-trigger="change" class="form-control parsley-validated" kl_virtual_keyboard_secure_input="on" > </asp:TextBox>
                                        </div>
                                     </div>


                                      <div class="form-group">
                                             <label class="col-sm-4 control-label" for="">Compliance Head Name  : </label>
                                        <div class="col-sm-8">
                                             <asp:TextBox ID="txtCompliancename"  runat="server" parsley-validation-minlength="1" parsley-type="email" parsley-minlength="4" parsley-required="true" parsley-trigger="change" class="form-control parsley-validated" kl_virtual_keyboard_secure_input="on"></asp:TextBox>
                                        </div>
                                        <label class="col-sm-4 control-label" for="">Compliance Head Mail  : </label>
                                        <div class="col-sm-8">
                                             <asp:TextBox ID="txtCompliancemail"  runat="server" parsley-validation-minlength="1" parsley-type="email" parsley-minlength="4" parsley-required="true" parsley-trigger="change" class="form-control parsley-validated" kl_virtual_keyboard_secure_input="on" > </asp:TextBox>
                                        </div>
                                     </div>

                                      <div class="form-group">
                                             <label class="col-sm-4 control-label" for="">HR Head Name  : </label>
                                        <div class="col-sm-8">
                                             <asp:TextBox ID="txthrname"  runat="server" parsley-validation-minlength="1" parsley-type="email" parsley-minlength="4" parsley-required="true" parsley-trigger="change" class="form-control parsley-validated" kl_virtual_keyboard_secure_input="on"></asp:TextBox>
                                        </div>
                                        <label class="col-sm-4 control-label" for="">HR Head Mail  : </label>
                                        <div class="col-sm-8">
                                             <asp:TextBox ID="txthrmail"  runat="server" parsley-validation-minlength="1" parsley-type="email" parsley-minlength="4" parsley-required="true" parsley-trigger="change" class="form-control parsley-validated" kl_virtual_keyboard_secure_input="on" > </asp:TextBox>
                                        </div>
                                     </div>

                                     <div class="form-group">
                                             <label class="col-sm-4 control-label" for="">Other Head Name  : </label>
                                        <div class="col-sm-8">
                                             <asp:TextBox ID="txtOthername"  runat="server" parsley-validation-minlength="1" parsley-type="email" parsley-minlength="4" parsley-required="true" parsley-trigger="change" class="form-control parsley-validated" kl_virtual_keyboard_secure_input="on"></asp:TextBox>
                                        </div>
                                        <label class="col-sm-4 control-label" for="">Other Head Mail  : </label>
                                        <div class="col-sm-8">
                                             <asp:TextBox ID="txtOthermail"  runat="server" parsley-validation-minlength="1" parsley-type="email" parsley-minlength="4" parsley-required="true" parsley-trigger="change" class="form-control parsley-validated" kl_virtual_keyboard_secure_input="on" > </asp:TextBox>
                                        </div>
                                     </div>
                                    
                                 </div>
                             </div>
                          </div>
                       </div>
       
           	           <div class="tile-footer color cyan text-muted">
                           <div class="row">
                              <div class="col-md-5">
                                 <div class="col-md-offset-5">
                                    <asp:Button ID="btnsave" class="btn btn-primary" runat="server" Text="Save" 
                                         onclick="btnsave_Click" />
                                    <asp:Button ID="btncncl" class="btn btn-default" runat="server" Text="Cancel" 
                                         onclick="btncncl_Click" />
                                 </div>
                              </div>
                           </div>
                       </div>
                    </asp:Panel>

                    <div class="row">
                       <div class="col-md-12">
                          <div class="tile-body nopadding margin-top-10">
                              <div class="table-responsive" id="divrep" runat="server" >
                                 <asp:Repeater ID="rptrcat"  DataSourceID="" runat="server" 
                                     onitemcommand="rptrcat_ItemCommand" onitemdatabound="rptrcat_ItemDataBound" >
                                     <HeaderTemplate>
                                        <table width="100%" border="0" cellspacing="0" class="table table-bordered" cellpadding="0">
                                          <tbody>
                                             <tr class="green color">
                                                <th width="5%">Sl.No</th>

                                               <%-- <th>Audit Name</th>--%>
                                               
                                               <th>SBU Name</th>
                                                <th>SBU MailId</th>
                                               
                                               <th> Option</th>
                                             </tr>                    
                                     </HeaderTemplate>

                                     <ItemTemplate>
                                             <tr id="rid">
                                                   <asp:HiddenField ID="hidcompanyid" runat="server" Value='<%# Eval("id") %>' />
                                               <td>
                                                   <asp:Label ID="Label2" runat="server" Text='<%#(((RepeaterItem)Container).ItemIndex+1).ToString() %>' />
                                               </td>
                                               
                                               <%--<td>
                                                   <asp:Label ID="lblcmpname" runat="server" Text='<%# Eval("auditname") %>' />
                                               </td>--%>
                                               
                                               <td>
                                                   <asp:Label ID="lblcmpshrtname" runat="server" Text='<%# Eval("sbuname") %>' />
                                               </td>

                                                <td>
                                                   <asp:Label ID="Label1" runat="server" Text='<%# Eval("sbumailid") %>' />
                                               </td>

                                               <td class="actions text-center">
                                                   <asp:LinkButton ID="lnkEdit" runat="server" CommandName="Edit" class="btn btn-primary" Text="Edit" CommandArgument='<%# Eval("id") %>'></asp:LinkButton>
                                                   <asp:LinkButton ID="lnkDelete" runat="server" class="btn btn-danger" meta:resourcekey="BtnUserDeleteResource1"
                                                       OnClientClick="if ( !confirm('Are you sure you want to delete this user?')) return false;"   CommandName="Delete" Text="Delete" CommandArgument='<%# Eval("id") %>'></asp:LinkButton>
                                               </td>
                                             </tr>
                                     </ItemTemplate>

                                     <FooterTemplate>
                                       </table>
                                     </FooterTemplate>
                                 </asp:Repeater>
                              </div>
                          </div>
                       </div>
                    </div>
              </section>
        </div>
    </div>
    </div>
</asp:Content>
