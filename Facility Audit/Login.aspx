﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Facility_Audit.Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title id="title" runat="server">IFazig - Feedback</title>
    <link href="logindet/log-pass-act.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div class="container">
        <div class="s60">
            <img src="AppThemes/images/JLL New.png" height="32" width="136" alt="" class="logo_box"
                id="logo" runat="server" />
        </div>
        <div class="login">
            <div class="form_hd">
                User ID</div>
            <div class="form">
                <asp:TextBox ID="txtUserName" runat="server" class="tbox1"></asp:TextBox>
            </div>
            <div class="form_hd">
                Password</div>
            <div class="form">
                <asp:TextBox runat="server" ID="txtPassword" TextMode="Password" class="tbox1"></asp:TextBox>
            </div>
            <div class="form">
                <div class="form_logbtn">
                    <asp:Button ID="Submit1" Text="Submit" runat="server" OnClick="Submit1_Click" class="btn_login" />
                </div>
                <div class="rememberme">
                    <input name="" type="checkbox" value="" />
                    Remember Me</div>
                <div class="rememberme">
                    Forgot Password?</div>
                <div class="cboth">
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
                 //<![CDATA[
        WebForm_AutoFocus('txtUserName');//]]>
    </script>
    </form>
</body>
</html>
