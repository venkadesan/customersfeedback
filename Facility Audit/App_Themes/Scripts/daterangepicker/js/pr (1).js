function pageLoad(sender, args) {
    if (args.get_isPartialLoad()) {
        $("input[type=text][id*=tQty]").bind("keyup", priceCalc).bind("keyup", goToCtrl);
        $("input[type=text][id*=tPrice]").bind("keyup", priceCalc).bind("keyup", goToCtrl);
        $("input[type=text][id*=tMonths]").bind("keyup", priceCalc).bind("keyup", goToCtrl);

        //$(".positive").numeric({ negative: false });
        //$(".numeric").numeric();

        $('.dt').daterangepicker({
            dateFormat: $("input[type=hidden][id*=hFormat]").val(),
            datepickerOptions: {
                changeMonth: true,
                changeYear: true
            }
        });
    }
}

$(document).ready(
	function () {
	    $("input[type=text][id*=tQty]").bind("keyup", priceCalc).bind("keyup", goToCtrl);
	    $("input[type=text][id*=tPrice]").bind("keyup", priceCalc).bind("keyup", goToCtrl);
	    $("input[type=text][id*=tMonths]").bind("keyup", priceCalc).bind("keyup", goToCtrl);

	    //$(".positive").numeric({ negative: false });
	    //$(".numeric").numeric();

	    $('.dt').daterangepicker({
	        dateFormat: $("input[type=hidden][id*=hFormat]").val(),
	        datepickerOptions: {
	            changeMonth: true,
	            changeYear: true
	        }
	    });
	}
);


    function priceCalc(e) {
        var ids = $(this).attr("id");
        var id = ids.substring(ids.lastIndexOf("_") + 1);

        $("[id*=lTotalPrice_" + id + "]").calc(
	        "qty * price * others",
	        {
	            qty: $("input[type=text][id*=tQty_" + id + "]"),
	            price: $("input[type=text][id*=tPrice_" + id + "]"),
	            others: $("input[type=text][id*=tMonths_" + id + "]")
	        },
	        function (s) {
	            return s.toFixed(2);
	        },
	        function ($this) {

	            var sum = $("[id*=lTotalPrice]").sum();
	            $("[id*=lGrandTotalPrice]").text(
			        sum.toFixed(2)
		        );
	        }
        );
	}

	function ddBlur(obj) {
	    var id = document.getElementById(obj.id).parentNode.id;
	    document.getElementById(id.replace("dvFr", "tUOMType")).style.display = 'block';
	    document.getElementById(id.replace("dvFr", "tUOMType")).value = document.getElementById('dUOM').options[document.getElementById('dUOM').selectedIndex].text;
	    document.getElementById(id.replace("dvFr", "hUOMSel")).value = document.getElementById('dUOM').options[document.getElementById('dUOM').selectedIndex].value;
	    document.getElementById(id).removeChild(document.getElementById('dUOM'));
	}

	function showdialog(form) {
	    var result = window.showModalDialog("importitems.aspx", form, "dialogWidth:800px; dialogHeight:201px; center:yes");
	}