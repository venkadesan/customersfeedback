﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Kowni.BusinessLogic;
using MCB = Kowni.Common.BusinessLogic;
using Kowni.Helper;
using System.Data;
using System.Web.Security;
using FacilityAudit.Code;
using System.Configuration;
using BL;
using FacilityAudit.BLL;

namespace Facility_Audit
{
    public partial class Login : System.Web.UI.Page
    {
        BLGroupcompany objgrpcmp = new BLGroupcompany();
        MCB.BLSingleSignOn objSingleSignOn = new MCB.BLSingleSignOn();
        MCB.BLAnnouncement objAnnouncement = new MCB.BLAnnouncement();
        BLUser objUser = new BLUser();
        
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session.Clear();                
            }
        }

        public void NotifyMessages(string Message)
        {
            //divMessage.Style.Clear();
            //divMessage.Attributes.Add("class", MessageType.ToString());
            //divMessage.Attributes.Add("class", "Error");
            //divMessage.InnerHtml = Message.Trim();
            //divMessage.Visible = true;
        }

        protected void Submit1_Click(object sender, EventArgs e)
        {
            int RoleID = 0;
            string RoleName = string.Empty;
            int UserID = 0;
            int CompanyID = 0;
            string CompanyName = string.Empty;
            int LocationID = 0;
            string LocationName = string.Empty;
            int GroupID = 0;
            int LanguageID = 0;
            string UserFName = string.Empty;
            string UserLName = string.Empty;
            string UserMailID = string.Empty;
            string ThemeFolderPath = string.Empty;

            if (!objSingleSignOn.Login(this.txtUserName.Text, this.txtPassword.Text, Convert.ToInt32(ConfigurationManager.AppSettings["toolid"].ToString()), out RoleID, out UserID, out CompanyID, out CompanyName, out LocationID, out LocationName, out GroupID, out LanguageID, out UserFName, out UserLName, out UserMailID, out ThemeFolderPath))
            {
                NotifyMessages(objSingleSignOn.Message);
            }
            else
            {
                UserSession.FromFramework = false;

                UserSession.CountryID = 1;
                UserSession.CountryIDCurrent = 1;
                UserSession.RoleID = RoleID;
                UserSession.UserID = UserID;
                UserSession.CompanyIDUser = CompanyID;
                UserSession.LocationIDUser = LocationID;
                
                

                UserSession.CompanyName = CompanyName;
                UserSession.Companyid = CompanyID;                

                UserSession.LocationName = LocationName;
                UserSession.locationid = LocationID;

                UserSession.GroupID = GroupID;
                
                UserSession.LanguageID = LanguageID;
                UserSession.UserFirstName = UserFName;
                UserSession.UserLastName = UserLName;
                UserSession.ThemeFolderPath = ThemeFolderPath;
                try
                {
                    DataSet dp = new DataSet();
                    dp = objgrpcmp.getsectorbycompany(UserSession.Companyid);
                    if (dp.Tables[0].Rows.Count > 0)
                    {
                        UserSession.sectorid = Convert.ToInt32(dp.Tables[0].Rows[0]["locationsectorid"]);
                    }
                }
                catch { }

                DataSet dsAnn = new DataSet();
                dsAnn = objAnnouncement.GetAnnouncement(UserSession.Companyid, Convert.ToInt32(Kowni.Common.BusinessLogic.BLMenu.ToolID.MasterFramework));
                if (dsAnn.Tables.Count > 0 && dsAnn.Tables[0].Rows.Count > 0)
                {
                    if (dsAnn.Tables[0].Rows[0]["AnnouncementType"].ToString() == "1")
                    {
                        UserSession.Announcement = dsAnn.Tables[0].Rows[0]["Announcement"].ToString();
                    }
                }
                else
                {
                    UserSession.Announcement = string.Empty;
                }
                Response.Redirect("datewisedatareport.aspx", true);
            }
        }
    }
}