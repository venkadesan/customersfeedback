﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FacilityAudit.BLL;
using KB = Kowni.BusinessLogic;
using KCB = Kowni.Common.BusinessLogic;
using FacilityAudit.Code;
using System.Text;
using BL;
using System.Configuration;

namespace Facility_Audit
{
    public partial class CompanySector : System.Web.UI.Page
    {
        #region Properties
        private bool IsAdd
        {
            get
            {
                if (!ID.HasValue)
                    return false;
                else if (ID.Value != 0)
                    return false;
                else
                    return true;
            }
        }


        private int? ID
        {
            get
            {
                if (ViewState["id"] == null)
                    ViewState["id"] = 0;
                return (int?)ViewState["id"];
            }

            set
            {
                ViewState["id"] = value;
            }
        }

        private bool DisableStatus
        {
            get
            {
                if (ViewState["disablestatus"] == null)
                    ViewState["disablestatus"] = 0;
                return (bool)ViewState["disablestatus"];
            }

            set
            {
                ViewState["disablestatus"] = value;
            }
        }

        #endregion

        BLGroupcompany objgrp = new BLGroupcompany();
        BLGroupcompany objgrpcmp = new BLGroupcompany();
        KB.BLUser.BLUserLocation objuserLocation1 = new KB.BLUser.BLUserLocation();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
              
                loadsector();
                loadrprtdetails();

             
            }
        }
        private void ButtonStatus(Common.ButtonStatus status)
        {
            ddlsector.ClearSelection();
            this.ddlsector.Items.FindByValue(UserSession.sectorid.ToString()).Selected = true;
            //ddlgroup.ClearSelection();


            if (status == Common.ButtonStatus.New)
            {
               
                this.lblError.Text = "";
                divmsg.Visible = false;
            }
            else if (status == Common.ButtonStatus.Edit)
            {

               
                this.lblError.Text = "";
                divmsg.Visible = false;
            }
            else if (status == Common.ButtonStatus.Cancel)
            {
              
              
                this.ID = 0;
                divrep.Visible = true;
              //  loadrprtdetails();
                this.lblError.Text = "";
                divmsg.Visible = false;
            }
            else if (status == Common.ButtonStatus.Save)
            {
               
             
                this.ID = 0;
                divrep.Visible = true;
                loadrprtdetails();

            }
        }
        public void NotifyMessages(string message, Common.ErrorType et)
        {
            divmsg.Visible = true;
            lblError.Text = message;
            if (et == Common.ErrorType.Error)
            {

            }
            else if (et == Common.ErrorType.Information)
            {

            }
        }
    
        private void loadsector()
        {
            try
            {
                DataSet ds = new DataSet();
                ds = objgrp.getsector();
                if (UserSession.sectorid == 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        ddlsector.DataSource = ds;
                        ddlsector.DataTextField = "locationsectorname";
                        ddlsector.DataValueField = "locationsectorid";
                        ddlsector.DataBind();
                        UserSession.sectorid = Convert.ToInt32(this.ddlsector.Items[0].Value);
                    }

                }
                else
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {

                        ddlsector.DataSource = ds;
                        ddlsector.DataTextField = "locationsectorname";
                        ddlsector.DataValueField = "locationsectorid";
                        ddlsector.DataBind();
                    }
                    this.ddlsector.ClearSelection();
                    this.ddlsector.Items.FindByValue(UserSession.sectorid.ToString()).Selected = true;

                }

            }
            catch { }
        }



        protected void btnsave_Click(object sender, EventArgs e)
        {
            try
            {

                string XmLIS = string.Empty;

                if (this.IsAdd)
                {
                    //if (objgrp.updatecompanynsector(Convert.ToInt32(UserSession.locationid), Convert.ToInt32(ddlsector.SelectedValue)) > 0)
                    //{
                    //    NotifyMessages("Saved successfully", Common.ErrorType.Information);
                    //}
                    //else
                    //{
                    //    NotifyMessages("Not saved sucessfully", Common.ErrorType.Error);
                    //    return;
                    //}

                    StringBuilder sb = new StringBuilder();
                    sb.Append("<root>");
                    for (int item = 0; item < rptrsector.Items.Count; item++)
                    {
                        HiddenField hCompanyID = rptrsector.Items[item].FindControl("hidcompanyid") as HiddenField;
                        Repeater rptLocation = rptrsector.Items[item].FindControl("rptrLocation") as Repeater;
                        for (int items = 0; items < rptLocation.Items.Count; items++)
                        {
                            CheckBox chkLocation = (CheckBox)rptLocation.Items[items].FindControl("chkLocation");

                            if (chkLocation.Checked)
                            {
                                HiddenField hLocationID = (HiddenField)rptLocation.Items[items].FindControl("hLocationID");

                                sb.Append(
                                       "<files sectorid=\"" + Convert.ToInt32(ddlsector.SelectedValue)+ "\"" +
                                       " companyid=\"" + Convert.ToInt32(hCompanyID.Value) + "\"" +
                                       " locationid=\"" + Convert.ToInt32(hLocationID.Value) + "\"" +
                                       " cuid=\"" + Convert.ToInt32(UserSession.UserID) + "\"" +
                                       " cdate=\"" + DateTime.Now.AddHours(Convert.ToInt32(ConfigurationManager.AppSettings["addhour"].ToString())).AddMinutes(Convert.ToInt32(ConfigurationManager.AppSettings["addminute"].ToString())) + "\" />");
                            }
                        }
                    }
                    sb.Append("</root>");
                    XmLIS = sb.ToString();
                    DataSet ds = new DataSet();
                    if ( objgrpcmp.addsectorlocation(XmLIS) > 0)
                    {
                        NotifyMessages("Saved Sucessfully", Common.ErrorType.Information);
                        ButtonStatus(Common.ButtonStatus.Save);

                    }
                 


                 
                   

                }
                else if (this.ID != null && this.ID.Value > 0)
                {
                    //    if (objgrp.updatecompanynsector(Convert.ToInt32(UserSession.locationid), Convert.ToInt32(ddlsector.SelectedValue)) > 0)
                    //    {
                    //        NotifyMessages("Updated successfully", Common.ErrorType.Information);
                    //    }
                    //    else
                    //    {
                    //        NotifyMessages("Not saved sucessfully", Common.ErrorType.Information);
                    //        return;
                    //    }
                    //}
                    UserSession.sectorid = Convert.ToInt32(ddlsector.SelectedValue);
                    StringBuilder sb = new StringBuilder();
                    sb.Append("<root>");
                    for (int item = 0; item < rptrsector.Items.Count; item++)
                    {
                        HiddenField hCompanyID = rptrsector.Items[item].FindControl("hidcompanyid") as HiddenField;
                        Repeater rptLocation = rptrsector.Items[item].FindControl("rptrLocation") as Repeater;
                        for (int items = 0; items < rptLocation.Items.Count; items++)
                        {
                            CheckBox chkLocation = (CheckBox)rptLocation.Items[items].FindControl("chkLocation");

                            if (chkLocation.Checked)
                            {
                                HiddenField hLocationID = (HiddenField)rptLocation.Items[items].FindControl("hLocationID");

                                sb.Append(
                                       "<files sectorid=\"" + Convert.ToInt32(ddlsector.SelectedValue) + "\"" +
                                       " companyid=\"" + Convert.ToInt32(hCompanyID.Value) + "\"" +
                                       " locationid=\"" + Convert.ToInt32(hLocationID.Value) +
                                       " cuid=\"" + Convert.ToInt32(UserSession.UserID) + "\"" +
                                       " cdate=\"" + DateTime.Now.AddHours(Convert.ToInt32(ConfigurationManager.AppSettings["addhour"].ToString())).AddMinutes(Convert.ToInt32(ConfigurationManager.AppSettings["addminute"].ToString())) + "\" />");
                            }
                        }
                    }
                    sb.Append("</root>");
                    XmLIS = sb.ToString();
                    DataSet ds = new DataSet();
                    if (objgrpcmp.addsectorlocation(XmLIS) > 0)
                    {
                        NotifyMessages("Saved Sucessfully", Common.ErrorType.Information);
                        ButtonStatus(Common.ButtonStatus.Save);

                    }
                }
                
            }
            catch { }
        }
        private void loadrprtdetails()
        {
            try
            {
                DataSet ds = new DataSet();
                ds = objgrpcmp.getcompanybygroupid(UserSession.GroupID, UserSession.RoleID);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    rptrsector.DataSource = ds;
                    rptrsector.DataBind();
                    ButtonStatus(Common.ButtonStatus.Cancel);
                }
                else
                {
                    rptrsector.DataSource = ds;
                    rptrsector.DataBind();
                    divmsg.Visible = false;
                }
                loaddata();
            }
            catch { }
        }
        private void loaddata()
        {
            try
            {
                DataSet Ds = new DataSet();
                Ds = objgrpcmp.getsectorlocation(Convert.ToInt32(ddlsector.SelectedValue));
                if (Ds.Tables[0].Rows.Count > 0)
                {
                    for (int item = 0; item < rptrsector.Items.Count; item++)
                    {
                        Repeater rptLocation = rptrsector.Items[item].FindControl("rptrLocation") as Repeater;
                        for (int items = 0; items < rptLocation.Items.Count; items++)
                        {
                            CheckBox chkLocation = (CheckBox)rptLocation.Items[items].FindControl("chkLocation");
                            HiddenField hLocationID = (HiddenField)rptLocation.Items[items].FindControl("hLocationID");
                            DataRow[] FilterValue = Ds.Tables[0].Select("locationid=" + hLocationID.Value + "");
                            if (FilterValue.Length > 0)
                                chkLocation.Checked = true;
                        }
                    }
                }
            }
            catch { }

        }

      
        protected void rptrsector_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HiddenField hCompanyID = (HiddenField)e.Item.FindControl("hidcompanyid");
                Repeater rptrLocation = (Repeater)e.Item.FindControl("rptrLocation");
                DataSet ds = new DataSet();
                int companyid = Convert.ToInt32(hCompanyID.Value);
                //ds = objuserLocation1.GetUserLocation(UserSession.UserID, companyid);
                ds = objgrpcmp.getlocationbylocationsector(UserSession.UserID, companyid,Convert.ToInt32(ddlsector.SelectedValue));
                if (ds.Tables[0].Rows.Count > 0)
                {
                    rptrLocation.DataSource = ds;
                    rptrLocation.DataBind();
                }

            }

        }

        protected void btncncl_Click(object sender, EventArgs e)
        {
            ButtonStatus(Common.ButtonStatus.Cancel);
            loadrprtdetails();
        }

        protected void ddllocation_SelectedIndexChanged(object sender, EventArgs e)
        {

            loadrprtdetails();
        }

        protected void ddlsector_SelectedIndexChanged(object sender, EventArgs e)
        {
            UserSession.sectorid = Convert.ToInt32(this.ddlsector.SelectedItem.Value);


            this.ddlsector.ClearSelection();
            this.ddlsector.Items.FindByValue(UserSession.sectorid.ToString()).Selected = true;
            loadrprtdetails();
        }


    }
}