﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FacilityAudit.BLL;
using BL;
using System.Data;
using FacilityAudit.Code;
using System.IO;

namespace Facility_Audit
{
    public partial class CateLocMapping : System.Web.UI.Page
    {
        #region "Datas"

        private DataTable ReportDet
        {
            get
            {
                if (ViewState["reportdetails"] == null)
                    ViewState["reportdetails"] = null;
                return (DataTable)ViewState["reportdetails"];
            }

            set
            {
                ViewState["reportdetails"] = value;
            }
        }

        #endregion

        BLCategory objauditcategory = new BLCategory();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindCategory();
                BindLocations();
                ddlcategory_SelectedIndexChanged(null, null);
            }
        }

        private void BindCategory()
        {
            try
            {
                DataSet dsCategory = new DataSet();
                //dsCategory = objauditcategory.GetCategoryDet(0);
                dsCategory = objauditcategory.GetCategoryDet_withGroup(0,UserSession.GroupID);
                if (dsCategory.Tables.Count > 0 && dsCategory.Tables[0].Rows.Count > 0)
                {
                    ddlcategory.DataSource = dsCategory.Tables[0];
                    ddlcategory.DataTextField = "categoryname";
                    ddlcategory.DataValueField = "categoryid";
                    ddlcategory.DataBind();
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, "error");
            }
        }

        private void BindLocations()
        {
            try
            {
                DataSet dsLocations = new DataSet();
                //dsLocations = objauditcategory.GetAllLocation();
                dsLocations = objauditcategory.GetAllLocation_withGroup(UserSession.GroupID);
                if (dsLocations.Tables.Count > 0 && dsLocations.Tables[0].Rows.Count > 0)
                {
                    lstlocation.DataSource = dsLocations.Tables[0];
                    lstlocation.DataTextField = "LocationName";
                    lstlocation.DataValueField = "LocationId";
                    lstlocation.DataBind();
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, "error");

            }
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            try
            {
                string locationxml = string.Empty;
                int categoryid = 0;
                if (!Common.DDVal(ddlcategory, out categoryid))
                {
                    NotifyMessages("Select Category", "error");
                    return;
                }
                if (!GetSelectedValues(lstlocation, "location", out locationxml))
                {
                    NotifyMessages("Select Location", "error");
                    return;
                }
                string[] result = objauditcategory.InsertCatLocMapping(categoryid, locationxml, UserSession.UserID);
                if (result[1] != "-1")
                {
                    ddlcategory_SelectedIndexChanged(null, null);
                    NotifyMessages(result[0], "info");
                }
                else
                {
                    NotifyMessages(result[0], "error");
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, "error");
            }
        }

        public void NotifyMessages(string message, string errortype)
        {
            divmsg.Visible = true;
            lblError.Text = message;
            if (errortype == "info")
            {
                divmsg.Attributes.Add("class", "alert alert-success fade in");
            }
            else if (errortype == "error")
            {
                divmsg.Attributes.Add("class", "alert alert-danger fade in");
            }
        }

        private bool GetSelectedValues(ListBox lstDetails, string colname, out string result)
        {
            try
            {
                result = string.Empty;
                int totalSelected = 0;
                DataTable dtSelectedValue = new DataTable(colname);
                dtSelectedValue.Columns.Add(colname + "ID", typeof(string));
                foreach (System.Web.UI.WebControls.ListItem li in lstDetails.Items)
                {
                    if (li.Selected == true)
                    {
                        //selectedvalues = selectedvalues + li.Value + ",";
                        dtSelectedValue.Rows.Add(li.Value);
                        totalSelected++;
                    }
                }
                string xmlString = string.Empty;
                using (TextWriter writer = new StringWriter())
                {
                    dtSelectedValue.WriteXml(writer);
                    xmlString = writer.ToString();
                }
                result = xmlString;
                return totalSelected > 0;
            }
            catch (Exception ex)
            {
                result = string.Empty;
                //NotifyMessages(ex.Message.ToString(), Common.ErrorType.Error);
                return false;
            }
        }

        protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                lblError.Text = string.Empty;
                divmsg.Visible = false;

                int categoryid = 0;
                if (!Common.DDVal(ddlcategory, out categoryid))
                {
                    NotifyMessages("Select Category", "error");
                    return;
                }
                DataSet dsMapDet = new DataSet();
                dsMapDet = objauditcategory.GetLocMapping(categoryid);
                lstlocation.ClearSelection();
                if (dsMapDet.Tables.Count > 0 && dsMapDet.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow drMap in dsMapDet.Tables[0].Rows)
                    {
                        string selectedvalue = drMap["LocationId"].ToString();
                        foreach (System.Web.UI.WebControls.ListItem li in lstlocation.Items)
                        {
                            if (li.Value == selectedvalue)
                            {
                                li.Selected = true;
                                break;
                            }
                        }
                    }
                }
                ReportDet = dsMapDet.Tables[0];
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, "error");
            }
        }

        protected void lnkdownload_Click(object sender, EventArgs e)
        {
            if (ReportDet == null)
            {
                NotifyMessages("No Record Found", "error");
                return;
            }
            DataSet dsreport = new DataSet();
            dsreport.Tables.Add(ReportDet);          
            if (dsreport.Tables.Count > 0 && dsreport.Tables[0].Rows.Count > 0)
            {
                DataView view = new DataView(dsreport.Tables[0]);
                DataTable dtlocations = view.ToTable(true, "CompanyName", "LocationName");

                GridView gvAsdetDet = new GridView();
                gvAsdetDet.DataSource = dtlocations;
                gvAsdetDet.DataBind();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=LocationDetails.xls");
                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                HtmlTextWriter hw = new HtmlTextWriter(sw);
                gvAsdetDet.RenderControl(hw);
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();
            }

        }
    }
}