﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using BL;
using FacilityAudit.Code;
namespace Facility_Audit
{
    public partial class RegionWiseContacts : System.Web.UI.Page
    {
        BLContacts objblCont = new BLContacts();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    BindDetails();
                    BindSbuDetails();
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, "error");
            }
        }

        private void BindDetails()
        {
            try
            {
                divmsg.Visible = false;
                lblError.Text = string.Empty;
                int regionid = 0;
                Common.DDVal(ddlregion, out regionid);
                DataSet dsContacts = new DataSet();
                //dsContacts = objblCont.GetRegionContacts(regionid);
                dsContacts = objblCont.GetRegionContacts_Group(regionid, UserSession.GroupID);
                if (dsContacts.Tables.Count > 0 && dsContacts.Tables[0].Rows.Count > 0)
                {

                    DataRow drContact = dsContacts.Tables[0].Rows[0];
                    txttraining.Text = drContact["TrainingMail"].ToString();
                    txtpayroll.Text = drContact["PayrollMail"].ToString();
                    txthr.Text = drContact["HRMail"].ToString();
                    txtscm.Text = drContact["SCMMail"].ToString();
                    txtcompliance.Text = drContact["ComplianceMail"].ToString();
                    txtqa.Text = drContact["QAMail"].ToString();
                    txtoperations.Text = drContact["OperationsMail"].ToString();
                    txtpanindia.Text = drContact["PanIndiaMails"].ToString();
                }
                else
                {
                    ClearControls();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void ClearControls()
        {
            txttraining.Text = string.Empty;
            txtpayroll.Text = string.Empty;
            txthr.Text = string.Empty;
            txtscm.Text = string.Empty;
            txtcompliance.Text = string.Empty;
            txtqa.Text = string.Empty;
            txtoperations.Text = string.Empty;
            txtpanindia.Text = string.Empty;
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            try
            {
                int regionid = 0;
                Common.DDVal(ddlregion, out regionid);
                //string[] results = objblCont.InsertUpdateRegionContacts(regionid, txttraining.Text.Trim(), txtpayroll.Text.Trim(), txthr.Text.Trim(), txtscm.Text.Trim(), txtcompliance.Text.Trim(), txtqa.Text.Trim(), txtoperations.Text.Trim(), txtpanindia.Text.Trim(), UserSession.UserID);
                string[] results = objblCont.InsertUpdateRegionContacts_Group(regionid, txttraining.Text.Trim(), txtpayroll.Text.Trim(), txthr.Text.Trim(), txtscm.Text.Trim(), txtcompliance.Text.Trim(), txtqa.Text.Trim(), txtoperations.Text.Trim(), txtpanindia.Text.Trim(), UserSession.UserID, UserSession.GroupID);
                if (results[1] != "-1")
                {
                    BindDetails();
                    NotifyMessages(results[0], "info");
                }
                else
                {
                    NotifyMessages(results[0], "error");
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, "error");
            }
        }

        public void NotifyMessages(string message, string errortype)
        {
            divmsg.Visible = true;
            lblError.Text = message;
            if (errortype == "info")
            {
                divmsg.Attributes.Add("class", "alert alert-success fade in");
            }
            else if (errortype == "error")
            {
                divmsg.Attributes.Add("class", "alert alert-danger fade in");
            }
        }

        protected void ddlregion_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindDetails();
                BindSbuDetails();
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, "error");
            }
        }

        private void BindSbuDetails()
        {
            int regionid = 0;
            Common.DDVal(ddlregion, out regionid);
            DataSet dsMapDet = new DataSet();
            //dsMapDet = objblCont.GetSbuContacts(regionid);
            dsMapDet = objblCont.GetSbuContacts_Group(regionid, UserSession.GroupID);
            if (dsMapDet != null && dsMapDet.Tables.Count > 0)
            {
                if (dsMapDet.Tables[0].Rows.Count > 0)
                {
                    rptrsbudet.DataSource = dsMapDet.Tables[0];
                    rptrsbudet.DataBind();
                }
                else
                {
                    rptrsbudet.DataSource = dsMapDet.Tables[1];
                    rptrsbudet.DataBind();
                }
            }
        }

        protected void bsavesbu_Click(object sender, EventArgs e)
        {
            try
            {
                int regionid = 0;
                Common.DDVal(ddlregion, out regionid);
                DataTable dtContacts = new DataTable();
                dtContacts.Columns.Add("SbuId", typeof(string));
                dtContacts.Columns.Add("SbuEmail", typeof(string));
                dtContacts.Columns.Add("AuditorEmail", typeof(string));
                if (rptrsbudet.Items.Count > 0)
                {
                    foreach (RepeaterItem riContacts in rptrsbudet.Items)
                    {
                        HiddenField hdnsbuid = (HiddenField)riContacts.FindControl("hdnSbuId");
                        TextBox txtsbuemail = (TextBox)riContacts.FindControl("txtsbuemail");
                        TextBox txtAuditorEmail = (TextBox)riContacts.FindControl("txtAuditorEmail");
                        if (hdnsbuid != null && txtsbuemail != null && txtAuditorEmail != null)
                        {
                            dtContacts.Rows.Add(hdnsbuid.Value, txtsbuemail.Text.Trim(), txtAuditorEmail.Text.Trim());
                        }
                    }

                    string xmlString = string.Empty;
                    using (TextWriter writer = new StringWriter())
                    {
                        dtContacts.TableName = "SbuDet";
                        dtContacts.WriteXml(writer);
                        xmlString = writer.ToString();
                    }

                    //string[] results = objblCont.InsertUpdateSbuContacts(regionid, xmlString);
                    string[] results = objblCont.InsertUpdateSbuContacts_Group(regionid, xmlString, UserSession.GroupID);
                    if (results[1] != "-1")
                    {
                        BindSbuDetails();
                        NotifyMessages(results[0], "info");
                    }
                    else
                    {
                        NotifyMessages(results[0], "error");
                    }
                }
                else
                {
                    NotifyMessages("No Record Found", "error");
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, "error");
            }
        }
    }
}