﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Mainpage.Master" AutoEventWireup="true"
    CodeBehind="TrainingReport.aspx.cs" Inherits="Facility_Audit.TrainingReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        function fnExcelReport(tablename) {
            var header = 'Training & Development Report';
            var tab_text = "<table border='2px'><tr><td style='font-weight:bold' align='center' colspan='9'>" + header + "</td></tr><tr>";
            var textRange; var j = 0;
            tab = document.getElementById(tablename); // id of table

            for (j = 0; j < tab.rows.length; j++) {
                tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
                //tab_text=tab_text+"</tr>";
            }

            tab_text = tab_text + "</table>";
            tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, ""); //remove if u want links in your table
            tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
            tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

            var ua = window.navigator.userAgent;
            var msie = ua.indexOf("MSIE ");

            if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
            {
                txtArea1.document.open("txt/html", "replace");
                txtArea1.document.write(tab_text);
                txtArea1.document.close();
                txtArea1.focus();

                sa = txtArea1.document.execCommand("SaveAs", true, "Training and Devlopment Details.xls");
            }
            else {             //other browser not tested on IE 11
                //sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));

                var a = document.createElement('a');
                //getting data from our div that contains the HTML table
                var data_type = 'data:application/vnd.ms-excel';

                a.href = data_type + ', ' + encodeURIComponent(tab_text);
                //setting the file name
                a.download = 'Training and Devlopment Details' + '.xls';
                //triggering the function
                a.click();
                //just in case, prevent default behaviour
                e.preventDefault();

            }

            return (sa);
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </cc1:ToolkitScriptManager>
    <div class="col-md-11">
        <div id="crumbs">
            <ul>
                <li class="active" style="width: 250px;"><a href="#">Training & Development Report</a></li>
            </ul>
        </div>
    </div>
    <div class="main" style="margin-left: 20px;">
        <div class=" col-md-5 col-xs-offset-4 alert alert-big alert-danger alert-dismissable fade in margin-top-10"
            runat="server" id="divmsg" visible="false">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                ×</button>
            <asp:Label runat="server" ID="lblError"></asp:Label>
        </div>
        <div class="row">
            <div class="form-box-content">
                <div class="row border-bottom margin-vertical-15">
                    <div class="col-md-8 col-xs-offset-4">
                        <div id="basicvalidations" class="form-horizontal">
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="">
                                </label>
                                <label class="col-sm-1 control-label" for="">
                                    From</label>
                                <div class="col-sm-3">
                                    <asp:TextBox ID="txtfrmdate" runat="server" class="form-control parsley-validated"></asp:TextBox>
                                    <cc1:CalendarExtender ID="calfromdate" runat="server" TargetControlID="txtfrmdate"
                                        Format="MM/dd/yyyy">
                                    </cc1:CalendarExtender>
                                </div>
                                <label class="col-sm-1 control-label margin-left15" for="">
                                    To</label>
                                <div class="col-sm-3">
                                    <asp:TextBox ID="txttodate" runat="server" class="form-control parsley-validated"></asp:TextBox>
                                    <cc1:CalendarExtender ID="caltodate" runat="server" TargetControlID="txttodate" Format="MM/dd/yyyy">
                                    </cc1:CalendarExtender>
                                </div>
                                <div class="col-sm-1">
                                    <asp:LinkButton ID="lnksearch" Style="text-decoration: none;" class="fa fa-2x fa-refresh"
                                        ToolTip="Refresh" runat="server" OnClick="lnksearch_Click"></asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <asp:LinkButton ID="lnkdownload" Style="float: right; text-decoration: none;" class="fa fa-2x fa-file-excel-o"
                OnClientClick="fnExcelReport('ContentPlaceHolder1_gridreport')" ToolTip="Export To Excel"
                runat="server"></asp:LinkButton>
            <div class="row">
                <asp:Panel ID="Panel1" runat="server" Height="100%" Width="100%">
                    <div class="col-md-12" id="divrep" runat="server">
                        <div class="tile-body nopadding margin-top-10">
                            <div class="table-responsive" id="customers">
                                <asp:GridView runat="server" class="table table-bordered" ID="gridreport" AutoGenerateColumns="true">
                                    <HeaderStyle CssClass="color green" />
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <!-- tile body -->
                <!-- /tile body -->
            </div>
        </div>
    </div>
    <iframe id="txtArea1" style="display: none"></iframe>
</asp:Content>
