﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using BL;
using FacilityAudit.Code;

namespace Facility_Audit
{
    public partial class RegionSBUMapping : System.Web.UI.Page
    {
        BLCategory objauditcategory = new BLCategory();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindSBU();
                ddlregion_SelectedIndexChanged(null, null);
            }
        }


        private void BindSBU()
        {





            try
            {
                //DataSet dsSbuDet = new DataSet();
                //dsSbuDet = objauditcategory.GetAllRegionSbu();
                //if (dsSbuDet.Tables.Count > 0 && dsSbuDet.Tables[0].Rows.Count > 0)
                //{
                //    lstsbu.DataSource = dsSbuDet.Tables[0];
                //    lstsbu.DataTextField = "locationsettingsname";
                //    lstsbu.DataValueField = "locationsettingsid";
                //    lstsbu.DataBind();
                //}

                int regionid = 0;
                Common.DDVal(ddlregion, out regionid);
                DataSet dsMapDet = new DataSet();
                //dsMapDet = objauditcategory.GetMappingRegSbuDet(regionid);
                dsMapDet = objauditcategory.GetMappingRegSbuDet_Group(regionid, UserSession.GroupID);
                if (dsMapDet.Tables.Count > 0 && dsMapDet.Tables[0].Rows.Count > 0)
                {
                    lstsbu.DataSource = dsMapDet.Tables[0];
                    lstsbu.DataTextField = "locationsettingsname";
                    lstsbu.DataValueField = "locationsettingsid";
                    lstsbu.DataBind();
                }

            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, "error");

            }
        }

        public void NotifyMessages(string message, string errortype)
        {
            divmsg.Visible = true;
            lblError.Text = message;
            if (errortype == "info")
            {
                divmsg.Attributes.Add("class", "alert alert-success fade in");
            }
            else if (errortype == "error")
            {
                divmsg.Attributes.Add("class", "alert alert-danger fade in");
            }
        }

        private bool GetSelectedValues(ListBox lstDetails, string colname, out string result)
        {
            try
            {
                result = string.Empty;
                int totalSelected = 0;
                DataTable dtSelectedValue = new DataTable(colname);
                dtSelectedValue.Columns.Add(colname + "ID", typeof(string));
                foreach (System.Web.UI.WebControls.ListItem li in lstDetails.Items)
                {
                    if (li.Selected == true)
                    {
                        //selectedvalues = selectedvalues + li.Value + ",";
                        dtSelectedValue.Rows.Add(li.Value);
                        totalSelected++;
                    }
                }
                string xmlString = string.Empty;
                using (TextWriter writer = new StringWriter())
                {
                    dtSelectedValue.WriteXml(writer);
                    xmlString = writer.ToString();
                }
                result = xmlString;
                return totalSelected > 0;
            }
            catch (Exception ex)
            {
                result = string.Empty;
                //NotifyMessages(ex.Message.ToString(), Common.ErrorType.Error);
                return false;
            }
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            try
            {
                string sbuxml = string.Empty;
                int regionid = 0;
                if (!Common.DDVal(ddlregion, out regionid))
                {
                    NotifyMessages("Select Region", "error");
                    return;
                }
                if (!GetSelectedValues(lstsbu, "sbu", out sbuxml))
                {
                    NotifyMessages("Select SBU", "error");
                    return;
                }
                //string[] result = objauditcategory.InsertRegSbuMapping(regionid, sbuxml, UserSession.UserID);
                string[] result = objauditcategory.InsertRegSbuMapping_Group(regionid, sbuxml, UserSession.UserID, UserSession.GroupID);
                if (result[1] != "-1")
                {
                    ddlregion_SelectedIndexChanged(null, null);
                    NotifyMessages(result[0], "info");
                }
                else
                {
                    NotifyMessages(result[0], "error");
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, "error");
            }
        }

        protected void ddlregion_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindSBU();
                lblError.Text = string.Empty;
                divmsg.Visible = false;

                int regionid = 0;
                if (!Common.DDVal(ddlregion, out regionid))
                {
                    NotifyMessages("Select Region", "error");
                    return;
                }
                DataSet dsMapDet = new DataSet();
                //dsMapDet = objauditcategory.GetMappingRegSbuDet(regionid);
                dsMapDet = objauditcategory.GetMappingRegSbuDet_Group(regionid, UserSession.GroupID);
                lstsbu.ClearSelection();
                if (dsMapDet.Tables.Count > 0 && dsMapDet.Tables[1].Rows.Count > 0)
                {
                    foreach (DataRow drMap in dsMapDet.Tables[1].Rows)
                    {
                        string selectedvalue = drMap["sbuId"].ToString();
                        foreach (System.Web.UI.WebControls.ListItem li in lstsbu.Items)
                        {
                            if (li.Value == selectedvalue)
                            {
                                li.Selected = true;
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, "error");
            }
        }
    }
}