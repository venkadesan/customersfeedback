﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Mainpage.Master" AutoEventWireup="true"
    CodeBehind="RegionWiseContacts.aspx.cs" Inherits="Facility_Audit.RegionWiseContacts" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="col-md-11">
        <div id="crumbs">
            <ul>
                <li class="active" style="width: 250px;"><a href="#">Region - Contacts</a></li>
            </ul>
        </div>
    </div>
    <div class="main">
        <div class=" col-md-5 col-xs-offset-4 alert alert-big alert-danger alert-dismissable fade in margin-top-10"
            runat="server" id="divmsg" visible="false">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                ×</button>
            <asp:Label runat="server" ID="lblError"></asp:Label>
        </div>
        <div class="col-lg-12">
            <div class="form-group">
                <label class="col-sm-2 control-label" for="">
                    Region</label>
                <div class="col-sm-10">
                    <asp:DropDownList ID="ddlregion" runat="server" class="form-control chosen-select"
                        AutoPostBack="True" OnSelectedIndexChanged="ddlregion_SelectedIndexChanged">
                        <asp:ListItem Text="North" Value="1"></asp:ListItem>
                        <asp:ListItem Text="South" Value="2"></asp:ListItem>
                        <asp:ListItem Text="East" Value="3"></asp:ListItem>
                        <asp:ListItem Text="West" Value="4"></asp:ListItem>
                    </asp:DropDownList>
                    Use ';' to Add Multiple receipents
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <section class="tile">
                 <section class="tile cornered margin-top-10">
        	    	<div class="tile-body nobottompadding">
                      <div class="row form-horizontal">                         
                      
                        <div class="col-lg-12">
                            <div class="form-group">
                                   <label class="col-sm-2 control-label" for="">Training</label>
                               <div class="col-sm-10">
                                 <asp:TextBox ID="txttraining" CssClass="form-control" runat="server"></asp:TextBox>
                               </div>
                            </div>
                        </div> 

                         <div class="col-lg-12">
                            <div class="form-group">
                                   <label class="col-sm-2 control-label" for="">Payroll</label>
                               <div class="col-sm-10">
                                 <asp:TextBox ID="txtpayroll" CssClass="form-control" runat="server"></asp:TextBox>
                               </div>
                            </div>
                        </div> 

                         <div class="col-lg-12">
                            <div class="form-group">
                                   <label class="col-sm-2 control-label" for="">HR</label>
                               <div class="col-sm-10">
                                 <asp:TextBox ID="txthr" CssClass="form-control" runat="server"></asp:TextBox>
                               </div>
                            </div>
                        </div>
                        
                         <div class="col-lg-12">
                            <div class="form-group">
                                   <label class="col-sm-2 control-label" for="">SCM</label>
                               <div class="col-sm-10">
                                 <asp:TextBox ID="txtscm" CssClass="form-control" runat="server"></asp:TextBox>
                               </div>
                            </div>
                        </div>  

                         <div class="col-lg-12">
                            <div class="form-group">
                                   <label class="col-sm-2 control-label" for="">Compliance</label>
                               <div class="col-sm-10">
                                 <asp:TextBox ID="txtcompliance" CssClass="form-control" runat="server"></asp:TextBox>
                               </div>
                            </div>
                        </div> 

                         <div class="col-lg-12">
                            <div class="form-group">
                                   <label class="col-sm-2 control-label" for="">QA</label>
                               <div class="col-sm-10">
                                 <asp:TextBox ID="txtqa" CssClass="form-control" runat="server"></asp:TextBox>
                               </div>
                            </div>
                        </div> 

                         <div class="col-lg-12">
                            <div class="form-group">
                                   <label class="col-sm-2 control-label" for="">Operations</label>
                               <div class="col-sm-10">
                                 <asp:TextBox ID="txtoperations" CssClass="form-control" runat="server"></asp:TextBox>
                               </div>
                            </div>
                        </div> 

                         <div class="col-lg-12">
                            <div class="form-group">
                                   <label class="col-sm-2 control-label" for="">Pan India</label>
                               <div class="col-sm-10">
                                 <asp:TextBox ID="txtpanindia" CssClass="form-control" Rows="3" TextMode="MultiLine" runat="server"></asp:TextBox>
                               </div>
                            </div>
                        </div> 

                      </div>
                    </div>
                
                <div class="tile-footer color cyan text-muted">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="col-md-offset-5">
                                <asp:Button ID="btnsave" class="btn btn-primary" runat="server" Text="Save" 
                                    onclick="btnsave_Click" />
                            </div>
                        </div>
                    </div>
                </div>
                 </section> </section>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <section class="tile">
                 <section class="tile cornered margin-top-10">
                <div class="tile-body nopadding margin-top-10">
                    <div class="table-responsive">
                        <asp:Repeater ID="rptrsbudet" DataSourceID="" runat="server">
                            <HeaderTemplate>
                                <table width="100%" border="0" cellspacing="0" class="table table-bordered" cellpadding="0">
                                    <tbody>
                                        <tr>
                                            <th width="5%">
                                                Sl.No
                                            </th>
                                            <th>
                                                SBU
                                            </th>
                                            <th>
                                                SBU Email
                                            </th>
                                            <th>
                                                Auditor Email
                                            </th>
                                        </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label2" runat="server" Text='<%#(((RepeaterItem)Container).ItemIndex+1).ToString() %>' />
                                    </td>
                                    <td>
                                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("sbu") %>' />
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtsbuemail" CssClass="form-control" runat="server" Text='<%# Eval("SbuEmail") %>'></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtAuditorEmail" CssClass="form-control" runat="server" Text='<%# Eval("AuditorEmail") %>'></asp:TextBox>
                                        <asp:HiddenField ID="hdnSbuId" Value='<%# Eval("SbuId") %>' runat="server" />
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                </div>
                <div class="tile-footer color cyan text-muted">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="col-md-offset-5">
                                <asp:Button ID="bsavesbu" class="btn btn-primary" runat="server" Text="Save" OnClick="bsavesbu_Click" />
                            </div>
                        </div>
                    </div>
                </div>
                 </section> </section>
            </div>
        </div>
    </div>
</asp:Content>
