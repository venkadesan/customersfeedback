﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FacilityAudit.Code;
using KB = Kowni.BusinessLogic;
using KCB = Kowni.Common.BusinessLogic;
using System.Data;
using System.Configuration;
using System.Text;
using FacilityAudit.BLL;
using Kowni.Common.BusinessLogic;

namespace Facility_Audit
{
    public partial class Mainpage : System.Web.UI.MasterPage
    {
        BLGroupcompany objgrpcmp = new BLGroupcompany();
        KB.BLCompanyMainForms objMainForms = new KB.BLCompanyMainForms();
        KB.BLCompanySubForms objSubForms = new KB.BLCompanySubForms();
        KB.BLUser.BLUserLocation objuserLocation1 = new KB.BLUser.BLUserLocation();
        KB.BLUser.BLUserCompany objuserCompany = new KB.BLUser.BLUserCompany();


        KB.BLRoleForms roleform = new KB.BLRoleForms();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string versionno = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
                llvno.Text = versionno;

                //loadsbu();

                bindlogo();
                LoadDDPermittedCompany();
                LoadDDPermittedGroup();
                LoadMainForms();

            }

            // lbladmin.Text = UserSession.RoleName;

        }

        private void bindlogo()
        {
            try
            {
                imgBanner.ImageUrl = ConfigurationManager.AppSettings["companylogourl"].ToString() + (UserSession.Companyid) + ".png";
            }
            catch
            {
            }
        }


        private void LoadDDPermittedGroup()
        {
            DataSet ds = new DataSet();
            ds = objuserLocation1.GetUserLocation(UserSession.UserID, UserSession.Companyid);
            //            ds = objgrpcmp.getgroup(UserSession.UserID);
            if (UserSession.locationid == 0)
            {
                DataTable dFiltered = ds.Tables[0].Clone();
                //DataTable dFiltered = ds;

                //DataRow[] dRowUpdates = ds.Tables[0].Select(" UserLocationID > 0", "GroupName");
                //foreach (DataRow dr in dRowUpdates)
                //    dFiltered.ImportRow(dr);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    this.ddlgrp.DataSource = ds;
                    this.ddlgrp.DataTextField = "LocationName";
                    this.ddlgrp.DataValueField = "LocationID";
                    this.ddlgrp.DataBind();
                }

                try
                {
                    //this.ddlgrp.ClearSelection();
                    //this.ddlgrp.Items.FindByValue(UserSession.GroupID.ToString()).Selected = true;
                }
                catch
                {
                    UserSession.GroupID = 0;
                    UserSession.LocationName = string.Empty;

                    if (dFiltered.Rows.Count > 0)
                    {
                        UserSession.locationid = Convert.ToInt32(this.ddlgrp.Items[0].Value);
                        UserSession.LocationName = this.ddlgrp.Items[0].Text;
                    }
                }

                try
                {
                    DataSet dp = new DataSet();
                    dp = objgrpcmp.getsectorbycompany(UserSession.locationid);
                    if (dp.Tables[0].Rows.Count > 0)
                    {
                        UserSession.sectorid = Convert.ToInt32(dp.Tables[0].Rows[0]["locationsectorid"]);
                    }
                }
                catch { }
            }
            else
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    this.ddlgrp.DataSource = ds;
                    this.ddlgrp.DataTextField = "LocationName";
                    this.ddlgrp.DataValueField = "LocationID";
                    this.ddlgrp.DataBind();
                    try
                    {
                        this.ddlgrp.ClearSelection();
                        this.ddlgrp.Items.FindByValue(UserSession.locationid.ToString()).Selected = true;
                    }
                    catch
                    {
                        UserSession.locationid = Convert.ToInt32(ddlgrp.SelectedValue);
                        this.ddlgrp.Items.FindByValue(UserSession.locationid.ToString()).Selected = true;
                    }
                }

                try
                {
                    DataSet dp = new DataSet();
                    dp = objgrpcmp.getsectorbycompany(UserSession.locationid);
                    if (dp.Tables[0].Rows.Count > 0)
                    {
                        UserSession.sectorid = Convert.ToInt32(dp.Tables[0].Rows[0]["locationsectorid"]);
                    }
                    else
                    {
                        UserSession.sectorid = 0;
                    }
                }
                catch { }
            }

        }


        private void loadsbu()
        {
            DataSet ds = new DataSet();
            //ds = objuserLocation1.GetUserLocation(UserSession.RoleID, UserSession.CompanyIDCurrent);
            ds = objgrpcmp.getgroup(UserSession.UserID);

            DataTable dFiltered = ds.Tables[0].Clone();
            //DataTable dFiltered = ds;

            //DataRow[] dRowUpdates = ds.Tables[0].Select(" UserLocationID > 0", "GroupName");
            //foreach (DataRow dr in dRowUpdates)
            //    dFiltered.ImportRow(dr);
            if (ds.Tables[0].Rows.Count > 0)
            {
                this.ddlgrp.DataSource = ds;
                this.ddlgrp.DataTextField = "GroupName";
                this.ddlgrp.DataValueField = "GroupID";
                this.ddlgrp.DataBind();
            }

            try
            {
                //this.ddlgrp.ClearSelection();
                //this.ddlgrp.Items.FindByValue(UserSession.GroupID.ToString()).Selected = true;
            }
            catch
            {
                UserSession.GroupID = 0;
                UserSession.LocationName = string.Empty;

                if (dFiltered.Rows.Count > 0)
                {
                    UserSession.GroupID = Convert.ToInt32(this.ddlgrp.Items[0].Value);
                    UserSession.GroupName = this.ddlgrp.Items[0].Text;
                }
            }
        }


        private void LoadDDPermittedCompany()
        {
            DataSet ds = new DataSet();
            //ds = objRoleComp.GetRoleCompany(UserSession.RoleID, UserSession.CompanyIDUser);
            ds = objgrpcmp.getcompanybygroupid(UserSession.GroupID, UserSession.RoleID);
            // ds = objgrpcmp.getusercompany(UserSession.UserID);
            if (UserSession.Companyid == 0)
            {
                DataTable dFiltered = ds.Tables[0].Clone();
                //DataRow[] dRowUpdates = ds.Tables[0].Select(" UserCompanyID > 0", "companyname");
                //foreach (DataRow dr in dRowUpdates)
                //    dFiltered.ImportRow(dr);
                if (ds.Tables[0].Rows.Count > 0)
                {

                    this.ddlcompany.DataSource = ds;
                    this.ddlcompany.DataTextField = "CompanyName";
                    this.ddlcompany.DataValueField = "CompanyID";
                    this.ddlcompany.DataBind();
                }

                //
                //get companies Country ID
                //
                DataSet dscomp = new DataSet();
                dscomp = objgrpcmp.getcompanybycompanyid(UserSession.Companyid);
                try
                {
                    UserSession.Settings.Images.ShowCompanyImage = Convert.ToBoolean(dscomp.Tables[0].Rows[0]["showimage"]);


                    UserSession.GroupID = Convert.ToInt32(dscomp.Tables[0].Rows[0]["groupID"]);
                }
                catch { }


                if (ds.Tables[0].Rows.Count > 0)
                {
                    UserSession.Companyid = Convert.ToInt32(this.ddlcompany.Items[0].Value);
                    UserSession.CompanyName = this.ddlcompany.Items[0].Text;
                }



                try
                {
                    DataSet dp = new DataSet();
                    dp = objgrpcmp.getsectorbycompany(UserSession.Companyid);
                    if (dp.Tables[0].Rows.Count > 0)
                    {
                        UserSession.sectorid = Convert.ToInt32(dp.Tables[0].Rows[0]["locationsectorid"]);
                    }
                }
                catch { }
            }
            else
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    this.ddlcompany.DataSource = ds;
                    this.ddlcompany.DataTextField = "CompanyName";
                    this.ddlcompany.DataValueField = "CompanyID";
                    this.ddlcompany.DataBind();
                }
                try
                {
                    this.ddlcompany.ClearSelection();
                    this.ddlcompany.Items.FindByValue(UserSession.Companyid.ToString()).Selected = true;
                }
                catch
                {
                    UserSession.Companyid = Convert.ToInt32(ddlcompany.SelectedValue);
                    this.ddlcompany.Items.FindByValue(UserSession.Companyid.ToString()).Selected = true;
                }
                try
                {
                    DataSet dp = new DataSet();
                    dp = objgrpcmp.getsectorbycompany(UserSession.Companyid);
                    if (dp.Tables[0].Rows.Count > 0)
                    {
                        UserSession.sectorid = Convert.ToInt32(dp.Tables[0].Rows[0]["locationsectorid"]);
                    }
                }
                catch { }

            }

        }





        protected void ddlcompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                UserSession.Companyid = Convert.ToInt32(this.ddlcompany.SelectedItem.Value);

                UserSession.locationid = 0;
                try
                {
                    DataSet dp = new DataSet();
                    dp = objgrpcmp.getsectorbycompany(UserSession.Companyid);
                    if (dp.Tables[0].Rows.Count > 0)
                    {
                        UserSession.sectorid = Convert.ToInt32(dp.Tables[0].Rows[0]["locationsectorid"]);
                    }
                }
                catch { }

                Response.Redirect(Request.RawUrl);
                //UserSession.Companyid = Convert.ToInt32(this.ddlcompany.SelectedItem.Value);
                //UserSession.CompanyName = this.ddlcompany.SelectedItem.Text.Trim();

                //this.ddlcompany.ClearSelection();
                //this.ddlcompany.Items.FindByValue(UserSession.Companyid.ToString()).Selected = true;
                //LoadDDPermittedCompany();

                ////
                ////get companies Country ID
                ////
                //DataSet ds = new DataSet();
                //ds = objgrpcmp.getcompanybycompanyid(UserSession.Companyid);
                //try
                //{
                //    UserSession.Settings.Images.ShowCompanyImage = Convert.ToBoolean(ds.Tables[0].Rows[0]["showimage"]);


                //    UserSession.GroupID = Convert.ToInt32(ds.Tables[0].Rows[0]["groupID"]);
                //}
                //catch { }

                //Response.Redirect(Request.RawUrl);
            }
            catch { }
        }

        protected void ddlgrp_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                UserSession.locationid = Convert.ToInt32(this.ddlgrp.SelectedItem.Value);
                UserSession.LocationName = this.ddlgrp.SelectedItem.Text.Trim();

                this.ddlgrp.ClearSelection();
                this.ddlgrp.Items.FindByValue(UserSession.locationid.ToString()).Selected = true;

                DataSet ds = new DataSet();
                ds = objgrpcmp.getgroup(UserSession.UserID);
                UserSession.Groupcompanyid = Convert.ToInt32(ds.Tables[0].Rows[0]["GroupID"].ToString());

                try
                {
                    DataSet dp = new DataSet();
                    dp = objgrpcmp.getsectorbycompany(UserSession.locationid);
                    if (dp.Tables[0].Rows.Count > 0)
                    {
                        UserSession.sectorid = Convert.ToInt32(dp.Tables[0].Rows[0]["locationsectorid"]);
                    }
                    else
                    {
                        UserSession.sectorid = 0;
                    }
                }
                catch { }
                Response.Redirect(Request.RawUrl);
            }
            catch { }
        }


        public void LoadMainForms()
        {
            try
            {
                DataSet dsRoel = roleform.GetRoleFormShow(UserSession.RoleID, UserSession.Companyid, Convert.ToInt32(ConfigurationManager.AppSettings["toolid"].ToString()));

                dsRoel.Tables[0].DefaultView.RowFilter = "isvisible=True";
                DataTable mainID = dsRoel.Tables[0].DefaultView.ToTable(true, "mainformsID");

                dsRoel.Tables[0].DefaultView.RowFilter = "isvisible=True";
                DataTable subID = dsRoel.Tables[0].DefaultView.ToTable(true, "subformsID");


                DataSet Ds = new DataSet();
                DataSet Ds1 = new DataSet();
                string Path = string.Empty;
                int MainFormD = 0;
                try
                {
                    Path = System.Web.HttpContext.Current.Request.Url.AbsolutePath;
                    System.IO.FileInfo Info = new System.IO.FileInfo(Path);
                    Path = Info.Name;
                }
                catch { }

                Ds = objMainForms.GetCompanyMainForms(UserSession.Companyid, Convert.ToInt32(ConfigurationManager.AppSettings["toolid"].ToString()), 1);
                //Ds = dsRoel.Tables[0].Select()
                string[] selectedValues = { "ifazility" };
                string[] FilterMenu = { "" };
                bool isAdmin = selectedValues.Contains(UserSession.UserLastName.ToString().ToLower());
                if (!isAdmin)
                {
                    for (int i = 0; i < FilterMenu.Length; i++)
                    {
                        foreach (DataRow row in Ds.Tables[0].Rows)
                        {
                            if (row["MainFormsPageName"].ToString().ToLower() == FilterMenu[i].ToString())
                                row.Delete();
                        }
                        Ds.Tables[0].AcceptChanges();
                    }
                }
                for (int k = 0; k < Ds.Tables[0].Rows.Count; k++)
                {
                    string MainPageName = string.Empty;
                    string[] MainPageIDValue = new string[3];
                    try
                    {
                        MainPageName = Ds.Tables[0].Rows[k]["MainFormsPageName"].ToString();
                        MainPageIDValue = MainPageName.Split('.');
                    }
                    catch { }
                    DataRow[] dr = new DataRow[10];
                    Ds1 = objSubForms.GetCompanySubForms(UserSession.Companyid, Convert.ToInt32(Ds.Tables[0].Rows[k]["MainFormsID"].ToString()), 1);
                    if (Ds1.Tables[0].Rows.Count > 0)
                    {
                        dr = Ds1.Tables[0].Select("SubFormsPageName='" + Path + "'");

                    }
                    int result1 = dr.Count(s => s != null);
                    if (result1 > 0)
                    {
                        MainFormD = Convert.ToInt32(Ds.Tables[0].Rows[k]["MainFormsID"].ToString());
                        break;
                    }
                }
                Ds = new DataSet();
                Ds = objMainForms.GetCompanyMainForms(UserSession.Companyid, Convert.ToInt32(ConfigurationManager.AppSettings["toolid"].ToString()), 1);

                string main = "";
                foreach (DataRow mainformID in mainID.Rows)
                {
                    main = main + mainformID["mainformsID"].ToString() + ",";
                }
                main.TrimEnd(',');

                Ds.Tables[0].DefaultView.RowFilter = "MainFormsID in (" + main + ")";

                DataTable dsTable = Ds.Tables[0].DefaultView.ToTable();

                if (!isAdmin)
                {
                    for (int i = 0; i < FilterMenu.Length; i++)
                    {
                        foreach (DataRow row in Ds.Tables[0].Rows)
                        {
                            if (row["MainFormsPageName"].ToString().ToLower() == FilterMenu[i].ToString())
                                row.Delete();
                        }
                        Ds.Tables[0].AcceptChanges();
                    }
                }
                StringBuilder objBuilder = new StringBuilder();
                for (int i = 0; i < dsTable.Rows.Count; i++)
                {
                    string MainPageID = string.Empty;
                    string mainmenuicon = string.Empty;
                    string[] MainPageIDValue = new string[3];
                    try
                    {
                        MainPageID = dsTable.Rows[i]["MainFormsPageName"].ToString();
                        MainPageIDValue = MainPageID.Split('.');
                        mainmenuicon = Ds.Tables[0].Rows[i]["cssclass"].ToString();
                    }
                    catch { }

                    if (Convert.ToInt32(dsTable.Rows[i]["MainFormsID"]) == MainFormD)
                    {
                        objBuilder.Append("<li class=\"dropdown open\" id=\"p" + MainPageIDValue[0].ToString() + "\" runat=\"server\"><a href=\"" + dsTable.Rows[i]["MainFormsPageName"].ToString() + "\" class=\"dropdown-toggle\"");
                    }
                    else
                    {
                        objBuilder.Append("<li class=\"dropdown\" id=\"p" + MainPageIDValue[0].ToString() + "\" runat=\"server\"><a href=\"" + dsTable.Rows[i]["MainFormsPageName"].ToString() + "\" class=\"dropdown-toggle\"");
                    }
                    objBuilder.Append("data-toggle=\"dropdown\" title=\"" + dsTable.Rows[i]["MainFormsPageText"].ToString() + "\"><i class=\"" + mainmenuicon + "\"><span class=\"overlay-label red\">");
                    objBuilder.Append("</span></i>" + dsTable.Rows[i]["MainFormsPageText"].ToString() + "<b class=\"fa fa-angle-left dropdown-arrow\"></b> </a>");
                    objBuilder.Append("<ul class=\"dropdown-menu\">");
                    Ds1 = new DataSet();
                    Ds1 = objSubForms.GetCompanySubForms(UserSession.Companyid, Convert.ToInt32(dsTable.Rows[i]["MainFormsID"].ToString()), 1);

                    string sub = "";
                    foreach (DataRow mainformID in subID.Rows)
                    {
                        sub = sub + mainformID["SubFormsID"].ToString() + ",";
                    }
                    sub.TrimEnd(',');

                    Ds1.Tables[0].DefaultView.RowFilter = "SubFormsID in (" + sub + ")";
                    DataTable ds1Table = Ds1.Tables[0].DefaultView.ToTable();

                    //foreach (DataRow row in Ds1.Tables[0].Rows)
                    //{
                    //for (int count = 0; count < Ds1.Tables[0].Rows.Count; count++)
                    //    foreach (DataRow sub in subID.Rows)
                    //    {
                    //        if (Convert.ToInt32(Ds1.Tables[0].Rows[count]["SubFormsID"].ToString()) != Convert.ToInt32(sub["SubFormsID"].ToString()))
                    //        {
                    //            Ds1.Tables[0].Rows[count].Delete();
                    //            Ds1.Tables[0].AcceptChanges();
                    //            break;
                    //        }
                    //    }
                    //}

                    if (ds1Table.Rows.Count > 0)
                    {
                        for (int j = 0; j < ds1Table.Rows.Count; j++)
                        {
                            string SubPageID = string.Empty;
                            string[] SubPageIDValue = new string[3];
                            string sidemenuicon = string.Empty;
                            try
                            {
                                SubPageID = dsTable.Rows[i]["MainFormsPageName"].ToString();
                                SubPageIDValue = SubPageID.Split('.');
                                sidemenuicon = Ds1.Tables[0].Rows[j]["cssclass"].ToString();
                            }
                            catch { }
                            if (Path == ds1Table.Rows[j]["SubFormsPageName"].ToString())
                                objBuilder.Append("<li runat=\"server\" id=\"p" + SubPageIDValue[0].ToString() + "\" class=\"active\"><a id=\"" + ds1Table.Rows[j]["SubFormsID"].ToString() + "\" title=\"" + ds1Table.Rows[j]["SubFormsPageText"].ToString() + "\" href=\"" + ds1Table.Rows[j]["SubFormsPageName"].ToString() + "\" runat=\"server\">");
                            else
                                objBuilder.Append("<li runat=\"server\" id=\"p" + SubPageIDValue[0].ToString() + "\"><a id=\"" + ds1Table.Rows[j]["SubFormsID"].ToString() + "\" title=\"" + ds1Table.Rows[j]["SubFormsPageText"].ToString() + "\" href=\"" + ds1Table.Rows[j]["SubFormsPageName"].ToString() + "\" runat=\"server\">");
                            objBuilder.Append("<i class=\"" + sidemenuicon + "\"><span class=\"overlay-label red80\"></span></i>" + ds1Table.Rows[j]["SubFormsPageText"].ToString() + "</a></li>");
                        }
                    }

                    objBuilder.Append("</ul>");
                    objBuilder.Append("</li>");
                }
                ltrlMenu.Text = objBuilder.ToString();
                lblusername.Text = UserSession.UserFirstName;
            }
            catch { }
        }

        //public void LoadMainForms()
        //{
        //    DataSet Ds = new DataSet();
        //    DataSet Ds1 = new DataSet();
        //    string Path = string.Empty;
        //    int MainFormD = 0;
        //    try
        //    {
        //        Path = System.Web.HttpContext.Current.Request.Url.AbsolutePath;
        //        System.IO.FileInfo Info = new System.IO.FileInfo(Path);
        //        Path = Info.Name;
        //    }
        //    catch { }
        //    Ds = objMainForms.GetCompanyMainForms(UserSession.Companyid, Convert.ToInt32(ConfigurationManager.AppSettings["toolid"].ToString()), 1);

        //    for (int k = 0; k < Ds.Tables[0].Rows.Count; k++)
        //    {
        //        string MainPageName = string.Empty;
        //        string[] MainPageIDValue = new string[3];
        //        try
        //        {
        //            MainPageName = Ds.Tables[0].Rows[k]["MainFormsPageName"].ToString();
        //            MainPageIDValue = MainPageName.Split('.');
        //        }
        //        catch { }
        //        DataRow[] dr = new DataRow[10];
        //        Ds1 = objSubForms.GetCompanySubForms(UserSession.Companyid, Convert.ToInt32(Ds.Tables[0].Rows[k]["MainFormsID"].ToString()), 1);
        //        if (Ds1.Tables[0].Rows.Count > 0)
        //        {
        //            dr = Ds1.Tables[0].Select("SubFormsPageName='" + Path + "'");

        //        }
        //        int result1 = dr.Count(s => s != null);
        //        if (result1 > 0)
        //        {
        //            MainFormD = Convert.ToInt32(Ds.Tables[0].Rows[k]["MainFormsID"].ToString());
        //            break;
        //        }
        //    }
        //    Ds = new DataSet();
        //    Ds = objMainForms.GetCompanyMainForms(UserSession.Companyid, Convert.ToInt32(ConfigurationManager.AppSettings["toolid"].ToString()), 1);


        //    StringBuilder objBuilder = new StringBuilder();
        //    for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
        //    {
        //        string MainPageID = string.Empty;
        //        string[] MainPageIDValue = new string[3];
        //        string mainmenuicon = string.Empty;
        //        try
        //        {
        //            MainPageID = Ds.Tables[0].Rows[i]["MainFormsPageName"].ToString();
        //            MainPageIDValue = MainPageID.Split('.');
        //            mainmenuicon = Ds.Tables[0].Rows[i]["cssclass"].ToString();
        //        }
        //        catch { }

        //        if (Convert.ToInt32(Ds.Tables[0].Rows[i]["MainFormsID"]) == MainFormD)
        //        {
        //            objBuilder.Append("<li class=\"dropdown open\" id=\"p" + MainPageIDValue[0].ToString() + "\" runat=\"server\"><a href=\"" + Ds.Tables[0].Rows[i]["MainFormsPageName"].ToString() + "\" class=\"dropdown-toggle\"");
        //        }
        //        else
        //        {
        //            objBuilder.Append("<li class=\"dropdown\" id=\"p" + MainPageIDValue[0].ToString() + "\" runat=\"server\"><a href=\"" + Ds.Tables[0].Rows[i]["MainFormsPageName"].ToString() + "\" class=\"dropdown-toggle\"");
        //        }
        //        objBuilder.Append("data-toggle=\"dropdown\" title=\"" + Ds.Tables[0].Rows[i]["MainFormsPageText"].ToString() + "\"><i class=\""+mainmenuicon+"\"><span class=\"overlay-label red\">");
        //        objBuilder.Append("</span></i>" + Ds.Tables[0].Rows[i]["MainFormsPageText"].ToString() + "<b class=\"fa fa-angle-left dropdown-arrow\"></b> </a>");
        //        objBuilder.Append("<ul class=\"dropdown-menu\">");
        //        Ds1 = new DataSet();
        //        Ds1 = objSubForms.GetCompanySubForms(UserSession.Companyid, Convert.ToInt32(Ds.Tables[0].Rows[i]["MainFormsID"].ToString()), 1);
        //        if (Ds1.Tables[0].Rows.Count > 0)
        //        {
        //            for (int j = 0; j < Ds1.Tables[0].Rows.Count; j++)
        //            {
        //                string SubPageID = string.Empty;
        //                string[] SubPageIDValue = new string[3];
        //                string sidemenuicon = string.Empty;
        //                try
        //                {
        //                    SubPageID = Ds.Tables[0].Rows[i]["MainFormsPageName"].ToString();
        //                    SubPageIDValue = SubPageID.Split('.');
        //                    sidemenuicon = Ds1.Tables[0].Rows[j]["cssclass"].ToString();
        //                }
        //                catch { }
        //                if (Path == Ds1.Tables[0].Rows[j]["SubFormsPageName"].ToString())
        //                    objBuilder.Append("<li runat=\"server\" id=\"p" + SubPageIDValue[0].ToString() + "\" class=\"active\"><a id=\"" + Ds1.Tables[0].Rows[j]["SubFormsID"].ToString() + "\" title=\"" + Ds1.Tables[0].Rows[j]["SubFormsPageText"].ToString() + "\" href=\"" + Ds1.Tables[0].Rows[j]["SubFormsPageName"].ToString() + "\" runat=\"server\">");
        //                else
        //                    objBuilder.Append("<li runat=\"server\" id=\"p" + SubPageIDValue[0].ToString() + "\"><a id=\"" + Ds1.Tables[0].Rows[j]["SubFormsID"].ToString() + "\" title=\"" + Ds1.Tables[0].Rows[j]["SubFormsPageText"].ToString() + "\" href=\"" + Ds1.Tables[0].Rows[j]["SubFormsPageName"].ToString() + "\" runat=\"server\">");
        //                objBuilder.Append("<i class=\""+sidemenuicon+"\"><span class=\"overlay-label red80\"></span></i>" + Ds1.Tables[0].Rows[j]["SubFormsPageText"].ToString() + "</a></li>");
        //            }
        //        }
        //        objBuilder.Append("</ul>");
        //        objBuilder.Append("</li>");
        //    }
        //    ltrlMenu.Text = objBuilder.ToString();
        //}
    }
}