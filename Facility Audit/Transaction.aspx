﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Mainpage.Master" AutoEventWireup="true"
    CodeBehind="Transaction.aspx.cs" Inherits="Facility_Audit.Transaction" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

<style type="text/css" >

.test tr input
{
    border:1px solid red;
    margin-right:10px;
    margin-left:10px;
    padding-right:10px;
}


</style>

    <script language="javascript" type="text/javascript">

        function expandcollapse(obj, row) {

            var div = document.getElementById(obj);

            var img = document.getElementById('img' + obj);


            if (div.style.display == "none") {
                div.style.display = "block";
                img.src = "AppThemes/images/minus.png";
            }
            else {
                div.style.display = "none";
                img.src = "AppThemes/images/plus.png";

            }
        }
    </script>
    <script type="text/javascript" src="Script/jquery-1.5.1min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js">
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="submenu">
        <h1>
            Facility Audit</h1>
     
    </div>
    <asp:DropDownList runat="server" ID="ddlCheckAnswer" Visible="false">
    </asp:DropDownList>
    <div class="main" style="margin-left: 20px;">
        <div class=" col-md-5 col-xs-offset-4 alert alert-big alert-danger alert-dismissable fade in margin-top-10"
            runat="server" id="divmsg" visible="false">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                ×</button>
            <asp:Label runat="server" ID="lblError"></asp:Label></div>
        <div class="row">
            <div class="col-md-12">
                <section class="tile">


                  <!-- tile header -->
                  
 
              
          <section class="tile cornered margin-top-10">
        			<div class="tile-body nobottompadding">
                    <div class="row form-horizontal">
                           <div class="col-lg-12">

                            <div class="form-group">
                        <label class="col-sm-2 control-label" for=""> SBU:</label>
                        <div class="col-sm-10">
                         <asp:DropDownList ID="ddlsbu" runat="server"  AutoPostBack="True"  
                                parsley-validation-minlength="1" parsley-type="email" parsley-minlength="4" 
                                parsley-required="true" parsley-trigger="change" 
                                class="form-control parsley-validated"  
                                kl_virtual_keyboard_secure_input="on"  
                                onselectedindexchanged="ddlsbu_SelectedIndexChanged" ></asp:DropDownList>
                                
                               
                            
                         
                          
                        </div>
                      </div>

                      
        <div class="form-group">
                        <label class="col-sm-2 control-label" for=""> Company:</label>
                        <div class="col-sm-10">
                         <asp:DropDownList ID="ddlcompany" runat="server"  AutoPostBack="True"  
                                parsley-validation-minlength="1" parsley-type="email" parsley-minlength="4" 
                                parsley-required="true" parsley-trigger="change" 
                                class="form-control parsley-validated"  
                                kl_virtual_keyboard_secure_input="on" 
                                onselectedindexchanged="ddlcompany_SelectedIndexChanged" ></asp:DropDownList>
                                
                               
                            
                         
                          
                        </div>
                      </div>
        

        <div class="form-group">
                        <label class="col-sm-2 control-label" for=""> Location:</label>
                        <div class="col-sm-10">
                         <asp:DropDownList ID="ddllocation" runat="server"  AutoPostBack="True"  
                                parsley-validation-minlength="1" parsley-type="email" parsley-minlength="4" 
                                parsley-required="true" parsley-trigger="change" 
                                class="form-control parsley-validated"  
                                kl_virtual_keyboard_secure_input="on" 
                                onselectedindexchanged="ddllocation_SelectedIndexChanged" ></asp:DropDownList>
                               
                            
                         
                          
                        </div>
                      </div>
        
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for=""> Sector:</label>
                        <div class="col-sm-10">
                        <asp:label ID="lblsector"  runat="server"></asp:label>
                               
                            
                         
                          
                        </div>
                      </div>
        

 
                <!-- row -->
                
                    
                       <div class="form-group">
                        <label class="col-sm-2 control-label" for="">Audit : </label>
                        <div class="col-sm-10">
                        <asp:DropDownList ID="ddlaudit" runat="server"  AutoPostBack="True"  
                                parsley-validation-minlength="1" parsley-type="email" parsley-minlength="4" 
                                parsley-required="true" parsley-trigger="change" 
                                class="form-control parsley-validated"  
                                kl_virtual_keyboard_secure_input="on" 
                                onselectedindexchanged="ddlaudit_SelectedIndexChanged"></asp:DropDownList>
                                 
                       
                               
                        
                          
                        </div>
                      </div>
                       <div class="form-group">
                     <label class="col-sm-2 control-label" for="">Audit Date : </label>
                        <div class="col-sm-10">
                         <asp:TextBox ID="txtauditdate"  runat="server" parsley-validation-minlength="1" parsley-type="email" parsley-minlength="4" parsley-required="true" parsley-trigger="change" class="form-control parsley-validated" kl_virtual_keyboard_secure_input="on" ></asp:TextBox>
                          
                               
                        
                          
                        </div>
                      </div>
                   
                      </div>
                        
                      
                        
                        </div>
                        
                        
                    </div>
                   
                    </section>
                      
         
           		
           <!-- end col-md-12 -->
          
               
</section>
            </div>
            <!--row-->
            <div class="row">
                <div class="col-md-12">
                    <div class="tile-body nopadding margin-top-10">
                        <div class="table-responsive" id="divrep" runat="server">
                            <asp:Repeater ID="rptrAnswer" runat="server" DataSourceID="" OnItemDataBound="rptrAnswer_ItemDataBound">
                                <HeaderTemplate>
                                    <table cellpadding="0" class="table table-bordered" cellspacing="0" border="0" width="80%">
                                        <tr style="background-color: #808080; color: White;">
                                            <th width="5%">
                                                <asp:Label Text="Q.No" runat="server" ID="lSNo" />
                                            </th>
                                            <th colspan="3">
                                                <asp:Label Text="Questions" style="margin-left:350px;"  runat="server" ID="Label6" />
                                            </th>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr id="tcat" runat="server" style="background-color: #e5e5e5; color: Black;">
                                        <th colspan="4">
                                            <asp:HiddenField runat="server" ID="SelectedCheckboxes" Value="0" />
                                            <asp:HiddenField runat="server" ID="hcategoryID" Value='<%# Eval("categoryid")%>' />
                                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("categoryname") %>' />
                                        </th>
                                    </tr>
                                    <tr style="background-color:#878787; color:white;" >
                                        <td>
                                            <asp:Label ID="lblNo" runat="server" Text='<%#(((RepeaterItem)Container).ItemIndex+1).ToString() %>' />
                                        </td>
                                        <td colspan="5" >
                                            <asp:HiddenField runat="server" ID="hQuestionID" Value='<%# Eval("auditqid")%>' />
                                            <asp:Label ID="Label7" runat="server" Text='<%# Eval("auditqname") %>' />
                                        </td>
                                    </tr>
                                    <tr  >
                                    </tr>
                                    <tr >
                                        <td>
                                        </td>
                                        <td >
                                        <asp:RadioButtonList ID="chkqnswer" runat="server" RepeatDirection="Horizontal"></asp:RadioButtonList>
                                           <%-- <asp:CheckBoxList ID="chkqnswer"  CssClass="test"   runat="server" RepeatDirection="Horizontal" >
                                            </asp:CheckBoxList>--%>
                                        </td>
                                        <td>
                                            <div style="width: 50%; float: left;">
                                                <asp:Label Text="Image" runat="server" ID="lblfileupload" />
                                                <asp:FileUpload ID="fuimageupload" runat="server" onchange="return CheckFile(this)" />
                                            </div>
                                            <div style="width: 50%; float: right;">
                                                <asp:Label Text="Remarks" runat="server" ID="Label2" /><br>
                                                <asp:TextBox runat="server" ID="txtfeedback" Text="" Style="background-color: transparent;height:100px;width:270px;" 
                                                    TextMode="MultiLine"></asp:TextBox>
                                            </div>
                                            &nbsp;
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                        <div class="row form-horizontal">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">
                                        Client Name :</label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtclient" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">
                                        SSA No :</label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtssano" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">
                                        SBU Name. :</label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtsubname" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">
                                        Auditor Name :</label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtauditor" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">
                                        Last Audit date :</label>
                                    <div id="selectbox" class="col-sm-6">
                                        <asp:TextBox ID="txtlastauditdate" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">
                                        Site Name :</label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtsite" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">
                                        Client Person Name :</label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtclientperson" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">
                                        OM/AOM Name :</label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtomname" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">
                                        Auditee Name :</label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtauditee" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                  
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">
                                        Current Audit Name :</label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtcurrentdate" class="form-control" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                            
                            </div>
                         
                        </div>
                        
                    </div>
                    <div class="tile-footer color cyan text-muted">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-offset-5">
                                        <asp:Button ID="btnlink" runat="server" class="btn btn-primary" Text="Submit" 
                                            onclick="btnlink_Click" />
                                        <asp:Button ID="btnback" class="btn btn-primary" runat="server" Text="Cancel" 
                                            onclick="btnback_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
                <!-- tile body -->
                <!-- /tile body -->
            </div>
        </div>
        <!--end row-->
    </div>
</asp:Content>
