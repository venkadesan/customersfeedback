﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using FacilityAudit.DA;
using DA;

namespace BL
{
    public class BLAudit
    {
        DAAudit objaudit = new DAAudit();

        public int saveaudit(int sectorid, string auditname, string auditdescription, DateTime startdate, DateTime enddate,
              int cuid, DateTime cdate)
        {
            return objaudit.saveaudit(sectorid, auditname, auditdescription, startdate, enddate, cuid, cdate);


        }
        public int updateaudit(int auditid, int sectorid, string auditname, string auditdescription,
                   DateTime startdate, DateTime enddate, int muid, DateTime mdate)
        {
            return objaudit.updateaudit(auditid, sectorid, auditname, auditdescription, startdate, enddate, muid, mdate);

        }
        public int deletemaudit(int auditid)
        {
            return objaudit.deletemaudit(auditid);

        }

        public DataSet getmaudit(int auditid, int id, int sectorid)
        {
            DataSet ds = new DataSet();
            ds = objaudit.getmaudit(auditid, id, sectorid);
            return ds;
        }

        public DataSet getscorefordashbaord(int regionid, int sbuid, int companyid, int locationid, DateTime frmdate, DateTime todate)
        {
            DataSet ds = new DataSet();
            ds = objaudit.getscorefordashbaord(regionid, sbuid, companyid, locationid, frmdate, todate);
            return ds;
        }


        public DataSet gettrendreport1(int regionid, int sbuid, string incharge, string auditorname, int sectorid, DateTime frmdate, DateTime todate, int customerid, int locationid)
        {
            DataSet ds = new DataSet();
            ds = objaudit.gettrendreport1(regionid, sbuid, incharge, auditorname, sectorid, frmdate, todate, customerid, locationid);
            return ds;
        }
        public DataSet gettrendreport2(int regionid, int sbuid, string incharge, string auditorname, int sectorid, int monthid, int year, int customerid, int locationid, int categoryid)
        {
            DataSet ds = new DataSet();
            ds = objaudit.gettrendreport2(regionid, sbuid, incharge, auditorname, sectorid, monthid, year, customerid, locationid, categoryid);
            return ds;
        }

        public DataSet getdashscorebyaudit(int regionid, int sbuid, int companyid, int locationid, DateTime frmdate, DateTime todate)
        {
            DataSet ds = new DataSet();
            ds = objaudit.getdashscorebyaudit(regionid, sbuid, companyid, locationid, frmdate, todate);
            return ds;
        }

        public DataSet getdashscorebycategory(int regionid, int sbuid, int companyid, int locationid, DateTime frmdate, DateTime todate, int auditid)
        {
            DataSet ds = new DataSet();
            ds = objaudit.getdashscorebycategory(regionid, sbuid, companyid, locationid, frmdate, todate, auditid);
            return ds;
        }

        #region "With Group"

        public DataSet gettrendreport1_Group(int regionid, int sbuid, string incharge, string auditorname,
            int sectorid, DateTime frmdate, DateTime todate, int customerid, int locationid, int groupid)
        {
            DataSet ds = new DataSet();
            ds = objaudit.gettrendreport1_Group(regionid, sbuid, incharge, auditorname, sectorid, frmdate, todate, customerid, locationid, groupid);
            return ds;
        }

        public DataSet gettrendreport2_Group(int regionid, int sbuid, string incharge, string auditorname,
            int sectorid, int monthid, int year, int customerid, int locationid, int categoryid, int groupid)
        {
            DataSet ds = new DataSet();
            ds = objaudit.gettrendreport2_Group(regionid, sbuid, incharge, auditorname, sectorid, monthid, year, customerid, locationid, categoryid, groupid);
            return ds;
        }

        #endregion
    }
}
