﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using FacilityAudit.DA;
using DA;

namespace BL
{
   public  class BLQuestion
    {

       DAQuestion objquestion = new DAQuestion();

       public int saveauditquestion(string auditqname, string qdescription, int categoryid,
                                     int ratingid, int weightageid, int cuid, DateTime cdate)
        {
            return objquestion.saveauditquestion(auditqname, qdescription, categoryid, ratingid, weightageid, cuid, cdate);


        }
       public int updatemauditquestion(int auditqid, string auditqname, string qdescription, int categoryid,
    int ratingid, int weightageid, int muid, DateTime mdate)
        {
            return objquestion.updatemauditquestion(auditqid, auditqname , qdescription , categoryid,ratingid , weightageid,muid,mdate);

        }
       public int deletemauditquestion(int auditqid)
        {
            return objquestion.deletemauditquestion(auditqid);

        }

       public DataSet getmauditquestion(int auditcatid, int auditqid, int id)
        {
            DataSet ds = new DataSet();
            ds = objquestion.getmauditquestion(auditcatid,auditqid, id);
            return ds;
        }

       public int savetransaction(string XmLIS)
       {
           return objquestion.savetransaction(XmLIS);


       }

       public DataSet getlastauditdate(int companyid, int locationid, int auditid)
       {
           DataSet ds = new DataSet();
           ds = objquestion.getlastauditdate(companyid, locationid, auditid);
           return ds;
       }

    }
}
