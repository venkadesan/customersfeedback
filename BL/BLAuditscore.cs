﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using FacilityAudit.DA;
using DA;

namespace BL
{
  public   class BLAuditscore
    {

      DAAuditscore objscore = new DAAuditscore();


      public int addscore(int auditid, string scorename, int score, int cuid, DateTime cdate, Boolean defaultchk, Boolean notapplicable)
        {
            return objscore.addscore(auditid, scorename, score, cuid, cdate,defaultchk,notapplicable);


        }
      public int updatemauditscore(int auditid, int scoreid, string scorename, int score,
	 int muid , DateTime mdate, Boolean defaultchk, Boolean notapplicable)
        {
            return objscore.updatemauditscore(auditid, scoreid, scorename, score, muid, mdate,defaultchk,notapplicable);

        }
        public int deletemauditscore(int scoreid)
        {
            return objscore.deletemauditscore(scoreid);

        }

        public DataSet getmauditscore(int auditid, int scoreid, int id)
        {
            DataSet ds = new DataSet();
            ds = objscore.getmauditscore(auditid, scoreid, id);
            return ds;
        }

    }
}
