﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using FacilityAudit.DA;
using DA;

namespace BL
{
   public class BLStatus
    {
       DAStatus objstatus = new DAStatus();

       public int addstatus(string statusname, string shortname, int groupid, int cuid, DateTime cdate)
        {
            return objstatus.addstatus(statusname, shortname, groupid, cuid, cdate);


        }
        public int updatemstatus(int statusid, int groupid, string statusname, string shortname,
        int muid, DateTime mdate)
        {
            return objstatus.updatemstatus(statusid, groupid, statusname, shortname, muid, mdate);

        }
        public int deletemstatus(int statusid)
        {
            return objstatus.deletemstatus(statusid);

        }

        public DataSet getmstatus(int groupid, int statusid, int id)
        {
            DataSet ds = new DataSet();
            ds = objstatus.getmstatus(groupid,statusid, id);
            return ds;
        } 


    }
}
