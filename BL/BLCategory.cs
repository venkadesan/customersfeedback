﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using FacilityAudit.DA;
using DA;

namespace BL
{
    public class BLCategory
    {

        DACategory objcategory = new DACategory();

        public int saveauditcategory(int auditid, string categoryname, string emailid, int cuid, DateTime cdate, bool isTraining)
        {
            return objcategory.saveauditcategory(auditid, categoryname, emailid, cuid, cdate, isTraining);


        }
        public int updatemauditcategory(int auditid, int categoryid, string categoryname, string emailid,
          int muid, DateTime mdate, bool isTraining)
        {
            return objcategory.updatemauditcategory(auditid, categoryid, categoryname, emailid, muid, mdate, isTraining);

        }
        public int deletemauditcategory(int categoryid)
        {
            return objcategory.deletemauditcategory(categoryid);

        }

        public DataSet getmauditcategory(int auditid, int categoryid, int id)
        {
            DataSet ds = new DataSet();
            ds = objcategory.getmauditcategory(auditid, categoryid, id);
            return ds;
        }
        public DataSet getquestionbyauditid(int auditid, DateTime date, int sectorid)
        {
            DataSet ds = new DataSet();
            ds = objcategory.getquestionbyauditid(auditid, date, sectorid);
            return ds;
        }

        #region "Location Mapper"

        public int addloctionmaildetails(int companyid, int locationid, string regionname, string regionmailid, string statename,
                                          string statemailid, string sbuname, string sbumailid, string omname,
                                          string ommailid, string aomname, string aommailid, string clientname,
                                          string clientmailid, string gmopsname, string gmopsmailid, string nationalheadname, string nationalheademail,
             string OperationHeadName,
     string OperationHeadMail,
     string TrainingHeadName,
     string TrainingHeadMail,
     string SCMHeadName,
     string SCMHeadMail,
     string ComplianceHeadName,
     string ComplianceHeadMail,
     string HRHeadName,
     string HRHeadMail,
     string OthersHeadName,
     string OthersHeadMail, int cuid, DateTime cdate)
        {
            return objcategory.addloctionmaildetails(companyid, locationid, regionname, regionmailid, statename,
                                          statemailid, sbuname, sbumailid, omname,
                                          ommailid, aomname, aommailid, clientname,
                                          clientmailid, gmopsname, gmopsmailid, nationalheadname, nationalheademail, OperationHeadName,
    OperationHeadMail,
    TrainingHeadName,
    TrainingHeadMail,
    SCMHeadName,
    SCMHeadMail,
    ComplianceHeadName,
    ComplianceHeadMail,
    HRHeadName,
    HRHeadMail,
    OthersHeadName,
    OthersHeadMail, cuid, cdate);


        }

        public DataSet getlocationmailid(int id, int companyid, int locationid, int locationmailid)
        {
            DataSet ds = new DataSet();
            ds = objcategory.getlocationmailid(id, companyid, locationid, locationmailid);
            return ds;
        }

        public int updatemlocationmail(int companyid, int locationid, string regionname, string regionmailid, string statename,
                                          string statemailid, string sbuname, string sbumailid, string omname,
                                          string ommailid, string aomname, string aommailid, string clientname,
                                          string clientmailid, string gmopsname, string gmopsmailid, string nationalheadname, string nationalheademail,
                string OperationHeadName,
     string OperationHeadMail,
     string TrainingHeadName,
     string TrainingHeadMail,
     string SCMHeadName,
     string SCMHeadMail,
     string ComplianceHeadName,
     string ComplianceHeadMail,
     string HRHeadName,
     string HRHeadMail,
     string OthersHeadName,
     string OthersHeadMail,
            int cuid, DateTime cdate, int locationmailid)
        {
            return objcategory.updatemlocationmail(companyid, locationid, regionname, regionmailid, statename,
                                          statemailid, sbuname, sbumailid, omname,
                                          ommailid, aomname, aommailid, clientname,
                                          clientmailid, gmopsname, gmopsmailid, nationalheadname, nationalheademail, OperationHeadName,
    OperationHeadMail,
    TrainingHeadName,
    TrainingHeadMail,
    SCMHeadName,
    SCMHeadMail,
    ComplianceHeadName,
    ComplianceHeadMail,
    HRHeadName,
    HRHeadMail,
    OthersHeadName,
    OthersHeadMail, cuid, cdate, locationmailid);

        }

        #endregion

        #region "Category"

        public DataSet GetCategoryDet(int categoryid)
        {
            try
            {
                return objcategory.GetCategoryDet(categoryid);
            }
            catch (Exception ex) { throw ex; }
        }

        public DataSet GetAllLocation()
        {
            try
            {
                return objcategory.GetAllLocation();
            }
            catch (Exception ex) { throw ex; }
        }

        public string[] InsertUpdateCategory(int CategoryId, string categoryname, int userid)
        {
            try
            {
                return objcategory.InsertUpdateCategory(CategoryId, categoryname, userid);
            }
            catch (Exception ex) { throw ex; }
        }


        public DataSet GetLocMapping(int categoryid)
        {
            try
            {
                return objcategory.GetLocMapping(categoryid);
            }
            catch (Exception ex) { throw ex; }
        }

        public string[] InsertCatLocMapping(int CategoryId, string catelocXML, int userid)
        {
            try
            {
                return objcategory.InsertCatLocMapping(CategoryId, catelocXML, userid);
            }
            catch (Exception ex) { throw ex; }
        }
        #endregion

        #region "Region - SBU mapping"

        public DataSet GetAllRegionSbu()
        {
            try
            {
                return objcategory.GetAllRegionSbu();
            }
            catch (Exception ex) { throw ex; }
        }

        public DataSet GetMappingRegSbuDet(int Regionid)
        {
            try
            {
                return objcategory.GetMappingRegSbuDet(Regionid);
            }
            catch (Exception ex) { throw ex; }
        }

        public string[] InsertRegSbuMapping(int RegionId, string sbuxml, int userid)
        {
            try
            {
                return objcategory.InsertRegSbuMapping(RegionId, sbuxml, userid);
            }
            catch (Exception ex) { throw ex; }
        }

        #endregion

        #region "Category - Group"

        public DataSet GetCategoryDet_withGroup(int categoryid, int groupid)
        {
            try
            {
                return objcategory.GetCategoryDet_withGroup(categoryid, groupid);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string[] InsertUpdateCategory_withGroup(int CategoryId, string categoryname, int userid, int groupid)
        {
            try
            {
                return objcategory.InsertUpdateCategory_withGroup(CategoryId, categoryname, userid, groupid);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataSet GetAllLocation_withGroup(int groupid)
        {
            try
            {
                return objcategory.GetAllLocation_withGroup(groupid);
            }
            catch (Exception ex) { throw ex; }
        }

        public string[] InsertRegSbuMapping_Group(int RegionId, string sbuxml, int userid, int groupid)
        {
            try
            {
                return objcategory.InsertRegSbuMapping_Group(RegionId, sbuxml, userid, groupid);
            }
            catch (Exception ex) { throw ex; }
        }


        public DataSet GetMappingRegSbuDet_Group(int Regionid, int groupid)
        {
            try
            {
                return objcategory.GetMappingRegSbuDet_Group(Regionid, groupid);
            }
            catch (Exception ex) { throw ex; }
        }

        #endregion


        #region "Multi Contacts"

        public DataSet GetContactsBySbuId(int sbuid, int groupid)
        {
            try
            {
                return objcategory.GetContactsBySbuId(sbuid, groupid);
            }
            catch (Exception ex) { throw ex; }
        }

        public string[] InsertUpdateMultiContacts(string xml, int userid)
        {
            try
            {
                return objcategory.InsertUpdateMultiContacts(xml, userid);
            }
            catch (Exception ex) { throw ex; }
        }

        #endregion


    }
}

