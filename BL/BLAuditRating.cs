﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using FacilityAudit.DA;
using DA;

namespace BL
{
   public  class BLAuditRating
    {
       DARating objrating = new DARating();


       public int addauditrating(int auditid, int  rating, int cuid, DateTime cdate)
          
       {
           return objrating.addauditrating(auditid, rating, cuid, cdate);


       }
       public int updatemauditrating(int auditid, int ratingid, int rating, int muid, DateTime mdate)
       {
           return objrating.updatemauditrating(auditid, ratingid ,rating, muid, mdate);

       }
       public int deletemauditrating(int ratingid)
       {
           return objrating.deletemauditrating(ratingid);

       }

       public DataSet getmauditrating(int auditid, int ratingid, int id)
       {
           DataSet ds = new DataSet();
           ds = objrating.getmauditrating(auditid,ratingid, id);
           return ds;
       } 

       // FOR Weightage



       public int addauditweightage(int auditid, int weightage, int cuid, DateTime cdate)
       {
           return objrating.addauditweightage(auditid, weightage, cuid, cdate);


       }
       public int updatemauditweightage(int auditid, int weightageid, int weightage, int muid, DateTime mdate)
       {
           return objrating.updatemauditweightage(auditid, weightageid, weightage, muid, mdate);

       }
       public int deletemauditweightage(int weightageid)
       {
           return objrating.deletemauditweightage(weightageid);

       }

       public DataSet getmauditweightage(int auditid, int weightageid, int id)
       {
           DataSet ds = new DataSet();
           ds = objrating.getmauditweightage(auditid, weightageid, id);
           return ds;
       } 
       


    }
}
