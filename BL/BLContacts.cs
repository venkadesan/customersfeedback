﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using FacilityAudit.DA;
using DA;
namespace BL
{
    public class BLContacts
    {
        DAContacts objcontacts = new DAContacts();
        public DataSet GetRegionContacts(int RegionId)
        {
            return objcontacts.GetRegionContacts(RegionId);
        }

        public string[] InsertUpdateRegionContacts(int Regionid, string TrainingMail, string PayrollMail,
            string HRMail, string SCMMail, string ComplianceMail, string QAMail, string OperationsMail, string PanIndiaMails, int userid)
        {
            return objcontacts.InsertUpdateRegionContacts(Regionid, TrainingMail, PayrollMail,
             HRMail, SCMMail, ComplianceMail, QAMail, OperationsMail, PanIndiaMails, userid);
        }

        public DataSet GetSbuContacts(int RegionId)
        {
            return objcontacts.GetSbuContacts(RegionId);
        }
        public string[] InsertUpdateSbuContacts(int Regionid, string ContactXML)
        {
            return objcontacts.InsertUpdateSbuContacts(Regionid, ContactXML);
        }

        #region "With Group"

        public DataSet GetRegionContacts_Group(int RegionId, int groupid)
        {
            return objcontacts.GetRegionContacts_Group(RegionId, groupid);
        }


        public string[] InsertUpdateRegionContacts_Group(int Regionid, string TrainingMail, string PayrollMail,
           string HRMail, string SCMMail, string ComplianceMail, string QAMail, string OperationsMail, string PanIndiaMails, int userid, int groupid)
        {
            return objcontacts.InsertUpdateRegionContacts_Group(Regionid, TrainingMail, PayrollMail,
             HRMail, SCMMail, ComplianceMail, QAMail, OperationsMail, PanIndiaMails, userid, groupid);
        }

        public DataSet GetSbuContacts_Group(int RegionId, int groupid)
        {
            return objcontacts.GetSbuContacts_Group(RegionId, groupid);
        }
        public string[] InsertUpdateSbuContacts_Group(int Regionid, string ContactXML, int groupid)
        {
            return objcontacts.InsertUpdateSbuContacts_Group(Regionid, ContactXML, groupid);
        }
        #endregion

        #region "Mail Template"

        public DataSet GetMailTemplate(int groupid)
        {
            return objcontacts.GetMailTemplate(groupid);
        }
        public string[] InsertMailTemplate(int groupid, string mailsubject, string mailbody, string trainingsubject, string trainingbody, string trainingmailid, int userid)
        {
            return objcontacts.InsertMailTemplate(groupid, mailsubject, mailbody, trainingsubject, trainingbody, trainingmailid, userid);
        }

        #endregion
    }
}
