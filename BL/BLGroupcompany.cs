﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using FacilityAudit.DA;



namespace FacilityAudit.BLL
{
    public class BLGroupcompany
    {
        DAGroupcompany groupcompany = new DAGroupcompany();

        public DataSet getgroup(int userid)
        {
            DataSet ds = new DataSet();
            ds = groupcompany.getgroupcompany(userid);
            return ds;
        }
        public DataSet getlocation(int CompanyID)
        {
            DataSet ds = new DataSet();
            ds = groupcompany.getlocation(CompanyID);
            return ds;
        }
        public DataSet getlocationbyid(int LocationID)
        {
            DataSet ds = new DataSet();
            ds = groupcompany.getlocationbyid(LocationID);
            return ds;
        }

        public DataSet getusercompany(int userid)
        {
            DataSet ds = new DataSet();
            ds = groupcompany.getusercompany(userid);
            return ds;
        }

        public DataSet getcompanybygroupid(int GroupID, int RoleID)
        {
            DataSet ds = new DataSet();
            ds = groupcompany.getcompanybygroupid(GroupID, RoleID);
            return ds;
        }
        public DataSet getlocationbylocationsector(int UserID, int CompanyID, int sectorid)
        {
            DataSet ds = new DataSet();
            ds = groupcompany.getlocationbylocationsector(UserID, CompanyID, sectorid);
            return ds;
        }

        public DataSet getcompanybycompanyid(int companyid)
        {
            DataSet ds = new DataSet();
            ds = groupcompany.getcompanybycompanyid(companyid);
            return ds;
        }
        public DataSet getlocationbycompany(int sbu)
        {
            DataSet ds = new DataSet();
            ds = groupcompany.getlocationbycompany(sbu);
            return ds;
        }
        public DataSet getcompanybysbu(int sbu)
        {
            DataSet ds = new DataSet();
            ds = groupcompany.getcompanybysbu(sbu);
            return ds;
        }
        public DataSet getlocationbysbu(int sbu, int companyid)
        {
            DataSet ds = new DataSet();
            ds = groupcompany.getlocationbysbu(sbu, companyid);
            return ds;
        }

        public DataSet getlocationsbubylocationid(int locationid)
        {
            DataSet ds = new DataSet();
            ds = groupcompany.getlocationsbubylocationid(locationid);
            return ds;
        }
        public DataSet getsbu()
        {
            DataSet ds = new DataSet();
            ds = groupcompany.getsbu();
            return ds;
        }

        public DataSet getsbubuUserid(int userid)
        {
            DataSet ds = new DataSet();
            ds = groupcompany.getsbubuUserid(userid);
            return ds;
        }

        public DataSet getinchargelistbysbu(int sbu)
        {
            DataSet ds = new DataSet();
            ds = groupcompany.getinchargelistbysbu(sbu);
            return ds;
        }

        public DataSet getauditornamebysbu(int sbu)
        {
            DataSet ds = new DataSet();
            ds = groupcompany.getauditornamebysbu(sbu);
            return ds;
        }

        public DataSet getsectorbytransaction()
        {
            DataSet ds = new DataSet();
            ds = groupcompany.getsectorbytransaction();
            return ds;
        }

        public DataSet getsectorbycompany(int locationid)
        {
            DataSet ds = new DataSet();
            ds = groupcompany.getsectorbycompany(locationid);
            return ds;
        }

        public int updatecompanynsector(int locationid, int sectorid)
        {
            return groupcompany.updatecompanynsector(locationid, sectorid);

        }
        public DataSet getsector()
        {
            DataSet ds = new DataSet();
            ds = groupcompany.getsector();
            return ds;
        }

        public DataSet getsectorbyid(int sectorid)
        {
            DataSet ds = new DataSet();
            ds = groupcompany.getsectorbyid(sectorid);
            return ds;
        }

        public DataSet getsectorlocation(int sectorid)
        {
            DataSet ds = new DataSet();
            ds = groupcompany.getsectorlocation(sectorid);
            return ds;
        }


        public int addsectorlocation(string XmLIS)
        {
            return groupcompany.addsectorlocation(XmLIS);


        }

        public DataSet GetTransactionCompany()
        {
            DataSet ds = new DataSet();
            ds = groupcompany.GetTransactionCompany();
            return ds;
        }

        public DataSet GetTransactionLocation()
        {
            DataSet ds = new DataSet();
            ds = groupcompany.GetTransactionLocation();
            return ds;
        }

        #region "Get Secor - Location Details"

        public DataSet GetMappedlocations(int sectorid, int userid)
        {
            return groupcompany.GetMappedlocations(sectorid, userid);
        }
        public string[] InsertSecLocMapping(int SectorId, string locXml, int userid)
        {
            return groupcompany.InsertSecLocMapping(SectorId, locXml, userid);
        }

        #endregion

        #region "With Group ID"

        public DataSet GetSectorwithGroup(int groupid)
        {
            DataSet ds = new DataSet();
            ds = groupcompany.GetSectorwithGroup(groupid);
            return ds;
        }

        public DataSet getcompanybysbuGroup(int sbu, int groupid)
        {
            DataSet ds = new DataSet();
            ds = groupcompany.getcompanybysbuGroup(sbu, groupid);
            return ds;
        }

        public DataSet getsbu_Group(int groupid)
        {
            DataSet ds = new DataSet();
            ds = groupcompany.getsbu_Group(groupid);
            return ds;
        }

        public DataSet GetTransactionLocation_Group(int groupid)
        {
            DataSet ds = new DataSet();
            ds = groupcompany.GetTransactionLocation_Group(groupid);
            return ds;
        }

        public DataSet GetTransactionCompany_Group(int groupid)
        {
            DataSet ds = new DataSet();
            ds = groupcompany.GetTransactionCompany_Group(groupid);
            return ds;
        }

        public DataSet getinchargelistbysbu_Group(int sbu, int groupid)
        {
            DataSet ds = new DataSet();
            ds = groupcompany.getinchargelistbysbu_Group(sbu, groupid);
            return ds;
        }

        public DataSet getauditornamebysbu_Group(int sbu, int groupid)
        {
            DataSet ds = new DataSet();
            ds = groupcompany.getauditornamebysbu_Group(sbu, groupid);
            return ds;
        }

        public DataSet getsectorbytransaction_Group(int groupid)
        {
            DataSet ds = new DataSet();
            ds = groupcompany.getsectorbytransaction_Group(groupid);
            return ds;
        }
        #endregion
    }
}
