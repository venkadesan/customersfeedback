﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DA
{
    public  class DAAuditscore
    {
        public int addscore(int auditid, string scorename, int score, int cuid, DateTime cdate, Boolean defaultchk, Boolean notapplicable)
        {
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("addscore");
            db.AddInParameter(cmd, "auditid", DbType.Int32, auditid);
            db.AddInParameter(cmd, "scorename", DbType.String, scorename);
            db.AddInParameter(cmd, "score", DbType.Int32, score);
            db.AddInParameter(cmd, "cuid", DbType.Int32, cuid);
            db.AddInParameter(cmd, "cdate", DbType.DateTime, cdate);
            db.AddInParameter(cmd, "defaultchk", DbType.Boolean, defaultchk);
            db.AddInParameter(cmd, "notapplicable", DbType.Boolean, notapplicable);
            return Convert.ToInt32(db.ExecuteScalar(cmd));
        }


        public int updatemauditscore(int auditid, int scoreid, string  scorename, int score,
     int muid, DateTime mdate, Boolean defaultchk, Boolean notapplicable)
        {
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("updatemauditscore");
            db.AddInParameter(cmd, "auditid", DbType.Int32, auditid);
            db.AddInParameter(cmd, "scoreid", DbType.Int32, scoreid);
            db.AddInParameter(cmd, "scorename", DbType.String, scorename);
            db.AddInParameter(cmd, "score", DbType.Int32, score);
            db.AddInParameter(cmd, "muid", DbType.Int32, muid);
            db.AddInParameter(cmd, "mdate", DbType.DateTime, mdate);
            db.AddInParameter(cmd, "defaultchk", DbType.Boolean, defaultchk);
            db.AddInParameter(cmd, "notapplicable", DbType.Boolean, notapplicable);
            return Convert.ToInt32(db.ExecuteScalar(cmd));
        }
        public int deletemauditscore(int scoreid)
        {
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("deletemauditscore");
            db.AddInParameter(cmd, "scoreid", DbType.Int32, scoreid);
            return Convert.ToInt32(db.ExecuteScalar(cmd));
        }

        public DataSet getmauditscore(int auditid, int scoreid, int id)
        {

            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("getmauditscore");
            db.AddInParameter(cmd, "auditid", DbType.Int32, auditid);
            db.AddInParameter(cmd, "scoreid", DbType.Int32, scoreid);
            db.AddInParameter(cmd, "id", DbType.Int32, id);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }


    }
}
