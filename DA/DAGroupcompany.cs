﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;
using System.Globalization;


namespace FacilityAudit.DA
{
   public class DAGroupcompany
    {
     

       public DataSet getgroupcompany(int userid)       
       {
           DataSet ds = new DataSet();
           Database db = DatabaseFactory.CreateDatabase("DBNameIs");
           DbCommand cmd = db.GetStoredProcCommand("sp_Get_GroupByUserID");
           db.AddInParameter(cmd, "userid", DbType.Int32, userid);
           ds = db.ExecuteDataSet(cmd);
           return ds;
       }

       public DataSet getusercompany(int userid)
       {
           DataSet ds = new DataSet();
           Database db = DatabaseFactory.CreateDatabase("DBNameIs");
           DbCommand cmd = db.GetStoredProcCommand("sp_Get_Users_Company");
           db.AddInParameter(cmd, "userid", DbType.Int32, userid);
           ds = db.ExecuteDataSet(cmd);
           return ds;
       }
      
       public DataSet getcompanybygroupid(int GroupID, int RoleID)
       {
           DataSet ds = new DataSet();
           Database db = DatabaseFactory.CreateDatabase("DBNameIs");
           DbCommand cmd = db.GetStoredProcCommand("sp_Get_CompanyByGroupID");
           db.AddInParameter(cmd, "GroupID", DbType.Int32, GroupID);
           db.AddInParameter(cmd, "RoleID", DbType.Int32, RoleID);
           ds = db.ExecuteDataSet(cmd);
           return ds;
       }

       public DataSet getlocationbylocationsector(int UserID, int CompanyID, int sectorid)
       {
           DataSet ds = new DataSet();
           Database db = DatabaseFactory.CreateDatabase("ConnectionString");
           DbCommand cmd = db.GetStoredProcCommand("getlocationbylocationsector");
           db.AddInParameter(cmd, "UserID", DbType.Int32, UserID);
           db.AddInParameter(cmd, "CompanyID", DbType.Int32, CompanyID);
           db.AddInParameter(cmd, "sectorid", DbType.Int32, sectorid);
           ds = db.ExecuteDataSet(cmd);
           return ds;
       }
       

       public DataSet getcompanybycompanyid(int companyid)
       {
           DataSet ds = new DataSet();
           Database db = DatabaseFactory.CreateDatabase("DBNameIs");
           DbCommand cmd = db.GetStoredProcCommand("sp_Get_CompanyByID");
           db.AddInParameter(cmd, "companyid", DbType.Int32, companyid);
           ds = db.ExecuteDataSet(cmd);
           return ds;
       }

       public DataSet getlocationbycompany(int sbu)
       {
           DataSet ds = new DataSet();
           Database db = DatabaseFactory.CreateDatabase("DBNameIs");
           DbCommand cmd = db.GetStoredProcCommand("sp_Get_Locationbysbu");
           db.AddInParameter(cmd, "sbu", DbType.Int32, sbu);
           ds = db.ExecuteDataSet(cmd);
           return ds;
       }

       public DataSet getlocation(int CompanyID)
       {
           DataSet ds = new DataSet();
           Database db = DatabaseFactory.CreateDatabase("DBNameIs");
           DbCommand cmd = db.GetStoredProcCommand("sp_Get_Location");
           db.AddInParameter(cmd, "CompanyID ", DbType.Int32, CompanyID);
           ds = db.ExecuteDataSet(cmd);
           return ds;
       }
       public DataSet getcompanybysbu( int sbu)
       {
           DataSet ds = new DataSet();
           Database db = DatabaseFactory.CreateDatabase("ConnectionString");
           DbCommand cmd = db.GetStoredProcCommand("getcompanybysbu");
            db.AddInParameter(cmd, "sbu", DbType.Int32, sbu);
           ds = db.ExecuteDataSet(cmd);
           return ds;
       }


       public DataSet getlocationbysbu(int sbu,int companyid)
       {
           DataSet ds = new DataSet();
           Database db = DatabaseFactory.CreateDatabase("ConnectionString");
           DbCommand cmd = db.GetStoredProcCommand("getlocationbysbu");
           db.AddInParameter(cmd, "sbu", DbType.Int32, sbu);
           db.AddInParameter(cmd, "companyid", DbType.Int32, companyid);
           ds = db.ExecuteDataSet(cmd);
           return ds;
       }

       public DataSet getlocationsbubylocationid(int locationid)
       {
           DataSet ds = new DataSet();
           Database db = DatabaseFactory.CreateDatabase("ConnectionString");
           DbCommand cmd = db.GetStoredProcCommand("getlocationsbubylocationid");
           db.AddInParameter(cmd, "locationid", DbType.Int32, locationid);
           ds = db.ExecuteDataSet(cmd);
           return ds;
       }

       public DataSet getsbu()
       {
           DataSet ds = new DataSet();
           Database db = DatabaseFactory.CreateDatabase("DBNameIs");
           DbCommand cmd = db.GetStoredProcCommand("sp_Get_LocationSettings");
           ds = db.ExecuteDataSet(cmd);
           return ds;
       }

       public DataSet getsbubuUserid(int userid)
       {
           DataSet ds = new DataSet();
           Database db = DatabaseFactory.CreateDatabase("ConnectionString");
           DbCommand cmd = db.GetStoredProcCommand("And_allsbu");
           db.AddInParameter(cmd, "userid", DbType.Int32, userid);
           ds = db.ExecuteDataSet(cmd);
           return ds;
       }



       public DataSet getinchargelistbysbu(int sbuid)
       {
           DataSet ds = new DataSet();
           Database db = DatabaseFactory.CreateDatabase("ConnectionString");
           DbCommand cmd = db.GetStoredProcCommand("getinchargelistbysbu");
           db.AddInParameter(cmd, "sbuid", DbType.Int32, sbuid);
           ds = db.ExecuteDataSet(cmd);
           return ds;
       }

       public DataSet getauditornamebysbu(int sbuid)
       {
           DataSet ds = new DataSet();
           Database db = DatabaseFactory.CreateDatabase("ConnectionString");
           DbCommand cmd = db.GetStoredProcCommand("getauditornamebysbu");
           db.AddInParameter(cmd, "sbuid", DbType.Int32, sbuid);
           ds = db.ExecuteDataSet(cmd);
           return ds;
       }
       public DataSet getsectorbytransaction()
       {
           DataSet ds = new DataSet();
           Database db = DatabaseFactory.CreateDatabase("ConnectionString");
           DbCommand cmd = db.GetStoredProcCommand("getsectorbytransaction");
           ds = db.ExecuteDataSet(cmd);
           return ds;
       }
       public DataSet getlocationbyid(int LocationID)
       {
           DataSet ds = new DataSet();
           Database db = DatabaseFactory.CreateDatabase("DBNameIs");
           DbCommand cmd = db.GetStoredProcCommand("sp_Get_LocationByID");
           db.AddInParameter(cmd, "LocationID", DbType.Int32, LocationID);
           ds = db.ExecuteDataSet(cmd);
           return ds;
       }

       public DataSet getsectorbycompany(int locationid)
       {
           DataSet ds = new DataSet();
           Database db = DatabaseFactory.CreateDatabase("DBNameIs");
           DbCommand cmd = db.GetStoredProcCommand("sp_Get_sectorbyid");
           db.AddInParameter(cmd, "locationid", DbType.Int32, locationid);
           ds = db.ExecuteDataSet(cmd);
           return ds;
       }

       public int updatecompanynsector(int locationid, int sectorid)
       {
           Database db = DatabaseFactory.CreateDatabase("DBNameIs");
           DbCommand cmd = db.GetStoredProcCommand("updatecompanynsector");
           db.AddInParameter(cmd, "locationid", DbType.Int32, locationid);
           db.AddInParameter(cmd, "sectorid", DbType.Int32, sectorid);
           return Convert.ToInt32(db.ExecuteScalar(cmd));
       }

       public DataSet getsector()
       {
           DataSet ds = new DataSet();
           Database db = DatabaseFactory.CreateDatabase("DBNameIs");
           DbCommand cmd = db.GetStoredProcCommand("sp_Get_LocationSector");
           ds = db.ExecuteDataSet(cmd);
           return ds;
       }
       public DataSet getsectorbyid(int sectorid)
       {
           DataSet ds = new DataSet();
           Database db = DatabaseFactory.CreateDatabase("DBNameIs");
           DbCommand cmd = db.GetStoredProcCommand("sp_Get_LocationSectorbyid");
           db.AddInParameter(cmd, "sectorid", DbType.Int32, sectorid);
           ds = db.ExecuteDataSet(cmd);
           return ds;
       }
       public DataSet getsectorlocation(int sectorid)
       {
           DataSet ds = new DataSet();
           Database db = DatabaseFactory.CreateDatabase("ConnectionString");
           DbCommand cmd = db.GetStoredProcCommand("getsectorlocation");
           db.AddInParameter(cmd, "sectorid", DbType.Int32, sectorid);
           ds = db.ExecuteDataSet(cmd);
           return ds;
       }
       
       public int addsectorlocation(string XmLIS)
       {
           Database db = DatabaseFactory.CreateDatabase("ConnectionString");
           DbCommand cmd = db.GetStoredProcCommand("addsectorlocation");
           db.AddInParameter(cmd, "XmLIS", DbType.String, XmLIS);
           return Convert.ToInt32(db.ExecuteScalar(cmd));
       }

       public DataSet GetTransactionCompany()
       {
           DataSet ds = new DataSet();
           Database db = DatabaseFactory.CreateDatabase("ConnectionString");
           DbCommand cmd = db.GetStoredProcCommand("Qry_GetTransactionCompany");
           ds = db.ExecuteDataSet(cmd);
           return ds;
       }

       public DataSet GetTransactionLocation()
       {
           DataSet ds = new DataSet();
           Database db = DatabaseFactory.CreateDatabase("ConnectionString");
           DbCommand cmd = db.GetStoredProcCommand("Qry_GetTransactionLocation");
           ds = db.ExecuteDataSet(cmd);
           return ds;
       }

       #region "Get Secor - Location Details"

       public DataSet GetMappedlocations(int sectorid, int userid)
       {
           DataSet ds = new DataSet();
           Database db = DatabaseFactory.CreateDatabase("ConnectionString");
           DbCommand cmd = db.GetStoredProcCommand("Qry_GetMappedlocations");
           db.AddInParameter(cmd, "sectorid", DbType.Int32, sectorid);
           db.AddInParameter(cmd, "userid", DbType.Int32, userid);
           ds = db.ExecuteDataSet(cmd);
           return ds;
       }

       public string[] InsertSecLocMapping(int SectorId, string locXml, int userid)
       {
           try
           {
               string[] Values = new string[3];
               Database db = DatabaseFactory.CreateDatabase("ConnectionString");
               DbCommand cmd = db.GetStoredProcCommand("SP_InsertSectorLocMapping");
               db.AddInParameter(cmd, "@SectorId", DbType.Int32, SectorId);
               db.AddInParameter(cmd, "@SecLocXML", DbType.String, locXml);
               db.AddInParameter(cmd, "@Uid", DbType.Int32, userid);
               db.AddOutParameter(cmd, "@ErrorMessage", DbType.String, 1000);
               db.AddOutParameter(cmd, "@Status", DbType.String, 10);
               db.ExecuteNonQuery(cmd);
               Values[0] = string.Format(CultureInfo.CurrentCulture, "{0}",
               db.GetParameterValue(cmd, "@ErrorMessage"));
               Values[1] = db.GetParameterValue(cmd, "@Status").ToString();
               return Values;
           }
           catch (Exception)
           {
               throw;
           }
       }

       #endregion


       #region "With Group ID"

       public DataSet GetSectorwithGroup(int groupid)
       {
           DataSet ds = new DataSet();
           Database db = DatabaseFactory.CreateDatabase("DBNameIs");
           DbCommand cmd = db.GetStoredProcCommand("sp_Get_LocationSector_GroupID");
           db.AddInParameter(cmd, "groupid", DbType.Int32, groupid);
           ds = db.ExecuteDataSet(cmd);
           return ds;
       }

       public DataSet getcompanybysbuGroup(int sbu,int groupid)
       {
           DataSet ds = new DataSet();
           Database db = DatabaseFactory.CreateDatabase("ConnectionString");
           DbCommand cmd = db.GetStoredProcCommand("getcompanybysbu_Group");
           db.AddInParameter(cmd, "groupid", DbType.Int32, groupid);
           db.AddInParameter(cmd, "sbu", DbType.Int32, sbu);
           ds = db.ExecuteDataSet(cmd);
           return ds;
       }

       public DataSet getsbu_Group(int groupid)
       {
           DataSet ds = new DataSet();
           Database db = DatabaseFactory.CreateDatabase("DBNameIs");
           DbCommand cmd = db.GetStoredProcCommand("sp_Get_LocationSettings_Group");
           db.AddInParameter(cmd, "groupid", DbType.Int32, groupid);
           ds = db.ExecuteDataSet(cmd);
           return ds;
       }

       public DataSet GetTransactionLocation_Group(int groupid)
       {
           DataSet ds = new DataSet();
           Database db = DatabaseFactory.CreateDatabase("ConnectionString");
           DbCommand cmd = db.GetStoredProcCommand("Qry_GetTransactionLocation_Group");
           db.AddInParameter(cmd, "groupid", DbType.Int32, groupid);
           ds = db.ExecuteDataSet(cmd);
           return ds;
       }

       public DataSet GetTransactionCompany_Group(int groupid)
       {
           DataSet ds = new DataSet();
           Database db = DatabaseFactory.CreateDatabase("ConnectionString");
           DbCommand cmd = db.GetStoredProcCommand("Qry_GetTransactionCompany_Group");
           db.AddInParameter(cmd, "groupid", DbType.Int32, groupid);
           ds = db.ExecuteDataSet(cmd);
           return ds;
       }


       public DataSet getinchargelistbysbu_Group(int sbuid,int groupid)
       {
           DataSet ds = new DataSet();
           Database db = DatabaseFactory.CreateDatabase("ConnectionString");
           DbCommand cmd = db.GetStoredProcCommand("getinchargelistbysbu_Group");
           db.AddInParameter(cmd, "sbuid", DbType.Int32, sbuid);
           db.AddInParameter(cmd, "groupid", DbType.Int32, groupid);
           ds = db.ExecuteDataSet(cmd);
           return ds;
       }

       public DataSet getauditornamebysbu_Group(int sbuid, int groupid)
       {
           DataSet ds = new DataSet();
           Database db = DatabaseFactory.CreateDatabase("ConnectionString");
           DbCommand cmd = db.GetStoredProcCommand("getauditornamebysbu_Group");
           db.AddInParameter(cmd, "sbuid", DbType.Int32, sbuid);
           db.AddInParameter(cmd, "groupid", DbType.Int32, groupid);
           ds = db.ExecuteDataSet(cmd);
           return ds;
       }

       public DataSet getsectorbytransaction_Group(int groupid)
       {
           DataSet ds = new DataSet();
           Database db = DatabaseFactory.CreateDatabase("ConnectionString");
           DbCommand cmd = db.GetStoredProcCommand("getsectorbytransaction_Group");
           db.AddInParameter(cmd, "groupid", DbType.Int32, groupid);
           ds = db.ExecuteDataSet(cmd);
           return ds;
       }

       #endregion
    }
}
