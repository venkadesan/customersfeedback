﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DA
{
   public  class DAQuestion
    {
    

        public int saveauditquestion(string  auditqname, string qdescription, int categoryid,
                                     int ratingid, int weightageid, int cuid, DateTime cdate)
        {
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("addauditquestion");
            db.AddInParameter(cmd, "auditqname", DbType.String, auditqname);
            db.AddInParameter(cmd, "qdescription", DbType.String, qdescription);
            db.AddInParameter(cmd, "categoryid", DbType.Int32, categoryid);
            db.AddInParameter(cmd, "ratingid", DbType.Int32, ratingid);
            db.AddInParameter(cmd, "weightageid", DbType.Int32, weightageid);
            db.AddInParameter(cmd, "cuid", DbType.Int32, cuid);
            db.AddInParameter(cmd, "cdate", DbType.DateTime, cdate);
            return Convert.ToInt32(db.ExecuteScalar(cmd));
        }
        public int updatemauditquestion(int auditqid, string  auditqname , string qdescription , int categoryid,
	int ratingid , int weightageid, int muid, DateTime mdate)
        {
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("updatemauditquestion");
            db.AddInParameter(cmd, "auditqid", DbType.Int32, auditqid);
            db.AddInParameter(cmd, "auditqname", DbType.String, auditqname);
            db.AddInParameter(cmd, "qdescription", DbType.String, qdescription);
            db.AddInParameter(cmd, "categoryid", DbType.Int32, categoryid);
            db.AddInParameter(cmd, "ratingid", DbType.Int32, ratingid);
            db.AddInParameter(cmd, "weightageid", DbType.Int32, weightageid);
            db.AddInParameter(cmd, "muid", DbType.Int32, muid);
            db.AddInParameter(cmd, "mdate", DbType.DateTime, mdate);
            return Convert.ToInt32(db.ExecuteScalar(cmd));
        }
        public int deletemauditquestion(int auditqid)
        {
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("deletemauditquestion");
            db.AddInParameter(cmd, "auditqid", DbType.Int32, auditqid);
            return Convert.ToInt32(db.ExecuteScalar(cmd));
        }

        public DataSet getmauditquestion(int auditcatid, int auditqid, int id)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("getmauditquestion");
            db.AddInParameter(cmd, "auditcatid", DbType.Int32, auditcatid);
            db.AddInParameter(cmd, "auditqid", DbType.Int32, auditqid);
            db.AddInParameter(cmd, "id", DbType.Int32, id);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }


        public int savetransaction(string XmLIS)
        {
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("addtransaction");
            db.AddInParameter(cmd, "XmLIS", DbType.String, XmLIS);
           return Convert.ToInt32(db.ExecuteScalar(cmd));
        }


        public DataSet getlastauditdate(int companyid, int locationid, int auditid)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("getlastauditdate");
            db.AddInParameter(cmd, "companyid", DbType.Int32, companyid);
            db.AddInParameter(cmd, "locationid", DbType.Int32, locationid);
            db.AddInParameter(cmd, "auditid", DbType.Int32, auditid);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }





    }
}
