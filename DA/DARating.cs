﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DA
{
   public class DARating
    {
       
        public int addauditrating(int auditid, int  rating, int cuid, DateTime cdate)
        {
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("addauditrating");
            db.AddInParameter(cmd, "auditid", DbType.Int32, auditid);
            db.AddInParameter(cmd, "rating", DbType.Int32, rating);
            db.AddInParameter(cmd, "cuid", DbType.Int32, cuid);
            db.AddInParameter(cmd, "cdate", DbType.DateTime, cdate);
            return Convert.ToInt32(db.ExecuteScalar(cmd));
        }
      
        public int updatemauditrating(int auditid, int ratingid ,int rating, int muid, DateTime mdate)
        {
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("updatemauditrating");
            db.AddInParameter(cmd, "auditid", DbType.Int32, auditid);
            db.AddInParameter(cmd, "ratingid", DbType.Int32, ratingid);
            db.AddInParameter(cmd, "rating", DbType.Int32, rating);
            db.AddInParameter(cmd, "muid", DbType.Int32, muid);
            db.AddInParameter(cmd, "mdate", DbType.DateTime, mdate);
            return Convert.ToInt32(db.ExecuteScalar(cmd));
        }
        public int deletemauditrating(int ratingid)
        {
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("deletemauditrating");
            db.AddInParameter(cmd, "ratingid", DbType.Int32, ratingid);
            return Convert.ToInt32(db.ExecuteScalar(cmd));
        }

        public DataSet getmauditrating(int auditid,int ratingid, int id)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("getmauditrating");
            db.AddInParameter(cmd, "auditid", DbType.Int32, auditid);
            db.AddInParameter(cmd, "ratingid", DbType.Int32, ratingid);
            db.AddInParameter(cmd, "id", DbType.Int32, id);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }


       // For Weightage


        public int addauditweightage(int auditid, int weightage, int cuid, DateTime cdate)
        {
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("addauditweightage");
            db.AddInParameter(cmd, "auditid", DbType.Int32, auditid);
            db.AddInParameter(cmd, "weightage", DbType.Int32, weightage);
            db.AddInParameter(cmd, "cuid", DbType.Int32, cuid);
            db.AddInParameter(cmd, "cdate", DbType.DateTime, cdate);
            return Convert.ToInt32(db.ExecuteScalar(cmd));
        }

        public int updatemauditweightage(int auditid, int weightageid, int weightage, int muid, DateTime mdate)
        {
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("updatemauditweightage");
            db.AddInParameter(cmd, "auditid", DbType.Int32, auditid);
            db.AddInParameter(cmd, "weightageid", DbType.Int32, weightageid);
            db.AddInParameter(cmd, "weightage", DbType.Int32, weightage);
            db.AddInParameter(cmd, "muid", DbType.Int32, muid);
            db.AddInParameter(cmd, "mdate", DbType.DateTime, mdate);
            return Convert.ToInt32(db.ExecuteScalar(cmd));
        }
        public int deletemauditweightage(int weightageid)
        {
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("deletemauditweightage");
            db.AddInParameter(cmd, "weightageid", DbType.Int32, weightageid);
            return Convert.ToInt32(db.ExecuteScalar(cmd));
        }

        public DataSet getmauditweightage(int auditid, int weightageid, int id)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("getmauditweightage");
            db.AddInParameter(cmd, "auditid", DbType.Int32, auditid);
            db.AddInParameter(cmd, "weightageid", DbType.Int32, weightageid);
            db.AddInParameter(cmd, "id", DbType.Int32, id);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }



    }
}
