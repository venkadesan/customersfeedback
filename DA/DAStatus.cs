﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DA
{
    public class DAStatus
    {

        

        public int addstatus(string  statusname, string shortname,int groupid,int cuid, DateTime cdate)
        {
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("addstatus");
            db.AddInParameter(cmd, "statusname", DbType.String, statusname);
            db.AddInParameter(cmd, "shortname", DbType.String, shortname);
            db.AddInParameter(cmd, "groupid", DbType.Int32, groupid);
            db.AddInParameter(cmd, "cuid", DbType.Int32, cuid);
            db.AddInParameter(cmd, "cdate", DbType.DateTime, cdate);
            return Convert.ToInt32(db.ExecuteScalar(cmd));
        }
        

        public int updatemstatus(int statusid, int groupid, string statusname, string shortname,
        int muid, DateTime mdate)
        {
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("updatemstatus");
            db.AddInParameter(cmd, "statusid", DbType.Int32, statusid);
            db.AddInParameter(cmd, "groupid", DbType.Int32, groupid);
            db.AddInParameter(cmd, "statusname", DbType.String, statusname);
            db.AddInParameter(cmd, "shortname", DbType.String, shortname);
            db.AddInParameter(cmd, "muid", DbType.Int32, muid);
            db.AddInParameter(cmd, "mdate", DbType.DateTime, mdate);
            return Convert.ToInt32(db.ExecuteScalar(cmd));
        }
        public int deletemstatus(int statusid)
        {
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("deletemstatus");
            db.AddInParameter(cmd, "statusid", DbType.Int32, statusid);
            return Convert.ToInt32(db.ExecuteScalar(cmd));
        }
        
        public DataSet getmstatus(int groupid,int statusid, int id)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("getmstatus");
            db.AddInParameter(cmd, "groupid", DbType.Int32, groupid);
            db.AddInParameter(cmd, "statusid", DbType.Int32, statusid);
            db.AddInParameter(cmd, "id", DbType.Int32, id);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }



    }
}
