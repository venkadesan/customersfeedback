﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;
using System.Globalization;

namespace DA
{
    public class DACategory
    {


        public int saveauditcategory(int auditid, string categoryname, string emailid, int cuid, DateTime cdate, bool isTraining)
        {
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("addauditcategory");
            db.AddInParameter(cmd, "auditid", DbType.Int32, auditid);
            db.AddInParameter(cmd, "categoryname", DbType.String, categoryname);
            db.AddInParameter(cmd, "emailid", DbType.String, emailid);
            db.AddInParameter(cmd, "cuid", DbType.Int32, cuid);
            db.AddInParameter(cmd, "cdate", DbType.DateTime, cdate);

            db.AddInParameter(cmd, "isTraining", DbType.Boolean, isTraining);
            return Convert.ToInt32(db.ExecuteScalar(cmd));
        }


        public int updatemauditcategory(int auditid, int categoryid, string categoryname, string emailid,
        int muid, DateTime mdate, bool isTraining)
        {
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("updatemauditcategory");
            db.AddInParameter(cmd, "auditid", DbType.Int32, auditid);
            db.AddInParameter(cmd, "categoryid", DbType.Int32, categoryid);
            db.AddInParameter(cmd, "categoryname", DbType.String, categoryname);
            db.AddInParameter(cmd, "emailid", DbType.String, emailid);
            db.AddInParameter(cmd, "muid", DbType.Int32, muid);
            db.AddInParameter(cmd, "mdate", DbType.DateTime, mdate);

            db.AddInParameter(cmd, "isTraining", DbType.Boolean, isTraining);
            return Convert.ToInt32(db.ExecuteScalar(cmd));
        }
        public int deletemauditcategory(int categoryid)
        {
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("deletemauditcategory");
            db.AddInParameter(cmd, "categoryid", DbType.Int32, categoryid);
            return Convert.ToInt32(db.ExecuteScalar(cmd));
        }

        public DataSet getmauditcategory(int auditid, int categoryid, int id)
        {

            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("getmauditcategory");
            db.AddInParameter(cmd, "auditid", DbType.Int32, auditid);
            db.AddInParameter(cmd, "categoryid", DbType.Int32, categoryid);
            db.AddInParameter(cmd, "id", DbType.Int32, id);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }

        public DataSet getquestionbyauditid(int auditid, DateTime date, int sectorid)
        {

            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("getquestionbyauditid");
            db.AddInParameter(cmd, "auditid", DbType.Int32, auditid);
            db.AddInParameter(cmd, "date", DbType.DateTime, date);
            db.AddInParameter(cmd, "sectorid", DbType.Int32, sectorid);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }

        #region "Location - Mails"


        public int addloctionmaildetails(int companyid, int locationid, string regionname, string regionmailid, string statename,
                                         string statemailid, string sbuname, string sbumailid, string omname,
                                     string ommailid, string aomname, string aommailid, string clientname,
                                     string clientmailid, string gmopsname, string gmopsmailid,
         string nationalheadname, string nationalheademail,
         string OperationHeadName,
  string OperationHeadMail,
  string TrainingHeadName,
  string TrainingHeadMail,
  string SCMHeadName,
  string SCMHeadMail,
  string ComplianceHeadName,
  string ComplianceHeadMail,
  string HRHeadName,
  string HRHeadMail,
  string OthersHeadName,
  string OthersHeadMail,
         int cuid, DateTime cdate)
        {
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("addloctionmaildetails");
            db.AddInParameter(cmd, "companyid", DbType.Int32, companyid);
            db.AddInParameter(cmd, "locationid", DbType.Int32, locationid);
            db.AddInParameter(cmd, "regionname", DbType.String, regionname);
            db.AddInParameter(cmd, "regionmailid", DbType.String, regionmailid);
            db.AddInParameter(cmd, "statename", DbType.String, statename);
            db.AddInParameter(cmd, "statemailid", DbType.String, statemailid);
            db.AddInParameter(cmd, "sbuname", DbType.String, sbuname);
            db.AddInParameter(cmd, "sbumailid", DbType.String, sbumailid);
            db.AddInParameter(cmd, "omname", DbType.String, omname);
            db.AddInParameter(cmd, "ommailid", DbType.String, ommailid);
            db.AddInParameter(cmd, "aomname", DbType.String, aomname);
            db.AddInParameter(cmd, "aommailid", DbType.String, aommailid);
            db.AddInParameter(cmd, "clientname", DbType.String, clientname);
            db.AddInParameter(cmd, "clientmailid", DbType.String, clientmailid);

            db.AddInParameter(cmd, "GMOPSName", DbType.String, gmopsname);
            db.AddInParameter(cmd, "GMOPSMailID", DbType.String, gmopsmailid);

            db.AddInParameter(cmd, "OperationHeadName", DbType.String, OperationHeadName);
            db.AddInParameter(cmd, "OperationHeadMail", DbType.String, OperationHeadMail);
            db.AddInParameter(cmd, "TrainingHeadName", DbType.String, TrainingHeadName);
            db.AddInParameter(cmd, "TrainingHeadMail", DbType.String, TrainingHeadMail);
            db.AddInParameter(cmd, "SCMHeadName", DbType.String, SCMHeadName);
            db.AddInParameter(cmd, "SCMHeadMail", DbType.String, SCMHeadMail);
            db.AddInParameter(cmd, "ComplianceHeadName", DbType.String, ComplianceHeadName);
            db.AddInParameter(cmd, "ComplianceHeadMail", DbType.String, ComplianceHeadMail);
            db.AddInParameter(cmd, "HRHeadName", DbType.String, HRHeadName);
            db.AddInParameter(cmd, "HRHeadMail", DbType.String, HRHeadMail);
            db.AddInParameter(cmd, "OtherHeadName", DbType.String, OthersHeadName);
            db.AddInParameter(cmd, "OtherHeadMail", DbType.String, OthersHeadMail);


            db.AddInParameter(cmd, "NationalHeadName", DbType.String, nationalheadname);
            db.AddInParameter(cmd, "NationalHeadEmail", DbType.String, nationalheademail);

            db.AddInParameter(cmd, "cuid", DbType.Int32, cuid);
            db.AddInParameter(cmd, "cdate", DbType.DateTime, cdate);
            return Convert.ToInt32(db.ExecuteScalar(cmd));
        }

        public DataSet getlocationmailid(int id, int companyid, int locationid, int locationmailid)
        {

            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("getlocationmailid");
            db.AddInParameter(cmd, "id", DbType.Int32, id);
            db.AddInParameter(cmd, "companyid", DbType.Int32, companyid);
            db.AddInParameter(cmd, "locationid", DbType.Int32, locationid);
            db.AddInParameter(cmd, "locationmailid", DbType.Int32, locationmailid);

            ds = db.ExecuteDataSet(cmd);
            return ds;
        }

        public int updatemlocationmail(int companyid, int locationid, string regionname, string regionmailid, string statename,
                                        string statemailid, string sbuname, string sbumailid, string omname,
                                        string ommailid, string aomname, string aommailid, string clientname,
                                        string clientmailid, string gmopsname, string gmopsmailid,
            string nationalheadname, string nationalheademail,
            string OperationHeadName,
     string OperationHeadMail,
     string TrainingHeadName,
     string TrainingHeadMail,
     string SCMHeadName,
     string SCMHeadMail,
     string ComplianceHeadName,
     string ComplianceHeadMail,
     string HRHeadName,
     string HRHeadMail,
     string OthersHeadName,
     string OthersHeadMail,
            int cuid, DateTime cdate, int locationmailid)
        {
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("updatemlocationmail");
            db.AddInParameter(cmd, "companyid", DbType.Int32, companyid);
            db.AddInParameter(cmd, "locationid", DbType.Int32, locationid);
            db.AddInParameter(cmd, "regionname", DbType.String, regionname);
            db.AddInParameter(cmd, "regionmailid", DbType.String, regionmailid);
            db.AddInParameter(cmd, "statename", DbType.String, statename);
            db.AddInParameter(cmd, "statemailid", DbType.String, statemailid);
            db.AddInParameter(cmd, "sbuname", DbType.String, sbuname);
            db.AddInParameter(cmd, "sbumailid", DbType.String, sbumailid);
            db.AddInParameter(cmd, "omname", DbType.String, omname);
            db.AddInParameter(cmd, "ommailid", DbType.String, ommailid);
            db.AddInParameter(cmd, "aomname", DbType.String, aomname);
            db.AddInParameter(cmd, "aommailid", DbType.String, aommailid);
            db.AddInParameter(cmd, "clientname", DbType.String, clientname);
            db.AddInParameter(cmd, "clientmailid", DbType.String, clientmailid);

            db.AddInParameter(cmd, "GMOPSName", DbType.String, gmopsname);
            db.AddInParameter(cmd, "GMOPSMailID", DbType.String, gmopsmailid);

            db.AddInParameter(cmd, "NationalHeadName", DbType.String, nationalheadname);
            db.AddInParameter(cmd, "NationalHeadEmail", DbType.String, nationalheademail);

            db.AddInParameter(cmd, "OperationHeadName", DbType.String, OperationHeadName);
            db.AddInParameter(cmd, "OperationHeadMail", DbType.String, OperationHeadMail);
            db.AddInParameter(cmd, "TrainingHeadName", DbType.String, TrainingHeadName);
            db.AddInParameter(cmd, "TrainingHeadMail", DbType.String, TrainingHeadMail);
            db.AddInParameter(cmd, "SCMHeadName", DbType.String, SCMHeadName);
            db.AddInParameter(cmd, "SCMHeadMail", DbType.String, SCMHeadMail);
            db.AddInParameter(cmd, "ComplianceHeadName", DbType.String, ComplianceHeadName);
            db.AddInParameter(cmd, "ComplianceHeadMail", DbType.String, ComplianceHeadMail);
            db.AddInParameter(cmd, "HRHeadName", DbType.String, HRHeadName);
            db.AddInParameter(cmd, "HRHeadMail", DbType.String, HRHeadMail);
            db.AddInParameter(cmd, "OtherHeadName", DbType.String, OthersHeadName);
            db.AddInParameter(cmd, "OtherHeadMail", DbType.String, OthersHeadMail);

            db.AddInParameter(cmd, "cuid", DbType.Int32, cuid);
            db.AddInParameter(cmd, "cdate", DbType.DateTime, cdate);
            db.AddInParameter(cmd, "locationmailid", DbType.Int32, locationmailid);
            return Convert.ToInt32(db.ExecuteScalar(cmd));
        }

        #endregion

        #region "Category"

        public DataSet GetCategoryDet(int categoryid)
        {
            try
            {
                DataSet ds = new DataSet();
                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("Qry_GetCategoryDet");
                db.AddInParameter(cmd, "@categoryid", DbType.Int32, categoryid);
                ds = db.ExecuteDataSet(cmd);
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string[] InsertUpdateCategory(int CategoryId, string categoryname, int userid)
        {
            try
            {
                string[] Values = new string[3];
                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("Sp_InsertUpdateCategoryDet");
                db.AddInParameter(cmd, "@CategoryId", DbType.Int32, CategoryId);
                db.AddInParameter(cmd, "@CategoryName", DbType.String, categoryname);
                db.AddInParameter(cmd, "@Uid", DbType.Int32, userid);
                db.AddOutParameter(cmd, "@ErrorMessage", DbType.String, 1000);
                db.AddOutParameter(cmd, "@Status", DbType.String, 10);
                db.ExecuteNonQuery(cmd);
                Values[0] = string.Format(CultureInfo.CurrentCulture, "{0}",
                db.GetParameterValue(cmd, "@ErrorMessage"));
                Values[1] = db.GetParameterValue(cmd, "@Status").ToString();
                return Values;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataSet GetAllLocation()
        {
            try
            {
                DataSet ds = new DataSet();
                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("Qry_GetAllLocations");
                ds = db.ExecuteDataSet(cmd);
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
        }


        public DataSet GetLocMapping(int categoryid)
        {
            try
            {
                DataSet ds = new DataSet();
                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("Qry_GetLocMapping");
                db.AddInParameter(cmd, "@categoryid", DbType.Int32, categoryid);
                ds = db.ExecuteDataSet(cmd);
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string[] InsertCatLocMapping(int CategoryId, string catelocxml, int userid)
        {
            try
            {
                string[] Values = new string[3];
                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("SP_InsertCatLocMapping");
                db.AddInParameter(cmd, "@CategoryId", DbType.Int32, CategoryId);
                db.AddInParameter(cmd, "@CatLocXML", DbType.String, catelocxml);
                db.AddInParameter(cmd, "@Uid", DbType.Int32, userid);
                db.AddOutParameter(cmd, "@ErrorMessage", DbType.String, 1000);
                db.AddOutParameter(cmd, "@Status", DbType.String, 10);
                db.ExecuteNonQuery(cmd);
                Values[0] = string.Format(CultureInfo.CurrentCulture, "{0}",
                db.GetParameterValue(cmd, "@ErrorMessage"));
                Values[1] = db.GetParameterValue(cmd, "@Status").ToString();
                return Values;
            }
            catch (Exception)
            {
                throw;
            }
        }



        #endregion

        #region "Region- Sbu Mapping"

        public DataSet GetAllRegionSbu()
        {
            try
            {
                DataSet ds = new DataSet();
                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("Qry_GetAllRegionSbu");
                ds = db.ExecuteDataSet(cmd);
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
        }


        public DataSet GetMappingRegSbuDet(int Regionid)
        {
            try
            {
                DataSet ds = new DataSet();
                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("Qry_GetMappingRegSbuDet");
                db.AddInParameter(cmd, "@Regionid", DbType.Int32, Regionid);
                ds = db.ExecuteDataSet(cmd);
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string[] InsertRegSbuMapping(int RegionId, string sbuxml, int userid)
        {
            try
            {
                string[] Values = new string[3];
                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("SP_InsertRegSbuMapping");
                db.AddInParameter(cmd, "@RegionId", DbType.Int32, RegionId);
                db.AddInParameter(cmd, "@SbuXML", DbType.String, sbuxml);
                db.AddInParameter(cmd, "@Uid", DbType.Int32, userid);
                db.AddOutParameter(cmd, "@ErrorMessage", DbType.String, 1000);
                db.AddOutParameter(cmd, "@Status", DbType.String, 10);
                db.ExecuteNonQuery(cmd);
                Values[0] = string.Format(CultureInfo.CurrentCulture, "{0}",
                db.GetParameterValue(cmd, "@ErrorMessage"));
                Values[1] = db.GetParameterValue(cmd, "@Status").ToString();
                return Values;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region "Category - Group"

        public DataSet GetCategoryDet_withGroup(int categoryid, int groupid)
        {
            try
            {
                DataSet ds = new DataSet();
                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("Qry_GetCategoryDet_Group");
                db.AddInParameter(cmd, "@categoryid", DbType.Int32, categoryid);
                db.AddInParameter(cmd, "@groupid", DbType.Int32, groupid);
                ds = db.ExecuteDataSet(cmd);
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string[] InsertUpdateCategory_withGroup(int CategoryId, string categoryname, int userid, int groupid)
        {
            try
            {
                string[] Values = new string[3];
                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("Sp_InsertUpdateCategoryDet_Group");
                db.AddInParameter(cmd, "@CategoryId", DbType.Int32, CategoryId);
                db.AddInParameter(cmd, "@CategoryName", DbType.String, categoryname);
                db.AddInParameter(cmd, "@Uid", DbType.Int32, userid);
                db.AddInParameter(cmd, "@groupid", DbType.Int32, groupid);
                db.AddOutParameter(cmd, "@ErrorMessage", DbType.String, 1000);
                db.AddOutParameter(cmd, "@Status", DbType.String, 10);
                db.ExecuteNonQuery(cmd);
                Values[0] = string.Format(CultureInfo.CurrentCulture, "{0}",
                db.GetParameterValue(cmd, "@ErrorMessage"));
                Values[1] = db.GetParameterValue(cmd, "@Status").ToString();
                return Values;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataSet GetAllLocation_withGroup(int groupid)
        {
            try
            {
                DataSet ds = new DataSet();
                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("Qry_GetAllLocations_Group");
                db.AddInParameter(cmd, "@groupid", DbType.Int32, groupid);
                ds = db.ExecuteDataSet(cmd);
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string[] InsertRegSbuMapping_Group(int RegionId, string sbuxml, int userid, int groupid)
        {
            try
            {
                string[] Values = new string[3];
                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("SP_InsertRegSbuMapping_Group");
                db.AddInParameter(cmd, "@RegionId", DbType.Int32, RegionId);
                db.AddInParameter(cmd, "@groupid", DbType.Int32, groupid);
                db.AddInParameter(cmd, "@SbuXML", DbType.String, sbuxml);
                db.AddInParameter(cmd, "@Uid", DbType.Int32, userid);
                db.AddOutParameter(cmd, "@ErrorMessage", DbType.String, 1000);
                db.AddOutParameter(cmd, "@Status", DbType.String, 10);
                db.ExecuteNonQuery(cmd);
                Values[0] = string.Format(CultureInfo.CurrentCulture, "{0}",
                db.GetParameterValue(cmd, "@ErrorMessage"));
                Values[1] = db.GetParameterValue(cmd, "@Status").ToString();
                return Values;
            }
            catch (Exception)
            {
                throw;
            }
        }


        public DataSet GetMappingRegSbuDet_Group(int Regionid, int GroupId)
        {
            try
            {
                DataSet ds = new DataSet();
                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("Qry_GetMappingRegSbuDet_Group");
                db.AddInParameter(cmd, "@Regionid", DbType.Int32, Regionid);
                db.AddInParameter(cmd, "@GroupId", DbType.Int32, GroupId);
                ds = db.ExecuteDataSet(cmd);
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region "Multi Contacts"

        public DataSet GetContactsBySbuId(int sbuid, int groupid)
        {
            try
            {
                DataSet ds = new DataSet();
                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("Qry_GetLocationsMailBySbu");
                db.AddInParameter(cmd, "@sbuid", DbType.Int32, sbuid);
                db.AddInParameter(cmd, "@groupid", DbType.Int32, groupid);
                ds = db.ExecuteDataSet(cmd);
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string[] InsertUpdateMultiContacts(string xml, int userid)
        {
            try
            {
                string[] Values = new string[3];
                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("SP_InsertUpateContacts_Multi");
                db.AddInParameter(cmd, "@userid", DbType.Int32, userid);
                db.AddInParameter(cmd, "@ContactXML", DbType.String, xml);
                db.AddOutParameter(cmd, "@ErrorMessage", DbType.String, 1000);
                db.AddOutParameter(cmd, "@Status", DbType.String, 10);
                db.ExecuteNonQuery(cmd);
                Values[0] = string.Format(CultureInfo.CurrentCulture, "{0}",
                db.GetParameterValue(cmd, "@ErrorMessage"));
                Values[1] = db.GetParameterValue(cmd, "@Status").ToString();
                return Values;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion
    }
}
