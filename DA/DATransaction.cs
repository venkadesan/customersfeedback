﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;
namespace DA
{
    public class DATransaction
    {

        public DataSet gettransaction(int companyid, int locationid, int auditid, DateTime auditdate)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("gettransaction");
            db.AddInParameter(cmd, "companyid", DbType.Int32, companyid);
            db.AddInParameter(cmd, "locationid", DbType.Int32, locationid);
            db.AddInParameter(cmd, "auditid", DbType.Int32, auditid);
            db.AddInParameter(cmd, "auditdate", DbType.DateTime, auditdate);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }

        public DataSet getallauditscore(int regionid, int sbuid, int companyid, int locationid, DateTime frmdate, DateTime todate)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("getallauditscore");
            db.AddInParameter(cmd, "regionid", DbType.Int32, regionid);
            db.AddInParameter(cmd, "sbuid", DbType.Int32, sbuid);
            db.AddInParameter(cmd, "companyid", DbType.Int32, companyid);
            db.AddInParameter(cmd, "locationid", DbType.Int32, locationid);
            db.AddInParameter(cmd, "frmdate", DbType.DateTime, frmdate);
            db.AddInParameter(cmd, "todate", DbType.DateTime, todate);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }

        public DataSet gettransactionbycategoryid(int companyid, int locationid, int auditid, DateTime auditdate, int categoryid)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("gettransactionbycategoryid");
            db.AddInParameter(cmd, "companyid", DbType.Int32, companyid);
            db.AddInParameter(cmd, "locationid", DbType.Int32, locationid);
            db.AddInParameter(cmd, "auditid", DbType.Int32, auditid);
            db.AddInParameter(cmd, "auditdate", DbType.DateTime, auditdate);
            db.AddInParameter(cmd, "categoryid", DbType.Int32, categoryid);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }

        public DataSet gettotalrating(int auditid, int sbuid, int companyid, int locationid, DateTime auditdate)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("getratingsum");
            db.AddInParameter(cmd, "auditid", DbType.Int32, auditid);
            db.AddInParameter(cmd, "sbuid", DbType.Int32, sbuid);
            db.AddInParameter(cmd, "companyid", DbType.Int32, companyid);
            db.AddInParameter(cmd, "locationid", DbType.Int32, locationid);
            db.AddInParameter(cmd, "auditdate", DbType.DateTime, auditdate);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }
        public DataSet getratingsumbycategory(int auditid, int categoryid, int sbuid, int companyid, int locationid, DateTime auditdate)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("getratingsumbycategory");
            db.AddInParameter(cmd, "auditid", DbType.Int32, auditid);
            db.AddInParameter(cmd, "categoryid", DbType.Int32, categoryid);
            db.AddInParameter(cmd, "sbuid", DbType.Int32, sbuid);
            db.AddInParameter(cmd, "companyid", DbType.Int32, companyid);
            db.AddInParameter(cmd, "locationid", DbType.Int32, locationid);
            db.AddInParameter(cmd, "auditdate", DbType.DateTime, auditdate);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }


        public DataSet gettotalweightage(int auditid, int sbuid, int companyid, int locationid, DateTime auditdate)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("getweightagesum");
            db.AddInParameter(cmd, "auditid", DbType.Int32, auditid);
            db.AddInParameter(cmd, "sbuid", DbType.Int32, sbuid);
            db.AddInParameter(cmd, "companyid", DbType.Int32, companyid);
            db.AddInParameter(cmd, "locationid", DbType.Int32, locationid);
            db.AddInParameter(cmd, "auditdate", DbType.DateTime, auditdate);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }

        public DataSet getweightagesumbycategoryid(int auditid, int categoryid, int sbuid, int companyid, int locationid, DateTime auditdate)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("getweightagesumbycategoryid");
            db.AddInParameter(cmd, "auditid", DbType.Int32, auditid);
            db.AddInParameter(cmd, "categoryid", DbType.Int32, categoryid);
            db.AddInParameter(cmd, "sbuid", DbType.Int32, sbuid);
            db.AddInParameter(cmd, "companyid", DbType.Int32, companyid);
            db.AddInParameter(cmd, "locationid", DbType.Int32, locationid);
            db.AddInParameter(cmd, "auditdate", DbType.DateTime, auditdate);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }


        public DataSet gettotalscore(int auditid, int sbuid, int companyid, int locationid, DateTime auditdate)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("getscoresum");
            db.AddInParameter(cmd, "auditid", DbType.Int32, auditid);
            db.AddInParameter(cmd, "sbuid", DbType.Int32, sbuid);
            db.AddInParameter(cmd, "companyid", DbType.Int32, companyid);
            db.AddInParameter(cmd, "locationid", DbType.Int32, locationid);
            db.AddInParameter(cmd, "auditdate", DbType.DateTime, auditdate);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }



        public DataSet getscoresumbycategory(int auditid, int categoryid, int sbuid, int companyid, int locationid, DateTime auditdate)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("getscoresumbycategory");
            db.AddInParameter(cmd, "auditid", DbType.Int32, auditid);
            db.AddInParameter(cmd, "categoryid", DbType.Int32, categoryid);
            db.AddInParameter(cmd, "sbuid", DbType.Int32, sbuid);
            db.AddInParameter(cmd, "companyid", DbType.Int32, companyid);
            db.AddInParameter(cmd, "locationid", DbType.Int32, locationid);
            db.AddInParameter(cmd, "auditdate", DbType.DateTime, auditdate);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }

        public DataSet GetDateWiseReport(int locationid, DateTime dtfdate, DateTime dttdate)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("Qry_GetDateWiseReport");
            db.AddInParameter(cmd, "LocationId", DbType.Int32, locationid);
            db.AddInParameter(cmd, "FromDate", DbType.DateTime, dtfdate);
            db.AddInParameter(cmd, "ToDate", DbType.DateTime, dttdate);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }

        public DataSet GetMailTrackingDetails(DateTime dtfdate, DateTime dttdate)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("Qry_GetTrackingDetails");
            db.AddInParameter(cmd, "FromDate", DbType.DateTime, dtfdate);
            db.AddInParameter(cmd, "ToDate", DbType.DateTime, dttdate);
            cmd.CommandTimeout = 9999;
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }

        public DataSet GenerateTraingNdevelopment_Report(DateTime dtfdate, DateTime dttdate)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("Qry_GenerateTraingNdevelopment_Report");
            db.AddInParameter(cmd, "FromDate", DbType.DateTime, dtfdate);
            db.AddInParameter(cmd, "ToDate", DbType.DateTime, dttdate);
            cmd.CommandTimeout = 9999;
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }

        public DataSet GetRawDatasDateWiseReport(DateTime auditdate, int locationid, int sbuid, int sectorid, string auditorname)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("Qry_GetRawDatasDateWiseReport");
            db.AddInParameter(cmd, "AuditDate", DbType.DateTime, auditdate);
            db.AddInParameter(cmd, "locationId", DbType.Int32, locationid);
            db.AddInParameter(cmd, "sbuid", DbType.Int32, sbuid);
            db.AddInParameter(cmd, "SectorId", DbType.Int32, sectorid);
            db.AddInParameter(cmd, "Auditor", DbType.String, auditorname);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }


        public DataSet getlocationmailid(int companyid, int locationid, bool training, bool hr, bool scm, bool compliance, bool operations)
        {
            try
            {
                DataSet ds = new DataSet();
                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("Qry_GetLocationwiseMailIDs");
                db.AddInParameter(cmd, "companyid", DbType.Int32, companyid);
                db.AddInParameter(cmd, "locationid", DbType.Int32, locationid);
                db.AddInParameter(cmd, "bTraining", DbType.Boolean, training);
                db.AddInParameter(cmd, "bhr", DbType.Boolean, hr);
                db.AddInParameter(cmd, "bscm", DbType.Boolean, scm);
                db.AddInParameter(cmd, "bcompliance", DbType.Boolean, compliance);
                db.AddInParameter(cmd, "boperations", DbType.Boolean, operations);

                ds = db.ExecuteDataSet(cmd);
                return ds;
            }
            catch
            {
                throw;
            }
        }

        #region "With GroupID"

        public DataSet GetDateWiseReport_Group(int locationid, DateTime dtfdate, DateTime dttdate, int groupid)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("Qry_GetDateWiseReport_Group");
            db.AddInParameter(cmd, "LocationId", DbType.Int32, locationid);
            db.AddInParameter(cmd, "FromDate", DbType.DateTime, dtfdate);
            db.AddInParameter(cmd, "ToDate", DbType.DateTime, dttdate);
            db.AddInParameter(cmd, "groupid", DbType.Int32, groupid);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }

        public DataSet GetMailTrackingDetails_Group(DateTime dtfdate, DateTime dttdate, int groupid)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("Qry_GetTrackingDetails_Group");
            db.AddInParameter(cmd, "FromDate", DbType.DateTime, dtfdate);
            db.AddInParameter(cmd, "ToDate", DbType.DateTime, dttdate);
            db.AddInParameter(cmd, "groupid", DbType.Int32, groupid);
            cmd.CommandTimeout = 9999;
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }

        public DataSet GenerateTraingNdevelopment_Report_Group(DateTime dtfdate, DateTime dttdate, int groupid)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("Qry_GenerateTraingNdevelopment_Report_Group");
            db.AddInParameter(cmd, "FromDate", DbType.DateTime, dtfdate);
            db.AddInParameter(cmd, "ToDate", DbType.DateTime, dttdate);
            db.AddInParameter(cmd, "groupid", DbType.Int32, groupid);
            cmd.CommandTimeout = 9999;
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }

        #endregion


        #region "Report - 1"

        public DataSet GetRegionWiseScore(int month, int year,int groupid)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("Qry_GetRegionWiseScore");
            db.AddInParameter(cmd, "Month", DbType.Int32, month);
            db.AddInParameter(cmd, "Year", DbType.Int32, year);
            db.AddInParameter(cmd, "groupid", DbType.Int32, groupid);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }


        public DataSet GetSbuWiseScore(int month, int year, int regionid, int groupid)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("Qry_GetSbuWiseScore");
            db.AddInParameter(cmd, "Month", DbType.Int32, month);
            db.AddInParameter(cmd, "Year", DbType.Int32, year);
            db.AddInParameter(cmd, "regionid", DbType.Int32, regionid);
            db.AddInParameter(cmd, "groupid", DbType.Int32, groupid);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }

        public DataSet GetLocationwiseScore(int sbuid, int month, int year,int groupid)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("Qry_GetLocationwiseScore");
            db.AddInParameter(cmd, "sbuid", DbType.Int32, sbuid);
            db.AddInParameter(cmd, "Month", DbType.Int32, month);
            db.AddInParameter(cmd, "Year", DbType.Int32, year);
            db.AddInParameter(cmd, "groupid", DbType.Int32, groupid);
            ds = db.ExecuteDataSet(cmd);
            //ds = db.ExecuteDataSet(cmd);
            return ds;
        }

        public DataSet GetDateWiseReportByLocation(int locationid, int month, int year)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("Qry_GetDateWiseReportByLocationId");
            db.AddInParameter(cmd, "locationid", DbType.Int32, locationid);
            db.AddInParameter(cmd, "Month", DbType.Int32, month);
            db.AddInParameter(cmd, "Year", DbType.Int32, year);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }
        #endregion
    }
}
